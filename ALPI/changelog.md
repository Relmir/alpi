## Version 0.2

This version adds the following features:

* indexable param and constraints

This version fixes the following bugs:

* unable to set negative bounds

This version changes intruction separator from line separator to semicolons

## Version 0.1

This version add the following features:

* vectorial sum
* vectorial product

This version fixes the following bugs:

* instruction of more than 133 characters in the fortran generated files

The vectorial sum (respectively the product) is called through the
`sum {ident in index} (expression)` sequence (respectively 
`prod {ident in index} (expression)`). 

## Version 0

This program is meant to be a more user-friendly interface to IBBA
than the Interface program was.

The syntax is close to that of a mod file in AMPL.

We can now define indexing sets. Currently only natural indexing are
allowed. This is done with the `set` keyword. For exemple
`set IX 1 .. 4 by 2` defines the indexing set `{1, 3}`. The step is
optionnal, if it is forgotten, one will be assumed.

Decision Variables are defined with the `var` keyword. the definition
form is either:
`var name {index}, <= upperBound, >= lowerBound, +/- precision` if it
is a vector (index must start by one and increase by one) or
`var name, <= upperBound, >= lowerBound, +/- precision` if it is a
simple variable.

Constants and functions are defined through the use of the `param`
keyword. Currently we are only supporting global functions as well as
constant functions. In coming versions, we will support parametrized
functions that accept a parameter to be computed.

`param pi = 3.14` defines the constant pi to be equal to 3.14.

`param c3 = cos(3)` defines the constant c3 t be equla to cos(3);

`param sq_norm = V(1)**2+V(2)**2` expects a decision variable V to
have been defined (and to be of length superior to 2) and creates the
auxiliary function sq_norm that will take all the decision variables
as parameters and compute the square of the norm of the vector made of
the two first coordinates of V.

Constraints are declared after a `subject to` keyword. They actually
declare a global function and put bounds on it.

`subject to PositiveProd: V(1)*V(2)*V(3)*V(4) >= 5 +/- 1.D-4`

The objective function is defined after the `minimize` keyword. For
example:

`minimize +/- 1.D-2 FONCT = sq_norm+c3-3`

Options then are defined through the use of the `option` keyword and
a key name for the option. For example `option logFreq = 10` will set
the logging frequency to 10 iterations. Options have default values
that are to be correctly chosen in coming versions.