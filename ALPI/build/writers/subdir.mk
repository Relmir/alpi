################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../writers/alpimakewriter.c \
../writers/alpimodwriter.c \
../writers/alpiparamwriter.c \
../writers/alpiproblemwriter.c 

OBJS += \
./writers/alpimakewriter.o \
./writers/alpimodwriter.o \
./writers/alpiparamwriter.o \
./writers/alpiproblemwriter.o 

C_DEPS += \
./writers/alpimakewriter.d \
./writers/alpimodwriter.d \
./writers/alpiparamwriter.d \
./writers/alpiproblemwriter.d 


# Each subdirectory must supply rules for building sources it contributes
writers/%.o: ../writers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


