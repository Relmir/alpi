################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../alpistructs/alpiconstants.c \
../alpistructs/alpiconstraints.c \
../alpistructs/alpifunctions.c \
../alpistructs/alpiindexing.c \
../alpistructs/alpiproblem.c \
../alpistructs/alpisymbols.c \
../alpistructs/alpivariables.c 

OBJS += \
./alpistructs/alpiconstants.o \
./alpistructs/alpiconstraints.o \
./alpistructs/alpifunctions.o \
./alpistructs/alpiindexing.o \
./alpistructs/alpiproblem.o \
./alpistructs/alpisymbols.o \
./alpistructs/alpivariables.o 

C_DEPS += \
./alpistructs/alpiconstants.d \
./alpistructs/alpiconstraints.d \
./alpistructs/alpifunctions.d \
./alpistructs/alpiindexing.d \
./alpistructs/alpiproblem.d \
./alpistructs/alpisymbols.d \
./alpistructs/alpivariables.d 


# Each subdirectory must supply rules for building sources it contributes
alpistructs/%.o: ../alpistructs/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


