################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../alpirules/alpiruleinfos.c \
../alpirules/alpiruleparsertools.c \
../alpirules/alpirulesdefinitions.c \
../alpirules/alpirulesexpr.c \
../alpirules/alpiruleslexer.c \
../alpirules/alpirulesoptions.c \
../alpirules/alpirulesparser.c 

OBJS += \
./alpirules/alpiruleinfos.o \
./alpirules/alpiruleparsertools.o \
./alpirules/alpirulesdefinitions.o \
./alpirules/alpirulesexpr.o \
./alpirules/alpiruleslexer.o \
./alpirules/alpirulesoptions.o \
./alpirules/alpirulesparser.o 

C_DEPS += \
./alpirules/alpiruleinfos.d \
./alpirules/alpiruleparsertools.d \
./alpirules/alpirulesdefinitions.d \
./alpirules/alpirulesexpr.d \
./alpirules/alpiruleslexer.d \
./alpirules/alpirulesoptions.d \
./alpirules/alpirulesparser.d 


# Each subdirectory must supply rules for building sources it contributes
alpirules/%.o: ../alpirules/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


