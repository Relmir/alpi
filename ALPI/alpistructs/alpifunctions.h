/*
 * alpifunctions.h
 *
 *  Created on: Dec 9, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIFUNCTIONS_H_
#define ALPIFUNCTIONS_H_

#include "alpivariables.h"
#include <stdio.h>

typedef struct Function Function;
struct Function {
  Variable ret;
  VariableList params;
  VariableList symbols;
  char** bodyInstructions;
  int bodySize;
};

Function createFunction(char* name);
void printFunction(Function f);
void fprintFunction(FILE*out, Function f);

struct FunctionCell {
    struct FunctionCell *next, *prev;
    Function* val;
};

typedef struct FunctionList FunctionList;
struct FunctionList {
    struct FunctionCell *head, *tail;
    int length;
};

FunctionList createFunctionList();
void addFunctionToList(FunctionList *list, Function *elem);
void printFunctionList(FunctionList list);
void fprintFunctionList(FILE* out, FunctionList list, char* sep);
Function* searchInFunctionList(FunctionList list, char* elem);

#endif /* ALPIFUNCTIONS_H_ */
