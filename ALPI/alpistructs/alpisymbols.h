/*
 * alpisymbols.h
 *
 *  Created on: Dec 19, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPISYMBOLS_H_
#define ALPISYMBOLS_H_

#include "alpistructures.h"

typedef struct Symbol Symbol;
struct Symbol {
    char* name;
    int isObj;
    int isIndexSet;
    Indexing* index;
    int isIndex;
    int isCst;
    Constant* cst;
    int isFunction;
    Function* fun;
    int isParam;
    int isVariable;
    Variable* var;
};

Symbol createIndexingSetSymbol(Indexing*indexing);
Symbol createSimpleConstantSymbol(Constant*constant);
Symbol createConstantSymbol(Function*constant);
Symbol createGlobalFunctionSymbol(Function*function);
Symbol createObjectiveFunctionSymbol(Function*function);
Symbol createDecisionVariableSymbol(Variable*variable);
Symbol createLocalVariableSymbol(Variable*local);
Symbol createIndexVariableSymbol(char*name, Indexing*index);
Symbol createParamSymbol(char*name);

struct SymbolCell {
    struct SymbolCell *next, *prev;
    Symbol* val;
};

typedef struct SymbolList SymbolList;
struct SymbolList {
    struct SymbolCell *head, *tail;
    int length;
};

SymbolList createSymbolList();
void deleteSymbolList(SymbolList *list);
void addSymbolToList(SymbolList *list, Symbol *elem);
void addAllToSymbolList(SymbolList *list, SymbolList toAdd);
Symbol* searchInSymbolList(SymbolList list, char* elem);
Symbol* getSymbolFromList(SymbolList list, int index);

struct SymbolListCell {
    struct SymbolListCell *next;
    SymbolList* val;
};

typedef struct SymbolListStack SymbolListStack;
struct SymbolListStack {
    struct SymbolListCell *head;
    int length;
};

SymbolListStack createSymbolListStack();
void pushSymbolListInStack(SymbolListStack *list, SymbolList *elem);
SymbolList* popSymbolListFromStack(SymbolListStack *list);
SymbolList* peekSymbolListFromStack(SymbolListStack list);
Symbol* searchInSymbolListStack(SymbolListStack list, char* elem);


#endif /* ALPISYMBOLS_H_ */
