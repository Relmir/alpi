/*
 * alpiindexing.h
 *
 *  Created on: Dec 10, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIINDEXING_H_
#define ALPIINDEXING_H_

#include <stdio.h>

typedef struct Indexing Indexing;
struct Indexing {
  char* name;
  int start;
  int end;
  int step;
};

Indexing createIndexing(char* name, int start, int stop, int step);
char* printIndexing(Indexing indexing);

struct IndexingCell {
  struct IndexingCell *next, *prev;
  Indexing* val;
};

typedef struct IndexingList IndexingList;
struct IndexingList {
  struct IndexingCell *head, *tail;
  int length;
};

IndexingList createIndexingList();
void addIndexingToList(IndexingList *list, Indexing *elem);
Indexing* searchIndexingInList(IndexingList list, char* elem);
void printIndexingList(IndexingList list);
void fprintIndexingList(FILE*out,IndexingList list, char*sep);
void sprintIndexingList(char*str,IndexingList list, char* sep);

#endif /* ALPIINDEXING_H_ */
