/*
 * alpisymbols.c
 *
 *  Created on: Dec 19, 2013
 *      Author: Emmanuel BIGEON
 */


#include "alpisymbols.h"
#include <stdlib.h>
#include <string.h>

Symbol createIndexingSetSymbol(Indexing*indexing) {
  Symbol s;
  s.cst = NULL;
  s.fun = NULL;
  s.index = indexing;
  s.isCst = 0;
  s.isFunction = 0;
  s.isIndex = 0;
  s.isIndexSet = 1;
  s.isObj = 0;
  s.isParam = 0;
  s.isVariable = 0;
  s.name = malloc((1+strlen(indexing->name))*sizeof(char));
  strcpy(s.name, indexing->name);
  s.var = NULL;
  return s;
}
Symbol createSimpleConstantSymbol(Constant*cst) {
  Symbol s;
  s.cst = cst;
  s.fun = NULL;
  s.index = NULL;
  s.isCst = 1;
  s.isFunction = 0;
  s.isIndex = 0;
  s.isIndexSet = 0;
  s.isObj = 0;
  s.isParam = 0;
  s.isVariable = 0;
  s.name = malloc((1+strlen(cst->name.name))*sizeof(char));
  strcpy(s.name, cst->name.name);
  s.var = NULL;
  return s;
}
Symbol createConstantSymbol(Function*cst) {
  Symbol s;
  s.cst = NULL;
  s.fun = cst;
  s.index = NULL;
  s.isCst = 1;
  s.isFunction = 1;
  s.isIndex = 0;
  s.isIndexSet = 1;
  s.isObj = 0;
  s.isParam = 0;
  s.isVariable = 0;
  s.name = malloc((1+strlen(cst->ret.name))*sizeof(char));
  strcpy(s.name, cst->ret.name);
  s.var = NULL;
  return s;

}
Symbol createGlobalFunctionSymbol(Function*fun) {
  Symbol s;
  s.cst = NULL;
  s.fun = fun;
  s.index = NULL;
  s.isCst = 0;
  s.isFunction = 1;
  s.isIndex = 0;
  s.isIndexSet = 0;
  s.isObj = 0;
  s.isParam = 0;
  s.isVariable = 0;
  s.name = malloc((1+strlen(fun->ret.name))*sizeof(char));
  strcpy(s.name, fun->ret.name);
  s.var = NULL;
  return s;
}
Symbol createObjectiveFunctionSymbol(Function*fun) {
  Symbol s;
  s.cst = NULL;
  s.fun = fun;
  s.index = NULL;
  s.isCst = 0;
  s.isFunction = 1;
  s.isIndex = 0;
  s.isIndexSet = 0;
  s.isObj = 1;
  s.isParam = 0;
  s.isVariable = 0;
  s.name = malloc((1+strlen(fun->ret.name))*sizeof(char));
  strcpy(s.name, fun->ret.name);
  s.var = NULL;
  return s;
}
Symbol createDecisionVariableSymbol(Variable*var) {
  Symbol s;
  s.cst = NULL;
  s.fun = NULL;
  s.index = NULL;
  s.isCst = 0;
  s.isFunction = 0;
  s.isIndex = 0;
  s.isIndexSet = 0;
  s.isObj = 0;
  s.isParam = 0;
  s.isVariable = 1;
  s.name = malloc((1+strlen(var->name))*sizeof(char));
  strcpy(s.name, var->name);
  s.var = var;
  return s;
}
Symbol createLocalVariableSymbol(Variable*var) {
  Symbol s;
  s.cst = NULL;
  s.fun = NULL;
  s.index = NULL;
  s.isCst = 0;
  s.isFunction = 0;
  s.isIndex = 0;
  s.isIndexSet = 0;
  s.isObj = 0;
  s.isParam = 1;
  s.isVariable = 1;
  s.name = malloc((1+strlen(var->name))*sizeof(char));
  strcpy(s.name, var->name);
  s.var = var;
  return s;
}

Symbol createIndexVariableSymbol(char*name, Indexing*index) {
  Symbol s;
  s.cst = NULL;
  s.fun = NULL;
  s.index = index;
  s.isCst = 0;
  s.isFunction = 0;
  s.isIndex = 1;
  s.isIndexSet = 0;
  s.isObj = 0;
  s.isParam = 0;
  s.isVariable = 0;
  s.name = malloc((1+strlen(name))*sizeof(char));
  strcpy(s.name, name);
  s.var = NULL;
  return s;
}

Symbol createParamSymbol(char*name) {
  Symbol s;
  s.cst = NULL;
  s.fun = NULL;
  s.index = NULL;
  s.isCst = 0;
  s.isFunction = 0;
  s.isIndex = 0;
  s.isIndexSet = 0;
  s.isObj = 0;
  s.isParam = 1;
  s.isVariable = 0;
  s.name = malloc((1+strlen(name))*sizeof(char));
  strcpy(s.name, name);
  s.var = NULL;
  return s;
}


struct SymbolCell* createSymbolCell() {
  struct SymbolCell* cell = malloc(sizeof(struct SymbolCell));
  cell->next = NULL;
  cell->prev = NULL;
  cell->val = NULL;
  return cell;
}

SymbolList createSymbolList() {
  SymbolList list;
  list.head = NULL;
  list.tail = NULL;
  list.length = 0;
  return list;
}

void deleteSymbolList(SymbolList *list) {
  if (list->length) {
    struct SymbolCell *it = list->head, *old;
    while(list->length) {
      (list->length)--;
      old = it;
      it = it->next;
      free(old);
    }
  }
}

void addSymbolToList(SymbolList *list, Symbol *elem) {
  struct SymbolCell* cell = createSymbolCell();
  cell->val = elem;
  cell->prev = list->tail;
  if (list->length) {
    list->tail->next = cell;
  } else {
    list->head = cell;
  }
  list->tail = cell;
  list->length += 1;
}

void addAllToSymbolList(SymbolList *list, SymbolList toAdd) {
  struct SymbolCell*cell = toAdd.head;
  while (cell!=NULL) {
    if (searchInSymbolList(*list, cell->val->name)==NULL) {
      addSymbolToList(list, cell->val);
    }
    cell = cell->next;
  }
}


Symbol* searchInSymbolList(SymbolList list, char* elem) {
  int i = 0;
  struct SymbolCell *cell = list.head;
  while (cell != NULL && strcmp(elem, cell->val->name)) {
    cell = cell->next;
    i++;
  }
  if (cell == NULL) {
    return NULL;
  }
  return cell->val;
}

Symbol* getSymbolFromList(SymbolList list, int index) {
  if (list.length<=index||index<0) {
    return NULL;
  }
  int i = index;
  struct SymbolCell* cell = list.head;
  while (cell!=NULL) {
    if (i == 0) {
      return cell->val;
    }
    cell = cell->next;
    i--;
  }
  return NULL;
}

struct SymbolListCell* createSymbolListCell() {
  struct SymbolListCell* cell = malloc(sizeof(struct SymbolListCell));
  cell->next = NULL;
  cell->val = NULL;
  return cell;
}

SymbolListStack createSymbolListStack() {
  SymbolListStack list;
  list.head = NULL;
  list.length = 0;
  return list;
}

void pushSymbolListInStack(SymbolListStack *list, SymbolList *elem) {
  struct SymbolListCell* cell = createSymbolListCell();
  cell->val = elem;
  cell->next = list->head;
  list->head = cell;
}
SymbolList* popSymbolListFromStack(SymbolListStack *list) {
  struct SymbolListCell* cell = list->head;
  SymbolList*l = cell->val;
  list->head = cell->next;
  free(cell);
  return l;
}
SymbolList* peekSymbolListFromStack(SymbolListStack list) {
  return list.head->val;
}
Symbol* searchInSymbolListStack(SymbolListStack list, char* elem) {
  struct SymbolListCell*it = list.head;
  Symbol*s = NULL;
  while (it != NULL) {
    s = searchInSymbolList(*(it->val), elem);
    if (s!= NULL) {
      return s;
    }
    it = it->next;
  }
  return s;
}
