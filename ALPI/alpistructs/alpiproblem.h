/*
 * alpiproblem.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIPROBLEM_H_
#define ALPIPROBLEM_H_

#include "alpiconstants.h"
#include "alpiconstraints.h"
#include "alpifunctions.h"
#include "alpiindexing.h"
#include "alpivariables.h"

typedef struct ProblemModel ProblemModel;
typedef struct ProblemOptions ProblemOptions;
struct ProblemOptions {
  int logFreq;
  char* logFile;
  char* reformulation;
  int timeLimit;
  int memLimit;
  char* knownMajor;

  // Booleans
  int computeBoundsWithTyalor;
  int constraintsPropagation;
  int CPLEX;
  int timeUnitInSeconds;
  int keepAll;
  int normalStoppingCriterion;
  int terminalDisplay;
  int notCertifiedLP;
  int exactConstraintChecking;
  int affineArithmeticForConstraints;
};
struct ProblemModel {
  char* name;
  IndexingList indexes;
  VariableList decisionVariables; // Variables
  FunctionList auxiliaryFunctions; // Functions
  ConstantList simpleConstants; // Constants
  FunctionList functionConstants; // Functions
  Function objective;
  ConstraintList constraints; // Constraints

  ProblemOptions options;
};

ProblemModel createProblemModel(char* name);
int findSymbolInProblem(ProblemModel model, char* name, int* isIndex,
    int* isDecisionVariable, int* isConstant, int* isFunction, int* isObjective);

void fprintModel(FILE* out, ProblemModel model);
void fprintProblemOptions(FILE* out, ProblemOptions model, char*sep);

#endif /* ALPIPROBLEM_H_ */
