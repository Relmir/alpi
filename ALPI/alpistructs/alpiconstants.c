/*
 * alpiconstants.c
 *
 *  Created on: Dec 10, 2013
 *      Author: Emmanuel BIGEON
 */


#include "alpiconstants.h"
#include <stdlib.h>
#include <string.h>

Constant createConstant(char*name, char* value) {
  Constant cst;
  cst.name = createVariable(name);
  cst.value = malloc((1+strlen(value))*sizeof(char));
  strcpy(cst.value, value);
  return cst;
}

void printConstant(Constant cst) {
  printf("Constant %s = %s\n", cst.name.name, cst.value);
}

void fprintConstant(FILE* out, Constant cst) {
  fprintf(out, "Constant %s = %s", cst.name.name, cst.value);
}

struct ConstantCell* createConstantCell() {
  struct ConstantCell* cell = malloc(sizeof(struct ConstantCell));
  cell->next = NULL;
  cell->prev = NULL;
  cell->val = NULL;
  return cell;
}

ConstantList createConstantList() {
  ConstantList list;
  list.head = NULL;
  list.tail = NULL;
  list.length = 0;
  return list;
}

void addConstantToList(ConstantList *list, Constant *elem) {
  struct ConstantCell* cell = createConstantCell();
  cell->val = elem;
  cell->prev = list->tail;
  if (list->length) {
    list->tail->next = cell;
  } else {
    list->head = cell;
  }
  list->tail = cell;
  list->length += 1;
}

void fprintConstantList(FILE*out, ConstantList list, char* sep) {
  struct ConstantCell *cell = list.head;
  if (cell != NULL) {
    fprintConstant(out, *(cell->val));
    cell = cell->next;
  }
  while (cell != NULL) {
    fprintf(out, sep);
    fprintConstant(out, *(cell->val));
    cell = cell->next;
  }
}

Constant* searchInConstantList(ConstantList list, char* elem) {
  int i = 0;
  struct ConstantCell *cell = list.head;
  while (cell != NULL && strcmp(elem, cell->val->name.name)) {
    cell = cell->next;
    i++;
  }
  if (cell == NULL) {
    return NULL;
  }
  return cell->val;
}
