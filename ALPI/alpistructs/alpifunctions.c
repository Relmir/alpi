/*
 * alpifunctions.c
 *
 *  Created on: Dec 9, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpifunctions.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

Function createFunction(char* name) {
  Function f;
  f.ret = createVariable(name);
  f.params = createVariableList();
  f.symbols = createVariableList();
  return f;
}

void printFunction(Function f) {
  int i;
  printf("Function %s(", f.ret.name);
  VariableList vl = f.params;
  if (vl.length) {
    struct VariableListCell* it = vl.head;
    printf("%s", it->var->name);
    it = it->next;
    for (i = 1; i < vl.length; ++i) {
      printf(", %s", it->var->name);
      it = it->next;
    }
  }
  printf(")\n");
}

void fprintFunction(FILE*out, Function f) {
  int i;
  char buffer[255];
  sprintf(buffer, "Function %s(", f.ret.name);
  fprintf(out, buffer);
  VariableList vl = f.params;
  if (vl.length) {
    struct VariableListCell* it = vl.head;
    sprintf(buffer, "%s", it->var->name);
    fprintf(out, buffer);
    it = it->next;
    for (i = 1; i < vl.length; ++i) {
      sprintf(buffer, ", %s", it->var->name);
      fprintf(out, buffer);
      it = it->next;
    }
  }
  fprintf(out, ")");
}

struct FunctionCell* createFunctionCell() {
  struct FunctionCell* cell = malloc(sizeof(struct FunctionCell));
  cell->next = NULL;
  cell->prev = NULL;
  cell->val = NULL;
  return cell;
}

FunctionList createFunctionList() {
  FunctionList list;
  list.head = NULL;
  list.tail = NULL;
  list.length = 0;
  return list;
}

void addFunctionToList(FunctionList *list, Function *elem) {
  struct FunctionCell* cell = createFunctionCell();
  cell->val = elem;
  cell->prev = list->tail;
  if (list->length) {
    list->tail->next = cell;
  } else {
    list->head = cell;
  }
  list->tail = cell;
  list->length += 1;
}

void printFunctionList(FunctionList list) {
  printf("List of size: %d\n", list.length);
  struct FunctionCell *cell = list.head;
  while (cell != NULL) {
    printFunction(*(cell->val));
    cell = cell->next;
  }
}

void fprintFunctionList(FILE*out, FunctionList list, char *sep) {
  struct FunctionCell *cell = list.head;
  if (cell != NULL) {
    fprintFunction(out, *(cell->val));
    cell = cell->next;
  }
  while (cell != NULL) {
    fprintf(out, sep);
    fprintFunction(out, *(cell->val));
    cell = cell->next;
  }
}

Function* searchInFunctionList(FunctionList list, char* elem) {
  int i = 0;
  struct FunctionCell *cell = list.head;
  while (cell != NULL && strcmp(elem, cell->val->ret.name)) {
    cell = cell->next;
    i++;
  }
  if (cell == NULL) {
    return NULL;
  }
  return cell->val;
}
