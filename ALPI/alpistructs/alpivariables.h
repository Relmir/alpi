/*
 * alpivariablestruct.h
 *
 *  Created on: Dec 6, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIVARIABLESTRUCT_H_
#define ALPIVARIABLESTRUCT_H_

#include "alpiindexing.h"

typedef struct Variable Variable;
struct Variable {
  char* name;
  int length;
  int isVector;
  int* isInteger;
  char** precision;
  char*** bounds;
  IndexingList indexings;
};

Variable createVariable(char* name);
Variable createIndexedVariable(char* name, Indexing indexing);
void setVariableLowerBounds(Variable* v, char* val);
void setVariableUpperBounds(Variable* v, char*val);
void setVariablePrecisions(Variable* v, char* val);
void deleteVariable(Variable v);
void printVariable(Variable v);
void fprintVariable(FILE*out, Variable v);
void sprintVariable(char*str, Variable v);

struct VariableListCell {
  Variable* var;
  struct VariableListCell* next;
  struct VariableListCell* prev;
};

typedef struct VariableList VariableList;
struct VariableList {
  struct VariableListCell *head, *tail;
  int length;
};

VariableList createVariableList();
int addVariableToVariableListAtIndex(VariableList* list,
    Variable* var, int index);
int addVariableToVariableList(VariableList* list, Variable* var);
Variable* getVariableFromVariableList(VariableList list, int index);
Variable* searchInVariableList(VariableList list, char* name);
int addAllToVariableList(VariableList* list, VariableList toAdd);
char* variableListToString(VariableList list, char*start, char*sep,
    char*end);
void fprintVariableList(FILE*out, VariableList list, char*sep);

#endif /* ALPIVARIABLESTRUCT_H_ */
