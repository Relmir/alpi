/*
 * alpiconstants.h
 *
 *  Created on: Dec 10, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPICONSTANTS_H_
#define ALPICONSTANTS_H_

#include "alpivariables.h"
#include <stdio.h>

typedef struct Constant Constant;
struct Constant {
  Variable name;
  char* value;
  int isInteger;
};

Constant createConstant(char*name, char* value);
void fprintConstant(FILE* out, Constant cst);

struct ConstantCell {
  struct ConstantCell *next, *prev;
  Constant* val;
};

typedef struct ConstantList ConstantList;
struct ConstantList {
  struct ConstantCell *head, *tail;
  int length;
};

ConstantList createConstantList();
void addConstantToList(ConstantList *list, Constant *elem);
void fprintConstantList(FILE* out, ConstantList list, char* separator);
Constant* searchInConstantList(ConstantList list, char* elem);

#endif /* ALPICONSTANTS_H_ */
