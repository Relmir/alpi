/*
 * alpiparam.h
 *
 *  Created on: Jan 7, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIPARAM_H_
#define ALPIPARAM_H_

#include "alpiindexing.h"
#include "alpisymbols.h"

typedef struct Param Param;
struct Param {
  IndexingList indexes;
  int length;
  int isVector;
  int* isInteger;
  char* expression;
  int isDefined;
  int* isDefinedCoord;
  int usesDecisionVariables;
  int* usesDecisionVariablesCoord;
  int isUsed;
  Symbol* symbol;
};

Param createParam(Symbol*s);

struct ParamCell {
    struct ParamCell *next, *prev;
    Param* val;
};

typedef struct ParamList ParamList;
struct ParamList {
    struct ParamCell *head, *tail;
    int length;
};

ParamList createParamList();
void addParamToList(ParamList *list, Param *elem);
void printParamList(ParamList list);
Param* searchInParamList(ParamList list, char* elem);


#endif /* ALPIPARAM_H_ */
