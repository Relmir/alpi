/*
 * alpiparam.c
 *
 *  Created on: Jan 7, 2014
 *      Author: Emmanuel BIGEON
 */

#include "alpiparam.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

Param createParam(Symbol*s) {
  Param p;
  p.isDefined = 0;
  p.symbol = s;
  return p;
}


void printParam(Param param) {
  printf(param.symbol->name);
}

struct ParamCell* createParamCell() {
  struct ParamCell* cell = malloc(sizeof(struct ParamCell));
  cell->next = NULL;
  cell->prev = NULL;
  cell->val = NULL;
  return cell;
}

ParamList createParamList() {
  ParamList list;
  list.head = NULL;
  list.tail = NULL;
  list.length = 0;
  return list;
}

void addParamToList(ParamList *list, Param *elem) {
  struct ParamCell* cell = createParamCell();
  cell->val = elem;
  cell->prev = list->tail;
  if (list->length) {
    list->tail->next = cell;
  } else {
    list->head = cell;
  }
  list->tail = cell;
  list->length += 1;
}

void printParamList(ParamList list) {
  printf("List of size: %d\n", list.length);
  struct ParamCell *cell = list.head;
  while (cell != NULL) {
    printParam(*(cell->val));
    cell = cell->next;
  }
}

Param* searchInParamList(ParamList list, char* elem) {
  int i = 0;
  struct ParamCell *cell = list.head;
  while (cell != NULL && strcmp(elem, cell->val->symbol->name)) {
    cell = cell->next;
    i++;
  }
  if (cell == NULL) {
    printf("not found index \"%s\"\n", elem);
    return NULL;
  }
  return cell->val;
}
