/*
 * alpiconstraints.h
 *
 *  Created on: Dec 13, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPICONSTRAINTS_H_
#define ALPICONSTRAINTS_H_

#include "alpifunctions.h"

typedef struct Constraint Constraint;
struct Constraint {
    Function*f;
    // 0 is equality, else its inequality and the second bound may have sense
    int type;
    // the lower bound (and a boolean to determine if it is set
    int lowerBounded;
    char* boundL;
    // the upper bound (and a boolean to determine if it is set
    int upperBounded;
    char* boundU;
    char* precision;
};

Constraint createEqualityConstraint(Function*f, char* bound);
Constraint createInequalityConstraint(Function*f, char* boundL, char* boundU);
Constraint createLowerBound(Function*f, char* boundL);
Constraint createUpperBound(Function*f, char* boundU);

void printConstraint(Constraint c);
void fprintConstraint(FILE*out, Constraint c);
void sprintConstraint(char*str, Constraint c);

struct ConstraintCell {
    struct ConstraintCell *next, *prev;
    Constraint* val;
};

typedef struct ConstraintList ConstraintList;
struct ConstraintList {
    struct ConstraintCell *head, *tail;
    int length;
};

ConstraintList createConstraintList();
void addConstraintToList(ConstraintList *list, Constraint *elem);
void printConstraintList(ConstraintList list);
void fprintConstraintList(FILE*out, ConstraintList list, char* sep);
void sprintConstraintList(char*str, ConstraintList list);
Constraint* searchInConstraintList(ConstraintList list, char* elem);


#endif /* ALPICONSTRAINTS_H_ */
