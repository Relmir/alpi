/*
 * alpivariables.c
 *
 *  Created on: Dec 8, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpivariables.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void initTablesOfVariable(Variable* var, int l);

// Variables

// Creation
Variable createVariable(char* name) {
  Variable v;
  v.name = malloc((1 + strlen(name)) * sizeof(char));
  strcpy(v.name, name);
  v.indexings = createIndexingList();
  Indexing* ind = malloc(sizeof(Indexing));
  *ind = createIndexing("", 1, 1, 1);
  addIndexingToList(&(v.indexings), ind);
  v.isVector = 0;
  initTablesOfVariable(&v, 1);
  return v;
}

Variable createIndexedVariable(char* name, Indexing index) {
  Variable v;
  v.name = malloc(sizeof(char) * (1 + strlen(name)));
  strcpy(v.name, name);
  v.indexings = createIndexingList();
  Indexing* ind = malloc(sizeof(Indexing));
  *ind = index;
  addIndexingToList(&(v.indexings), ind);
  v.isVector = index.start != index.end;
  initTablesOfVariable(&v,
      (index.end - index.start + 1) / index.step);
  return v;
}

void initTablesOfVariable(Variable* var, int l) {
  var->length = l;
  var->precision = malloc(l * sizeof(double));
  var->bounds = malloc(l * sizeof(double*));
  int i;
  for (i = 0; i < l; ++i) {
    var->bounds[i] = malloc(2 * sizeof(double));
  }
  var->isInteger = malloc(l * sizeof(int));
}

void setVariableLowerBounds(Variable* v, char* val) {
  int i;
  for (i = 0; i < v->length; ++i) {
    v->bounds[i][0] = malloc((1 + strlen(val)) * sizeof(char));
    strcpy(v->bounds[i][0], val);
  }
}

void setVariableUpperBounds(Variable* v, char* val) {
  int i;
  for (i = 0; i < v->length; ++i) {
    v->bounds[i][1] = malloc((1 + strlen(val)) * sizeof(char));
    strcpy(v->bounds[i][1], val);
  }
}

void setVariablePrecisions(Variable* v, char* val) {
  int i;
  for (i = 0; i < v->length; ++i) {
    v->precision[i] = malloc((1 + strlen(val)) * sizeof(char));
    strcpy(v->precision[i], val);
  }
}

// Deletion
void deleteVariable(Variable v) {
  free(v.isInteger);
  free(v.precision);
  free(v.name);
}

void printVariable(Variable v) {
  printf("Variable: \"%s\"\n", v.name);
  int i;
  for (i = 0; i < v.length; i++) {
    printf("  ");
    if (v.isInteger[i]) {
      printf("int ");
    }
    printf("%s(%d): [%s, %s]", v.name, i + 1, v.bounds[i][0],
        v.bounds[i][1]);
    if (!v.isInteger[i]) {
      printf(" +/- %s", v.precision[i]);
    }
    printf("\n");
  }
}

char* variableToString(Variable v) {
  char* buff = malloc(255*sizeof(char));
  char buffer2[255];
  sprintf(buff, "%s", v.name);
  if (v.isVector) {
    strcat(buff, "[");
    sprintf(buffer2, "%d", v.length);
    strcat(buff, buffer2);
    strcat(buff, "]");
  }
  if (v.isInteger) {
    strcat(buff, ": int");
  }
  return buff;
}

// VariableList
// Creation
VariableList createVariableList() {
  VariableList list;
  list.head = NULL;
  list.length = 0;
  list.tail = NULL;
  return list;
}

struct VariableListCell* createVariableListCell() {
  struct VariableListCell* cell = malloc(
      sizeof(struct VariableListCell));
  cell->next = NULL;
  cell->var = NULL;
  cell->prev = NULL;
  return cell;
}

int addVariableToVariableListAtIndex(VariableList* list,
    Variable* var, int index) {
  struct VariableListCell* varCell = createVariableListCell();
  varCell->var = var;
  if (list->length == 0) {
    list->length = 1;
    if (index == 0) {
      list->head = varCell;
      list->tail = varCell;
      return 0;
    } else {
      struct VariableListCell *first = createVariableListCell();
      list->head = first;
      list->tail = first;
    }
  }
  while (list->length < index) {
    struct VariableListCell* cell = createVariableListCell();
    cell->prev = list->tail;
    list->tail->next = cell;
    list->tail = cell;
    list->length++;
  }
  if (index == list->length) {
    list->tail->next = varCell;
    list->tail = varCell;
  } else {
    struct VariableListCell *pointer = list->head;
    struct VariableListCell *next;
    int i = 0;
    while (i < index) {
      pointer = pointer->next;
      i++;
    }
    if (pointer->var == NULL) {
      pointer->var = var;
      return 0;
    }
    next = pointer->next;
    pointer->next = varCell;
    varCell->prev = pointer;
    varCell->next = next;
    next->prev = varCell;
  }
  list->length++;
  return 0;
}

int addVariableToVariableList(VariableList* list, Variable* var) {
  struct VariableListCell* varCell = createVariableListCell();
  varCell->var = var;
  varCell->prev = list->tail;
  if (list->length == 0) {
    list->head = varCell;
  } else {
    list->tail->next = varCell;
  }
  list->tail = varCell;
  list->length += 1;
  return 0;
}

Variable* getVariableFromVariableList(VariableList list, int index) {
  if (index > list.length) {
    return NULL;
  }
  int i = 0;
  struct VariableListCell *cell = list.head;
  while (i < index) {
    cell = cell->next;
    i++;
  }
  return cell->var;
}

Variable* searchInVariableList(VariableList list, char* name) {
  int i = 0;
  struct VariableListCell *cell = list.head;
  while (cell != NULL && strcmp(name, cell->var->name)) {
    cell = cell->next;
    i++;
  }
  if (cell == NULL) {
    return NULL;
  }
  return cell->var;
}

int addAllToVariableList(VariableList* list, VariableList toAdd) {
  struct VariableListCell *cell = toAdd.head;
  while (cell != NULL) {
    addVariableToVariableList(list, cell->var);
    cell = cell->next;
  }
  return 0;
}

char* variableListToString(VariableList list, char*start, char*sep,
    char*end) {
  // size is strating +ending + size of name of head +
  // for each other cell, separator+size of name
  struct VariableListCell*it = list.head;
  int size = strlen(start) + strlen(end) + 1;
  char* val;
  if (list.length) {
    size += strlen(it->var->name);
    it = it->next;
    while (it != NULL) {
      size += strlen(sep);
      size += strlen(it->var->name);
      it = it->next;
    }
  }

  val = malloc((size) * sizeof(char));
  sprintf(val, start);
  if (list.length) {
    it = list.head;
    strcat(val, it->var->name);
    it = it->next;
    while (it != NULL) {
      strcat(val, sep);
      strcat(val, it->var->name);
      it = it->next;
    }
  }
  strcat(val, end);
  return val;
}

void fprintVariableList(FILE*out, VariableList list, char*sep) {
  struct VariableListCell*it = list.head;
  if (list.length) {
    char* tmp;
    it = list.head;
    tmp = variableToString(*(it->var));
    fprintf(out, tmp);
    it = it->next;
    while (it != NULL) {
      fprintf(out, sep);
      tmp = variableToString(*(it->var));
      fprintf(out, tmp);
      it = it->next;
    }
  }
}
