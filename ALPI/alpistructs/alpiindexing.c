/*
 * alpiindexing.c
 *
 *  Created on: Dec 10, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpiindexing.h"
#include <stdlib.h>
#include <string.h>

Indexing createIndexing(char* name, int start, int stop, int step) {
  Indexing i;
  i.start = start;
  i.end = stop;
  i.step = step;
  i.name = malloc((1+strlen(name))*sizeof(char));
  strcpy(i.name, name);
  return i;
}

struct IndexingCell* createIndexingCell() {
  struct IndexingCell* cell = malloc(sizeof(struct IndexingCell));
  cell->next = NULL;
  cell->prev = NULL;
  cell->val = NULL;
  return cell;
}

IndexingList createIndexingList() {
  IndexingList l;
  l.head = NULL;
  l.tail = NULL;
  l.length = 0;
  return l;
}
void addIndexingToList(IndexingList *list, Indexing *elem) {
  struct IndexingCell* cell = createIndexingCell();
  cell->val = elem;
  cell->prev = list->tail;
  if (list->length) {
    list->tail->next = cell;
  } else {
    list->head = cell;
  }
  list->tail = cell;
  list->length += 1;
}

Indexing* searchIndexingInList(IndexingList list, char* elem) {
  int i = 0;
  struct IndexingCell *cell = list.head;
  while (cell != NULL && strcmp(elem, cell->val->name)) {
    cell = cell->next;
    i++;
  }
  if (cell == NULL) {
    return NULL;
  }
  return cell->val;
}

void printIndexingList(IndexingList list) {
  printf("List of size: %d\n", list.length);
  struct IndexingCell *cell = list.head;
  while (cell != NULL) {
    printf(printIndexing(*(cell->val)));
    cell = cell->next;
  }
}

void fprintIndexing(FILE*out, Indexing indexing) {
  char buffer[255];
  char buffer2[255];
  sprintf(buffer, "%d, %d", indexing.start,
      indexing.end);
  if (indexing.step!=1) {
    sprintf(buffer2, ", %d", indexing.step);
    strcat(buffer, buffer2);
  }
  fprintf(out, buffer);
}
void sprintIndexing(char* str, Indexing indexing) {
  char buffer2[255];
  sprintf(str, "%d, %d", indexing.start,
      indexing.end);
  if (indexing.step!=1) {
    sprintf(buffer2, ", %d", indexing.step);
    strcat(str, buffer2);
  }
}

void fprintIndexingList(FILE*out,IndexingList list, char*sep) {
  struct IndexingCell *cell = list.head;
  if (cell != NULL) {
    fprintIndexing(out, *(cell->val));
    cell = cell->next;
  }
  while (cell != NULL) {
    fprintf(out, sep);
    fprintIndexing(out, *(cell->val));
    cell = cell->next;
  }
}
void sprintIndexingList(char*str,IndexingList list, char* sep) {
  struct IndexingCell *cell = list.head;
  if (cell != NULL) {
    sprintIndexing(str, *(cell->val));
    cell = cell->next;
  }
  while (cell != NULL) {
    sprintf(str, sep);
    sprintIndexing(str, *(cell->val));
    cell = cell->next;
  }
}

char* printIndexing(Indexing indexing) {
  char *buffer = malloc(64*sizeof(char));
  char buffer2[22];
  sprintf(buffer, "%d, %d", indexing.start,
      indexing.end);
  if (indexing.step!=1) {
    sprintf(buffer2, ", %d", indexing.step);
    strcat(buffer, buffer2);
  }
  return buffer;
}

