/*
 * alpistruct.c
 *
 *  Created on: Dec 9, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpiproblem.h"
#include "../src/alpiprinter.h"
#include <stdlib.h>
#include <string.h>

ProblemModel createProblemModel(char* name) {
  ProblemModel pm;
  // Name
  pm.name = name;
  // Variables
  pm.indexes = createIndexingList();
  pm.decisionVariables = createVariableList();
  pm.simpleConstants = createConstantList();
  pm.functionConstants = createFunctionList();
  // Functions
  pm.auxiliaryFunctions = createFunctionList();
  // Constraints
  pm.constraints = createConstraintList();
  // Reformulation
  pm.options.reformulation = NULL;
  pm.options.CPLEX = 1;
  pm.options.affineArithmeticForConstraints = 1;
  pm.options.computeBoundsWithTyalor = 1;
  pm.options.constraintsPropagation = 1;
  pm.options.exactConstraintChecking = 1;
  pm.options.keepAll = 1;
  pm.options.knownMajor = malloc(7 * sizeof(char));
  strcpy(pm.options.knownMajor, "3.D100");
  pm.options.logFile = malloc(11 * sizeof(char));
  strcpy(pm.options.logFile, "Result.res");
  pm.options.logFreq = 10000;
  pm.options.memLimit = 100000;
  pm.options.normalStoppingCriterion = 1;
  pm.options.notCertifiedLP = 1;
  pm.options.terminalDisplay = 1;
  pm.options.timeLimit = 1000;
  pm.options.timeUnitInSeconds = 1;
  // objective function
  pm.objective = createFunction("");
  return pm;
}

int findSymbolInProblem(ProblemModel model, char* name, int* isIndex,
    int* isDecisionVariable, int* isConstant, int* isFunction,
    int* isObj) {
  int nb = 0;
  *isDecisionVariable = 0;
  *isConstant = 0;
  *isFunction = 0;
  *isIndex = 0;
  *isObj = 0;
  Indexing* index = searchIndexingInList(model.indexes, name);
  if (index != NULL) {
    *isIndex = 1;
    nb++;
  }

  Variable* v = searchInVariableList(model.decisionVariables, name);
  if (v != NULL) {
    *isDecisionVariable = 1;
    nb++;
  }

  Constant* c = searchInConstantList(model.simpleConstants, name);
  if (c != NULL) {
    *isConstant = 1;
    nb++;
  }

  Function*f = searchInFunctionList(model.functionConstants, name);
  if (f != NULL) {
    *isFunction = 1;
    *isConstant = 1;
    nb++;
  }

  f = searchInFunctionList(model.auxiliaryFunctions, name);
  if (f != NULL) {
    *isFunction = 1;
    nb++;
  }

  if (!strcmp(model.objective.ret.name, name)) {
    *isObj = 1;
    nb++;
  }

  return nb;
}

void fprintModel(FILE* out, ProblemModel model) {
  fprintf(out, model.name);
  fprintf(out, "\n==========");
  if (model.simpleConstants.length+model.functionConstants.length) {
    fprintf(out, "\nConstants\n---------");
    if (model.simpleConstants.length) {
      fprintf(out, "\n\t");
      fprintConstantList(out, model.simpleConstants, "\n\t");
    }
    if (model.functionConstants.length) {
      fprintf(out, "\n\t");
      fprintFunctionList(out, model.functionConstants, "\n\t");
    }
  }
  if (model.indexes.length) {
    fprintf(out, "\nIndexes\n-------\n\t");
    fprintIndexingList(out, model.indexes, "\n\t");
  }
  if (model.decisionVariables.length) {
    fprintf(out, "\nDecision Variables\n------------------\n\t");
    fprintVariableList(out, model.decisionVariables, "\n\t");
  }
  if (model.auxiliaryFunctions.length) {
    fprintf(out, "\nAuxiliary Functions\n------------------\n\t");
    fprintFunctionList(out, model.auxiliaryFunctions, "\n\t");
  }
  if (model.constraints.length) {
    fprintf(out, "\nConstraints\n-----------\n\t");
    fprintConstraintList(out, model.constraints, "\n\t");
  }
  fprintf(out, "\nObjective Function\n------------------\n\t");
  fprintFunction(out, model.objective);
  fprintf(out, "\nOptions\n-------\n\t");
  fprintProblemOptions(out, model.options, "\n\t");
  fprintf(out, "\n_____________________________\n");

}

void fprintProblemOptions(FILE* out, ProblemOptions options, char*sep) {
  char buffer[255];
  sprintf(buffer, "logFile: %s%s", options.logFile, sep);
  fprintf(out, buffer);
  sprintf(buffer, "logFreq: %d%s", options.logFreq, sep);
  fprintf(out, buffer);
  sprintf(buffer, "Reformulation: %s%s", options.reformulation, sep);
  fprintf(out, buffer);
  sprintf(buffer, "time limit: %d%s", options.timeLimit, sep);
  fprintf(out, buffer);
  sprintf(buffer, "memory limit: %d%s", options.memLimit, sep);
  fprintf(out, buffer);
  sprintf(buffer, "known upper bound of minimum: %s",
      options.knownMajor);
  fprintf(out, buffer);
  if (options.computeBoundsWithTyalor) {
    sprintf(buffer, "%staylor for bound computation", sep);
    fprintf(out, buffer);
  }
  if (options.constraintsPropagation) {
    sprintf(buffer, "%sconstraint propagation", sep);
    fprintf(out, buffer);
  }
  if (options.CPLEX) {
    sprintf(buffer, "%susing CPLEX", sep);
    fprintf(out, buffer);
  }
  if (options.timeUnitInSeconds) {
    sprintf(buffer, "%stime is in seconds", sep);
    fprintf(out, buffer);
  }
  if (options.keepAll) {
    sprintf(buffer, "%skeep all results", sep);
    fprintf(out, buffer);
  }
  if (options.normalStoppingCriterion) {
    sprintf(buffer, "%snormal stopping criterion", sep);
    fprintf(out, buffer);
  }
  if (options.terminalDisplay) {
    sprintf(buffer, "%sdisplaying results in terminal", sep);
    fprintf(out, buffer);
  }
  if (options.notCertifiedLP) {
    sprintf(buffer, "%snot certified LP (or is MIP)", sep);
    fprintf(out, buffer);
  }
  if (options.exactConstraintChecking) {
    sprintf(buffer, "%sexact checking for constraints", sep);
    fprintf(out, buffer);
  }
  if (options.affineArithmeticForConstraints) {
    sprintf(buffer, "%sAffine arithmetic for constraints", sep);
    fprintf(out, buffer);
  }
}
