/*
 * alpiconstraints.c
 *
 *  Created on: Dec 13, 2013
 *      Author: Emmanuel BIGEON
 */


#include "alpiconstraints.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

Constraint createEqualityConstraint(Function*f, char* bound) {
  Constraint c;
  c.f = f;
  c.lowerBounded = 0;
  c.upperBounded = 0;
  c.type = 0;
  c.boundL = malloc((1+strlen(bound))*sizeof(char));
  strcpy(c.boundL, bound);
  c.boundU = NULL;
  c.precision = NULL;
  return c;
}

Constraint createInequalityConstraint(Function*f, char* boundL, char* boundU) {
  Constraint c;
  c.f = f;
  c.lowerBounded = 1;
  c.upperBounded = 1;
  c.type = 1;
  c.boundL = malloc((1+strlen(boundL))*sizeof(char));
  strcpy(c.boundL, boundL);
  c.boundU = malloc((1+strlen(boundU))*sizeof(char));
  strcpy(c.boundU, boundU);
  c.precision = NULL;
  return c;
}

Constraint createLowerBound(Function*f, char* boundL) {
  Constraint c;
  c.f = f;
  c.lowerBounded = 1;
  c.upperBounded = 0;
  c.type = 1;
  c.boundL = malloc((1+strlen(boundL))*sizeof(char));
  strcpy(c.boundL, boundL);
  c.boundU = NULL;
  c.precision = NULL;
  return c;
}

Constraint createUpperBound(Function*f, char* boundU) {
  Constraint c;
  c.f = f;
  c.lowerBounded = 0;
  c.upperBounded = 1;
  c.type = 1;
  c.boundL = NULL;
  c.boundU = malloc((1+strlen(boundU))*sizeof(char));
  strcpy(c.boundU, boundU);
  c.precision = NULL;
  return c;
}

void printConstraint(Constraint c) {
  if (c.type) {
    printf("%s == %s\n", c.f->ret.name, c.boundL);
  } else {
    if (c.upperBounded) {
      if (c.lowerBounded) {
        printf("%s <= %s <= %s", c.boundL, c.f->ret.name, c.boundU);
      } else {
        printf("%s <= %s", c.f->ret.name, c.boundU);
      }
    } else {
      printf("%s >= %s\n", c.f->ret.name, c.boundL);
    }
  }
}

char* constraintToString(Constraint c) {
  char* buff = malloc(255*sizeof(char));
  if (c.type) {
    sprintf(buff, "%s == %s", c.f->ret.name, c.boundL);
  } else {
    if (c.upperBounded) {
      if (c.lowerBounded) {
        sprintf(buff, "%s <= %s <= %s", c.boundL, c.f->ret.name, c.boundU);
      } else {
        sprintf(buff, "%s <= %s", c.f->ret.name, c.boundU);
      }
    } else {
      sprintf(buff, "%s >= %s", c.f->ret.name, c.boundL);
    }
  }
  return buff;
}

struct ConstraintCell* createConstraintCell() {
  struct ConstraintCell* cell = malloc(sizeof(struct ConstraintCell));
  cell->next = NULL;
  cell->prev = NULL;
  cell->val = NULL;
  return cell;
}

ConstraintList createConstraintList() {
  ConstraintList list;
  list.head = NULL;
  list.tail = NULL;
  list.length = 0;
  return list;
}

void addConstraintToList(ConstraintList *list, Constraint *elem) {
  struct ConstraintCell* cell = createConstraintCell();
  cell->val = elem;
  cell->prev = list->tail;
  if (list->length) {
    list->tail->next = cell;
  } else {
    list->head = cell;
  }
  list->tail = cell;
  list->length += 1;
}

void printConstraintList(ConstraintList list) {
  printf("List of size: %d\n", list.length);
  struct ConstraintCell *cell = list.head;
  while (cell != NULL) {
    printConstraint(*(cell->val));
    cell = cell->next;
  }
}

void fprintConstraintList(FILE*out, ConstraintList list, char*sep) {
  struct ConstraintCell *cell = list.head;
  char* tmp;
  if (cell != NULL) {
    tmp = constraintToString(*(cell->val));
    fprintf(out, tmp);
    cell = cell->next;
  }
  while (cell != NULL) {
    fprintf(out, sep);
    tmp = constraintToString(*(cell->val));
    fprintf(out, tmp);
    cell = cell->next;
  }
}

Constraint* searchInConstraintList(ConstraintList list, char* elem) {
  int i = 0;
  struct ConstraintCell *cell = list.head;
  while (cell != NULL && strcmp(elem, cell->val->f->ret.name)) {
    cell = cell->next;
    i++;
  }
  if (cell == NULL) {
    printf("not found index \"%s\"\n", elem);
    return NULL;
  }
  return cell->val;
}
