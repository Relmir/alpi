/*
 * alpistructures.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPISTRUCTURES_H_
#define ALPISTRUCTURES_H_

#include "alpiconstants.h"
#include "alpiconstraints.h"
#include "alpifunctions.h"
#include "alpiindexing.h"
#include "alpivariables.h"
#include "alpiproblem.h"
#include "alpisymbols.h"

#endif /* ALPISTRUCTURES_H_ */
