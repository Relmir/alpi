/*
 * forgen.h
 *
 *  Created on: Dec 9, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef FORGEN_H_
#define FORGEN_H_

#include <stdio.h>

/*
 * This file declare the minimum functions to generate Fortran files.
 */

// Module writing
void writeModuleHeader(FILE*file, char*moduleName, char**moduleUsed,
    int nbModules);
void writeModuleConstant(FILE*file, char*type, char*name, char*value);
void writeModuleInterface(FILE*file, char*interfaceName,
    char**interfacedNames, int nbInterfaced);
void writeModuleStartingDefinitions(FILE*file);
void writeModuleComment(FILE*file, char*comment);
void writeModuleFunctionHeader(FILE*mof, char*name, char**params,
    int nbParams, char*resultName, char**types, char**intents);
void writeModuleFunctionInstruction(FILE*mod, char* inst);
void writeModuleFunctionEnd(FILE*mod, char*name);
void writeModuleSubroutineHeader(FILE*mof, char*name, char**params,
    int nbParams, char**types, char**intents);
void writeModuleSubroutineEnd(FILE*mod, char*name);
void writeModuleEnd(FILE*mod_out, char*name);

// Program
void writeProgramHeader(FILE*file, char*moduleName, char**moduleUsed,
    int nbModules);
void writeProgramInstruction(FILE*file, char*inst);
void writeProgramEnd(FILE*mod_out, char*name);

#endif /* FORGEN_H_ */
