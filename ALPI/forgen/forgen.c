/*
 * forgen.c
 *
 *  Created on: Dec 9, 2013
 *      Author: Emmanuel BIGEON
 */

#include "forgen.h"
#include <stdlib.h>
#include <string.h>

// Modules
// Writing module by lines
void writeModuleHeader(FILE*file, char*moduleName, char**moduleUsed,
    int nbModules) {
  int i;
  fprintf(file, "module %s\n", moduleName);
  for (i = 0; i < nbModules; ++i) {
    fprintf(file, "  use %s\n", moduleUsed[i]);
  }
  fprintf(file, "  implicit none\n\n");
}
void writeModuleConstant(FILE*file, char*type, char*name, char*value) {
  char buffer[255];
  char line[80];
  sprintf(line, "    %s, parameter:: %s = ", type, name);
  sprintf(buffer, "%s%s", line, value);
  if (strlen(buffer) > 80) {
    if (strlen(line) > 80) {
      // TODO Mega aie !!!
      // temp
      fprintf(file, "    %s, &\n", type);
      fprintf(file, "        & parameter:: &\n");
      fprintf(file, "        & %s = &\n", name);
      fprintf(file, "        & %s\n", value);
    } else {
      fprintf(file, "%s&\n", line);
      sprintf(line, "        & %s", value);
      if (strlen(line) > 80) {
        // TODO Aie !
        // temp
        fprintf(file, line);
      } else {
        fprintf(file, line);
      }
    }
  } else {
    fprintf(file, "%s\n", buffer);
  }
}
void writeModuleInterface(FILE*file, char*interfaceName,
    char**interfacedNames, int nbInterfaced) {
  if (nbInterfaced) {
    char line[80];
    char buffer[70];
    int i;
    fprintf(file, "    interface %s\n", interfaceName);
    sprintf(line, "    module procedure %s", interfacedNames[0]);
    fprintf(file, line);
    for (i = 1; i < nbInterfaced; ++i) {
      if(strlen(line)+strlen(interfacedNames[i])>80) {
        sprintf(line, "        & %s", interfacedNames[i]);
        fprintf(file, ", &\n%s", line);
      } else {
        sprintf(buffer, ", %s", interfacedNames[i]);
        strcat(line, buffer);
        fprintf(file, buffer);
      }
    }
    fprintf(file, "\n    end interface\n");
  }
}
void writeModuleStartingDefinitions(FILE*file) {
  fprintf(file, "  contains\n");
}
void writeModuleComment(FILE*file, char*comment) {
  fprintf(file, "  !%s\n", comment);
}

void writeModuleFunctionHeader(FILE*mod, char*name, char**params,
    int nbParams, char*resultName, char**types, char**intents) {
  int i;
  char blanks[255];
  sprintf(blanks, "            ");
  for (i = 0; i < strlen(name); ++i) {
    strcat(blanks, " ");
  }

  // Head
  fprintf(mod, "    function %s(", name);
  if (nbParams) {
    fprintf(mod, "%s", params[0]);
    for (i = 1; i < nbParams; ++i) {
      fprintf(mod, ", &\n%s& %s", blanks, params[i]);
    }
  }
  fprintf(mod, ")");
  if (resultName != NULL) {
    fprintf(mod, " &\n%s& result (%s)", blanks, resultName);
  }
  fprintf(mod, "\n");

  // Types
  if (resultName == NULL) {
    fprintf(mod, "      %s:: %s\n", types[0], name);
  } else {
    fprintf(mod, "      %s:: %s\n", types[0], resultName);
  }
  for (i = 0; i < nbParams; ++i) {
    fprintf(mod, "      %s, intent(%s):: %s\n", types[i + 1],
        intents[i], params[i]);
  }

}

void writeModuleFunctionInstructionFollow(FILE*mod, char* inst);

void writeModuleFunctionInstruction(FILE*mod, char* inst) {
  if (strlen(inst)>70) {
    char* point = inst;
    char* end = inst+70;
    char* last = NULL;
    while(point!=end) {
      if (*point==' ') {
        last = point;
      }
      point++;
    }
    if (last==NULL) {
      // We cannot split !!!
      printf("[ ERR ] Instruction is too long without any space to split at (%s).", inst);
    } else {
      *last = '\0';
      fprintf(mod, "        %s &\n", inst);
      *last = ' ';
      writeModuleFunctionInstructionFollow(mod, last+1);
    }
  } else {
    fprintf(mod, "        %s\n", inst);
  }
}

void writeModuleFunctionInstructionFollow(FILE*mod, char* inst) {
  if (strlen(inst)>60) {
    char* point = inst;
    char* end = inst+60;
    char* last = NULL;
    while(point!=end) {
      if (*point==' ') {
        last = point;
      }
      point++;
    }
    if (last==NULL) {
      // We cannot split !!!
      printf("[ ERR ] Instruction is too long without any space to split at (%s).", inst);
    } else {
      *last = '\0';
      fprintf(mod, "            & %s &\n", inst);
      writeModuleFunctionInstructionFollow(mod, last+1);
      *last = ' ';
    }
  } else {
  fprintf(mod, "            & %s\n", inst);
  }
}

void writeModuleFunctionEnd(FILE*mod, char*name) {
  fprintf(mod, "    end function %s\n", name);
}

void writeModuleSubroutineHeader(FILE*mod, char*name, char**params,
    int nbParams, char**types, char**intents) {
  int i;
  char blanks[255];
  sprintf(blanks, "              ");
  for (i = 0; i < strlen(name); ++i) {
    strcat(blanks, " ");
  }

  // Head
  fprintf(mod, "    subroutine %s(", name);
  if (nbParams) {
    fprintf(mod, "%s", params[0]);
    for (i = 1; i < nbParams; ++i) {
      fprintf(mod, ", &\n%s%s", blanks, params[i]);
    }
  }
  fprintf(mod, ")\n");

  // Types
  for (i = 0; i < nbParams; ++i) {
    fprintf(mod, "      %s, intent(%s):: %s\n", types[i], intents[i],
        params[i]);
  }

}

void writeModuleSubroutineEnd(FILE*mod, char*name) {
  fprintf(mod, "    end subroutine %s\n", name);
}

void writeModuleEnd(FILE*mod_out, char*name) {
  fprintf(mod_out, "end module %s\n", name);
}

// Program
void writeProgramHeader(FILE*mod, char*name, char**mods, int nbMods) {
  int i;
  fprintf(mod, "program %s\n", name);
  for (i = 0; i < nbMods; ++i) {
    fprintf(mod, "  use %s\n", mods[i]);
  }
  fprintf(mod, "  implicit none\n\n");
}

void writeProgramInstruction(FILE*file, char*inst) {
  fprintf(file, "  %s\n", inst);
}

void writeProgramEnd(FILE*mod_out, char*name) {
  fprintf(mod_out, "end program %s\n", name);
}
