/*
 * alpirulenumerals.c
 *
 *  Created on: Jan 7, 2014
 *      Author: Emmanuel BIGEON
 */

#include "alpirulenumerals.h"
#include "alpiruleparsertools.h"
#include <stdlib.h>
#include <stdio.h>

RuleInformation alpiRuleRelativeFromNatural(RuleInformation nat) {
  return nat;
}
RuleInformation alpiRuleRelativeFromOpposeNatural(
    RuleInformation minus, RuleInformation nat) {
  freeRuleInformation(minus);
  nat.ival = -nat.ival;
  return nat;
}
RuleInformation alpiRuleRelativeFromIdent(RuleInformation ident) {
  Param *p;
  char buffer[255];
  findParam(ident.text, &p);
  RuleInformation info;
  if (p == NULL) {
    sprintf(buffer, "Undefined parameter %s", ident.text);
    yyerror(buffer);
    setRuleInformationBase(&info, "");
  } else if (p->isDefined) {
    Symbol* s;
    s = p->symbol;
    if (s->isCst && !s->isFunction) {
      if (s->cst->isInteger) {
        setRuleInformationBase(&info, s->cst->value);
        info.ival = atoi(s->cst->value);
      } else {
        sprintf(buffer, "Parameter %s isn't an integer", ident.text);
        yyerror(buffer);
        setRuleInformationBase(&info, "");
      }
    }
  } else {
    sprintf(buffer, "Parameter %s has no value yet", ident.text);
    yyerror(buffer);
    setRuleInformationBase(&info, "");
  }
  freeRuleInformation(ident);
  return info;
}

RuleInformation alpiRuleRelativeFromOpposeIdent(RuleInformation minus,
    RuleInformation ident) {
  Param *p;
  char buffer[255];
  findParam(ident.text, &p);
  RuleInformation info;
  if (p == NULL) {
    sprintf(buffer, "Undefined parameter %s", ident.text);
    yyerror(buffer);
    setRuleInformationBase(&info, "");
  } else if (p->isDefined) {
    Symbol* s;
    s = p->symbol;
    if (s->isCst && !s->isFunction) {
      if (s->cst->isInteger) {
        setRuleInformationBase(&info, s->cst->value);
        info.ival = -atoi(s->cst->value);
      } else {
        sprintf(buffer, "Parameter %s isn't an integer", ident.text);
        yyerror(buffer);
        setRuleInformationBase(&info, "");
      }
    }
  } else {
    sprintf(buffer, "Parameter %s has no value yet", ident.text);
    yyerror(buffer);
    setRuleInformationBase(&info, "");
  }
  freeRuleInformation(ident);
  return info;
}

RuleInformation alpiRuleCstRealFromNatural(RuleInformation nat) {
  nat.fval = nat.ival;
  return nat;
}
RuleInformation alpiRuleCstRealFromOpposeNatural(
    RuleInformation minus, RuleInformation nat) {
  nat.fval = -nat.ival;
  return nat;
}
RuleInformation alpiRuleCstRealFromNumber(RuleInformation num) {
  return num;
}
RuleInformation alpiRuleCstRealFromOpposeNumber(RuleInformation minus,
    RuleInformation num) {
  num.fval = -num.fval;
  return num;
}
RuleInformation alpiRuleRealFromIdent(RuleInformation ident) {
  Param *p;
  char buffer[255];
  findParam(ident.text, &p);
  RuleInformation info;
  if (p == NULL) {
    sprintf(buffer, "Undefined parameter %s", ident.text);
    yyerror(buffer);
    setRuleInformationBase(&info, "");
  } else if (p->isDefined) {
    Symbol* s;
    s = p->symbol;
    if (s->isCst && !s->isFunction) {
      if (!s->cst->isInteger) {
        setRuleInformationBase(&info, s->cst->value);
        info.ival = atof(s->cst->value);
      } else {
        sprintf(buffer, "Parameter %s isn't a real", ident.text);
        yyerror(buffer);
        setRuleInformationBase(&info, "");
      }
    }
  } else {
    sprintf(buffer, "Parameter %s has no value yet", ident.text);
    yyerror(buffer);
    setRuleInformationBase(&info, "");
  }
  freeRuleInformation(ident);
  return info;
}

RuleInformation alpiRuleRealFromOpposeIdent(RuleInformation minus,
    RuleInformation ident) {
  Param *p;
  char buffer[255];
  findParam(ident.text, &p);
  RuleInformation info;
  if (p == NULL) {
    sprintf(buffer, "Undefined parameter %s", ident.text);
    yyerror(buffer);
    setRuleInformationBase(&info, "");
  } else if (p->isDefined) {
    Symbol* s;
    s = p->symbol;
    if (s->isCst && !s->isFunction) {
      if (!s->cst->isInteger) {
        setRuleInformationBase(&info, s->cst->value);
        info.ival = -atof(s->cst->value);
      } else {
        sprintf(buffer, "Parameter %s isn't a real", ident.text);
        yyerror(buffer);
        setRuleInformationBase(&info, "");
      }
    }
  } else {
    sprintf(buffer, "Parameter %s has no value yet", ident.text);
    yyerror(buffer);
    setRuleInformationBase(&info, "");
  }
  freeRuleInformation(ident);
  return info;
}
