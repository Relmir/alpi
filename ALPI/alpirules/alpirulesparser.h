/*
 * alpirulesparser.h
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULESPARSER_H_
#define ALPIRULESPARSER_H_

#include "alpirulesdefinitions.h"
#include "alpirulesexpr.h"
#include "alpirulesoptions.h"
#include "alpirulenumerals.h"
#include "alpirulesets.h"
#include "alpiruleparams.h"

void alpiRuleParserInit();
void alpiRuleParserClean();

#endif /* ALPIRULESPARSER_H_ */
