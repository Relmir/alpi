/*
 * alpirulesdefinitions.h
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULESDEFINITIONS_H_
#define ALPIRULESDEFINITIONS_H_

#include "alpiruleparsertools.h"

// Set defintion
RuleInformation alpiRuleSetDef(RuleInformation set,
    RuleInformation ident, RuleInformation affect,
    RuleInformation natural1, RuleInformation span,
    RuleInformation natural2, RuleInformation byStep);
RuleInformation alpiRuleByStepEmpty();

// Variables definitions
void alpiRuleVariableDefBeforeIndexing();
RuleInformation alpiRuleVariableDefBeforeVarOpt(RuleInformation var,
    RuleInformation ident, RuleInformation index);

RuleInformation alpiVarOptUpperBound(RuleInformation leq,
    RuleInformation number);
RuleInformation alpiVarOptLowerBound(RuleInformation geq,
    RuleInformation number);
RuleInformation alpiVarOptPrecision(RuleInformation prec,
    RuleInformation number);

// Constraint Definition
RuleInformation alpiRuleConstraintDefinition(
    RuleInformation subjectTo, RuleInformation ident, RuleInformation index,
    RuleInformation colon, RuleInformation e1, RuleInformation test,
    RuleInformation e2, RuleInformation optOtherTest,
    RuleInformation prec, RuleInformation real);
RuleInformation alpiRuleEndConstraintEmpty();
RuleInformation alpiRuleEndConstraintBound(RuleInformation leq,
    RuleInformation real);

RuleInformation alpiRuleObjDef(RuleInformation minimize,
    RuleInformation prec, RuleInformation real, RuleInformation ident,
    RuleInformation affect, RuleInformation expr);

#endif /* ALPIRULESDEFINITIONS_H_ */
