/*
 * alpiruleslexer.c
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpiruleslexer.h"
#include "alpiruletools.h"
#include <stdlib.h>
#include <string.h>

//============
// Lexer rules
//------------

// Converts text to rule information for a given rule
RuleInformation alpiLRuleNatural(char* text) {
  RuleInformation info;
  setRuleInformationBase(&info, text);
  info.fval = atof(text);
  info.ival = atoi(text);
  return info;
}
RuleInformation alpiLRuleReal(char* text) {
  RuleInformation info;
  setRuleInformationBase(&info, text);
  char * t = formatReal(text);
  info.fval = atof(t);
  free(t);
  return info;
}
RuleInformation alpiLRuleBoolean(char* text) {
  RuleInformation info;
  setRuleInformationBase(&info, text);
  info.bval = strcmp(text, ".false.");
  return info;
}

RuleInformation alpiLRuleSimpleToken(char* text) {
  RuleInformation info;
  setRuleInformationBase(&info, text);
  return info;
}

RuleInformation alpiLRuleBuiltInFunction(char* text) {
  RuleInformation info;
  setRuleInformationBase(&info, text);
  return info;
}
RuleInformation alpiLRuleFilename(char* text) {
  RuleInformation info;
  setRuleInformationBase(&info, text);
  return info;
}
RuleInformation alpiLRuleIdent(char* text) {
  RuleInformation info;
  setRuleInformationBase(&info, text);
  return info;
}

