/*
 * alpirulesdefinitions.c
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpirulesdefinitions.h"
#include "alpiruleparsertools.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

Function createFunctionFromExpr(char*name, RuleInformation expr,
    VariableList params);
void invalidConstraint();

RuleInformation alpiRuleSetDef(RuleInformation set,
    RuleInformation ident, RuleInformation affect,
    RuleInformation natural1, RuleInformation span,
    RuleInformation natural2, RuleInformation byStep) {
  RuleInformation info;

  Symbol *s;
  if (findSymbol(ident.sval, &s)) {
    declaredSymbol(ident.sval, s->isIndexSet, s->isVariable, s->isCst,
        s->isFunction, s->isObj);
  } else {
    Indexing *i = malloc(sizeof(Indexing));
    *i = createIndexing(ident.sval, natural1.ival, natural2.ival,
        byStep.ival);
    addIndexingToList(&(alpiPb.indexes), i);
    s = malloc(sizeof(Symbol));
    *s = createIndexingSetSymbol(i);
    addSymbolToList(peekSymbolListFromStack(symbolTable), s);
  }

  freeRuleInformation(set);
  freeRuleInformation(ident);
  freeRuleInformation(natural1);
  freeRuleInformation(natural2);
  freeRuleInformation(byStep);

  return info;
}

RuleInformation alpiRuleByStepEmpty() {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  info.ival = 1;
  return info;
}

void alpiRuleVariableDefBeforeIndexing() {
  SymbolList * nsl = malloc(sizeof(SymbolList));
  *nsl = createSymbolList();
  pushSymbolListInStack(&symbolTable, nsl);
}

RuleInformation alpiRuleVariableDefBeforeVarOpt(RuleInformation var,
    RuleInformation ident, RuleInformation indexlist) {
  RuleInformation info;

  Symbol* s = NULL;
  if (findSymbol(ident.sval, &s)) {
    declaredSymbol(ident.sval, s->isIndexSet, s->isVariable, s->isCst,
        s->isFunction, s->isObj);
  } else {
    char buffer[255];
    Variable* v = malloc(sizeof(Variable));
    Indexing* ind = indexlist.suppl[0];
    if (ind->start != 1 || ind->step != 1) {
      sprintf(buffer, "Variable definitions can only use indexing "
          "set starting with one and with a step of one.");
      yyerror(buffer);
    } else {
      *v = createIndexedVariable(ident.sval, *ind);
    }
    addVariableToVariableList(&alpiPb.decisionVariables, v);
    s = malloc(sizeof(Symbol));
    *s = createDecisionVariableSymbol(v);
    SymbolList * nsl =popSymbolListFromStack(&symbolTable);
    addSymbolToList(peekSymbolListFromStack(symbolTable), s);
    pushSymbolListInStack(&symbolTable, nsl);
    free(downInfo.head->suppl);
    downInfo.head->suppl = malloc(sizeof(Variable*));
    downInfo.head->suppl[0] = v;
    downInfo.head->supplSize = 1;
  }

  return info;
}

RuleInformation alpiVarOptUpperBound(RuleInformation leq,
    RuleInformation number) {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  setVariableUpperBounds(downInfo.head->suppl[0], number.sval);
  freeRuleInformation(number);
  return info;
}
RuleInformation alpiVarOptLowerBound(RuleInformation geq,
    RuleInformation number) {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  setVariableLowerBounds(downInfo.head->suppl[0], number.sval);
  freeRuleInformation(number);
  return info;
}
RuleInformation alpiVarOptPrecision(RuleInformation prec,
    RuleInformation number) {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  setVariablePrecisions(downInfo.head->suppl[0], number.sval);
  freeRuleInformation(number);
  return info;
}

RuleInformation alpiRuleConstraintDefinition(
    RuleInformation subjectTo, RuleInformation ident,
    RuleInformation index, RuleInformation colon, RuleInformation e1,
    RuleInformation test, RuleInformation e2,
    RuleInformation optOtherTest, RuleInformation prec,
    RuleInformation real) {
  RuleInformation info;
  Function*f = malloc(sizeof(Function));
  Constraint *c = malloc(sizeof(Constraint));
  setRuleInformationBase(&info, "");
  if (!(e1.bval % 2)) {
    // e1 is actually a constant
    if (e2.bval % 2) {
      // and e2 is a non constant expression, all right!
      *f = createFunctionFromExpr(ident.sval, e2,
          alpiPb.decisionVariables);
      addFunctionToList(&(alpiPb.auxiliaryFunctions), f);
      if (!strcmp(test.sval, ">=")) {
        *c = createUpperBound(f, e1.sval);
        c->precision = malloc(
            sizeof(char*) * (1 + strlen(real.sval)));
        strcpy(c->precision, real.sval);
        addConstraintToList(&(alpiPb.constraints), c);
        if (optOtherTest.bval) {
          invalidConstraint();
        }
      } else if (!strcmp(test.sval, "<=")) {
        if (optOtherTest.bval) {
          *c = createInequalityConstraint(f, e1.sval,
              optOtherTest.sval);
        } else {
          *c = createLowerBound(f, e1.sval);
          c->upperBounded = 0;
        }
        c->precision = malloc(
            sizeof(char*) * (1 + strlen(real.sval)));
        strcpy(c->precision, real.sval);
        addConstraintToList(&(alpiPb.constraints), c);
      } else if (!strcmp(test.sval, "==")) {
        *c = createEqualityConstraint(f, e1.sval);
        c->precision = malloc(
            sizeof(char*) * (1 + strlen(real.sval)));
        strcpy(c->precision, real.sval);
        addConstraintToList(&(alpiPb.constraints), c);
        if (optOtherTest.bval) {
          invalidConstraint();
        }
      } else {
        invalidConstraint();
      }
    } else {
      invalidConstraint();
    }
  } else if (!(e2.bval % 2)) {
    // Ok, e1 is non constant, but e2 is constant
    *f = createFunctionFromExpr(ident.sval, e1,
        alpiPb.decisionVariables);
    addFunctionToList(&(alpiPb.auxiliaryFunctions), f);
    if (!strcmp(test.sval, ">=")) {
      *c = createLowerBound(f, e2.sval);
      c->precision = malloc(sizeof(char*) * (1 + strlen(real.sval)));
      strcpy(c->precision, real.sval);
      addConstraintToList(&(alpiPb.constraints), c);
    } else if (!strcmp(test.sval, "<=")) {
      *c = createUpperBound(f, e2.sval);
      c->precision = malloc(sizeof(char*) * (1 + strlen(real.sval)));
      strcpy(c->precision, real.sval);
      addConstraintToList(&(alpiPb.constraints), c);
    } else if (!strcmp(test.sval, "==")) {
      *c = createEqualityConstraint(f, e2.sval);
      c->precision = malloc(sizeof(char*) * (1 + strlen(real.sval)));
      strcpy(c->precision, real.sval);
      addConstraintToList(&(alpiPb.constraints), c);
    } else {
      invalidConstraint();
    }
    if (optOtherTest.bval) {
      invalidConstraint();
    }
  } else {
    invalidConstraint();
  }

  freeRuleInformation(ident);
  freeRuleInformation(e1);
  freeRuleInformation(e2);
  freeRuleInformation(optOtherTest);
  freeRuleInformation(real);
  return info;
}
RuleInformation alpiRuleEndConstraintEmpty() {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  info.bval = 0;
  return info;
}
RuleInformation alpiRuleEndConstraintBound(RuleInformation leq,
    RuleInformation real) {
  RuleInformation info;
  setRuleInformationBase(&info, real.sval);
  info.bval = 1;
  info.fval = real.fval;

  freeRuleInformation(real);
  return info;
}

RuleInformation alpiRuleObjDef(RuleInformation minimize,
    RuleInformation prec, RuleInformation real, RuleInformation ident,
    RuleInformation affect, RuleInformation expr) {
  RuleInformation info;
  setRuleInformationBase(&info, ident.sval);
  int i;
  alpiPb.objective = createFunction(ident.sval);
  alpiPb.objective.ret.precision[0] = malloc(
      (1 + strlen(real.sval)) * sizeof(char));
  strcpy(alpiPb.objective.ret.precision[0], real.sval);
  alpiPb.objective.params = alpiPb.decisionVariables;
  alpiPb.objective.bodySize = expr.supplSize + 1;
  char** bodyInstructions = malloc(
      (1 + expr.supplSize) * sizeof(char*));

  for (i = 0; i < expr.supplSize; ++i) {
    bodyInstructions[i] = malloc(
        (1 + strlen(expr.suppl[i])) * sizeof(char));
    strcpy(bodyInstructions[i], expr.suppl[i]);
  }
  bodyInstructions[expr.supplSize] = malloc(
      (strlen(ident.sval) + 9 + strlen(expr.sval)) * sizeof(char));
  sprintf(bodyInstructions[expr.supplSize], "USER_%s = %s",
      ident.sval, expr.sval);
  alpiPb.objective.bodyInstructions = bodyInstructions;

  return info;
}

Function createFunctionFromExpr(char* name, RuleInformation expr,
    VariableList params) {
  int i;
  Function f = createFunction(name);
  addAllToVariableList(&(f.params), params);
  f.bodySize = expr.supplSize + 1;
  f.bodyInstructions = malloc(f.bodySize * sizeof(char*));
  for (i = 0; i < expr.supplSize; ++i) {
    f.bodyInstructions[i] = malloc(
        (1 + strlen(expr.suppl[i])) * sizeof(char));
    strcpy(f.bodyInstructions[i], expr.suppl[i]);
  }
  f.bodyInstructions[expr.supplSize] = malloc(
      (strlen(expr.sval) + 9 + strlen(name)) * sizeof(char));
  sprintf(f.bodyInstructions[expr.supplSize], "USER_%s = %s", name,
      expr.sval);
  struct SymbolCell* it = expr.symbols.head;
  Symbol*s;
  Variable *v;
  while (it != NULL) {
    s = it->val;
    v = malloc(sizeof(Variable));
    *v = createVariable(s->name);
    if (s->isIndex) {
      v->isInteger[0] = 1;
    }
    addVariableToVariableList(&(f.symbols), v);
    it = it->next;
  }
  return f;
}

void invalidConstraint() {
  char buffer[255];
  sprintf(buffer, "The constraint expression is invalid.");
  yyerror(buffer);
  sprintf(buffer, "  Rewrite it to be of the form : a <= f(x) <= b");
  yyerror(buffer);
  sprintf(buffer, "                              or a <= f(x)");
  yyerror(buffer);
  sprintf(buffer, "                              or f(x) <= a");
  yyerror(buffer);
  sprintf(buffer, "                              or a >= f(x)");
  yyerror(buffer);
  sprintf(buffer, "                              or f(x) >= a");
  yyerror(buffer);
  sprintf(buffer, "                              or f(x) == a");
  yyerror(buffer);
  sprintf(buffer, "                              or a == f(x)");
  yyerror(buffer);
  sprintf(buffer,
      "  with a and b real and f(x) the non constant expression");
  yyerror(buffer);
}
