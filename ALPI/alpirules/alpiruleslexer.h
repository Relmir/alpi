/*
 * alpiruleslexer.h
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULESLEXER_H_
#define ALPIRULESLEXER_H_

#include "alpiruletools.h"

//============
// Lexer rules
//------------

// Converts text to rule information for a given rule
RuleInformation alpiLRuleNatural(char* text);
RuleInformation alpiLRuleReal(char* text);
RuleInformation alpiLRuleBoolean(char* text);

RuleInformation alpiLRuleSimpleToken(char* text);

RuleInformation alpiLRuleBuiltInFunction(char* text);
RuleInformation alpiLRuleFilename(char* text);
RuleInformation alpiLRuleIdent(char* text);


#endif /* ALPIRULESLEXER_H_ */
