/*
 * alpirulenumerals.h
 *
 *  Created on: Jan 7, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULENUMERALS_H_
#define ALPIRULENUMERALS_H_

#include "alpiruleparsertools.h"

RuleInformation alpiRuleRelativeFromNatural(RuleInformation nat);
RuleInformation alpiRuleRelativeFromOpposeNatural(
    RuleInformation minus, RuleInformation nat);
RuleInformation alpiRuleRelativeFromIdent(RuleInformation ident);
RuleInformation alpiRuleRelativeFromOpposeIdent(RuleInformation minus,
    RuleInformation ident);

RuleInformation alpiRuleCstRealFromNatural(RuleInformation nat);
RuleInformation alpiRuleCstRealFromOpposeNatural(
    RuleInformation minus, RuleInformation nat);
RuleInformation alpiRuleCstRealFromNumber(RuleInformation num);
RuleInformation alpiRuleCstRealFromOpposeNumber(RuleInformation minus,
    RuleInformation num);
RuleInformation alpiRuleRealFromIdent(RuleInformation ident);
RuleInformation alpiRuleRealFromOpposeIdent(RuleInformation minus,
    RuleInformation ident);

#endif /* ALPIRULENUMERALS_H_ */
