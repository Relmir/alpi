/*
 * alpirulesexpr.c
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpirulesexpr.h"
#include "alpiruleparsertools.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void copySuppl(RuleInformation* toChange, RuleInformation* in,
    int start, int stop, int size) {
  if (size) {
    if (toChange->supplSize) {
      free(toChange->suppl);
    }
    toChange->supplSize = size;
    toChange->suppl = malloc(size * sizeof(void*));
  }
  int i;
  for (i = start; i < stop; ++i) {
    toChange->suppl[i] = malloc(
        (1 + strlen(in->suppl[i - start])) * sizeof(char));
    strcpy(toChange->suppl[i], in->suppl[i - start]);
  }
  if (in != NULL) {
    addAllToSymbolList(&(toChange->symbols), in->symbols);
  }
}

RuleInformation alpiRuleExpressionBuiltInFunction(RuleInformation fun,
    RuleInformation opp, RuleInformation expression,
    RuleInformation clp) {
  RuleInformation info;
  char string[255];
  sprintf(string, "%s(%s)", fun.sval, expression.sval);
  setRuleInformationBase(&info, string);
  info.bval = 2 + expression.bval;
  copySuppl(&info, &expression, 0, expression.supplSize,
      expression.supplSize);

  freeRuleInformation(expression);
  freeRuleInformation(fun);
  return info;
}

RuleInformation alpiRuleExpressionQualifiedIdent(
    RuleInformation ident, RuleInformation qualif) {
  // TODO Factor
  char buffer[255];
  char* bufferP = NULL;

  sprintf(buffer, "USER_%s%s", ident.sval, qualif.sval);

  Symbol *s = NULL;
  if (!findSymbol(ident.sval, &s)) {
    sprintf(buffer, "Undeclared symbol: %s", ident.sval);
    yyerror(buffer);
    RuleInformation info;
    setRuleInformationBase(&info, buffer);
    return info;
  } else if (s->isCst) {
    if (s->isFunction) {
      Function*f = s->fun;
      if (s->isParam) {
        // Coming feature: parametrized functions
        if (qualif.ival == f->params.length) {
          // Okay, check types of arguments ?
          sprintf(buffer, "USER_%s%s", ident.sval, qualif.sval);
        } else {
          sprintf(buffer, "Parametrized function %s must have %d "
              "arguments, there were %d.", f->ret.name,
              f->params.length, qualif.ival);
        }
      } else if (qualif.ival && !(f->ret.isVector)) {
        sprintf(buffer, "Constant %s cannot be qualified",
            ident.sval);
        yyerror(buffer);
      } else if (qualif.ival) {
        // TODO check the qualifier (must be integer or
        // indexing set)
        sprintf(buffer, "USER_%s()%s", ident.sval, qualif.sval);
      } else {
        sprintf(buffer, "USER_%s()", ident.sval);
      }
    } else {
      if (qualif.ival) {
        sprintf(buffer, "Constant %s cannot be qualified",
            ident.sval);
        yyerror(buffer);
      } else {
        sprintf(buffer, "USER_%s", ident.sval);
      }
    }
  } else if (s->isVariable) {
    if (qualif.ival && !(s->var->isVector)) {
      sprintf(buffer, "The variable %s cannot be qualified",
          ident.sval);
      yyerror(buffer);
    } else if (qualif.ival) {
      if (qualif.ival != 1) {
        sprintf(buffer, "Invalid Qualifier for the variable %s",
            ident.sval);
        yyerror(buffer);
      } else {
        // TODO check the qualifier (must be integer or
        // indexing set)
        // TODO Check that the index is <= length
        sprintf(buffer, "USER_%s%s", ident.sval, qualif.sval);
      }
    } else {
      sprintf(buffer, "USER_%s", ident.sval);
    }
  } else if (s->isObj) {
    sprintf(buffer,
        "An objective function (%s) cannot appear in any expression",
        ident.sval);
    yyerror(buffer);
  } else if (s->isFunction) {
    Function*f = s->fun;
    // TODO cut in sub cases: global function, parametrized global
    // function, parametrized constant function.
    // For the moment only global functions
    if (qualif.ival && !(f->ret.isVector)) {
      sprintf(buffer, "Function %s cannot be qualified", ident.sval);
      yyerror(buffer);
    } else if (qualif.ival) {
      bufferP = variableListToString(alpiPb.decisionVariables,
          "(USER_", ", USER_", ")");
      sprintf(buffer, "USER_%s%s%s", ident.sval, bufferP,
          qualif.sval);
      free(bufferP);
    } else {
      bufferP = variableListToString(alpiPb.decisionVariables,
          "(USER_", ", USER_", ")");
      sprintf(buffer, "USER_%s%s", ident.sval, bufferP);
      free(bufferP);
    }
  } else if (s->isIndexSet) {
    if (qualif.ival) {
      sprintf(buffer, "Indexing set %s cannot be qualified",
          ident.sval);
      yyerror(buffer);
    } else {
      sprintf(buffer, "Indexing set %s cannot appear in expression",
          ident.sval);
      yyerror(buffer);
    }
  } else if (s->isParam) {
    Param* p;
    if (findParam(s->name, &p)) {
      if (p->isDefined) {
        if (!p->isUsed) {
          addSymbolToProblem(p->symbol);
        }
        p->isUsed = 1;
        if (p->isVector && qualif.ival) {
          sprintf(buffer, "USER_%s%s", ident.sval, qualif.sval);
        } else if (qualif.ival) {
          sprintf(buffer, "param %s cannot be qualified", ident.sval);
          yyerror(buffer);
        } else {
          sprintf(buffer, "USER_%s", ident.sval);
        }
      } else {
        sprintf(buffer, "%s is used but still undefined !", ident.sval);
        yyerror(buffer);
      }
    } else {
      // symbol is a param but no params found...
      sprintf(buffer, "%s is registered as a param but no param"
          " with this name exist...", ident.sval);
      yyerror(buffer);
    }
  } else {
    // index integer !
    if (qualif.ival) {
      sprintf(buffer, "%s cannot be qualified", ident.sval);
      yyerror(buffer);
    }
    sprintf(buffer, "USER_%s", ident.sval);
  }
  RuleInformation info;
  setRuleInformationBase(&info, buffer);
  copySuppl(&info, &qualif, 0, qualif.supplSize, qualif.supplSize);
  info.bval = qualif.bval + 2 * s->isCst
      + (1 - s->isCst) * (1 - qualif.bval % 2);

  freeRuleInformation(ident);
  freeRuleInformation(qualif);
  return info;
}

RuleInformation alpiRuleExpressionNatural(RuleInformation nat) {
  RuleInformation info;
  info = nat;
  info.bval = 0;
  return info;
}

RuleInformation alpiRuleExpressionReal(RuleInformation real) {
  RuleInformation info;
  info = real;
  info.bval = 0;
  return info;
}

RuleInformation alpiRuleExpressionAdd(RuleInformation e1,
    RuleInformation sign, RuleInformation e2) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "%s + %s", e1.sval, e2.sval);
  setRuleInformationBase(&info, buffer);
  info.bval = 2 - ((e1.bval % 2) + (e2.bval % 2) + 1) / 2;
  copySuppl(&info, &e1, 0, e1.supplSize, e1.supplSize + e2.supplSize);
  copySuppl(&info, &e2, e1.supplSize, e2.supplSize,
      e1.supplSize + e2.supplSize);

  freeRuleInformation(e1);
  freeRuleInformation(e2);
  return info;
}

RuleInformation alpiRuleExpressionDiff(RuleInformation e1,
    RuleInformation sign, RuleInformation e2) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "%s - %s", e1.sval, e2.sval);
  setRuleInformationBase(&info, buffer);
  info.bval = 2 - ((e1.bval % 2) + (e2.bval % 2) + 1) / 2;
  copySuppl(&info, &e1, 0, e1.supplSize, e1.supplSize + e2.supplSize);
  copySuppl(&info, &e2, e1.supplSize, e2.supplSize,
      e1.supplSize + e2.supplSize);

  freeRuleInformation(e1);
  freeRuleInformation(e2);
  return info;
}

RuleInformation alpiRuleExpressionTimes(RuleInformation e1,
    RuleInformation sign, RuleInformation e2) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "%s * %s", e1.sval, e2.sval);
  setRuleInformationBase(&info, buffer);
  info.bval = 2 - ((e1.bval % 2) + (e2.bval % 2) + 1) / 2;
  copySuppl(&info, &e1, 0, e1.supplSize, e1.supplSize + e2.supplSize);
  copySuppl(&info, &e2, e1.supplSize, e2.supplSize,
      e1.supplSize + e2.supplSize);

  freeRuleInformation(e1);
  freeRuleInformation(e2);
  return info;
}

RuleInformation alpiRuleExpressionDiv(RuleInformation e1,
    RuleInformation sign, RuleInformation e2) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "%s / %s", e1.sval, e2.sval);
  setRuleInformationBase(&info, buffer);
  info.bval = 2 - ((e1.bval % 2) + (e2.bval % 2) + 1) / 2;
  copySuppl(&info, &e1, 0, e1.supplSize, e1.supplSize + e2.supplSize);
  copySuppl(&info, &e2, e1.supplSize, e2.supplSize,
      e1.supplSize + e2.supplSize);

  freeRuleInformation(e1);
  freeRuleInformation(e2);
  return info;
}

RuleInformation alpiRuleExpressionPow(RuleInformation e1,
    RuleInformation sign, RuleInformation e2) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "%s ** %s", e1.sval, e2.sval);
  setRuleInformationBase(&info, buffer);
  info.bval = 2 - ((e1.bval % 2) + (e2.bval % 2) + 1) / 2;
  copySuppl(&info, &e1, 0, e1.supplSize, e1.supplSize + e2.supplSize);
  copySuppl(&info, &e2, e1.supplSize, e2.supplSize,
      e1.supplSize + e2.supplSize);

  freeRuleInformation(e1);
  freeRuleInformation(e2);
  return info;
}

RuleInformation alpiRuleExpressionNeg(RuleInformation sign,
    RuleInformation expr) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "-%s", expr.sval);
  setRuleInformationBase(&info, buffer);
  info.bval = expr.bval;
  info.ival = -expr.ival;
  info.fval = -expr.fval;
  copySuppl(&info, &expr, 0, expr.supplSize, expr.supplSize);

  freeRuleInformation(expr);
  return info;
}

RuleInformation alpiRuleExpressionParenthesedExpression(
    RuleInformation opp, RuleInformation expr, RuleInformation clp) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "(%s)", expr.sval);
  setRuleInformationBase(&info, buffer);
  info.bval = expr.bval;
  info.ival = expr.ival;
  info.fval = expr.fval;
  copySuppl(&info, &expr, 0, expr.supplSize, expr.supplSize);

  freeRuleInformation(expr);
  return info;
}

RuleInformation alpiRuleIdentQualifEmpty() {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  return info;
}

RuleInformation alpiRuleIdentQualifExpression(RuleInformation opp,
    RuleInformation lexpr, RuleInformation clp) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "(%s)", lexpr.sval);
  setRuleInformationBase(&info, buffer);
  info.bval = 2 - (lexpr.bval % 2);
  info.ival = lexpr.ival;
  freeRuleInformation(lexpr);
  return info;
}

void alpiRuleExpressionSumExpressionBeforeTerm(RuleInformation sum,
    RuleInformation opb, RuleInformation ident, RuleInformation in,
    RuleInformation identSet, RuleInformation clb) {
  Symbol *s = malloc(sizeof(Symbol));
  Symbol *ind = NULL;
  Symbol *ind2 = NULL;
  char buffer[255];
  SymbolList * nsl = malloc(sizeof(SymbolList));
  *nsl = createSymbolList();
  if (findSymbol(identSet.sval, &ind)) {
    if (ind->isIndexSet) {
      if (findSymbol(ident.sval, &ind2)) {
        declaredSymbol(ident.sval, ind2->isIndexSet, ind2->isVariable,
            ind2->isCst, ind2->isFunction, ind2->isObj);
      } else {
        *s = createIndexVariableSymbol(ident.sval, ind->index);
        addSymbolToList(nsl, s);
      }
    } else {
      sprintf(buffer, "%s isn't an index !", ident.sval);
      yyerror(buffer);
    }
  } else {
    sprintf(buffer, "%s isn't defined !", ident.sval);
    yyerror(buffer);
  }
  pushSymbolListInStack(&symbolTable, nsl);
}

RuleInformation alpiRuleExpressionSumExpression(RuleInformation sum,
    RuleInformation opb, RuleInformation ident, RuleInformation in,
    RuleInformation identSet, RuleInformation clb,
    RuleInformation opp, RuleInformation expr, RuleInformation clp) {
  RuleInformation info;
  char buffer[255];
  Symbol* s = NULL;
  // TODO create the fresh variable for the accumulator
  Variable *fresh = malloc(sizeof(Variable));
  char *n = freshName();
  *fresh = createVariable(n);
  free(n);
  setRuleInformationBase(&info, fresh->name);
  s = searchInSymbolList(info.symbols, fresh->name);
  if (s == NULL) {
    s = malloc(sizeof(Symbol));
    *s = createLocalVariableSymbol(fresh);
    addSymbolToList(&(info.symbols), s);
  }
  sprintf(buffer, "USER_%s", ident.sval);
  s = searchInSymbolList(info.symbols, buffer);
  if (s == NULL) {
    s = malloc(sizeof(Symbol));
    *s = createIndexVariableSymbol(buffer, NULL);
    addSymbolToList(&(info.symbols), s);
  }
  findSymbol(ident.sval, &s);
  info.suppl = malloc((expr.supplSize + 2) * sizeof(char*));
  info.supplSize = expr.supplSize + 2;
  char* ind = printIndexing(*(s->index));
  copySuppl(&info, &expr, 0, 0, expr.supplSize + 3);
  info.suppl[0] = malloc(
      sizeof(char) * (18 + strlen(ident.sval) + strlen(ind)));
  sprintf(info.suppl[0], "do USER_%s = %s", ident.sval, ind);
  copySuppl(&info, &expr, 1, expr.supplSize + 1, 0);
  info.suppl[expr.supplSize + 1] = malloc(
      sizeof(char)
          * (11 + 2 * strlen(fresh->name) + strlen(expr.sval)));
  sprintf(info.suppl[expr.supplSize + 1], "  %s = %s + (%s)",
      fresh->name, fresh->name, expr.sval);
  info.suppl[expr.supplSize + 2] = malloc(sizeof(char) * 7);
  sprintf(info.suppl[expr.supplSize + 2], "end do");
  info.bval = 2 + expr.bval;
  popSymbolListFromStack(&symbolTable);

  freeRuleInformation(ident);
  freeRuleInformation(identSet);
  freeRuleInformation(expr);
  return info;
}
void alpiRuleExpressionProdExpressionBeforeTerm(RuleInformation prod,
    RuleInformation opb, RuleInformation ident, RuleInformation in,
    RuleInformation identSet, RuleInformation clb) {
  SymbolList *sl = malloc(sizeof(SymbolList));
  *sl = createSymbolList();
  Symbol *s = malloc(sizeof(Symbol));
  Symbol *ind = NULL;
  char buffer[255];
  if (findSymbol(identSet.sval, &ind)) {
    if (ind->isIndexSet) {
      *s = createIndexVariableSymbol(ident.sval, ind->index);
      addSymbolToList(sl, s);
    } else {
      sprintf(buffer, "%s isn't an index !", ident.sval);
      yyerror(buffer);
    }
  } else {
    sprintf(buffer, "%s isn't defined !", ident.sval);
    yyerror(buffer);
  }
  pushSymbolListInStack(&symbolTable, sl);
}
RuleInformation alpiRuleExpressionProdExpression(RuleInformation prod,
    RuleInformation opb, RuleInformation ident, RuleInformation in,
    RuleInformation identSet, RuleInformation clb,
    RuleInformation opp, RuleInformation expr, RuleInformation clp) {
  RuleInformation info;
  char buffer[255];
  Symbol* s = NULL;
  // TODO create the fresh variable for the accumulator
  Variable *fresh = malloc(sizeof(Variable));
  char *n = freshName();
  *fresh = createVariable(n);
  free(n);
  setRuleInformationBase(&info, fresh->name);
  s = searchInSymbolList(info.symbols, fresh->name);
  if (s == NULL) {
    s = malloc(sizeof(Symbol));
    *s = createLocalVariableSymbol(fresh);
    addSymbolToList(&(info.symbols), s);
  }
  s = searchInSymbolList(info.symbols, buffer);
  if (s == NULL) {
    s = malloc(sizeof(Symbol));
    *s = createIndexVariableSymbol(buffer, NULL);
    addSymbolToList(&(info.symbols), s);
  }
  findSymbol(ident.sval, &s);
  info.suppl = malloc((expr.supplSize + 2) * sizeof(char*));
  info.supplSize = expr.supplSize + 2;
  char* ind = printIndexing(*(s->index));
  copySuppl(&info, NULL, 0, 0, expr.supplSize + 3);
  info.suppl[0] = malloc(
      sizeof(char) * (12 + strlen(ident.sval) + strlen(ind)));
  sprintf(info.suppl[0], "do USER_%s = %s", ident.sval, ind);
  copySuppl(&info, &expr, 1, expr.supplSize + 1, 0);
  info.suppl[expr.supplSize + 1] = malloc(
      sizeof(char)
          * (11 + 2 * strlen(fresh->name) + strlen(expr.sval)));
  sprintf(info.suppl[expr.supplSize + 1], "  %s = %s * (%s)",
      fresh->name, fresh->name, expr.sval);
  info.suppl[expr.supplSize + 2] = malloc(sizeof(char) * 7);
  sprintf(info.suppl[expr.supplSize + 2], "end do");
  info.bval = 2 + expr.bval;

  popSymbolListFromStack(&symbolTable);

  freeRuleInformation(ident);
  freeRuleInformation(identSet);
  freeRuleInformation(expr);
  return info;
}

RuleInformation alpiRuleListExpressionSimple(RuleInformation e) {
  RuleInformation info;
  setRuleInformationBase(&info, e.sval);
  info.bval = 2 + e.bval;
  info.ival = 1;

  freeRuleInformation(e);
  return info;
}
RuleInformation alpiRuleListExpressionNext(RuleInformation le,
    RuleInformation comma, RuleInformation e) {
  RuleInformation info;
  char buffer[255];
  sprintf(buffer, "%s, %s", le.sval, e.sval);
  setRuleInformationBase(&info, buffer);
  copySuppl(&info, &le, 0, le.supplSize, le.supplSize + e.supplSize);
  copySuppl(&info, &e, le.supplSize, le.supplSize + e.supplSize, 0);
  info.bval = 2 - ((le.bval % 2) + (e.bval % 2) + 1) / 2;
  info.ival = le.ival + 1;

  freeRuleInformation(le);
  freeRuleInformation(e);
  return info;
}

// Tool functions
void addSymbolToProblem(Symbol* s) {
  if (s->isParam) {
    if (s->isFunction) {
      if (s->isCst) {
        addFunctionToList(&(alpiPb.functionConstants), s->fun);
      } else {
        addFunctionToList(&(alpiPb.auxiliaryFunctions), s->fun);
      }
    } else if (s->isCst) {
      addConstantToList(&(alpiPb.simpleConstants), s->cst);
    }
  } else {
    exit(2);
  }
}
