/*
 * alpirulesparser.c
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpiruleparsertools.h"
#include <stdlib.h>

void alpiRuleParserInit() {
  RuleInformation* info = malloc(sizeof(RuleInformation));
  setRuleInformationBase(info, "");
  pushRuleInformationToStack(&downInfo, info);
  symbolTable = createSymbolListStack();
  SymbolList *root = malloc(sizeof(SymbolList));
  *root = createSymbolList();
  pushSymbolListInStack(&symbolTable, root);
}

void alpiRuleParserClean() {
  SymbolList *root = popSymbolListFromStack(&symbolTable);
  free(root);
}
