/*
 * alpirulesets.h
 *
 *  Created on: Jan 7, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULESETS_H_
#define ALPIRULESETS_H_

#include "alpiruleparsertools.h"

RuleInformation alpiRuleIndexingOptEmpty();
RuleInformation alpiRuleIndexingOptIdentified(RuleInformation index,
    RuleInformation supplIndex, RuleInformation indexBoolExpr);

RuleInformation alpiRuleIndexesFromEmpty();
RuleInformation alpiRuleIndexesFromIndexList(RuleInformation list,
    RuleInformation index);

RuleInformation alpiRuleIndexFromQualified(RuleInformation ident,
    RuleInformation qualif);

RuleInformation alpiRuleIndexInFromIndexSet(RuleInformation indexSet);
RuleInformation alpiRuleIndexInFromEmpty();

RuleInformation alpiRuleIndexSetFromIdent(RuleInformation ident);
RuleInformation alpiRuleIndexSetFromIndexDefinition(
    RuleInformation start, RuleInformation stop, RuleInformation step);

RuleInformation alpiRuleIndexesOptFromEmpty();
RuleInformation alpiRuleIndexesOptFromBoolExpr(RuleInformation e1, RuleInformation test, RuleInformation e2);

#endif /* ALPIRULESETS_H_ */
