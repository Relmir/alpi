/*
 * alpiruleparams.h
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULEPARAMS_H_
#define ALPIRULEPARAMS_H_

#include "alpiruleparsertools.h"

void alpiRuleDefParamBeforeIndexing();
void alpiRuleDefParamHead(RuleInformation ident,
    RuleInformation index);
RuleInformation alpiRuleDefParamFinalize(RuleInformation ident,
    RuleInformation index, RuleInformation attr);

RuleInformation alpiRuleParamAttrOptFromEmpty();
RuleInformation alpiRuleParamAttrOptFromList(RuleInformation list,
    RuleInformation elem);

RuleInformation alpiRuleParamAttrFromDefinition(RuleInformation expr);
#endif /* ALPIRULEPARAMS_H_ */
