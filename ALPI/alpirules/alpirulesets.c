/*
 * alpirulesets.c
 *
 *  Created on: Jan 7, 2014
 *      Author: Emmanuel BIGEON
 */

#include "alpirulesets.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

RuleInformation alpiRuleIndexingOptEmpty() {
  RuleInformation info;
  info.supplSize = 1;
  Indexing* index = malloc(sizeof(Indexing));
  *index = createIndexing("", 1, 1, 1);
  info.suppl = malloc(sizeof(Indexing*));
  info.suppl[0] = index;
  return info;
}

/**
 * <p>
 * The identified indexing opt rule. It retrieves the index in the
 * problem model.
 *
 * @param ident the ident rule info
 */
RuleInformation alpiRuleIndexingOptIdentified(RuleInformation index,
    RuleInformation supplIndex, RuleInformation indexOpt) {
  RuleInformation info;
//  char buffer[255];
  int i;
  setRuleInformationBase(&info, "");
  info.ival = 1 + supplIndex.ival;
//  Indexing* index = searchIndexingInList(alpiPb.indexes, ident.sval);
//  if (index == NULL) {
//    sprintf(buffer, "Undefined indexing set: %s", ident.sval);
//    yyerror(buffer);
//  }
  info.suppl = malloc(sizeof(Indexing*));
  info.supplSize = 1;
  Indexing* tab = malloc(info.ival * sizeof(Indexing));
  tab[0] = ((Indexing*) index.suppl[0])[0];
  for (i = 0; i < supplIndex.ival; ++i) {
    tab[1 + i] = ((Indexing*) supplIndex.suppl[0])[i];
  }
  info.suppl[0] = tab;
  info.bval = 1;

  freeRuleInformation(index);
  freeRuleInformation(supplIndex);
  freeRuleInformation(indexOpt);
  return info;
}

RuleInformation alpiRuleIndexesFromEmpty() {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  return info;
}
RuleInformation alpiRuleIndexesFromIndexList(RuleInformation list,
    RuleInformation index) {
  RuleInformation info;
  int i;
  setRuleInformationBase(&info, "");
  info.supplSize = 1;
  info.ival = 1 + list.ival;
  Indexing* tab = malloc(info.ival * sizeof(Indexing));
  for (i = 0; i < list.ival; ++i) {
    tab[i] = ((Indexing*) list.suppl[0])[i];
  }
  tab[list.ival] = ((Indexing*) index.suppl[0])[0];
  info.suppl = malloc(sizeof(Indexing*));
  info.suppl[0] = tab;
  return info;
}

RuleInformation alpiRuleIndexFromQualified(RuleInformation ident,
    RuleInformation qualif) {
  RuleInformation info;
  char buffer[255];
  setRuleInformationBase(&info, "");
  info.supplSize = 1;
  Indexing* tab = malloc(sizeof(Indexing));
  info.suppl = malloc(sizeof(Indexing*));
  info.suppl[0] = tab;
  Symbol*s;
  if (qualif.bval) {
    Indexing* ind = qualif.suppl[0];
    if (findSymbol(ident.text, &s)) {
      sprintf(buffer, "Already defined %s", ident.text);
      yyerror(buffer);
    } else {
      s = malloc(sizeof(Symbol));
      *s = createIndexVariableSymbol(ident.text, ind);
      addSymbolToList(peekSymbolListFromStack(symbolTable), s);
    }
    tab[0] = *ind;
  } else {
    if (findSymbol(ident.text, &s)) {
      if (s->isIndexSet) {
        tab[0] = *(s->index);
      } else {
        sprintf(buffer, "%s is not an index...", ident.text);
        yyerror(buffer);
      }
    } else {
      sprintf(buffer, "Undefined index %s", ident.text);
      yyerror(buffer);
    }
  }
  return info;
}

RuleInformation alpiRuleIndexInFromIndexSet(RuleInformation indexSet) {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  info.supplSize = 1;
  Indexing* tab = malloc(info.ival * sizeof(Indexing));
  tab[0] = ((Indexing*) indexSet.suppl[0])[0];
  info.suppl = malloc(sizeof(Indexing*));
  info.suppl[0] = tab;
  info.bval = 1;
  return info;
}
RuleInformation alpiRuleIndexInFromEmpty() {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  return info;
}

RuleInformation alpiRuleIndexSetFromIdent(RuleInformation ident) {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  Symbol *s;
  char buffer[255];
  if (findSymbol(ident.text, &s)) {
    if (s->isIndexSet) {
      info.supplSize = 1;
      info.suppl = malloc(sizeof(Indexing*));
      info.suppl[0] = s->index;
    } else {
      sprintf(buffer, "%s isn't an index set", ident.text);
      yyerror(buffer);
    }
  } else {
    sprintf(buffer, "%s isn't defined", ident.text);
    yyerror(buffer);
  }

  return info;
}
RuleInformation alpiRuleIndexSetFromIndexDefinition(
    RuleInformation start, RuleInformation stop, RuleInformation step) {
  RuleInformation info;
  setRuleInformationBase(&info, "");
  Indexing *i = malloc(sizeof(Indexing));
  *i = createIndexing("", start.ival, stop.ival, step.ival);
  addIndexingToList(&(alpiPb.indexes), i);

  info.supplSize = 1;
  info.suppl = malloc(sizeof(Indexing*));
  info.suppl[0] = i;

  freeRuleInformation(start);
  freeRuleInformation(stop);
  freeRuleInformation(step);

  return info;

}
RuleInformation alpiRuleIndexesOptFromEmpty() {
  return newRuleInformation("");
}
RuleInformation alpiRuleIndexesOptFromBoolExpr(RuleInformation e1,
    RuleInformation test, RuleInformation e2) {
  // TODO
  return newRuleInformation("");
}

