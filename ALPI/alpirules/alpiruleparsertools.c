/*
 * alpiruleparsertools.c
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpiruleparsertools.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

RuleInformationStack downInfo;
SymbolListStack symbolTable;
ParamList params;

void resetDownInfo() {
  freeRuleInformationFullStack(downInfo);
  RuleInformation* info = malloc(sizeof(RuleInformation));
  setRuleInformationBase(info, "");
  pushRuleInformationToStack(&downInfo, info);
}

void declaredSymbol(char *name, int isInd, int isVar, int isCst,
    int isFun, int isObj) {
  char buffer[255];
  sprintf(buffer, "Already declared symbol: %s", name);
  if (isInd) {
    strcat(buffer, " (index)");
  } else if (isVar) {
    strcat(buffer, " (variable)");
  } else if (isCst) {
    strcat(buffer, " (constant)");
  } else if (isObj) {
    strcat(buffer, " (objective function)");
  } else if (isFun) {
    strcat(buffer, " (auxiliary function)");
  }
  yyerror(buffer);
}

int findSymbol(char*name, Symbol** sym) {
  *sym = searchInSymbolListStack(symbolTable, name);
  return (*sym) != NULL;
}

int findParam(char*name, Param **s) {
  *s = searchInParamList(params, name);
  return (*s) != NULL;
}

static int freshCount = 0;

char* freshName() {
  char* name = malloc(28 * sizeof(char));
  sprintf(name, "_fresh%d", freshCount++);
  return name;
}

void setReformulation(char* name) {
  if (alpiPb.options.reformulation == NULL) {
    free(alpiPb.options.reformulation);
  }
  alpiPb.options.reformulation = malloc(sizeof(char) * (12));
  if (!strcmp(name, "AFFINE_RB")) {
    strcpy(alpiPb.options.reformulation, "AFFINE_RB");
  } else if (!strcmp(name, "AFFINE2")) {
    strcpy(alpiPb.options.reformulation, "AFFINE2");
  } else if (!strcmp(name, "AFFINE3_RB")) {
    strcpy(alpiPb.options.reformulation, "AFFINE3_RB");
  } else if (!strcmp(name, "rAF2_IA")) {
    strcpy(alpiPb.options.reformulation, "rAF2_IA");
  } else if (!strcmp(name, "rAF3_IA")) {
    strcpy(alpiPb.options.reformulation, "rAF3_IA");
  } else if (!strcmp(name, "AFFINE3")) {
    strcpy(alpiPb.options.reformulation, "AFFINE3");
  } else if (!strcmp(name, "AF3_IA")) {
    strcpy(alpiPb.options.reformulation, "AF3_IA");
  } else if (!strcmp(name, "AFFINE_S")) {
    strcpy(alpiPb.options.reformulation, "AFFINE_S");
  } else if (!strcmp(name, "AFFINE_F")) {
    strcpy(alpiPb.options.reformulation, "AFFINE_F");
  } else if (!strcmp(name, "AF2_IA")) {
    strcpy(alpiPb.options.reformulation, "AF2_IA");
  } else if (!strcmp(name, "AFFINE_Rev")) {
    strcpy(alpiPb.options.reformulation, "AFFINE_Rev");
  } else {
    free(alpiPb.options.reformulation);
    alpiPb.options.reformulation = NULL;
  }
}

void alpiRuleGeneralPopSymbolList() {
  popSymbolListFromStack(&symbolTable);
}
