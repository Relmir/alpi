/*
 * alpirulesoptions.h
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULESOPTIONS_H_
#define ALPIRULESOPTIONS_H_

#include "alpiruleparsertools.h"

RuleInformation alpiRuleOptionsGeneric(RuleInformation option,
    RuleInformation any1, RuleInformation affect,
    RuleInformation any2);

#endif /* ALPIRULESOPTIONS_H_ */
