/*
 * alpiruleparams.c
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#include "alpiruleparams.h"
#include <stdlib.h>

void alpiRuleDefParamBeforeIndexing() {
  SymbolList * nsl = malloc(sizeof(SymbolList));
  *nsl = createSymbolList();
  pushSymbolListInStack(&symbolTable, nsl);
}

void alpiRuleDefParamHead(RuleInformation ident,
    RuleInformation index) {
  Symbol *s = malloc(sizeof(Symbol));
  *s = createParamSymbol(ident.text);
  Param* p = malloc(sizeof(Param));
  *p = createParam(s);
  p->isVector = index.bval;
  p->length = 1;
  if (p->isVector) {
    int i;
    for (i = 0; i < index.supplSize; ++i) {
      p->length *= (((Indexing*) index.suppl[i])->end
          - ((Indexing*) index.suppl[i])->start + 1)
          / (((Indexing*) index.suppl[i])->step);
    }
  }

  SymbolList * nsl = popSymbolListFromStack(&symbolTable);
  addSymbolToList(peekSymbolListFromStack(symbolTable), s);
  pushSymbolListInStack(&symbolTable, nsl);
  addParamToList(&params, p);

  // TODO put the param into the descending information
  RuleInformation *info = malloc(sizeof(RuleInformation));
  *info = newRuleInformation("");
  addSymbolToList(&(info->symbols), s);
  pushRuleInformationToStack(&downInfo, info);
}
RuleInformation alpiRuleDefParamFinalize(RuleInformation ident,
    RuleInformation index, RuleInformation attr) {
  popRuleInformationFromStack(&downInfo);
  alpiRuleGeneralPopSymbolList();
  freeRuleInformation(ident);
  freeRuleInformation(index);
  freeRuleInformation(attr);
  return newRuleInformation("");
}

RuleInformation alpiRuleParamAttrOptFromEmpty() {
  return newRuleInformation("");
}
RuleInformation alpiRuleParamAttrOptFromList(RuleInformation list,
    RuleInformation elem) {
  return newRuleInformation("");
}

RuleInformation alpiRuleParamAttrFromDefinition(RuleInformation expr) {
  int i;
  RuleInformation *info = peekRuleInformationFromStack(downInfo);
  Symbol *s = getSymbolFromList(info->symbols, 0);
  Param* p = searchInParamList(params, s->name);
  if (p->isDefined) {
    yyerror("param can only have one definition");
  }
  p->isDefined = 1;
  for (i = 0; i < p->length; ++i) {
    p->isDefinedCoord[i] = 1;
  }
  if (expr.bval == 0) {
    s = p->symbol;
    s->isCst = 1;
    Constant *cst = malloc(sizeof(Constant));
    *cst = createConstant(s->name, expr.sval);
    s->cst = cst;
  } else if (expr.bval%2 == 0) {
    s = p->symbol;
    s->isCst = 1;
    s->isFunction = 1;
    Function *f = malloc(sizeof(Function));
    *f = createFunctionFromExpr(s->name, expr, createVariableList());
  }
  return newRuleInformation("");
}
