/*
 * alpirulesoptions.c
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpirulesoptions.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

RuleInformation alpiRuleOptionsGeneric(RuleInformation option,
    RuleInformation any1, RuleInformation affect,
    RuleInformation any2) {
  char buffer[255];
  if (!strcmp(any1.sval, "logFreq")) {
    if (atoi(any2.sval) == any2.ival) {
      alpiPb.options.logFreq = any2.ival;
    } else {
      sprintf(buffer, "Unrecognized value (%s) for logFreq option "
          "(it should be an integer)", any2.sval);
      yyerror(buffer);
    }
  } else if (!strcmp(any1.sval, "logFile")) {
    free(alpiPb.options.logFile);
    alpiPb.options.logFile = malloc(
        (1 + strlen(any2.sval)) * sizeof(char));
    strcpy(alpiPb.options.logFile, any2.sval);
  } else if (!strcmp(any1.sval, "reformulation")) {
    setReformulation(any2.sval);
  } else if (!strcmp(any1.sval, "taylor")) {
    alpiPb.options.computeBoundsWithTyalor = !strcmp(any2.sval,
        "true");
  } else if (!strcmp(any1.sval, "propagation")) {
    alpiPb.options.constraintsPropagation = !strcmp(any2.sval,
        "true");
  } else if (!strcmp(any1.sval, "cplex")) {
    alpiPb.options.CPLEX = !strcmp(any2.sval, "true");
  } else if (!strcmp(any1.sval, "timeUnit")) {
    if (!strcmp(any2.sval, "s")) {
      alpiPb.options.timeUnitInSeconds = 1;
    } else if (!strcmp(any2.sval, "m")) {
      alpiPb.options.timeUnitInSeconds = 0;
    } else {
      sprintf(buffer, "Unrecognized value (%s) for timeUnit option "
          "(it should be 's' or 'm')", any2.sval);
      yyerror(buffer);
    }
  } else if (!strcmp(any1.sval, "keepAll")) {
    alpiPb.options.keepAll = !strcmp(any2.sval, "true");
  } else if (!strcmp(any1.sval, "normalStoppingCriterion")) {
    alpiPb.options.normalStoppingCriterion = !strcmp(any2.sval,
        "true");
  } else if (!strcmp(any1.sval, "terminalDisplay")) {
    alpiPb.options.terminalDisplay = !strcmp(any2.sval, "true");
  } else if (!strcmp(any1.sval, "nlpMip")) {
    alpiPb.options.notCertifiedLP = !strcmp(any2.sval, "true");
  } else if (!strcmp(any1.sval, "exactConstraints")) {
    alpiPb.options.exactConstraintChecking = !strcmp(any2.sval,
        "true");
  } else if (!strcmp(any1.sval, "affineConstraints")) {
    alpiPb.options.affineArithmeticForConstraints = !strcmp(any2.sval,
        "true");
  } else if (!strcmp(any1.sval, "timeLimit")) {
    if (atoi(any2.sval) == any2.ival) {
      alpiPb.options.timeLimit = any2.ival;
    } else {
      sprintf(buffer, "Unrecognized value (%s) for timeLimit option "
          "(it should be an integer)", any2.sval);
      yyerror(buffer);
    }
  } else if (!strcmp(any1.sval, "memoryLimit")) {
    if (atoi(any2.sval) == any2.ival) {
      alpiPb.options.memLimit = any2.ival;
    } else {
      sprintf(buffer,
          "Unrecognized value (%s) for memoryLimit option "
              "(it should be an integer)", any2.sval);
      yyerror(buffer);
    }
  } else if (!strcmp(any1.sval, "knownUpperBound")) {
    char* t = formatReal(any2.sval);
    if (atof(t) == any2.fval) {
      free(alpiPb.options.knownMajor);
      alpiPb.options.knownMajor = malloc(
          (1 + strlen(any2.sval)) * sizeof(char));
      strcpy(alpiPb.options.knownMajor, any2.sval);
    }
    free(t);
  } else {

  }
  RuleInformation info;
  setRuleInformationBase(&info, "");
  return info;
}

