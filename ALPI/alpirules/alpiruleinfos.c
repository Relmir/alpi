/*
 * alpiruleinfos.c
 *
 *  Created on: Dec 8, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpiruletools.h"
#include <stdlib.h>
#include <string.h>
#include "../src/alpilangglobal.h"

void freeRuleInformation(struct RuleInformation info) {
  free(info.sval);
  free(info.text);
  if (info.supplSize) {
    int i;
    for (i = 0; i < info.supplSize; ++i) {
      free(info.suppl[i]);
    }
    free(info.suppl);
  }
  deleteSymbolList(&(info.symbols));
}

void concatenateInstrs(void** instrResult, void** instrList1,
    int size1, void** instrList2, int size2) {
  int i;
  for (i = 0; i < size1; i++) {
    instrResult[i] = instrList1[i];
  }
  for (i = 0; i < size2; ++i) {
    instrResult[i + size1] = instrList2[i];
  }
}

int yyerror(char const *s) {
  fprintf(alpiOut, "[ERR ] (line %d) %s\n", yylinenum,s);
  fatal = 1;
  return 1;
}
void setRuleInformationBase(RuleInformation*info, char* text) {
  info->bval = 0;
  info->fval = 0;
  info->ival = 0;
  info->supplSize = 0;
  info->sval = malloc((1 + strlen(text)) * sizeof(char));
  info->text = malloc((1 + strlen(text)) * sizeof(char));
  strcpy(info->sval, text);
  strcpy(info->text, text);
  info->symbols = createSymbolList();
}

RuleInformation newRuleInformation(char* text) {
  RuleInformation info;
  info.bval = 0;
  info.fval = 0;
  info.ival = 0;
  info.supplSize = 0;
  info.sval = malloc((1 + strlen(text)) * sizeof(char));
  info.text = malloc((1 + strlen(text)) * sizeof(char));
  strcpy(info.sval, text);
  strcpy(info.text, text);
  info.symbols = createSymbolList();
  return info;
}
char* formatReal(char* str) {
  char* formatted = malloc((1 + strlen(str)) * sizeof(char));
  strcpy(formatted, str);
  int i = 0;
  while (formatted[i] != '\0') {
    if (formatted[i] == 'd' || formatted[i] == 'D') {
      formatted[i] = 'E';
    }
    i++;
  }
  return formatted;
}

void pushRuleInformationToStack(RuleInformationStack* stack, RuleInformation* info) {
  RuleInformationStack* s = malloc(sizeof(RuleInformationStack));
  s->head = stack->head;
  s->tail = stack->tail;
  stack->tail = s;
  stack->head = info;
}
RuleInformation* popRuleInformationFromStack(RuleInformationStack* stack) {
  if (stack->tail==NULL) {
    return NULL;
  }
  RuleInformation* h = stack->head;
  RuleInformationStack* tail = stack->tail->tail;
  RuleInformation* nh = stack->tail->head;
  free(stack->tail);
  stack->tail = tail;
  stack->head = nh;
  return h;
}
RuleInformation* peekRuleInformationFromStack(RuleInformationStack stack) {
  if (stack.tail==NULL) {
    return NULL;
  }
  return stack.head;
}

void freeRuleInformationFullStack(struct RuleInformationStack stack) {
  if (stack.tail == NULL) {
    return;
  }
  freeRuleInformationFullStack(*(stack.tail));
  free(stack.tail);
  free(stack.head);
}
