/*
 * alpiruletools.h
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULETOOLS_H_
#define ALPIRULETOOLS_H_

#include "../alpistructs/alpisymbols.h"
#include "../alpistructs/alpiparam.h"

typedef struct RuleInformation RuleInformation;
struct RuleInformation {
  // An integer
  int ival;
  // A boolean value
  int bval;
  // A floating point
  float fval;
  // The text of the rule
  char* text;
  // A text value
  char* sval;
  // Supplementary values
  void** suppl;
  // Size of suppl
  int supplSize;
  // Symbols list
  SymbolList symbols;
};

RuleInformation newRuleInformation(char* text);
void setRuleInformationBase(RuleInformation*info, char* text);
char* formatReal(char* str);
void freeRuleInformation(struct RuleInformation);

void concatenateInstrs(void** instrResulting, void** list1,
    int list1Size, void** instrList2, int list2Size);
int yyerror(char const *s);

typedef struct RuleInformationStack RuleInformationStack;

struct RuleInformationStack {
    RuleInformation* head;
    RuleInformationStack* tail;
};

void pushRuleInformationToStack(RuleInformationStack* stack, RuleInformation* info);
RuleInformation* popRuleInformationFromStack(RuleInformationStack* stack);
RuleInformation* peekRuleInformationFromStack(RuleInformationStack stack);
void freeRuleInformationFullStack(struct RuleInformationStack stack);

#endif /* ALPIRULETOOLS_H_ */


