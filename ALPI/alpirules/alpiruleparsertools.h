/*
 * alpiruleparsertools.h
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULEPARSERTOOLS_H_
#define ALPIRULEPARSERTOOLS_H_

#include "alpiruletools.h"

extern RuleInformationStack downInfo;
extern SymbolListStack symbolTable;
extern ProblemModel alpiPb;
extern ParamList params;

void resetDownInfo();
void declaredSymbol(char *name, int isInd, int isVar, int isCst,
    int isFun, int isObj);
int findSymbol(char*name, Symbol **s);
int findParam(char*name, Param **s);
char* freshName();
void setReformulation(char* name);
void alpiRuleGeneralPopSymbolList();

#endif /* ALPIRULEPARSERTOOLS_H_ */
