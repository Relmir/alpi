/*
 * alpirulesexpr.h
 *
 *  Created on: Dec 20, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIRULESEXPR_H_
#define ALPIRULESEXPR_H_

#include "alpiruleparsertools.h"

/*
 * In RuleInformation i returned by Expression, (i.bval == 0) => simple constant,
 * (i.bval%2 == 0) => constant
 */


RuleInformation alpiRuleExpressionBuiltInFunction(RuleInformation fun,
    RuleInformation opp, RuleInformation expression,
    RuleInformation clp);
RuleInformation alpiRuleExpressionQualifiedIdent(
    RuleInformation ident, RuleInformation qualif);
RuleInformation alpiRuleExpressionNatural(RuleInformation nat);
RuleInformation alpiRuleExpressionReal(RuleInformation real);
RuleInformation alpiRuleExpressionAdd(RuleInformation e1,
    RuleInformation sign, RuleInformation e2);
RuleInformation alpiRuleExpressionDiff(RuleInformation e1,
    RuleInformation sign, RuleInformation e2);
RuleInformation alpiRuleExpressionTimes(RuleInformation e1,
    RuleInformation sign, RuleInformation e2);
RuleInformation alpiRuleExpressionDiv(RuleInformation e1,
    RuleInformation sign, RuleInformation e2);
RuleInformation alpiRuleExpressionPow(RuleInformation e1,
    RuleInformation sign, RuleInformation e2);
RuleInformation alpiRuleExpressionNeg(RuleInformation sign,
    RuleInformation expr);
RuleInformation alpiRuleExpressionParenthesedExpression(
    RuleInformation opp, RuleInformation expr, RuleInformation clp);
RuleInformation alpiRuleIdentQualifEmpty();
RuleInformation alpiRuleIdentQualifExpression(RuleInformation opp,
    RuleInformation expr, RuleInformation clp);
void alpiRuleExpressionSumExpressionBeforeTerm(RuleInformation sum,
    RuleInformation opb, RuleInformation ident, RuleInformation in,
    RuleInformation identSet, RuleInformation clb);
RuleInformation alpiRuleExpressionSumExpression(RuleInformation sum,
    RuleInformation opb, RuleInformation ident, RuleInformation in,
    RuleInformation identSet, RuleInformation clb,
    RuleInformation opp, RuleInformation expr, RuleInformation clp);
void alpiRuleExpressionProdExpressionBeforeTerm(RuleInformation prod,
    RuleInformation opb, RuleInformation ident, RuleInformation in,
    RuleInformation identSet, RuleInformation clb);
RuleInformation alpiRuleExpressionProdExpression(RuleInformation prod,
    RuleInformation opb, RuleInformation ident, RuleInformation in,
    RuleInformation identSet, RuleInformation clb,
    RuleInformation opp, RuleInformation expr, RuleInformation clp);

RuleInformation alpiRuleListExpressionSimple(RuleInformation e);
RuleInformation alpiRuleListExpressionNext(RuleInformation le,
    RuleInformation comma, RuleInformation e);


#endif /* ALPIRULESEXPR_H_ */
