/*
 * alpiparamwriter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIPARAMWRITER_H_
#define ALPIPARAMWRITER_H_

#include <stdio.h>
#include "../alpistructs/alpistructures.h"

void compileProblemModelToParameter(ProblemModel model, FILE* mod_out);


#endif /* ALPIPARAMWRITER_H_ */
