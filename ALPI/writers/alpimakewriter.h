/*
 * alpimakewriter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIMAKEWRITER_H_
#define ALPIMAKEWRITER_H_

#include <stdio.h>
#include "../alpistructs/alpistructures.h"

void compileProblemModelToMakeFile(ProblemModel model, FILE*mod_out);

#endif /* ALPIMAKEWRITER_H_ */
