/*
 * alpiparamwriter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include <stdlib.h>
#include "alpiparamwriter.h"
#include "../forgen/forgen.h"

void compileProblemModelToParameter(ProblemModel model, FILE* mod_out) {
  writeModuleHeader(mod_out, "Mod_Parametres", NULL, 0);
  char buffer[255];
  int l = 0;
  VariableList vl = model.decisionVariables;
  struct VariableListCell* it = vl.head;
  while (it != NULL) {
    l += it->var->length;
    it = it->next;
  }
  sprintf(buffer, "%d", l);
  writeModuleConstant(mod_out, "integer", "NB_VARIA_CPLEX", buffer);
  writeModuleEnd(mod_out, "Mod_Parametres");
}
