/*
 * alpimakewriter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpimakewriter.h"
#include <string.h>

void addObjectsForReformulation(FILE*file, char*reformulation);
void addReformulationTarget(FILE*file, char*reformulation);
void addReformulationDependencies(FILE*file, char*reformulation);

void compileProblemModelToMakeFile(ProblemModel model, FILE*mod_out) {
  char buffer[255];
  sprintf(buffer, "PROG      = %s\n", model.name);
  fprintf(mod_out, buffer);
  fprintf(mod_out, "\n");

  fprintf(mod_out, "OPTSF90   = -O3 -xia -m64 -r8const \n");
  fprintf(mod_out, "OPTS      = \n");
  fprintf(mod_out, "L1        = $(CPLEXHOME)/lib/x86-64_sles10_4.1/");
  fprintf(mod_out, "static_pic/libcplex.a  $(LIB_GCC)/libgcc.a\n");
  fprintf(mod_out, "I1        = $(CPLEXHOME)/include/ilcplex\n");
  fprintf(mod_out, "LIBS      = $(L1) -lc -lm -lpthread -lnsl\n");
  fprintf(mod_out, "INCLUDE   = -I$(I1) -I.\n");
  fprintf(mod_out,
      "COMPILE   = $(COMPILATEUR) $(OPTIONS_COMPILATEUR) "
          "$(INCLUDE) -c\n");
  fprintf(mod_out, "\n");
  fprintf(mod_out, "\n");

  fprintf(mod_out, "OBJETS    = cplexcf.o Mod_Interval.o ");
  if (model.options.reformulation != NULL) {
    addObjectsForReformulation(mod_out, model.options.reformulation);
  }
  fprintf(mod_out, "Mod_Gest_Minimier.o Mod_Cplex.o "
      "Mod_Propagation.o Mod_Opt_Glob.o ");
  sprintf(buffer, "Mod_%s.o  %s.o", model.name, model.name);
  fprintf(mod_out, buffer);

  fprintf(mod_out, "\n");
  fprintf(mod_out, "COMP.CC   = c++\n");
  fprintf(mod_out, "COMP.cc   = cc\n");
  fprintf(mod_out, "COMP.F77  = f77\n");
  fprintf(mod_out, "COMP.F90  = f90\n");
  fprintf(mod_out, "\n");
  fprintf(mod_out, ".SUFFIXES: .o .C .c .f90\n");
  fprintf(mod_out, "\n");
  fprintf(mod_out, "$(PROG): $(OBJETS)\n");
  fprintf(mod_out,
      "\t$(COMP.F90) -o $(PROG) $(OBJETS) $(LIBS) $(OPTSF90)\n");
  fprintf(mod_out, "\n");
  fprintf(mod_out, "cplexcf.o: cplexcf.c\n");
  fprintf(mod_out, "\t$(COMP.cc) $(OPTS) $(INCLUDE) -c $<\n");
  fprintf(mod_out, "\n");

  if (model.options.reformulation != NULL) {
    addReformulationTarget(mod_out, model.options.reformulation);
    fprintf(mod_out, "\n");
  }

  sprintf(buffer, "Mod_%s.o: Mod_%s.f90 ", model.name, model.name);
  fprintf(mod_out, buffer);
  if (model.options.reformulation != NULL) {
    addReformulationDependencies(mod_out,
        model.options.reformulation);
  }
  fprintf(mod_out,
      "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90\n");
  fprintf(mod_out, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
  fprintf(mod_out, "\n");
  fprintf(mod_out, "Mod_Cplex.o: Mod_Cplex.f90 Mod_Interval.o\n");
  fprintf(mod_out, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
  fprintf(mod_out, "\n");
  fprintf(mod_out, "Mod_Opt_Glob.o: Mod_Opt_Glob.f90 "
      "Mod_Gest_Minimier.o Mod_Interval.o\n");
  fprintf(mod_out, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
  fprintf(mod_out, "\n");
  sprintf(buffer, "%s.o: %s.f90 Mod_%s.f90 Mod_%s.o\n", model.name,
      model.name, model.name, model.name);
  fprintf(mod_out, buffer);
  fprintf(mod_out, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
  fprintf(mod_out, "\n");
  fprintf(mod_out, "%%.o: %%.f90\n");
  fprintf(mod_out, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
  fprintf(mod_out, "\n");
  fprintf(mod_out, "clean:\n");
  fprintf(mod_out, "\trm *.mod *.o\n");
}

void addObjectsForReformulation(FILE*mod_out, char*reformulation) {
  if (!strcmp(reformulation, "AFFINE_RB")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine_RB.o ");
  } else if (!strcmp(reformulation, "AFFINE2")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine2.o ");
  } else if (!strcmp(reformulation, "AFFINE3_RB")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine3_RB.o ");
  } else if (!strcmp(reformulation, "rAF2_IA")) {
    fprintf(mod_out,
        "Mod_Parametres.o Mod_Affine2_RB.o Mod_rAF2_IA.o ");
  } else if (!strcmp(reformulation, "rAF3_IA")) {
    fprintf(mod_out,
        "Mod_Parametres.o Mod_Affine3_RB.o Mod_rAF3_IA.o ");
  } else if (!strcmp(reformulation, "AFFINE3")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine3.o ");
  } else if (!strcmp(reformulation, "AF3_IA")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine3.o Mod_AF3_IA.o ");
  } else if (!strcmp(reformulation, "AFFINE_S")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine_S.o ");
  } else if (!strcmp(reformulation, "AFFINE_F")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine_F.o ");
  } else if (!strcmp(reformulation, "AF2_IA")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine2.o Mod_AF2_IA.o ");
  } else if (!strcmp(reformulation, "AFFINE_Rev")) {
    fprintf(mod_out, "Mod_Parametres.o Mod_Affine_Rev.o ");
  }
}

void addReformulationTarget(FILE*file, char*reformulation) {
  int exist = 0;
  if (!strcmp(reformulation, "AFFINE_RB")) {
    fprintf(file, "Mod_Affine_RB.o: Mod_Affine_RB.f90 Mod_Interval.o"
        " Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "AFFINE2")) {
    fprintf(file, "Mod_Affine2.o: Mod_Affine2.f90 Mod_Interval.o "
        "Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "AFFINE3_RB")) {
    fprintf(file, "Mod_Affine3_RB.o: Mod_Affine3_RB.f90 "
        "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "rAF2_IA")) {
    // Non-common
    fprintf(file, "Mod_Affine2_RB.o: Mod_Affine2_RB.f90 "
        "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90\n");
    fprintf(file, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
    fprintf(file, "\n");
    fprintf(file, "Mod_rAF2_IA.o: Mod_rAF2_IA.f90 Mod_Interval.o "
        "Mod_Affine2_RB.o Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "rAF3_IA")) {
    // Non-common
    fprintf(file,
        "Mod_Affine3_RB.o: Mod_Affine3_RB.f90 Mod_Interval.o "
            "Mod_Parametres.o Mod_Parametres.f90\n");
    fprintf(file, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
    fprintf(file, "\n");
    fprintf(file, "Mod_rAF3_IA.o: Mod_rAF3_IA.f90 Mod_Affine3_RB.o "
        "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "AFFINE3")) {
    fprintf(file, "Mod_Affine3.o: Mod_Affine3.f90 Mod_Interval.o "
        "Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "AF3_IA")) {
    fprintf(file, "Mod_Affine3.o: Mod_Affine3.f90 Mod_Interval.o "
        "Mod_Parametres.o Mod_Parametres.f90\n");
    fprintf(file, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
    fprintf(file, "\n");
    fprintf(file, "Mod_AF3_IA.o: Mod_AF3_IA.f90  Mod_Affine3.o "
        "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "AFFINE_S")) {
    fprintf(file, "Mod_Affine_S.o: Mod_Affine_S.f90 Mod_Interval.o "
        "Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "AFFINE_F")) {
    fprintf(file, "Mod_Affine_F.o: Mod_Affine_F.f90 Mod_Interval.o "
        "Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "AF2_IA")) {
    fprintf(file, "Mod_Affine2.o: Mod_Affine2.f90 Mod_Interval.o "
        "Mod_Parametres.o Mod_Parametres.f90\n");
    fprintf(file, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
    fprintf(file, "\n");
    fprintf(file, "Mod_AF2_IA.o: Mod_AF2_IA.f90 Mod_Interval.o "
        "Mod_Affine2.o Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  } else if (!strcmp(reformulation, "AFFINE_Rev")) {
    fprintf(file,
        "Mod_Affine_Rev.o: Mod_Affine_Rev.f90 Mod_Interval.o "
            "Mod_Parametres.o Mod_Parametres.f90\n");
    exist++;
  }

  if (exist) {
    fprintf(file, "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<\n");
  }

}
void addReformulationDependencies(FILE*file, char*reformulation) {
  if (!strcmp(reformulation, "AFFINE_RB")) {
    fprintf(file, "Mod_Affine_RB.o Mod_Affine_RB.f90 ");
  } else if (!strcmp(reformulation, "AFFINE2")) {
    fprintf(file, "Mod_Affine2.o Mod_Affine2.f90 ");
  } else if (!strcmp(reformulation, "AFFINE3_RB")) {
    fprintf(file, "Mod_Affine3_RB.o Mod_Affine3_RB.f90 ");
  } else if (!strcmp(reformulation, "rAF2_IA")) {
    fprintf(file, "Mod_rAF2_IA.o Mod_Affine2_RB.o ");
  } else if (!strcmp(reformulation, "rAF3_IA")) {
    fprintf(file, "Mod_Affine3_RB.o Mod_rAF3_IA.o");
  } else if (!strcmp(reformulation, "AFFINE3")) {
    fprintf(file, "Mod_Affine3.o Mod_Affine3.f90 ");
  } else if (!strcmp(reformulation, "AF3_IA")) {
    fprintf(file, "Mod_AF3_IA.o Mod_Affine3.o ");
  } else if (!strcmp(reformulation, "AFFINE_S")) {
    fprintf(file, "Mod_Affine_S.o Mod_Affine_S.f90 ");
  } else if (!strcmp(reformulation, "AFFINE_F")) {
    fprintf(file, "Mod_Affine_F.o Mod_Affine_F.f90 ");
  } else if (!strcmp(reformulation, "AF2_IA")) {
    fprintf(file, "Mod_AF2_IA.o Mod_Affine2.o ");
  } else if (!strcmp(reformulation, "AFFINE_Rev")) {
    fprintf(file, "Mod_Affine_Rev.o Mod_Affine_Rev.f90 ");
  }
}

