/*
 * alpimodwriter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIMODWRITER_H_
#define ALPIMODWRITER_H_

#include <stdio.h>
#include "../alpistructs/alpiproblem.h"

void compileProblemModelToMod(ProblemModel model, FILE* mod_out);
void writeProblemModuleHeader(FILE*mod, ProblemModel model);

void writeProblemSimpleConstantsToMod(ProblemModel model,
    FILE*mod_out);
void writeProblemInterfacesToMod(ProblemModel model, FILE*mod_out);
void writeProblemSingleInterfaceToMod(ProblemModel model, FILE*mod_out, Function f);
void writeProblemConstantFunctions(ProblemModel model, FILE*mod_out);
void writeProblemAuxiliaryFunctions(ProblemModel model, FILE*mod_out);
void writeProblemFunctionToMod(FILE*mod_out, Function f, char* name,
    char*resultName, char* baseType);
void writeProblemObjectiveFunction(ProblemModel model, FILE* mod_out);
void writeProblemConstraintsToMod(ProblemModel model, FILE*mod_out);
void writeBoundComputationToMod(ProblemModel model, FILE*mod_out);
void writeConstraintPropagationToMod(ProblemModel model, FILE*mod_out);

#endif /* ALPIMODWRITER_H_ */
