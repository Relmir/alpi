/*
 * alpimodwriter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include <stdlib.h>
#include <string.h>
#include "alpimodwriter.h"
#include "../forgen/forgen.h"

char* splitParameterInVariables(VariableList vl);

void compileProblemModelToMod(ProblemModel model, FILE* mod_out) {
  char buffer[255];

  // Modules
  writeProblemModuleHeader(mod_out, model);

  // Fortran Constants
  writeProblemSimpleConstantsToMod(model, mod_out);
  fprintf(mod_out, "\n");

  // Fortran Interfaces
  writeProblemInterfacesToMod(model, mod_out);

  writeModuleStartingDefinitions(mod_out);

  // Constant functions
  writeProblemConstantFunctions(model, mod_out);

  // Auxiliary Functions
  writeProblemAuxiliaryFunctions(model, mod_out);

  // Objective Function
  writeProblemObjectiveFunction(model, mod_out);

  fprintf(mod_out, "\n");
  writeProblemConstraintsToMod(model, mod_out);

  fprintf(mod_out, "\n");
  writeBoundComputationToMod(model, mod_out);

  fprintf(mod_out, "\n");
  writeConstraintPropagationToMod(model, mod_out);

  sprintf(buffer, "Mod_%s", model.name);
  writeModuleEnd(mod_out, buffer);
}

void writeProblemModuleHeader(FILE*mod, ProblemModel model) {
  char buffer[255];
  sprintf(buffer, "Mod_%s", model.name);
  int nbMods = 2;
  if (model.options.reformulation != NULL) {
    // Uses reformulation
    nbMods++;
  }
  char** used = malloc(nbMods * sizeof(char*));
  used[0] = "MOD_INTERVAL";
  if (model.options.reformulation != NULL) {
    used[1] = malloc(
        (5 + strlen(model.options.reformulation)) * sizeof(char));
    sprintf(used[1], "MOD_%s", model.options.reformulation);
  }
  used[nbMods - 1] = "MOD_PROPAGATION";
  writeModuleHeader(mod, buffer, used, nbMods);
}

void writeProblemSimpleConstantsToMod(ProblemModel model,
    FILE*mod_out) {
  struct ConstantCell* it;
  int i;
  char buffer[255];
  int nbConstants = model.simpleConstants.length;
  if (nbConstants) {
    fprintf(mod_out, "      !==============================\n");
    fprintf(mod_out, "      ! Constants\n");
    fprintf(mod_out, "      !--------------------\n");
    it = model.simpleConstants.head;
    for (i = 0; i < nbConstants; ++i) {
      sprintf(buffer, "USER_%s", it->val->name.name);
      writeModuleConstant(mod_out, "INTERVAL(8)", buffer,
          it->val->value);
      it = it->next;
    }
  }
}

void writeProblemInterfacesToMod(ProblemModel model, FILE*mod_out) {
  fprintf(mod_out, "      !==============================\n");
  fprintf(mod_out, "      ! Interfaces\n");
  fprintf(mod_out, "      !--------------------\n");
  int i;
  struct FunctionCell* it;
  // Auxiliary Functions
  it = model.auxiliaryFunctions.head;
  for (i = 0; i < model.auxiliaryFunctions.length; ++i) {
    writeProblemSingleInterfaceToMod(model, mod_out, *(it->val));
    fprintf(mod_out, "\n");
    it = it->next;
  }
  it = NULL;

  // Objective
  writeProblemSingleInterfaceToMod(model, mod_out, model.objective);
}

void writeProblemSingleInterfaceToMod(ProblemModel model,
    FILE*mod_out, Function f) {
  char interface1[255];
  char interface2[255];
  char interface3[255];
  char* interfaced[3];
  int nbInterfaced = 2;
  sprintf(interface1, "USER_%s", f.ret.name);
  interfaced[0] = interface1;
  sprintf(interface2, "USER_%s_P", f.ret.name);
  interfaced[1] = interface2;
  if (model.options.reformulation != NULL) {
    nbInterfaced++;
    sprintf(interface3, "USER_%s_%s", f.ret.name,
        model.options.reformulation);
    interfaced[2] = interface3;
  }
  writeModuleInterface(mod_out, interfaced[0], interfaced,
      nbInterfaced);
}

void writeProblemConstantFunctions(ProblemModel model, FILE*mod_out) {
  int nbCstFuns = model.functionConstants.length;
  int i;
  char buffer[255];
  if (nbCstFuns) {
    fprintf(mod_out, "      !==============================\n");
    fprintf(mod_out, "      ! Constant Functions\n");
    fprintf(mod_out, "      !--------------------\n");
    struct FunctionCell* it = model.functionConstants.head;
    for (i = 0; i < nbCstFuns; ++i) {
      sprintf(buffer, "USER_%s", it->val->ret.name);
      writeProblemFunctionToMod(mod_out, *(it->val), buffer, NULL,
          "INTERVAL(8)");
      fprintf(mod_out, "\n");
      it = it->next;
    }
  }
}

void writeProblemAuxiliaryFunctions(ProblemModel model, FILE*mod_out) {
  int auxFunLength = model.auxiliaryFunctions.length;
  int i;
  char name[255], buffer[255];
  char buffer2[255];
  struct FunctionCell*it;
  if (auxFunLength) {
    fprintf(mod_out, "      !==============================\n");
    fprintf(mod_out, "      ! Auxiliary Functions\n");
    fprintf(mod_out, "      !--------------------\n");

    it = model.auxiliaryFunctions.head;
    for (i = 0; i < auxFunLength; ++i) {
      sprintf(name, "USER_%s", it->val->ret.name);
      writeProblemFunctionToMod(mod_out, *(it->val), name, NULL,
          "INTERVAL(8)");
      fprintf(mod_out, "\n");
      sprintf(buffer, "%s_P", name);
      writeProblemFunctionToMod(mod_out, *(it->val), buffer, name,
          "type(PNODE)");
      fprintf(mod_out, "\n");
      if (model.options.reformulation != NULL) {
        sprintf(buffer2, "type(T%s)", model.options.reformulation);
        sprintf(buffer, "%s_%s", name, model.options.reformulation);
        writeProblemFunctionToMod(mod_out, *(it->val), buffer, name,
            buffer2);
        fprintf(mod_out, "\n");
      }
      it = it->next;
    }
  }
}

char* declareVariable(Variable v, char* basetype);

void writeProblemFunctionToMod(FILE*mod_out, Function f, char* name,
    char*resultName, char* baseType) {
  int i, j;
  char buffer[255];
  char* t;
  VariableList vl = f.params;
  int vNumber = vl.length;
  struct VariableListCell* it = vl.head;
  char** params = malloc(vNumber * sizeof(char*));
  char** intents = malloc(vNumber * sizeof(char*));
  char** pTypes = malloc((vNumber + 1) * sizeof(char*));
  pTypes[0] = malloc(
      (1 + strlen(baseType) + 14 * f.ret.isVector) * sizeof(char));
  strcpy(pTypes[0], baseType);
  if (f.ret.isVector) {
    strcat(pTypes[0], ", dimension(:)");
  }
  for (i = 0; i < vNumber; ++i) {
    Variable* v = it->var;
    intents[i] = "in";
    params[i] = malloc((6 + strlen(v->name)));
    sprintf(params[i], "USER_%s", v->name);
    pTypes[i + 1] = malloc(
        (strlen(baseType) + 1 + 14 * v->isVector) * sizeof(char));
    strcpy(pTypes[i + 1], baseType);
    if (v->isVector) {
      sprintf(buffer, ", dimension(%d)", v->length);
      strcat(pTypes[i + 1], buffer);
    }
    it = it->next;
  }
  it = f.symbols.head;

  writeModuleFunctionHeader(mod_out, name, params, vNumber,
      resultName, pTypes, intents);
  // freeing types &params
  for (i = 0; i < vNumber; ++i) {
    free(params[i]);
    free(pTypes[i + 1]);
  }
  free(params);
  free(intents);
  free(pTypes);

  while (it != NULL) {
    t = declareVariable(*(it->var), baseType);
    writeModuleFunctionInstruction(mod_out, t);
    free(t);
    it = it->next;
  }

  for (j = 0; j < f.bodySize; ++j) {
    writeModuleFunctionInstruction(mod_out, f.bodyInstructions[j]);
  }

  writeModuleFunctionEnd(mod_out, name);
}

char* declareVariable(Variable v, char*basetype) {
  char* buffer;
  char buffer2[255];
  int i;
  int isInt = v.isInteger[0];
  for (i = 0; i < v.length; ++i) {
    if (!v.isInteger[i] != !isInt) {
      // error
    }
  }
  if (v.isVector) {
    sprintf(buffer2, ", dimension(%d):: %s", v.length, v.name);
  } else {
    sprintf(buffer2, ":: %s", v.name);
  }
  if (isInt) {
    buffer = malloc((8 + strlen(buffer2)) * sizeof(char));
    sprintf(buffer, "integer%s", buffer2);
  } else {
    buffer = malloc(
        (1 + strlen(basetype) + strlen(buffer2)) * sizeof(char));
    sprintf(buffer, "%s%s", basetype, buffer2);
  }
  return buffer;
}

void writeProblemObjectiveFunction(ProblemModel model, FILE* mod_out) {
  char name[255];
  char buffer[255];
  char* bufferPointer;
  char buffer2[255];
  char* vars[4];
  char *intents[4];
  char* types[4];

  fprintf(mod_out, "      !==============================\n");
  fprintf(mod_out, "      ! Objective Function\n");
  fprintf(mod_out, "      !--------------------\n");

  Function f = model.objective;
  sprintf(name, "USER_%s", f.ret.name);
  writeProblemFunctionToMod(mod_out, f, name, NULL, "INTERVAL(8)");
  fprintf(mod_out, "\n");
  sprintf(buffer, "%s_P", name);
  writeProblemFunctionToMod(mod_out, f, buffer, name, "type(PNODE)");
  fprintf(mod_out, "\n");
  if (model.options.reformulation != NULL) {
    char buffer2[255];
    sprintf(buffer2, "type(T%s)", model.options.reformulation);
    sprintf(buffer, "%s_%s", name, model.options.reformulation);
    writeProblemFunctionToMod(mod_out, f, buffer, name, buffer2);
    fprintf(mod_out, "\n");
  }

  // Objective function Dispatch
  types[0] = "INTERVAL(8)";
  types[1] = "INTERVAL(8), dimension(:)";

  bufferPointer = splitParameterInVariables(f.params);
  strcpy(buffer, bufferPointer);
  free(bufferPointer);

  sprintf(buffer2, "%s = %s(%s)", f.ret.name, name, buffer);

  char* var = "X";
  char* in = "in";
  // For obscure reasons (in my point of view), &n != &name

  writeModuleFunctionHeader(mod_out, f.ret.name, &(var), 1, NULL,
      types, &(in));
  writeModuleFunctionInstruction(mod_out, buffer2);
  writeModuleFunctionEnd(mod_out, f.ret.name);
  fprintf(mod_out, "\n");

  // Linearization
  vars[0] = "XI";
  vars[1] = "VAL";
  vars[2] = "REF";
  vars[3] = "ERR";
  types[0] = "INTERVAL(8), dimension(:)";
  types[1] = "logical";
  types[2] = "double precision, dimension(SIZE(XI))";
  types[3] = "INTERVAL(8)";
  intents[0] = "in";
  intents[1] = "out";
  intents[2] = "out";
  intents[3] = "out";

  if (model.options.reformulation) {
    sprintf(name, "%s_%s", f.ret.name, model.options.reformulation);
  } else {
    sprintf(name, "%s_RIEN", f.ret.name);
  }
  writeModuleSubroutineHeader(mod_out, name, vars, 4, types, intents);
  VariableList vl = f.params;
  if (model.options.reformulation == NULL) {
    writeModuleFunctionInstruction(mod_out, "REF = 0");
    writeModuleFunctionInstruction(mod_out, "VAL = .false.");
    writeModuleFunctionInstruction(mod_out, "ERR = [0]");
  } else {
    bufferPointer = splitParameterInVariables(vl);
    strcpy(buffer, bufferPointer);
    free(bufferPointer);

    sprintf(buffer2, "type(T%s), dimension(size(XI)):: X",
        model.options.reformulation);
    writeModuleFunctionInstruction(mod_out, buffer2);
    sprintf(buffer2, "type(T%s):: %s", model.options.reformulation,
        f.ret.name);
    writeModuleFunctionInstruction(mod_out, buffer2);
    writeModuleFunctionInstruction(mod_out, "call INIT_VARIA(X, XI)");
    sprintf(buffer2, "%s = USER_%s(%s)", f.ret.name, f.ret.name,
        buffer);
    writeModuleFunctionInstruction(mod_out, buffer2);
    sprintf(buffer2, "call LINEARIZATION(%s, REF, VAL, ERR)",
        f.ret.name);
    writeModuleFunctionInstruction(mod_out, buffer2);
  }

  writeModuleSubroutineEnd(mod_out, name);
}

void writeProblemConstraintsToMod(ProblemModel model, FILE*mod_out) {
  struct ConstraintCell *itCstr = model.constraints.head;
  Constraint*c;
  int nbCstrs = 0;
  while (itCstr != NULL) {
    c = itCstr->val;
    nbCstrs += (1 - c->type) + c->lowerBounded + c->upperBounded;
    itCstr = itCstr->next;
  }
  struct VariableListCell*itVar;
  int i, l;
  char buffer[255];
  char buffer2[255];

  fprintf(mod_out, "      !==============================\n");
  fprintf(mod_out, "      ! Constraints\n");
  fprintf(mod_out, "      !--------------------\n");
  char* params[2];
  params[0] = "i";
  params[1] = "X";
  char* types[3];
  types[0] = "INTERVAL(8)";
  types[1] = "integer";
  types[2] = "INTERVAL(8), dimension(:)";
  char* intents[2];
  intents[0] = "in";
  intents[1] = "in";

  itVar = model.decisionVariables.head;
  l = 0;
  strcpy(buffer, "(");
  if (model.decisionVariables.length) {
    sprintf(buffer2, "X(%d:%d)", l + 1, l + itVar->var->length);
    strcat(buffer, buffer2);
    l += itVar->var->length;
    itVar = itVar->next;
    for (i = 1; i < model.decisionVariables.length; ++i) {
      sprintf(buffer2, ",X(%d:%d)", l + 1, l + itVar->var->length);
      strcat(buffer, buffer2);
      l += itVar->var->length;
      itVar = itVar->next;
    }
  }
  strcat(buffer, ")");

  writeModuleFunctionHeader(mod_out, "CONTRAINTE_I", params, 2,
      "CONTRAINTE", types, intents);
  writeModuleFunctionInstruction(mod_out, "select case(i)");

  l = 1;
  i = 1;
  itCstr = model.constraints.head;
  while (itCstr != NULL) {
    c = itCstr->val;
    if (1 - c->type) {
      sprintf(buffer2, "case(%d)", l++);
      writeModuleFunctionInstruction(mod_out, buffer2);
      sprintf(buffer2, "  CONTRAINTE = (USER_%s%s-%s)",
          c->f->ret.name, buffer, c->boundL);
      writeModuleFunctionInstruction(mod_out, buffer2);
    }
    itCstr = itCstr->next;
  }
  fprintf(mod_out, "\n");
  itCstr = model.constraints.head;
  while (itCstr != NULL) {
    c = itCstr->val;
    if (c->type) {
      if (c->lowerBounded) {
        sprintf(buffer2, "case(%d)", l++);
        writeModuleFunctionInstruction(mod_out, buffer2);
        sprintf(buffer2, "  CONTRAINTE = (%s-USER_%s%s)", c->boundL,
            c->f->ret.name, buffer);
        writeModuleFunctionInstruction(mod_out, buffer2);
      }
      if (c->upperBounded) {
        sprintf(buffer2, "case(%d)", l++);
        writeModuleFunctionInstruction(mod_out, buffer2);
        sprintf(buffer2, "  CONTRAINTE = (USER_%s%s-%s)",
            c->f->ret.name, buffer, c->boundU);
        writeModuleFunctionInstruction(mod_out, buffer2);
      }
    }
    itCstr = itCstr->next;
  }

  writeModuleFunctionInstruction(mod_out, "case default");
  writeModuleFunctionInstruction(mod_out,
      "  CONTRAINTE = INTERVAL(moinsinfini, plusinfini)");
  writeModuleFunctionInstruction(mod_out, "end select");

  writeModuleFunctionEnd(mod_out, "CONTRAINTE_I");

  if (model.options.reformulation != NULL) {
    fprintf(mod_out, "\n");

    params[1] = "XI";
    types[0] = malloc(
        (8 + strlen(model.options.reformulation)) * sizeof(char));
    sprintf(types[0], "type(T%s)", model.options.reformulation);

    sprintf(buffer2, "CONTRAINTE_%s", model.options.reformulation);
    writeModuleFunctionHeader(mod_out, buffer2, params, 2,
        "CONTRAINTE", types, intents);

    sprintf(buffer2, "type(T%s), dimension(size(XI)):: X",
        model.options.reformulation);
    writeModuleFunctionInstruction(mod_out, buffer2);
    writeModuleFunctionInstruction(mod_out, "call INIT_VARIA(X, XI)");
    writeModuleFunctionInstruction(mod_out, "select case(i)");

    l = 1;
    i = 3;
    itCstr = model.constraints.head;
    while (itCstr != NULL) {
      c = itCstr->val;
      if (1 - c->type) {
        sprintf(buffer2, "case(%d)", l++);
        writeModuleFunctionInstruction(mod_out, buffer2);
        sprintf(buffer2, "  CONTRAINTE = (USER_%s%s-%s)",
            c->f->ret.name, buffer, c->boundL);
        writeModuleFunctionInstruction(mod_out, buffer2);
      }
      itCstr = itCstr->next;
    }
    fprintf(mod_out, "\n");
    itCstr = model.constraints.head;
    while (itCstr != NULL) {
      c = itCstr->val;
      if (c->type) {
        if (c->lowerBounded) {
          sprintf(buffer2, "case(%d)", l++);
          writeModuleFunctionInstruction(mod_out, buffer2);
          sprintf(buffer2, "  CONTRAINTE = (%s-USER_%s%s)", c->boundL,
              c->f->ret.name, buffer);
          writeModuleFunctionInstruction(mod_out, buffer2);
        }
        if (c->upperBounded) {
          sprintf(buffer2, "case(%d)", l++);
          writeModuleFunctionInstruction(mod_out, buffer2);
          sprintf(buffer2, "  CONTRAINTE = (USER_%s%s-%s)",
              c->f->ret.name, buffer, c->boundU);
          writeModuleFunctionInstruction(mod_out, buffer2);
        }
      }
      itCstr = itCstr->next;
    }

    writeModuleFunctionInstruction(mod_out, "case default");
    writeModuleFunctionInstruction(mod_out,
        "  call A_INFINI(CONTRAINTE)");
    writeModuleFunctionInstruction(mod_out, "end select");

    sprintf(buffer2, "CONTRAINTE_%s", model.options.reformulation);
    writeModuleFunctionEnd(mod_out, buffer2);
  }
  fprintf(mod_out, "\n");

  params[1] = "X";
  types[0] = "INTERVAL(8)";

  writeModuleFunctionHeader(mod_out, "CONTRAINTE", params, 2, NULL,
      types, intents);

  if (model.options.reformulation != NULL
      && model.options.affineArithmeticForConstraints) {
    writeModuleFunctionInstruction(mod_out, "INTERVAL(8):: TMP");
    sprintf(buffer2, "TMP = CONTRAINTE_%s(i, X)",
        model.options.reformulation);
    writeModuleFunctionInstruction(mod_out,
        "CONTRAINTE = (TMP .IX.(CONTRAINTE_I(i, X)))");
  } else {
    writeModuleFunctionInstruction(mod_out,
        "CONTRAINTE = (CONTRAINTE_I(i, X))");
  }

  writeModuleFunctionEnd(mod_out, "CONTRAINTE");

  fprintf(mod_out, "\n");
  char* p[5];
  p[0] = "i";
  p[1] = "X";
  p[2] = "VAL";
  p[3] = "REF";
  p[4] = "ERR";
  char* tps[5];
  tps[0] = "integer";
  tps[1] = "INTERVAL(8), dimension(:)";
  tps[2] = "logical";
  tps[3] = "double precision, dimension(size(X))";
  tps[4] = "INTERVAL(8)";
  char* ins[5];
  ins[0] = ins[1] = "in";
  ins[2] = ins[3] = ins[4] = "out";

  writeModuleSubroutineHeader(mod_out, "CONTRAINTE_RELAX", p, 5, tps,
      ins);
  if (model.options.reformulation == NULL) {
    writeModuleFunctionInstruction(mod_out, "REF = 0");
    writeModuleFunctionInstruction(mod_out, "VAL = .false.");
    writeModuleFunctionInstruction(mod_out, "ERR = [0]");
  } else {
    sprintf(buffer, "type(T%s):: CONTRAINTE",
        model.options.reformulation);
    writeModuleFunctionInstruction(mod_out, buffer);
    sprintf(buffer, "CONTRAINTE = CONTRAINTE_%s(i, X)",
        model.options.reformulation);
    writeModuleFunctionInstruction(mod_out, buffer);
    writeModuleFunctionInstruction(mod_out,
        "call LINEARIZATION(CONTRAINTE, REF, VAL, ERR)");
  }
  writeModuleSubroutineEnd(mod_out, "CONTRAINTE_RELAX");
}

void writeBoundComputationToMod(ProblemModel model, FILE*mod) {
  char* params[1];
  char* types[2];
  char* intents[1];
  char name[255];
  char buffer[255];
  char buffer2[255];
  char* bufferP;

  fprintf(mod, "      !==============================\n");
  fprintf(mod, "      ! Bound computations\n");
  fprintf(mod, "      !--------------------\n");
  params[0] = "YV";
  types[0] = "INTERVAL(8)";
  types[1] = "INTERVAL(8), dimension(:)";
  intents[0] = "in";
  writeModuleFunctionHeader(mod, "T_RIEN", params, 1,
  NULL, types, intents);
  writeModuleFunctionInstruction(mod,
      "T_RIEN = INTERVAL(moinsinfini, plusinfini)");
  writeModuleFunctionEnd(mod, "T_RIEN");

  if (model.options.reformulation != NULL) {

    bufferP = splitParameterInVariables(model.decisionVariables);
    sprintf(buffer, "(%s)", bufferP);
    free(bufferP);

    sprintf(name, "T_%s", model.options.reformulation);
    types[0] = "INTERVAL(8)";
    types[1] = "interval, dimension(:)";

    params[0] = "XI";
    writeModuleFunctionHeader(mod, name, params, 1,
        model.objective.ret.name, types, intents);
    writeModuleFunctionInstruction(mod, "integer:: i");
    sprintf(buffer2, "type(T%s), dimension(size(XI)):: X",
        model.options.reformulation);
    writeModuleFunctionInstruction(mod, buffer2);
    writeModuleFunctionInstruction(mod, "call INIT_VARIA(X, XI)");
    sprintf(buffer2, "%s = USER_%s%s", model.objective.ret.name,
        model.objective.ret.name, buffer);
    writeModuleFunctionInstruction(mod, buffer2);
    writeModuleFunctionEnd(mod, name);
  }
}

void writeConstraintPropagationToMod(ProblemModel model, FILE*mod) {
  char* params[3];
  char* types[3];
  char* intents[3];
  char buffer[255];
  char buffer2[255];
  int nbVar = 0;
  int nbCstr = 0;
  int j, l;
  struct VariableListCell* itVar = model.decisionVariables.head;
  char*bufferP = splitParameterInVariables(model.decisionVariables);
  sprintf(buffer, "(%s)", bufferP);
  free(bufferP);
  while (itVar != NULL) {
    nbVar += itVar->var->length;
    itVar = itVar->next;
  }

  struct ConstraintCell*itCstr = model.constraints.head;
  while (itCstr != NULL) {
    nbCstr += (1 - (itCstr->val->type)) + (itCstr->val->lowerBounded)
        + (itCstr->val->upperBounded);
    itCstr = itCstr->next;
  }

  fprintf(mod, "      !==============================\n");
  fprintf(mod, "      ! Constraint Propagation\n");
  fprintf(mod, "      !--------------------\n");

  params[0] = "XV";
  types[0] = "INTERVAL(8), dimension(:)";
  intents[0] = "inout";
  params[1] = "BOOL";
  types[1] = "logical";
  intents[1] = "inout";
  params[2] = "minmaxF";
  types[2] = "INTERVAL(8)";
  intents[2] = "in";
  writeModuleSubroutineHeader(mod, "REDUIRE_RIEN", params, 3, types,
      intents);
  writeModuleFunctionInstruction(mod, "BOOL = .true.");
  writeModuleSubroutineEnd(mod, "REDUIRE_RIEN");

  writeModuleSubroutineHeader(mod, "REDUIRE_ARBRE", params, 3, types,
      intents);

  sprintf(buffer2, "type(PNODE), dimension(%d), save:: CONTRAINTE",
      nbCstr);
  writeModuleFunctionInstruction(mod, buffer2);
  sprintf(buffer2, "type(PNODE), dimension(%d), save:: X", nbVar);
  writeModuleFunctionInstruction(mod, buffer2);
  sprintf(buffer2, "INTERVAL(8), dimension(%d), save:: XI", nbVar);
  writeModuleFunctionInstruction(mod, buffer2);
  writeModuleFunctionInstruction(mod, "if (BOOL) then");
  writeModuleFunctionInstruction(mod,
      "  if (.not. associated(X(1)%P)) then");
  writeModuleFunctionInstruction(mod, "    call INIT_VARIA(X, XI)");

  nbVar = 0;
  itVar = model.decisionVariables.head;
  while (itVar != NULL) {
    for (j = 0; j < itVar->var->length; ++j) {
      nbVar++;
      if (itVar->var->isInteger[j]) {
        sprintf(buffer2, "    call ENTIER(X(%d))", nbVar);
        writeModuleFunctionInstruction(mod, buffer2);
      }
    }
    itVar = itVar->next;
  }

  l = 0;
  itCstr = model.constraints.head;
  while (itCstr != NULL) {
    if (1 - (itCstr->val->type)) {
      l++;
      sprintf(buffer2, "    CONTRAINTE(%d) = (USER_%s%s - %s)", l,
          itCstr->val->f->ret.name, buffer, itCstr->val->boundL);
      writeModuleFunctionInstruction(mod, buffer2);
    }
    itCstr = itCstr->next;
  }

  itCstr = model.constraints.head;
  while (itCstr != NULL) {
    if ((itCstr->val->type)) {
      if (itCstr->val->lowerBounded) {
        l++;
        sprintf(buffer2, "    CONTRAINTE(%d) = (%s - USER_%s%s)", l,
            itCstr->val->boundL, itCstr->val->f->ret.name, buffer);
        writeModuleFunctionInstruction(mod, buffer2);
      }
      if (itCstr->val->upperBounded) {
        l++;
        sprintf(buffer2, "    CONTRAINTE(%d) = (USER_%s%s - %s)", l,
            itCstr->val->f->ret.name, buffer, itCstr->val->boundU);
        writeModuleFunctionInstruction(mod, buffer2);
      }
    }
    itCstr = itCstr->next;
  }

  l++;
  sprintf(buffer2, "    CONTRAINTE(%d) = (USER_%s%s)", l,
      model.objective.ret.name, buffer);
  writeModuleFunctionInstruction(mod, buffer2);

  writeModuleFunctionInstruction(mod, "  else");
  writeModuleFunctionInstruction(mod,
      "    call DETRUIRE(CONTRAINTE)");
  writeModuleFunctionInstruction(mod, "    call DETRUIRE_VARIA(X)");
  writeModuleFunctionInstruction(mod, "  end if");
  writeModuleFunctionInstruction(mod, "else");
  writeModuleFunctionInstruction(mod, "  XI = XV");
  writeModuleFunctionInstruction(mod, "  if (.false.) then");
  writeModuleFunctionInstruction(mod, "    ! Dead code");
  l = 0;
  itCstr = model.constraints.head;
  while (itCstr != NULL) {
    if (1 - (itCstr->val->type)) {
      l++;
      if (model.options.exactConstraintChecking) {
        sprintf(buffer2, "  elseif (.not.CONTRAINTE(%d) == [0]) then",
            l);
      } else {
        sprintf(buffer2,
            "  elseif (.not.CONTRAINTE(%d) == [-%s, %s]) then", l,
            itCstr->val->precision, itCstr->val->precision);
      }
      writeModuleFunctionInstruction(mod, buffer2);
      writeModuleFunctionInstruction(mod, "    BOOL = .false.");
      writeModuleFunctionInstruction(mod, "    return");
    }
    itCstr = itCstr->next;
  }

  itCstr = model.constraints.head;
  while (itCstr != NULL) {
    if ((itCstr->val->type)) {
      if (itCstr->val->lowerBounded) {
        l++;
        sprintf(buffer2, "  elseif (.not.CONTRAINTE(%d) == &", l);
        writeModuleFunctionInstruction(mod, buffer2);
        sprintf(buffer2,
            "          INTERVAL(moinsinfini, 0.D0)) then");
        writeModuleFunctionInstruction(mod, buffer2);
        writeModuleFunctionInstruction(mod, "    BOOL = .false.");
        writeModuleFunctionInstruction(mod, "    return");
      }
      if (itCstr->val->upperBounded) {
        l++;
        sprintf(buffer2, "  elseif (.not.CONTRAINTE(%d) == [0]) then",
            l);
        writeModuleFunctionInstruction(mod, buffer2);
        writeModuleFunctionInstruction(mod, "    BOOL = .false.");
        writeModuleFunctionInstruction(mod, "    return");
      }
    }
    itCstr = itCstr->next;
  }
  sprintf(buffer2, "  elseif (.not.CONTRAINTE(%d) == minmaxF) then",
      nbCstr + 1);
  writeModuleFunctionInstruction(mod, buffer2);
  writeModuleFunctionInstruction(mod, "    BOOL = .false.");
  writeModuleFunctionInstruction(mod, "    return");
  writeModuleFunctionInstruction(mod, "  end if");
  writeModuleFunctionInstruction(mod, "  XV = XI");
  writeModuleFunctionInstruction(mod, "end if");
  writeModuleFunctionInstruction(mod, "BOOL = .true.");

  writeModuleSubroutineEnd(mod, "REDUIRE_ARBRE");
}

char* splitParameterInVariables(VariableList vl) {
  char*buff = malloc(255 * sizeof(char));
  struct VariableListCell* itVar = vl.head;
  int l = 0, i;
  char buffer2[255];
  if (itVar->var->isVector) {
    sprintf(buff, "X(%d:%d)", l + 1, l + itVar->var->length);
    l += itVar->var->length;
  } else {
    sprintf(buff, "X(%d)", l + 1);
    l++;
  }
  itVar = itVar->next;
  for (i = 1; i < vl.length; ++i) {
    if (itVar->var->isVector) {
      sprintf(buffer2, ", X(%d:%d)", l + 1, l + itVar->var->length);
      strcat(buff, buffer2);
      l += itVar->var->length;
    } else {
      sprintf(buffer2, ", X(%d)", l + 1);
      strcat(buff, buffer2);
      l++;
    }
    itVar = itVar->next;
  }
  return buff;
}
