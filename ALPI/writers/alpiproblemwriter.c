/*
 * alpiproblemwriter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include <stdlib.h>
#include "alpiproblemwriter.h"
#include "../forgen/forgen.h"
#include <string.h>

void compileProblemModelToProblem(ProblemModel model, FILE* mod_out) {
  int i, j;
  int nbVars, nbCstrs, nbCeq;
  struct VariableListCell* itVar;
  struct ConstraintCell* itCstrs;
  char buffer[255];

  writeProblemProgramHeader(mod_out, model.name);

  nbVars = 0;
  itVar = model.decisionVariables.head;
  while (itVar != NULL) {
    nbVars += itVar->var->length;
    itVar = itVar->next;
  }

  nbCstrs = 0;
  itCstrs = model.constraints.head;
  while (itCstrs != NULL) {
    nbCstrs += 1 - (itCstrs->val->type) + (itCstrs->val->lowerBounded)
        + (itCstrs->val->upperBounded);
    itCstrs = itCstrs->next;
  }

  sprintf(buffer, "INTERVAL(8), dimension(%d):: BUTEES", nbVars);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out, "type(solution):: YI_SOL");
  writeProgramInstruction(mod_out, "INTERVAL(8):: AF_interval");
  sprintf(buffer, "DOUBLE PRECISION, dimension(%d):: PRECISION_X",
      nbVars);
  writeProgramInstruction(mod_out, buffer);
  sprintf(buffer, "DOUBLE PRECISION, dimension(%d):: PRECISION_C",
      nbCstrs);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out,
      "DOUBLE PRECISION:: PRECISION_F, Fmin_sol");
  writeProgramInstruction(mod_out, "logical, dimension(9):: OPTION");
  writeProgramInstruction(mod_out, "logical:: BOOL_MIP");
  sprintf(buffer, "integer:: NB_ITS = %d", model.options.logFreq);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out,
      "integer:: nb_list_max, nombre_contrainte");
  writeProgramInstruction(mod_out,
      "integer:: nombre_in, TMAX, L_max_in");
  // TODO variable de categaorie
  int nbVarCategory = 0;
  sprintf(buffer, "integer, dimension(%d):: nb_var_cat, size_cat",
      nbVarCategory);
  writeProgramInstruction(mod_out, buffer);
  sprintf(buffer, "type(Pmatrix), dimension(%d):: V_cat",
      nbVarCategory);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out, "integer:: i, j, k");
  sprintf(buffer, "interval, dimension(%d):: TMP_SOL", nbVars);
//  sprintf(buffer, "interval, dimension(%d):: TMP_SOL",
//  nbVars+nbVarCategoryTot);
  writeProgramInstruction(mod_out, buffer);
  itVar = model.decisionVariables.head;
  i = 1;
  while (itVar != NULL) {
    for (j = 0; j < itVar->var->length; ++j) {
      sprintf(buffer, "PRECISION_X(%d) = %s", i,
          itVar->var->precision[j]);
      writeProgramInstruction(mod_out, buffer);
      sprintf(buffer, "BUTEES(%d) = [%s, %s]", i,
          itVar->var->bounds[j][0], itVar->var->bounds[j][1]);
      writeProgramInstruction(mod_out, buffer);
      i++;
    }
    itVar = itVar->next;
  }
  i = 1;
  itCstrs = model.constraints.head;
  while (itCstrs != NULL) {
    if (1 - (itCstrs->val->type)) {
      sprintf(buffer, "PRECISION_C(%d) = %s", i,
          itCstrs->val->precision);
      writeProgramInstruction(mod_out, buffer);
      i++;
    }
    itCstrs = itCstrs->next;
  }
  nbCeq = i;
  itCstrs = model.constraints.head;
  while (itCstrs != NULL) {
    if ((itCstrs->val->type)) {
      sprintf(buffer, "PRECISION_C(%d) = %s", i,
          itCstrs->val->precision);
      writeProgramInstruction(mod_out, buffer);
      i++;
      if ((itCstrs->val->lowerBounded)
          * (itCstrs->val->upperBounded)) {
        sprintf(buffer, "PRECISION_C(%d) = %s", i,
            itCstrs->val->precision);
        writeProgramInstruction(mod_out, buffer);
        i++;
      }
    }
    itCstrs = itCstrs->next;
  }
  sprintf(buffer, "PRECISION_F = %s",
      model.objective.ret.precision[0]);
  writeProgramInstruction(mod_out, buffer);
  sprintf(buffer, "nombre_contrainte = %d", nbCstrs);
  writeProgramInstruction(mod_out, buffer);
  sprintf(buffer, "nombre_in = %d", nbCstrs - nbCeq);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out, "nb_list_max = 2147483630");
  sprintf(buffer, "Fmin_sol = %s", model.options.knownMajor);
  writeProgramInstruction(mod_out, buffer);
  sprintf(buffer, "TMAX = %d", model.options.timeLimit);
  writeProgramInstruction(mod_out, buffer);
  sprintf(buffer, "L_max_in = %d", model.options.memLimit);
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.computeBoundsWithTyalor) {
    sprintf(buffer, "OPTION(1) = .false.");
  } else {
    sprintf(buffer, "OPTION(1) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.constraintsPropagation) {
    sprintf(buffer, "OPTION(2) = .false.");
  } else {
    sprintf(buffer, "OPTION(2) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.CPLEX) {
    sprintf(buffer, "OPTION(3) = .false.");
  } else {
    sprintf(buffer, "OPTION(3) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.timeUnitInSeconds) {
    sprintf(buffer, "OPTION(4) = .false.");
  } else {
    sprintf(buffer, "OPTION(4) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.keepAll) {
    sprintf(buffer, "OPTION(5) = .false.");
  } else {
    sprintf(buffer, "OPTION(5) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.normalStoppingCriterion) {
    sprintf(buffer, "OPTION(6) = .false.");
  } else {
    sprintf(buffer, "OPTION(6) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.terminalDisplay) {
    sprintf(buffer, "OPTION(7) = .false.");
  } else {
    sprintf(buffer, "OPTION(7) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.notCertifiedLP) {
    sprintf(buffer, "OPTION(8) = .false.");
  } else {
    sprintf(buffer, "OPTION(8) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);
  if (!model.options.exactConstraintChecking) {
    sprintf(buffer, "OPTION(9) = .false.");
  } else {
    sprintf(buffer, "OPTION(9) = .true.");
  }
  writeProgramInstruction(mod_out, buffer);

  // AF_interval... depending on the reformulation
  if (model.options.reformulation == NULL) {
    writeProgramInstruction(mod_out, "AF_interval = 0");
    writeProgramInstruction(mod_out, "OPTION(3) = .false.");
  } else if (!strcmp(model.options.reformulation, "AFFINE_RB")) {
    writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
  } else if (!strcmp(model.options.reformulation, "AFFINE2")) {
    writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
  } else if (!strcmp(model.options.reformulation, "AFFINE3_RB")) {
    writeProgramInstruction(mod_out, "AF_interval = [0, 1]");
  } else if (!strcmp(model.options.reformulation, "rAF2_IA")) {
    writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
  } else if (!strcmp(model.options.reformulation, "rAF3_IA")) {
    writeProgramInstruction(mod_out, "AF_interval = [0, 1]");
  } else if (!strcmp(model.options.reformulation, "AFFINE3")) {
    writeProgramInstruction(mod_out, "AF_interval = [0, 1]");
  } else if (!strcmp(model.options.reformulation, "AF3_IA")) {
    writeProgramInstruction(mod_out, "AF_interval = [0, 1]");
  } else if (!strcmp(model.options.reformulation, "AFFINE_S")) {
    writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
  } else if (!strcmp(model.options.reformulation, "AFFINE_F")) {
    writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
  } else if (!strcmp(model.options.reformulation, "AF2_IA")) {
    writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
  } else if (!strcmp(model.options.reformulation, "AFFINE_Rev")) {
    writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
  } else {
    writeProgramInstruction(mod_out, "AF_interval = 0");
    writeProgramInstruction(mod_out, "OPTION(3) = .false.");
  }
  // XXX Category Variable
//  if (nbVarCategory) {
//
//  }
  sprintf(buffer, "print *, \"%s & %d & %d\"", model.name, nbVars,
      nbCstrs);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out, "if (OPTION(7)) then");
  writeProgramInstruction(mod_out, "  print *, \" BUTEES : \"");
  sprintf(buffer, "  do i = 1, %d", nbVars);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out,
      "    print *, \"   variable \", i, \" : \", BUTEES(i)");
  writeProgramInstruction(mod_out, "  end do");
  // TODO category variables
  writeProgramInstruction(mod_out, "end if");

  fprintf(mod_out, "\n");
  writeModuleComment(mod_out,
      "Argument de la fonction MIN_GLOB_CPLEX");
  writeModuleComment(mod_out, "");
  writeModuleComment(mod_out,
      "  FINT      Fonction a minimiser en arithmetique d'intervale");
  writeModuleComment(mod_out,
      "  FAFFINE_F Fonction objective en arithmetique affine");
  writeModuleComment(mod_out, "  AF_interval");
  writeModuleComment(mod_out,
      "            Variable des fonctions affines");
  writeModuleComment(mod_out,
      "  T1        Fonction de calcul des bornes par d'autres");
  writeModuleComment(mod_out, "            methodes");
  writeModuleComment(mod_out, "  REDUIRE_X Fonction de reduction");
  writeModuleComment(mod_out, "  CONTRAINTE");
  writeModuleComment(mod_out, "            Fonction des contraintes");
  writeModuleComment(mod_out, "  CONTRAINTE_RELAX");
  writeModuleComment(mod_out,
      "            Relaxation des contraintes");
  writeModuleComment(mod_out, "  BUTEES    Bornes des X(i)");
  writeModuleComment(mod_out,
      "  EPS       Precision des contraintes");
  writeModuleComment(mod_out, "  EPSV      Precision des variables");
  writeModuleComment(mod_out,
      "  EPSF      Precision de la fonction a minimiser");
  writeModuleComment(mod_out, "  NC        Nombre de contraintes");
  writeModuleComment(mod_out,
      "  NC_INEG   Nombre de contraintes d'inegalite");
  writeModuleComment(mod_out,
//      "  NNC_NOIR  Nombre de contraintes de type boite noire (non");
//  writeModuleComment(mod_out, "            pris en compte)");
//  writeModuleComment(mod_out,
      "  NB        Nombre d'iteration pour l'affichage");
  writeModuleComment(mod_out,
      "  nb_list   Taille maximum de la liste (methode heuristique)");
  writeModuleComment(mod_out,
      "  FFMIN     Borne de depart de la fonction a minimiser");
  writeModuleComment(mod_out, "  OPTION    options de IBBA");
  writeModuleComment(mod_out,
      "  NOMFICH   Nom du fichier pour stocker les resultats");
  writeModuleComment(mod_out, "  TMAX      Temps maximal");
  writeModuleComment(mod_out,
      "  L_max_in  Limite sur le nombre d'element pouvant etre");
  writeModuleComment(mod_out, "            stockes");
  writeModuleComment(mod_out, "  nb_var_cat");
  writeModuleComment(mod_out,
      "            Tableau contenant le nombre d'elements de");
  writeModuleComment(mod_out,
      "            variables associees a chaque categorie");
  writeModuleComment(mod_out,
      "  size_cat  Tableau contenant le nombre d'elements par");
  writeModuleComment(mod_out, "            categorie");
  writeModuleComment(mod_out,
      "  V_cat     Tableau contenant des matrices representant les");
  writeModuleComment(mod_out,
      "            valeurs de chaque element * chaque variable");
  fprintf(mod_out, "\n");
  sprintf(buffer, "YI_SOL = IBBA(%s, &", model.objective.ret.name);
  writeProgramInstruction(mod_out, buffer);
  if (model.options.reformulation == NULL) {
    sprintf(buffer, "              %s_RIEN, &",
        model.objective.ret.name);
  } else {
    sprintf(buffer, "              %s_%s, &",
        model.objective.ret.name, model.options.reformulation);
  }
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out, "              AF_interval, &");
  if (model.options.reformulation == NULL) {
    writeProgramInstruction(mod_out, "              T_RIEN, &");
  } else {
    sprintf(buffer, "              T_%s, &",
        model.options.reformulation);
    writeProgramInstruction(mod_out, buffer);
  }
  if (nbCstrs) {
    writeProgramInstruction(mod_out,
        "              REDUIRE_ARBRE, &");
  } else {
    writeProgramInstruction(mod_out, "              REDUIRE_RIEN, &");
  }
  writeProgramInstruction(mod_out, "              CONTRAINTE, &");
  writeProgramInstruction(mod_out,
      "              CONTRAINTE_RELAX, &");
  writeProgramInstruction(mod_out, "              BUTEES, &");
  writeProgramInstruction(mod_out, "              PRECISION_C, &");
  writeProgramInstruction(mod_out, "              PRECISION_X, &");
  writeProgramInstruction(mod_out, "              PRECISION_F, &");
  writeProgramInstruction(mod_out,
      "              nombre_contrainte, &");
  writeProgramInstruction(mod_out, "              nombre_in, &");
  writeProgramInstruction(mod_out, "              NB_ITS, &");
  writeProgramInstruction(mod_out, "              nb_list_max, &");
  writeProgramInstruction(mod_out, "              Fmin_sol, &");
  writeProgramInstruction(mod_out, "              OPTION, &");
  sprintf(buffer, "              '%s', &", model.options.logFile);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out, "              TMAX, &");
  writeProgramInstruction(mod_out, "              L_max_in, &");
  writeProgramInstruction(mod_out, "              nb_var_cat, &");
  writeProgramInstruction(mod_out, "              size_cat, &");
  writeProgramInstruction(mod_out, "              V_cat)");
  fprintf(mod_out, "\n");
  writeProgramInstruction(mod_out, "if (OPTION(7)) then");
  writeProgramInstruction(mod_out, "  print *, \" SOLUTION : \"");
  sprintf(buffer, "  do i = 1, %d", nbVars);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out,
      "    print *, \"   variable \", i, \" : \", YI_SOL%Y(i)");
  writeProgramInstruction(mod_out, "  end do");
  // TODO variable de categorie
//  sprintf(buffer, "  do i = 1, %d", nbVarCategory);
//  writeProgramInstruction(mod_out, buffer);
//  writeProgramInstruction(mod_out,
//      "    print *, \"   variable \", i, \" : \", YI_SOL%CAT(i)");
//  writeProgramInstruction(mod_out, "  end do");
  sprintf(buffer, "  TMP_SOL(1:%d) = YI_SOL%%Y", nbVars);
  writeProgramInstruction(mod_out, buffer);
  sprintf(buffer, "  k=1+%d", nbVars);
  writeProgramInstruction(mod_out, buffer);
  // XXX Category Variables
//  sprintf(buffer, "  do i = 1, %d", nbVarCategory);
//  writeProgramInstruction(mod_out, buffer);
//  writeProgramInstruction(mod_out, "    do j = 1, nb_var_cat(i)");
//  writeProgramInstruction(mod_out,
//      "      TMP_SOL(k) = V_cat(i)%P(YI_SOL%CAT(i), j)");
//  writeProgramInstruction(mod_out, "      k = k+1");
//  writeProgramInstruction(mod_out, "    end do");
//  writeProgramInstruction(mod_out, "  end do");
  writeProgramInstruction(mod_out,
      "  print *, \" valeur minimale de la fonction : \", &");
  sprintf(buffer, "           %s(TMP_SOL)", model.objective.ret.name);
  writeProgramInstruction(mod_out, buffer);
  writeProgramInstruction(mod_out, "  print *, \" \"");
  if (nbCstrs) {
    writeProgramInstruction(mod_out,
        "  print *, \" CONTRAINTES : \"");
    writeProgramInstruction(mod_out, "  do i = 1, nombre_contrainte");
    writeProgramInstruction(mod_out,
        "    print *, \" variable \", i, \" : \", &");
    writeProgramInstruction(mod_out,
        "             CONTRAINTE(i, TMP_SOL)");
    writeProgramInstruction(mod_out, "  end do");
  }
  writeProgramInstruction(mod_out, "end if");
  fprintf(mod_out, "\n");
  writeProgramInstruction(mod_out, "deallocate(YI_SOL%Y)");
  writeProgramInstruction(mod_out, "deallocate(YI_SOL%CAT)");

  writeProgramEnd(mod_out, model.name);
}

void writeProblemProgramHeader(FILE*mod, char*name) {
  char* mods[2];
  char m[20];

  mods[0] = "MOD_OPT_GLOB_CPLEX";
  sprintf(m, "Mod_%s", name);
  mods[1] = m;
  writeProgramHeader(mod, name, mods, 2);
}
