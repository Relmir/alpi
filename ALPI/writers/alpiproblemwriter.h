/*
 * alpiproblemwriter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIPROBLEMWRITER_H_
#define ALPIPROBLEMWRITER_H_

#include <stdio.h>
#include "../alpistructs/alpistructures.h"

void compileProblemModelToProblem(ProblemModel model, FILE* mod_out);
void writeProblemProgramHeader(FILE*mod, char*name);


#endif /* ALPIPROBLEMWRITER_H_ */
