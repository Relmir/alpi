/*
 * alpiprinter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include <stdio.h>

void printTitleSec(char* string) {
  printf("==================================================\n");
  printf("  %s\n", string);
  printf("==================================================\n");
}

void printSubSec(char* string) {
  printf("------------------------------\n");
  printf("  %s\n", string);
  printf("------------------------------\n");
}
