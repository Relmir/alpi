/*
 * alpilangrules.c
 *
 *  Created on: Dec 11, 2013
 *      Author: Emmanuel BIGEON
 */

#include <stdlib.h>
#include <string.h>

//=============
// Parser rules
//-------------




// In expressions:
// if bval = 0, it is a simple value constant
// else if bval%2 = 0, it is a constant functions
// else, it is a function depending on the decision variables

// isSimpleConstant = !bval
// isConstant = !(bval%2)

// Tools
