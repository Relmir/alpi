/*
 * alpiprinter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIPRINTER_H_
#define ALPIPRINTER_H_

void printTitleSec(char* title);
void printSubSec(char* title);

#endif /* ALPIPRINTER_H_ */
