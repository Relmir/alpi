/*
 * alpilangglobal.h
 *
 *  Created on: Dec 6, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPILANGGLOBAL_H_
#define ALPILANGGLOBAL_H_

#include <stdio.h>

#ifndef YYSTYPE
#define YYSTYPE struct RuleInformation
#endif

#ifndef YYDEBUG
#define YYDEGUG 1
#endif

#include "../yacclex/alpilang.h"
#include "../alpistructs/alpistructures.h"

extern int fatal;
extern FILE* alpiOut;
extern ProblemModel alpiPb;
extern int yylinenum;


#endif /* ALPILANGGLOBAL_H_ */
int yyerror(char const *s);



