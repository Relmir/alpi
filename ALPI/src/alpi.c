/*
 ============================================================================
 Name        : alpi.c
 Author      : Bigeon, Emmanuel
 Version     :
 Copyright   : GPL3
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../yacclex/alpilang.h"
#include "../alpistructs/alpistructures.h"
#include "alpiprinter.h"
#include "../writers/alpiwriters.h"

// Input file
extern FILE *yyin;
// Error will be printed here
extern FILE *alpiOut;
// Problem holder
extern ProblemModel alpiPb;
// translation status
extern int fatal;

void compileProblemModel(ProblemModel model, FILE* mod_out,
    FILE* mod_param, FILE* out, FILE* makefile);

void usageError(int argc, char** argv);

int main(int argc, char** argv) {
  char* filename;
  char name[255];
  char modName[255];
  char *outputDir;
  if (argc > 1) {
    filename = argv[1];
    yyin = fopen(filename, "r");
    if (argc == 3) {
      usageError(argc, argv);
    }
    if (argc > 3) {
      if (strcmp(argv[2], "-dir")) {
        usageError(argc, argv);
      }
      outputDir = argv[3];
    } else {
      outputDir = "";
    }
    if (argc > 4) {
      usageError(argc, argv);
    }
  } else {
    usageError(argc, argv);
  }
  // init
  printf("initialize...\n");
  alpiOut = stdout;
  if (strchr(filename, '/') != NULL) {
    strcpy(name, strrchr(filename, '/') + 1);
  } else {
    strcpy(name, filename);
  }
  int i = strcspn(name, ".");
  name[i] = '\0';
  alpiPb = createProblemModel(name);
  // parse
  printf("done\nparse...\n");
  yyparse();

  if (fatal) {
    printf("there were errors !");
    exit(1);
  }
  sprintf(modName, "%sMod_%s.f90", outputDir, name);
  FILE* mod_out = fopen(modName, "w");
  sprintf(modName, "%s%s.f90", outputDir, name);
  FILE* out = fopen(modName, "w");
  sprintf(modName, "%sMod_Parametres.f90", outputDir);
  FILE* mod_param = fopen(modName, "w");
  sprintf(modName, "%smakefile", outputDir);
  FILE* make = fopen(modName, "w");

  printf("done\nwriting out\n");

  fprintModel(stdout, alpiPb);

//  compileProblemModel(alpiPb, mod_out, mod_param, out, make);

  fclose(mod_out);
  fclose(out);
  fclose(mod_param);
  fclose(make);

  // Cleaning

  printf("done\nwriting successful\n");

  exit(0);
}

void compileProblemModel(ProblemModel model, FILE* mod_out,
    FILE* mod_param, FILE* out, FILE* makefile) {
  compileProblemModelToParameter(model, mod_param);
  compileProblemModelToMod(model, mod_out);
  compileProblemModelToProblem(model, out);
  compileProblemModelToMakeFile(model, makefile);
}

void usageError(int argc, char** argv) {
  printf("Usage: %s <filename> [-dir <outputDirectory>]", argv[0]);
  exit(1);
}
