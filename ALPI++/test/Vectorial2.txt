set IX = 1 .. 5;

! TODO give values
var eps_inf, >= 10, <= 100, prec 1.D-5;
var delta_eps {IX}, >= 10, <= 100, prec 1.D-5;
var tau {IX}, >= 10, <= 100, prec 1.D-5;
var sigma_DC, >= 10, <= 100, prec 1.D-5;

param eps_0 = 1;
param omega = 1;

param term {i in IX} = delta_eps(i)/(1+omega*tau(i));

param s = sum {i in IX} (term(i));

minimize eps_omega = eps_inf + s + sigma_DC/eps_0/omega, prec 1.D-8;

option reformulation = AFFINE_F;