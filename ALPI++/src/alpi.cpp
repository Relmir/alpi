/*
 * alpi.c
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#include "driver/driver.hh"
#include <iostream>
#include <fstream>
#include <cstring>
#include "writers/alpiwriters.h"

using namespace alpiparsertools;
using namespace std;
using namespace alpi::problem;

void usageError(int argc, char** argv);
void printVersion();

int main(int argc, char*argv[]) {
  char*filename;
  char name[255];
  std::string outputDir;

  if (argc > 1) {
    if (!strcmp(argv[1], "-V")) {
      printVersion();
      exit(0);
    }
    filename = argv[1];
    if (argc == 3) {
      usageError(argc, argv);
    }
    if (argc > 3) {
      if (argv[2] != std::string("-dir")) {
        usageError(argc, argv);
      }
      outputDir = argv[3];
    } else {
      outputDir = "";
    }
    if (argc > 4) {
      usageError(argc, argv);
    }
  } else {
    usageError(argc, argv);
  }
  if (strchr(filename, '/') != NULL) {
    strcpy(name, strrchr(filename, '/') + 1);
  } else {
    strcpy(name, filename);
  }
  int i = strcspn(name, ".");
  name[i] = '\0';

  ProblemModel mod(name);
  alpiparsertools::driver driver(&mod);
  bool p = driver.parse(filename);
  if (p|| driver.fatal) {
    std::cout << "parse error !" << std::endl;
    exit(2);
  }
  std::cout << std::endl << "====================" << std::endl;

  std::cout << std::endl << "generating problem functions..." << std::flush;
  ofstream mod_out, out, mod_param, make;
  mod_out.open((outputDir + "Mod_" + name + ".f90").c_str());
  alpiwriter::compileProblemModelToMod(mod, mod_out);
  std::cout << "\r" << "generating problem functions... done" << std::endl;

  std::cout << "generating solver launcher..." << std::flush;
  out.open((outputDir + name + ".f90").c_str());
  alpiwriter::compileProblemModelToProblem(mod, out);
  std::cout << "\r" << "generating solver launcher... done" << std::endl;

  std::cout << "\rgenerating parameters..." << std::flush;
  mod_param.open((outputDir + "Mod_Parametres.f90").c_str());
  alpiwriter::compileProblemModelToParameter(mod, mod_param);
  std::cout << "\r" << "generating parameters... done" << std::endl;

  std::cout << "generating makefile..." << std::flush;
  make.open((outputDir + "makefile").c_str());
  alpiwriter::compileProblemModelToMakeFile(mod, make);
  std::cout << "\r" << "generating makefile... done" << std::endl;


  std::cout << std::endl << "generation ended" << std::endl;

  exit(0);
}

void usageError(int argc, char** argv) {
  std::cout << "Usage: " << argv[0]
      << " <filename> [-dir <outputDirectory>]" << endl;
  exit(1);
}

void printVersion() {
  std::cout << "0.6-4" << endl;
}
