/*
 * NumericExpression.h
 *
 *  Created on: Feb 5, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef NUMERICEXPRESSION_H_
#define NUMERICEXPRESSION_H_

#include "../ExpressionNode.h"

namespace alpi {
  namespace exprs {
    namespace numerics {

      class NumericExpression: virtual public alpi::exprs::ExpressionNode {
        public:
          // Inherited
          bool isInteger() const = 0;
          bool isIndependant() const = 0;
          virtual ~NumericExpression() {}
      };

    } /* namespace numerics */
  } /* namespace exprs */
} /* namespace alpi */

#endif /* NUMERICEXPRESSION_H_ */
