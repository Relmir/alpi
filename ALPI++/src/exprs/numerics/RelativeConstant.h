/*
 * RelativeConstant.h
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef RELATIVECONSTANT_H_
#define RELATIVECONSTANT_H_
#include "NumericExpression.h"
namespace alpi {
  namespace exprs {
    namespace numerics {
      /** \brief Representation of an integer
       */
      class RelativeConstant: public NumericExpression {
        public:
          /** \brief Create the representation of the integer zero
           */
          RelativeConstant();
          /** \brief Create the representation of the integer represented
           * in this string
           */
          RelativeConstant(std::string str);
          /** \brief Create the representation of an integer
           *
           * \param i the integer to represent
           *
           */
          RelativeConstant(int i);
          /** \brief Get the represented integer
           *
           * \return int the represented integer
           *
           */
          int toInt() const;
          // Inherited
          std::string toString() const;
          bool isConstant() const;
          bool isIndependant() const;
          bool isInteger() const;
          bool isVector() const;
          int priority() const {
            return 0;
          }
          virtual ~RelativeConstant();
        private:
          int value; //!< The value
      };

      /** \brief Test equality between integer representations
       *
       * \param cst one integer
       * \param other the other integer
       * \return if they represent the same integer
       */
      bool operator==(RelativeConstant const &cst,
          RelativeConstant const &other);
    }
  }
} /* namespace alpiparsertools */

#endif /* RELATIVECONSTANT_H_ */
