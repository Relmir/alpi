/*
 * RelativeConstant.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#include "RelativeConstant.h"

#include <cstdlib>
#include <sstream>
#include <string>

namespace alpi {
  namespace exprs {
    namespace numerics {
      RelativeConstant::RelativeConstant(int i) :
          value(i) {
      }

      RelativeConstant::RelativeConstant(std::string rep) :
          value(atoi(rep.c_str())) {
      }

      int RelativeConstant::toInt() const {
        return value;
      }

      RelativeConstant::~RelativeConstant() {
      }

      std::string RelativeConstant::toString() const {
        std::ostringstream str;
        str << value;
        return str.str();
      }
      bool RelativeConstant::isIndependant() const {
        return true;
      }
      bool RelativeConstant::isInteger() const {
        return true;
      }
      bool RelativeConstant::isConstant() const {
        return true;
      }
      bool RelativeConstant::isVector() const {
        return false;
      }

      bool operator==(RelativeConstant const &cst,
          RelativeConstant const &other) {
        return cst.toInt() == other.toInt();
      }
    }
  }
}
