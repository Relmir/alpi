/*
 * RealConstant.cpp
 *
 *  Created on: Jan 12, 2014
 *      Author: Emmanuel BIGEON
 */

#include "RealConstant.h"

#include <cstdlib>
#include <sstream>
#include <string>

double atof(const char *__nptr);

namespace alpi {
  namespace exprs {
    namespace numerics {
      char* formatToCppReal(std::string yytext) {
        char* formatted = (char*) malloc(
            (1 + yytext.length()) * sizeof(char));
        unsigned int i = 0;
        while (i < yytext.length()) {
          formatted[i] = yytext.at(i);
          if (formatted[i] == 'd' || formatted[i] == 'D') {
            formatted[i] = 'E';
          }
          i++;
        }
        return formatted;
      }
      RealConstant::RealConstant() :
          value(0), strVal("0.D0") {
      }

      RealConstant::RealConstant(std::string rep) :
          value(atof(formatToCppReal(rep.c_str()))), strVal(rep) {
      }

      RealConstant::RealConstant(double val) :
          value(val) {
        std::ostringstream str;
        str << value;
        strVal = str.str();

      }

      double RealConstant::toDouble() const {
        return value;
      }

      std::string RealConstant::toString() const {
        return strVal;
      }

      RealConstant::~RealConstant() {
      }

      RealConstant operator-(RealConstant r) {
        return RealConstant(r.toDouble());
      }
      bool RealConstant::isConstant() const {
        return true;
      }
      bool RealConstant::isIndependant() const {
        return true;
      }
      bool RealConstant::isInteger() const {
        return false;
      }
      bool RealConstant::isVector() const {
        return false;
      }
      bool RealConstant::operator==(RealConstant const &real) {
        return real.value == value;
      }
    }
  }
}
