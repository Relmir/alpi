/*
 * RealConstant.h
 *
 *  Created on: Jan 12, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef REALCONSTANT_H_
#define REALCONSTANT_H_

#include "NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace numerics {
    /** \brief The representation of reals
     */
    class RealConstant: public NumericExpression {
      public:
        /** \brief Create a real constant that represents zero
         */
        RealConstant();
        /** \brief Create a real constant
         *
         * \param real double the value
         *
         */
        RealConstant(double real);
        /** \brief Create a real constant that represents this string given value
         */
        RealConstant(std::string str);
        /** \brief Converts this constant to a double value
         *
         * \return double the double represented by this constants
         */
        double toDouble() const;
        /** \brief Test equality between real constants
         *
         * \param real the other
         * \return bool if they represent the same double
         *
         */
        bool operator==(RealConstant const& real);
        // Inherited
        std::string toString() const;
        bool isConstant() const;
        bool isIndependant() const;
        bool isInteger() const;
        bool isVector() const;
        int priority() const {
          return 0;
        }
        ~RealConstant();
      private:
        double value; //!< The value
        std::string strVal; //!< The string representation of the value
    };

    /** \brief Takes the opposite of a real constant.
     *
     * This is simply the representation of the opposite of the
     * represented double.
     *
     * \param real the representation to take the opposite of
     * \return RealConstant the representation of the opposite
     *
     */
    RealConstant operator-(RealConstant const& real);
    }
  }
} /* namespace alpiparsertools */

#endif /* REALCONSTANT_H_ */
