/*
 * AssignmentMap.h
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef ASSIGNMENTMAP_H_
#define ASSIGNMENTMAP_H_

#include <map>
#include <string>

#include "indexing/IndexingMultiRelation.h"

namespace alpi {
  namespace exprs {
    /** \brief An association of masks and Expressions.

    An assignment map can only be used in definition expressions. It
    figures a succession of instruction to give a value to a parameter.
     */
    template<class T>
    class AssignmentMap: public T {
        /** \brief the default definition
         */
        T* defolt;
        indexing::IndexingMultiRelation* defoltIndexing;
        /** \brief the map of masks to the corresponding definitions.
         */
        std::map<indexing::IndexingMultiRelation*, T*> maskedVals;
      public:
        /** \brief Create an assignment map with no default value.
         */
        AssignmentMap();
        /** \brief Create an assignment map with default value
         *
         * \param defo the default value
         */
        AssignmentMap(T* defo, indexing::IndexingMultiRelation* ind);
        /** \brief Add an association of mask and value.
         *
         * \param mask the indexing to associate the value with
         * \param node the definition.
         */
        void addAssignement(indexing::IndexingMultiRelation* mask,
            T* node);
        /** \brief Get the default value
         *
         * \return ExpressionNode* the default definition
         *
         */
        T* getDefault();
        indexing::IndexingMultiRelation* getDefaultIndexing();
        /** \brief Get the assignment associated to a mask.
         *
         * Be aware that if the mask was not associated with a
         * definition, this function will return null even though the
         * mask might be covered by an other definition.
         * \param index the mask
         * \return ExpressionNode* the corresponding definition
         *
         */
        T* getAssignment(
            indexing::IndexingMultiRelation* index);
        /** \brief Get all masks associated.
         *
         * \return std::list<indexing::IndexingMultiRelation*> the
         *      associated masks
         *
         */
        std::list<indexing::IndexingMultiRelation*> getIndexingRelations();
        // Inherited
        bool isIndependant() const;
        bool isConstant() const;
        bool isInteger() const;
        bool isVector() const;
        std::string toString() const;
        int priority() const {
          return -1;
        }
        virtual ~AssignmentMap();
    };
  }
} /* namespace alpiparsertools */

#include "AssignmentMap.tpp"

#endif /* ASSIGNMENTMAP_H_ */
