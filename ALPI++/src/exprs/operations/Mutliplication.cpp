/*
 * Multiplication.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Multiplication.h"

namespace alpi {
  namespace exprs {
    namespace operations {

      Multiplication::Multiplication(NumericExpression* left,
          NumericExpression* right) :
          BinOp(left, right) {
      }

      Multiplication::~Multiplication() {
      }
      std::string Multiplication::toString() const {
        return "(" + left->toString() + ") * (" + right->toString()
            + ")";
      }
      bool Multiplication::isConstant() const {
        return false;
      }
    }
  }
}
