/*
 * Difference.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Difference.h"

namespace alpi {
  namespace exprs {
    namespace operations {

      Difference::Difference(NumericExpression* left,
          NumericExpression* right) :
          BinOp(left, right) {
      }
      Difference::~Difference() {
      }
      std::string Difference::toString() const {
        return "(" + left->toString() + ") - (" + right->toString()
            + ")";
      }
      bool Difference::isConstant() const {
        return false;
      }
    }
  }
} /* namespace alpiparsertools */
