/*
 * Multiplication.h
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef MULTIPLICATION_H_
#define MULTIPLICATION_H_

#include "BinOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      using namespace numerics;

      /** \brief The representation of multiplications
       */
      class Multiplication: public BinOp<numerics::NumericExpression,
          numerics::NumericExpression> {
        public:
          /** \brief Create a multiplication
           *
           * \param left first term
           * \param right second term
           *
           */
          Multiplication(NumericExpression* left, NumericExpression* right);
          virtual std::string toString() const;
          virtual bool isConstant() const;
          int priority() const {
            return 1;
          }
          virtual ~Multiplication();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* MUTIPLICATION_H_ */
