/*
 * Power.h
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef POWER_H_
#define POWER_H_

#include "BinOp.h"
#include "../numerics/NumericExpression.h"
namespace alpi {
  namespace exprs {
    namespace operations {
      using namespace numerics;

      /** \brief The power operation representation
       */
      class Power: public BinOp<numerics::NumericExpression,
          numerics::NumericExpression> {
        public:
          /** \brief Create a to the power of operation
           *
           * \param val the value
           * \param exponent the exponent
           *
           */
          Power(NumericExpression* val, NumericExpression* exponent);
          std::string toString() const;
          bool isConstant() const;
          bool isInteger() const;
          int priority() const {
            return 0;
          }
          virtual ~Power();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* POWER_H_ */
