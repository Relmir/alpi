/*
 * Power.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Power.h"

namespace alpi {
  namespace exprs {
    namespace operations {

      Power::Power(NumericExpression* val, NumericExpression* exp) :
          BinOp(val, exp) {
      }

      Power::~Power() {
      }
      std::string Power::toString() const {
        return "(" + left->toString() + ") ^ (" + right->toString()
            + ")";
      }
      bool Power::isConstant() const {
        return false;
      }
      bool Power::isInteger() const {
        // XXX power of integers: test positivity of exp ?
        return false;
      }

    }
  }
}
