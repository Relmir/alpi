/*
 * Difference.h
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef DIFFERENCE_H_
#define DIFFERENCE_H_

#include "BinOp.h"
#include "../numerics/NumericExpression.h"
namespace alpi {
  namespace exprs {
    namespace operations {
      using namespace numerics;

      /** \brief The difference operation
       */
      class Difference: public BinOp<numerics::NumericExpression, numerics::NumericExpression> {
        public:
          /** \brief Create a difference
           *
           * \param left first term
           * \param right second term
           *
           */
          Difference(NumericExpression* left, NumericExpression* right);
          virtual std::string toString() const;
          virtual bool isConstant() const;
          int priority() const {
            return 2;
          }
          virtual ~Difference();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* DIFFERENCE_H_ */
