/*
 * SumOnIndex.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#include "SumOnIndex.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      SumOnIndex::SumOnIndex(indexing::IndexingMultiRelation* indexVar,
          numerics::NumericExpression* node) :
          BigOp<numerics::NumericExpression>(indexVar, node) {
      }

      SumOnIndex::~SumOnIndex() {
      }
      std::string SumOnIndex::toString() const {
        return "Sum for " + index->toString() + " of ("
            + node->toString() + ")";
      }
      bool SumOnIndex::isConstant() const {
        return false;
      }
    }
  }
}
