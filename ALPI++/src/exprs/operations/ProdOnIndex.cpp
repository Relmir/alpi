/*
 * ProdOnIndex.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#include "ProdOnIndex.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      ProdOnIndex::ProdOnIndex(indexing::IndexingMultiRelation* indexVar,
          numerics::NumericExpression* node) :
          BigOp<numerics::NumericExpression>(indexVar, node) {
      }

      ProdOnIndex::~ProdOnIndex() {
      }

      std::string ProdOnIndex::toString() const {
        return "Prod for " + index->toString() + " of ("
            + node->toString() + ")";
      }
      bool ProdOnIndex::isConstant() const {
        return false;
      }
    }
  }
}
