/*
 * Oppose.h
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef OPPOSE_H_
#define OPPOSE_H_

#include "UnOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      /** \brief The opposite unary operator
       */
      class Oppose: public UnOp<numerics::NumericExpression,
          numerics::NumericExpression> {
        public:
          /** \brief Create the oppoiste operation representation
           *
           * \param inside the element to take the opposite of
           */
          Oppose(numerics::NumericExpression* inside);
          // Inherited
          virtual std::string toString() const;
          virtual bool isConstant() const;
          int priority() const {
            return 3;
          }
          virtual ~Oppose();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* OPPOSE_H_ */
