/*
 * BinOp.h
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef BINOP_H_
#define BINOP_H_

#include "../ExpressionNode.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      /** \brief A binary operator representation
       */
      template<class Param, class Result>
      class BinOp: virtual public Result {
        public:
          /** \brief Create a binary operator representation
           *
           * \param left the left-hand side of the operation
           * \param right the right-hand side of the operation
           *
           */
          BinOp(Param* left, Param* right) :
              left(left), right(right) {
          }
          /** \brief Get the left-hand side of this operation
           *
           * \return ExpressionNode* the left-hand side
           *
           */
          Param* getLeft() {
            return left;
          }
          /** \brief Get the right-hand side of this operation
           *
           * \return ExpressionNode* the right-hand side
           *
           */
          Param* getRight() {
            return right;
          }
          virtual std::string toString() const = 0;
          virtual bool isConstant() const = 0;
          virtual bool isIndependant() const {
            return left->isIndependant() && right->isIndependant();
          }
          virtual bool isInteger() const {
            return left->isInteger() && right->isInteger();
          }
          virtual bool isVector() const {
            return left->isVector() && right->isVector();
          }
          virtual ~BinOp() {}
        protected:
          Param* left; //!< The left-hand side
          Param* right; //!< The right-hand side
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* BINOP_H_ */

