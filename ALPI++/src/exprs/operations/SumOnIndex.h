/*
 * SumOnIndex.h
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef SUMONINDEX_H_
#define SUMONINDEX_H_

#include <string>

//#include "../indexing/Index.h"
//#include "../symbol/Variable.h"
#include "BigOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      /** \brief The Big Sum operation representation
       */
      class SumOnIndex: public BigOp<numerics::NumericExpression> {
      public:
        /** \brief Create a big sum
         *
         * \param index the indexing
         * \param node the expression to sum
         */
        SumOnIndex (indexing::IndexingMultiRelation* rel,
                    numerics::NumericExpression*node);
        // Inherited
        std::string toString() const;
        bool isConstant() const;
        int priority() const {
          return 0;
        }
        ~SumOnIndex();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* SUMONINDEX_H_ */
