/*
 * CosineFunctionCall.h
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef COSINEFUNCTIONCALL_H_
#define COSINEFUNCTIONCALL_H_

#include <string>

#include "UnOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace builtin {
      /** \brief The cosine function call representation
       */
      class CosineFunctionCall: public operations::UnOp<
          numerics::NumericExpression, numerics::NumericExpression> {
        public:
          /** \brief Create a cosine function call
           *
           * \param in the argument
           *
           */
          CosineFunctionCall(numerics::NumericExpression* in);
          // Inherited
          std::string toString() const;
          bool isConstant() const;
          int priority() const {
            return 0;
          }
          bool isInteger() const;
          ~CosineFunctionCall();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* COSINEFUNCTIONCALL_H_ */
