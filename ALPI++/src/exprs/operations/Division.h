/*
 * Division.h
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef DIVISION_H_
#define DIVISION_H_

#include "BinOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      using namespace numerics;

      /** \brief The division operation representation
       */
      class Division: public BinOp<numerics::NumericExpression, numerics::NumericExpression> {
        public:
          /** \brief Create a division
           *
           * \param left numerator
           * \param right denominator
           *
           */
          Division(NumericExpression* left, NumericExpression* right);
          std::string toString() const;
          bool isConstant() const;
          bool isInteger() const;
          int priority() const {
            return 1;
          }
          virtual ~Division();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* DIVISION_H_ */
