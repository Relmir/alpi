/*
 * SqrtFunctionCall.cpp
 *
 *  Created on: Jan 29, 2014
 *      Author: Emmanuel BIGEON
 */

#include "SqrtFunctionCall.h"

namespace alpi {
  namespace exprs {
    namespace builtin {

      SqrtFunctionCall::SqrtFunctionCall(numerics::NumericExpression* arg) :
          UnOp(arg) {
      }

      SqrtFunctionCall::~SqrtFunctionCall() {
      }

      std::string SqrtFunctionCall::toString() const {
        return "sin(" + argument->toString() + ")";
      }
      bool SqrtFunctionCall::isConstant() const {
        return false;
      }
      bool SqrtFunctionCall::isInteger() const {
        return false;
      }
    } /* namespace builtin */
  } /* namespace exprs */
} /* namespace alpi */
