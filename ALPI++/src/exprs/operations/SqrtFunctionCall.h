/*
 * SqrtFunctionCall.h
 *
 *  Created on: Jan 29, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef SQRTFUNCTIONCALL_H_
#define SQRTFUNCTIONCALL_H_

#include "UnOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace builtin {

      /** \brief The square root function call representation
       */
      class SqrtFunctionCall: public operations::UnOp<
          numerics::NumericExpression, numerics::NumericExpression> {
        public:
          /** \brief Create a square root operation
           *
           * \param arg the argument
           *
           */
          SqrtFunctionCall(numerics::NumericExpression* arg);
          std::string toString() const;
          bool isConstant() const;
          bool isInteger() const;
          int priority() const {
            return 0;
          }
          virtual ~SqrtFunctionCall();
      };

    } /* namespace builtin */
  } /* namespace exprs */
} /* namespace alpi */

#endif /* SQRTFUNCTIONCALL_H_ */
