/*
 * UnOp.h
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef UNOP_H_
#define UNOP_H_

#include "../ExpressionNode.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      /** \brief Abstract unary operation representation
       */
      template<class P, class R>
      class UnOp: public R {
        public:
          /** \brief Create an abstract unary operation
           *
           * \param arg the argument
           */
          UnOp(P*arg) :
              argument(arg) {
          }
          /** \brief Get the argument
           *
           * \return ExpressionNode* the operation argument
           *
           */
          P*getArgument() {
            return argument;
          }
          // Inherited
          virtual std::string toString() const = 0;
          virtual bool isConstant() const = 0;
          virtual bool isIndependant() const {
            return argument->isIndependant();
          }
          virtual bool isInteger() const {
            return argument->isInteger();
          }
          virtual bool isVector() const {
            return argument->isVector();
          }
          virtual ~UnOp() {
          }
        protected:
          P*argument;
      };
    }
  }
} /* namespace alpiparsertools */

#endif /* UNOP_H_ */
