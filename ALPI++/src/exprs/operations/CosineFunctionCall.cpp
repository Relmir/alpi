/*
 * CosineFunctionCall.cpp
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#include "CosineFunctionCall.h"

namespace alpi {
  namespace exprs {
    namespace builtin {

      CosineFunctionCall::CosineFunctionCall(numerics::NumericExpression *in) :
          UnOp(in) {

      }

      CosineFunctionCall::~CosineFunctionCall() {
      }

      std::string CosineFunctionCall::toString() const {
        return "cos(" + argument->toString() + ")";
      }
      bool CosineFunctionCall::isConstant() const {
        return false;
      }
      bool CosineFunctionCall::isInteger() const {
        return false;
      }
    }
  }
} /* namespace alpiparsertools */
