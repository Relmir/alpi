/*
 * Division.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Division.h"

namespace alpi {
  namespace exprs {
    namespace operations {

      Division::Division(NumericExpression* left, NumericExpression* right) :
          BinOp(left, right) {
      }

      Division::~Division() {
      }
      std::string Division::toString() const {
        return "(" + left->toString() + ") / (" + right->toString()
            + ")";
      }
      bool Division::isConstant() const {
        return false;
      }
      bool Division::isInteger() const {
        return false;
      }
    }
  }
} /* namespace alpiparsertools */
