/*
 * Addition.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Addition.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      Addition::Addition(NumericExpression* left, NumericExpression* right) :
          BinOp(left, right) {
      }

      Addition::~Addition() {
      }

      std::string Addition::toString() const {
        return "(" + left->toString() + ") + (" + right->toString()
            + ")";
      }
      bool Addition::isConstant() const {
        return false;
      }
    }
  }
}
/* namespace alpiparsertools */
