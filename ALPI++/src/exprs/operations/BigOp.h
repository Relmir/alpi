/*
 * BigOp.h
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef BIGOP_H_
#define BIGOP_H_

#include "../ExpressionNode.h"
#include "../indexing/IndexedElement.h"
#include "../indexing/IndexingMultiRelation.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      /** \brief The representation of indexed operations
       */
      template<class P>
      class BigOp: public P, public indexing::IndexedElement {
        protected:
          /** \brief The element the operation is performed on
           */
          P* node;
        public:
          /** \brief Create an operation running on index
           *
           * \param ind the indexing
           * \param node the element to run on
           *
           */
          BigOp(indexing::IndexingMultiRelation* ind, P* node) :
              node(node) {
            setIndex(ind);
          }
          /** \brief Get the inner expression this operation performs on
           *
           * \return ExpressionNode* the inner expression
           *
           */
          P* getExpression() {
            return node;
          }
          // Inherited
          virtual bool isInteger() const {
            return node->isInteger();
          }
          virtual bool isConstant() const = 0;
          virtual bool isIndependant() const {
            return node->isIndependant();
          }
          virtual bool isVector() const {
            return node->isVector();
          }
          virtual ~BigOp() {
          }
      };
    }
  }
} /* namespace alpiparsertools */

#endif /* BIGOP_H_ */
