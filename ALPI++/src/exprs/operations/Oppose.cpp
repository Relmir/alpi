/*
 * Oppose.cpp
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Oppose.h"

namespace alpi {
  namespace exprs {
    namespace operations {

      Oppose::~Oppose() {
      }

      Oppose::Oppose(numerics::NumericExpression* inside) :
          UnOp(inside) {
      }

      std::string Oppose::toString() const {
        return "-(" + argument->toString() + ")";
      }
      bool Oppose::isConstant() const {
        return argument->isConstant();
      }
    }
  }
}
