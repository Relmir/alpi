/*
 * Addition.h
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef ADDITION_H_
#define ADDITION_H_

#include <string>

#include "BinOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      using namespace numerics;

      /** \brief The addition representation
       */
      class Addition: public BinOp<numerics::NumericExpression, numerics::NumericExpression> {
        public:
          /** \brief Create an addition
           *
           * \param left the first term
           * \param right the second term
           *
           */
          Addition(NumericExpression* left, NumericExpression* right);
          virtual std::string toString() const;
          virtual bool isConstant() const;
          int priority() const {
            return 2;
          }
          virtual ~Addition();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* ADDITION_H_ */
