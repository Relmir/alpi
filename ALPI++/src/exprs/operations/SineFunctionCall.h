/*
 * SineFunctionCall.h
 *
 *  Created on: Jan 22, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef SINEFUNCTIONCALL_H_
#define SINEFUNCTIONCALL_H_

#include "UnOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace builtin {
      /** \brief The sine function call representation
       */
      class SineFunctionCall: public operations::UnOp<
          numerics::NumericExpression, numerics::NumericExpression> {
        public:
          /** \brief Create a call to the sine function
           *
           * \param in the argument
           *
           */
          SineFunctionCall(numerics::NumericExpression* in);
          // Inherited
          std::string toString() const;
          bool isConstant() const;
          bool isInteger() const;
          int priority() const {
            return 0;
          }
          virtual ~SineFunctionCall();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* SINEFUNCTIONCALL_H_ */
