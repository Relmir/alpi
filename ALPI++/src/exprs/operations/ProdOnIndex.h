/*
 * ProdOnIndex.h
 *
 *  Created on: Jan 14, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef PRODONINDEX_H_
#define PRODONINDEX_H_

#include "BigOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace operations {
      /** \brief The big product operation representation
       */
      class ProdOnIndex: public BigOp<numerics::NumericExpression> {
      public:
        /** \brief Create a big product operation
         *
         * \param index the indexing for this big product
         * \param node the expression to be multiplied
         */
        ProdOnIndex (indexing::IndexingMultiRelation* index,
                     numerics::NumericExpression *node);
        // Inherited
        std::string toString() const;
        bool isConstant() const;
        int priority() const {
          return 0;
        }
        ~ProdOnIndex();
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* PRODONINDEX_H_ */
