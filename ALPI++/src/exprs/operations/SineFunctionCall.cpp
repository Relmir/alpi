/*
 * SineFunctionCall.cpp
 *
 *  Created on: Jan 22, 2014
 *      Author: Emmanuel BIGEON
 */

#include "SineFunctionCall.h"

namespace alpi {
  namespace exprs {
    namespace builtin {
      SineFunctionCall::SineFunctionCall(numerics::NumericExpression* in) :
          UnOp(in) {
      }

      SineFunctionCall::~SineFunctionCall() {
      }

      std::string SineFunctionCall::toString() const {
        return "sin(" + argument->toString() + ")";
      }
      bool SineFunctionCall::isConstant() const {
        return false;
      }
      bool SineFunctionCall::isInteger() const {
        return false;
      }
    }
  }
}
