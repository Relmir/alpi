/*
 * IndexingMultiRelation.h
 *
 *  Created on: Jan 20, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef INDEXINGMULTIRELATION_H_
#define INDEXINGMULTIRELATION_H_

#include <string>
#include <vector>

#include "../symbol/IndexVariable.h"
#include "Index.h"

namespace alpi {
  namespace exprs {
    namespace booleans {
      class BoolOp;
    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */

namespace alpi {
  namespace exprs {
    namespace symbol {
      class IndexVariable;
    }
    namespace indexing {
      /** \brief An array of indexes and a boolean relation to reduce
       *      the resulting index
       */
      class IndexingMultiRelation {
      public:
        /** \brief Create an empty relation
         */
        IndexingMultiRelation();
        /** \brief Get all the indexes of this relation
         *
         * \return std::vector<Index*> the indexes
         *
         */
        std::vector<Index *> getIndexingSets();
        /** \brief Get the index variables of this relation
         *
         * \return std::vector<symbol::IndexVariable*> the variables
         *
         */
        std::vector<symbol::IndexVariable *> getIndexingVariables();
        /** \brief Get the reducing relation
         *
         * \return ExpressionNode* the reducing relation
         *
         */
        booleans::BoolOp *getPredicate();
        /** \brief Add a relation
         *
         * \param v the variable representing the relation
         */
        void addRelation (alpi::exprs::symbol::IndexVariable *v);
        /** \brief Set the reducing expression
         *
         * \param node the reducing expression
         */
        void setPredicate (booleans::BoolOp *node);
        /** \brief Get a string representation
         *
         * \return std::string the representation
         *
         */
        std::string toString();
        virtual ~IndexingMultiRelation();
      private:
        std::vector<alpi::exprs::symbol::IndexVariable *> rels;
        booleans::BoolOp *predicate;
      };
    }
  }
} /* namespace alpiparsertools */

#endif /* INDEXINGMULTIRELATION_H_ */
