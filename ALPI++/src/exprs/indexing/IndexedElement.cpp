/*
 * IndexedElement.cpp
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#include "IndexedElement.h"

#include <vector>
#include <iostream>

#include "Index.h"
#include "IndexingMultiRelation.h"

namespace alpi {
  namespace exprs {
    namespace indexing {
      using namespace std;

      IndexedElement::IndexedElement() :
          index(NULL) {

      }
      int IndexedElement::dimension() const {
        if (index == NULL) {
          return 0;
        }
        int d = 0;
        d = index->getIndexingSets().size();
        return d;
      }
      int IndexedElement::length(unsigned int i) const {
        if (index == NULL) {
          if (i == 0) return 1;
          return 0;
        } else {
          if (i > index->getIndexingSets().size()
              || index->getIndexingSets().size() == 0) {
            if (i == 0) return 1;
            return 0;
          }
        }
        int j = index->getIndexingSets()[i]->length();
        return j;
      }
      void IndexedElement::setIndex(IndexingMultiRelation* ind) {
        if (ind->getIndexingVariables().size()
            == 0&& ind->getPredicate()== NULL) {
          index = NULL;
        } else index = ind;
      }
      IndexingMultiRelation* IndexedElement::getIndexing() {
        return index;
      }

      IndexedElement::~IndexedElement() {
      }
    }
  }
}
