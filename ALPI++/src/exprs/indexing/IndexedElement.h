/*
 * IndexedElement.h
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef INDEXEDELEMENT_H_
#define INDEXEDELEMENT_H_

#include <string>

namespace alpi {
  namespace exprs {
    namespace indexing {
      class IndexingMultiRelation;
      /** \brief An indexed element is an element that refers to an indexing relation
       */
      class IndexedElement {
      public:
        /** \brief Create an indexed element
         */
        IndexedElement();
        /** \brief Get the length of the element along its ith
         *      dimension.
         *
         * \param i the dimension to look at the length to
         * \return int the length along that dimension
         */
        virtual int length (unsigned int i) const;
        /** \brief Get the number of dimensions
         *
         * \return virtual int the number of dimensions
         */
        virtual int dimension() const;
        /** \brief Set the index relation
         *
         * \param ind the relation
         */
          virtual void setIndex (IndexingMultiRelation *ind);
        /** \brief Get the index relation
         *
         * \return IndexingMultiRelation* the index array
         *
         */
        IndexingMultiRelation *getIndexing();
        /** \brief Get a string representation
         *
         * \return virtual std::string the string representation
         *
         */
        virtual std::string toString() const = 0;
        virtual ~IndexedElement();
      protected:
        IndexingMultiRelation *index;
      };
    }
  }
} /* namespace alpiparsertools */

#endif /* INDEXEDELEMENT_H_ */
