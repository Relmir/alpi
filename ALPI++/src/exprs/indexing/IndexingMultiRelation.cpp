/*
 * IndexingMultiRelation.cpp
 *
 *  Created on: Jan 20, 2014
 *      Author: Emmanuel BIGEON
 */

#include "IndexingMultiRelation.h"

//#include <iostream>
#include <iterator>

#include "../booleans/BoolOp.h"
//#include "../symbol/IndexVariable.h"

namespace alpi { namespace exprs{namespace indexing{

  using namespace std;
  using namespace symbol;


  IndexingMultiRelation::IndexingMultiRelation() :
      predicate(NULL) {
  }

  IndexingMultiRelation::~IndexingMultiRelation() {
  }

  std::vector<Index*> IndexingMultiRelation::getIndexingSets() {
    vector<IndexVariable*>::iterator it;
    vector<Index*> l;
    vector<IndexVariable*> li = rels;
    for (it = li.begin(); it != li.end(); ++it) {
      l.push_back((*it)->getOverIndex());
    }
    return l;
  }
  std::vector<IndexVariable*> IndexingMultiRelation::getIndexingVariables() {
    return rels;
  }
  booleans::BoolOp* IndexingMultiRelation::getPredicate() {
    return predicate;
  }
  void IndexingMultiRelation::setPredicate(booleans::BoolOp* node) {
    predicate = node;
  }
  void IndexingMultiRelation::addRelation(IndexVariable* v) {
    rels.push_back(v);
  }

  string IndexingMultiRelation::toString() {
    if (rels.empty()) {
      return "";
    }
    string s = "{";
    vector<IndexVariable*>::iterator it = rels.begin();
    s += (*it)->toString();
    ++it;
    for (; it != rels.end(); ++it) {
      s += ", " + (*it)->toString();
    }
    if (predicate != NULL) {
      s += ", " + predicate->toString();
    }
    return s + "}";
  }
}
}}
