/*
 * Index.h
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef INDEX_H_
#define INDEX_H_

#include "../ExpressionNode.h"
#include "../symbol/Symbol.h"

namespace alpi {
  namespace exprs {
    namespace indexing {
      /** \brief An index is a symbol that represent a set of integer.
       */
      class Index: public symbol::Symbol {
        public:
          /** \brief Create an anonymous index reduced to one as its
           *      only element
           */
          Index();
          /** \brief Create an index with the specified range and step.
           *
           * If the step is null (or unspecified), this class behaves
           * the same as if the step is one.
           * \param start the first element of this index
           * \param stop the biggest element of this index
           * \param step the distance between elements in this index
           */
          Index(numerics::NumericExpression* start,
              numerics::NumericExpression* stop,
              numerics::NumericExpression* step = NULL);
          /** \brief Create a named index reduced to one.
           *
           * \param name std::string the name of the index
           *
           */
          Index(std::string name);
          /** \brief Set the starting, stopping and stepping values.
           *
           * This method sets the start, stop and step values to be the
           * same as the ones of the specified index
           * \param ind the index to take the values from
           */
          void setDefinition(Index* ind);
          /** \brief Get the starting value
           *
           * \return ExpressionNode* the start value
           *
           */
          numerics::NumericExpression* getStart();
          /** \brief Get the stopping value
           *
           * \return ExpressionNode* the stop value
           *
           */
          numerics::NumericExpression* getStop();
          /** \brief Get the stepping value
           *
           * \return ExpressionNode* the step value
           *
           */
          numerics::NumericExpression* getStep();
          /** \brief Test equality between indexes
           *
           * The equality between indexes is given by the equality of
           * the start, stop and step bounds.
           * \param other Index& the other index
           * \return bool if they define the same sets
           *
           */
          bool operator==(Index& other);
          /** \brief Get the length of this index.
           *
           * The length is computed as (stop-start+1)/step.
           * \return int the length
           *
           */
          int length() const;
          // Inherited
          std::string toString() const;
          bool isIndependant() const;
          bool isConstant() const;
          bool isInteger() const;
          bool isVector() const;
          int priority() const {
            return -1;
          }
          virtual ~Index();
        private:
          numerics::NumericExpression *start, *stop, *step;
      };
    }
  }
} /* namespace alpiparsertools */

#endif /* INDEX_H_ */
