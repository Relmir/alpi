/*
 * Index.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Index.h"
#include "../../evaluators/constant_evaluator_relative.h"
#include "../numerics/RelativeConstant.h"
#include <iostream>

namespace alpi {
  namespace exprs {
    namespace indexing {
  using namespace alpi::exprs::numerics;


  Index::Index() :
      Symbol(""), start(new RelativeConstant(1)),
          stop(new RelativeConstant(1)), step(NULL) {
  }

  Index::Index(NumericExpression* start, NumericExpression* stop,
      NumericExpression* step) :
      Symbol(""), start(start), stop(stop), step(step) {
  }

  int Index::length() const {
    int size = (alpi_eval::constantEvaluateRelative(stop)->toInt()
        - alpi_eval::constantEvaluateRelative(start)->toInt() + 1);
    if (step != NULL) {
      size /= alpi_eval::constantEvaluateRelative(step)->toInt();
    }
    return size;
  }

  Index::Index(std::string name) :
      Symbol(name), start(NULL), stop(NULL), step(NULL) {
  }

  void Index::setDefinition(Index* ind) {
    if (start == NULL && stop == NULL && step == NULL) {
      start = ind->getStart();
      stop = ind->getStop();
      step = ind->getStep();
    }
  }
  Index::~Index() {
  }

  std::string Index::toString() const {
    std::string base = getName() + " " + start->toString() + ".."
        + stop->toString();
    if (step != NULL) {
      return base + " by " + step->toString();
    }
    return base;
  }

  bool Index::operator==(Index& other) {
    RelativeConstant* stCst = dynamic_cast<RelativeConstant*>(step);
    RelativeConstant* ostCst = dynamic_cast<RelativeConstant*>(other
        .step);
    RelativeConstant* stopCst = dynamic_cast<RelativeConstant*>(stop);
    RelativeConstant* ostopCst = dynamic_cast<RelativeConstant*>(other
        .stop);
    RelativeConstant* startCst =
        dynamic_cast<RelativeConstant*>(start);
    RelativeConstant* ostartCst =
        dynamic_cast<RelativeConstant*>(other.start);
    if (step == NULL || *stCst == RelativeConstant(1)) {
      if (other.step == NULL || (*(ostCst)) == RelativeConstant(1)) {
        if (stopCst != NULL && ostopCst != NULL && startCst != NULL
            && ostartCst != NULL) {
          return (*startCst) == (*(ostartCst))
              || (*stopCst) == (*(ostopCst));
        }
      }
    }
    return false;
  }

  NumericExpression* Index::getStart() {
    return start;
  }
  NumericExpression* Index::getStop() {
    return stop;
  }
  NumericExpression* Index::getStep() {
    return step;
  }
  bool Index::isIndependant() const {
    bool b = true;
    if (step != NULL) {
      b = step->isIndependant();
    }
    return b && start->isIndependant() && stop->isIndependant();
  }

  bool Index::isConstant() const {
    // Normally should never be called
    std::cerr << "is Index ever constant ?" << std::endl;
    return false;
  }
  bool Index::isInteger() const {
    std::cerr << "can Index be integer ?" << std::endl;
    return false;
  }
  bool Index::isVector() const {
    // Normally should never be called
    std::cerr << "Is Index ever variable ?" << std::endl;
    return false;
  }
}
}}
