/*
 * AssignmentMap.cpp
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

namespace alpi {
  namespace exprs {
    using namespace indexing;

    template<class T>
    AssignmentMap<T>::AssignmentMap() :
        defolt(NULL), defoltIndexing(NULL) {
    }
    template<class T>
    AssignmentMap<T>::AssignmentMap(T* defo, indexing::IndexingMultiRelation* ind) :
        defolt(defo), defoltIndexing(ind) {
    }
    template<class T>
    void AssignmentMap<T>::addAssignement(IndexingMultiRelation* mask,
        T* node) {
      maskedVals[mask] = node;
    }
    template<class T>
    AssignmentMap<T>::~AssignmentMap() {
    }
    template<class T>
    bool AssignmentMap<T>::isIndependant() const {
      bool r = true;
      if (defolt != NULL) {
        r = defolt->isIndependant();
      }
      std::map<IndexingMultiRelation*, T*> m = maskedVals;
      typename std::map<IndexingMultiRelation*, T*>::iterator it;
      for (it = m.begin(); it != m.end(); ++it) {
        r = r && it->second->isIndependant();
      }
      return r;
    }
    template<class T>
    bool AssignmentMap<T>::isConstant() const {
      return false;
    }
    template<class T>
    bool AssignmentMap<T>::isVector() const {
      return true;
    }
    template<class T>
    bool AssignmentMap<T>::isInteger() const {
      bool r = true;
      if (defolt != NULL) {
        r = defolt->isInteger();
      }
      std::map<IndexingMultiRelation*, T*> m = maskedVals;
      typename std::map<IndexingMultiRelation*, T*>::iterator it;
      for (it = m.begin(); it != m.end(); ++it) {
        r = r && it->second->isInteger();
      }
      return r;
    }
    template<class T>
    std::string AssignmentMap<T>::toString() const {
      std::string res = "(";
      std::map<IndexingMultiRelation*, T*> m = maskedVals;
      for (typename std::map<IndexingMultiRelation*, T*>::iterator it =
          m.begin(); it != m.end(); ++it) {
        res += it->first->toString() + " = " + it->second->toString()
            + ", ";
      }
      if (defolt == NULL) {
        res += "* = 0";
      } else {
        res += "* = " + defolt->toString();
      }
      return res + ")";
    }
    template<class T>
    T* AssignmentMap<T>::getDefault() {
      return defolt;
    }
    template<class T>
    indexing::IndexingMultiRelation* AssignmentMap<T>::getDefaultIndexing() {
      return defoltIndexing;
    }
    template<class T>
    T* AssignmentMap<T>::getAssignment(
        IndexingMultiRelation* index) {
      if (maskedVals.find(index) != maskedVals.end()) return maskedVals[index];
      return defolt;
    }
    template<class T>
    std::list<IndexingMultiRelation*> AssignmentMap<T>::getIndexingRelations() {
      std::list<IndexingMultiRelation*> l;
      std::map<IndexingMultiRelation*, T*> m = maskedVals;
      for (typename std::map<IndexingMultiRelation*, T*>::iterator it =
          m.begin(); it != m.end(); ++it) {
        l.push_back(it->first);
      }
      return l;
    }
  }
}
