/*
 * ExpressionNode.h
 *
 *  Created on: Jan 12, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef EXPRESSIONNODE_H_
#define EXPRESSIONNODE_H_

#include <string>
#include <list>

namespace alpi {
  namespace exprs {
    /** \brief The representation of an expression
     */
    class ExpressionNode {
      public:
        /** \brief A conversion to a string representation
         *
         * \return virtual std::string the string representation of this node
         */
        virtual std::string toString() const = 0;
        /** \brief Test if this expression is a constant.
         *
         * Usually, the constant status is geiven by the fact that
         * this node is actually a leaf representing a numeric constant
         *
         * \return virtual bool if it is a constant
         */
        virtual bool isConstant() const = 0;
        /** \brief Test if this expression is a dependant on decision
         *      variables.
         *
         * \return virtual bool if it is independant of Decision
         *      Variables
         */
        virtual bool isIndependant() const = 0;
        /** \brief Test if this expression is representing an integer.
         *
         * Actually tests if this is an integer or a vector of integer.
         *
         * \return virtual bool if it is an integer (or a vector of
         *      integers)
         */
        virtual bool isInteger() const = 0;
        /** \brief Test if this expression is a vector.
         *
         * \return virtual bool if it is a vector
         */
        virtual bool isVector() const = 0;
        /** \brief gives the priority of this operator.
         *
         * This is for the purpose of parenthesis position
         *
         * \return virtual int the priority
         */
        virtual int priority() const = 0;
        virtual ~ExpressionNode() {
        }
    };
  }
} /* namespace alpiparsertools */

#endif /* EXPRESSIONNODE_H_ */
