/*
 * QualifiedExpression.h
 *
 *  Created on: Jan 22, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef QUALIFIEDEXPRESSION_H_
#define QUALIFIEDEXPRESSION_H_

#include "ExpressionNode.h"
#include "Qualifier.h"

namespace alpi {
  namespace exprs {
    /** \brief Representation of an expression followed by arguments.
     */
    template<class T>
    class QualifiedExpression: public T {
      public:
        /** \brief Create a qualified expression
         *
         * \param val the qualified expression
         * \param qual the qualifer
         *
         */
        QualifiedExpression(T* val, Qualifier* qual);
        /** \brief Get the qualified part of this element
         *
         * \return ExpressionNode* the qualified part
         *
         */
        T* getQualified();
        /** \brief Get the qulifying part of this element
         *
         * \return Qualifier* the qualifying part
         *
         */
        Qualifier* getQualifier();
        // Inherited
        std::string toString() const;
        bool isConstant() const;
        bool isIndependant() const;
        bool isInteger() const;
        bool isVector() const;
        int priority() const {
          return 0;
        }
        virtual ~QualifiedExpression();
      private:
        /** \brief The qualified part
         */
        T* qualified;
        /** \brief The qualifying part
         */
        Qualifier* qualifier;
    };

  }
} /* namespace alpiparsertools */

#include "QualifiedExpression.tpp"

#endif /* QUALIFIEDEXPRESSION_H_ */
