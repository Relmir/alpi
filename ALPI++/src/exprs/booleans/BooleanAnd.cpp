/*
 * BooleanAnd.cpp
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

#include "BooleanAnd.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      BooleanAnd::BooleanAnd(BoolOp* lhs, BoolOp* rhs) :
          ExpressionNode(), BinOp<BoolOp, BoolOp>(lhs, rhs) {
      }

      BooleanAnd::~BooleanAnd() {
      }
      std::string BooleanAnd::toString() const {
        return "(" + left->toString() + ") .AND. ("
            + right->toString() + ")";
      }
      bool BooleanAnd::isConstant() const {
        return false;
      }
    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */
