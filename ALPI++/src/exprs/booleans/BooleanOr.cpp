/*
 * BooleanOr.cpp
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

#include "BooleanOr.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      BooleanOr::BooleanOr(BoolOp* lhs, BoolOp* rhs) :
          ExpressionNode(), BinOp<BoolOp, BoolOp>(lhs, rhs) {
      }

      BooleanOr::~BooleanOr() {
      }
      std::string BooleanOr::toString() const {
        return "(" + left->toString() + ") .OR. ("
            + right->toString() + ")";
      }
      bool BooleanOr::isConstant() const {
        return false;
      }

    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */
