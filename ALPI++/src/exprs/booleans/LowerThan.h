/*
 * LowerThan.h
 *
 *  Created on: Feb 4, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef LOWERTHAN_H_
#define LOWERTHAN_H_

#include "BoolOp.h"
#include "../operations/BinOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      class LowerThan: public alpi::exprs::operations::BinOp<numerics::NumericExpression, BoolOp> {
        public:
          LowerThan(numerics::NumericExpression* up, numerics::NumericExpression* low);
          // Inherited
          std::string toString() const;
          bool isConstant() const;
          bool isVector() const {
            return BinOp<numerics::NumericExpression, BoolOp>::isVector();
          }
          bool isInteger() const {
            return BoolOp::isInteger();
          }
          bool isIndependant() const {
            return BinOp<numerics::NumericExpression, BoolOp>::isIndependant();
          }
          int priority() const {
            return -1;
          }
          virtual ~LowerThan();
      };

    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */

#endif /* LOWERTHAN_H_ */
