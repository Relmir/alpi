/*
 * LowerThan.cpp
 *
 *  Created on: Feb 4, 2014
 *      Author: Emmanuel BIGEON
 */

#include "LowerThan.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      LowerThan::LowerThan(numerics::NumericExpression* low,
          numerics::NumericExpression* up) :
          BinOp(low, up) {
      }

      LowerThan::~LowerThan() {
      }
      std::string LowerThan::toString() const {
        return left->toString() + " < " + right->toString();
      }
      bool LowerThan::isConstant() const {
        return false;
      }

    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */
