/*
 * BoolOp.h
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef BOOLOP_H_
#define BOOLOP_H_

#include "../ExpressionNode.h"

namespace alpi {
  namespace exprs {
    namespace booleans {
      /** \brief Representation of booleans
       */
      class BoolOp: virtual public ExpressionNode {
      public:
        // Inherited
        bool isInteger() const {
          return false;
        }
        bool isIndependant() const = 0;
        virtual ~BoolOp() {}
      };
    }
  }
} /* namespace alpiparsertools */

#endif /* BOOLOP_H_ */
