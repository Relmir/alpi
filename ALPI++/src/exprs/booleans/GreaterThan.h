/*
 * GreaterThan.h
 *
 *  Created on: Jan 29, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef GREATERTHAN_H_
#define GREATERTHAN_H_

#include "../operations/BinOp.h"
#include "BoolOp.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      /** \brief The representation of the order test greate than.
       */
      class GreaterThan: public operations::BinOp<
          numerics::NumericExpression, BoolOp> {
        public:
          /** \brief Creates a GreaterThan testing relation
           *
           * \param low the left-hand side of the test
           * \param up the right hand side of the test
           */
          GreaterThan(numerics::NumericExpression* up,
              numerics::NumericExpression* low);
          // Inherited
          std::string toString() const;
          bool isConstant() const;
          bool isVector() const {
            return BinOp<numerics::NumericExpression, BoolOp>::isVector();
          }
          bool isInteger() const {
            return BoolOp::isInteger();
          }
          bool isIndependant() const {
            return BinOp<numerics::NumericExpression, BoolOp>::isIndependant();
          }
          int priority() const {
            return -1;
          }
          virtual ~GreaterThan();
      };

    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */

#endif /* GREATERTHAN_H_ */
