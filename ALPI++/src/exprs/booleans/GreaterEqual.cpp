/*
 * GreaterEqual.cpp
 *
 *  Created on: Jan 29, 2014
 *      Author: Emmanuel BIGEON
 */

#include "GreaterEqual.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      GreaterEqual::GreaterEqual(numerics::NumericExpression* up,
          numerics::NumericExpression* low) :
          operations::BinOp<numerics::NumericExpression, BoolOp>(up, low) {
      }

      GreaterEqual::~GreaterEqual() {
      }

      std::string GreaterEqual::toString() const {
        return left->toString() + " >= " + right->toString();
      }
      bool GreaterEqual::isConstant() const {
        return false;
      }
    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */
