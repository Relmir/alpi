/*
 * LowerEqual.cpp
 *
 *  Created on: Feb 4, 2014
 *      Author: Emmanuel BIGEON
 */

#include "LowerEqual.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      LowerEqual::LowerEqual(numerics::NumericExpression* up,
          numerics::NumericExpression* low) :
              operations::BinOp<numerics::NumericExpression, BoolOp>(
                  up, low) {
      }

      LowerEqual::~LowerEqual() {
        // TODO Auto-generated destructor stub
      }
      std::string LowerEqual::toString() const {
        return left->toString() + " <= " + right->toString();
      }
      bool LowerEqual::isConstant() const {
        return false;
      }

    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */
