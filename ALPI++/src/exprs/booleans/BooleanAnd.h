/*
 * BooleanAnd.h
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef BOOLEANAND_H_
#define BOOLEANAND_H_

#include "../operations/BinOp.h"
#include "BoolOp.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      /** \brief The representation of the boolean and operation.
       */
      class BooleanAnd: public operations::BinOp<BoolOp, BoolOp> {
        public:
          /** \brief Create the and operation on booleans.
           *
           * \param lhs the left-hand side
           * \param rhs the right-hand side
           * \return an and operation representation
           *
           */
          BooleanAnd(BoolOp* lhs, BoolOp* rhs);
          // Inherited
          std::string toString() const;
          bool isConstant() const;
          bool isVector() const {
            return BinOp<BoolOp, BoolOp>::isVector();
          }
          bool isInteger() const {
            return BoolOp::isInteger();
          }
          bool isIndependant() const {
            return BinOp<BoolOp, BoolOp>::isIndependant();
          }
          int priority() const {
            return 1;
          }
          virtual ~BooleanAnd();
      };

    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */

#endif /* BOOLEANAND_H_ */
