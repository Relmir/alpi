/*
 * BooleanAnd.h
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef BOOLEANOR_H_
#define BOOLEANOR_H_

#include "../operations/BinOp.h"
#include "BoolOp.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      /** \brief The representation of the boolean and operation.
       */
      class BooleanOr: public operations::BinOp<BoolOp, BoolOp> {
        public:
          /** \brief Create the and operation on booleans.
           *
           * \param lhs the left-hand side
           * \param rhs the right-hand side
           * \return an and operation representation
           *
           */
          BooleanOr(BoolOp* lhs, BoolOp* rhs);
          // Inherited
          std::string toString() const;
          bool isConstant() const;
          bool isVector() const {
            return BinOp<BoolOp, BoolOp>::isVector();
          }
          bool isInteger() const {
            return BinOp<BoolOp, BoolOp>::isInteger();
          }
          bool isIndependant() const {
            return BinOp<BoolOp, BoolOp>::isIndependant();
          }
          int priority() const {
            return 1;
          }
          virtual ~BooleanOr();
      };

    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */

#endif /* BOOLEANOR_H_ */
