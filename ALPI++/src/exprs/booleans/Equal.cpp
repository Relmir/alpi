/*
 * Equal.cpp
 *
 *  Created on: Jan 29, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Equal.h"

#include <string>


namespace alpi {
  namespace exprs {
    namespace booleans {

      Equal::Equal(numerics::NumericExpression* up,
          numerics::NumericExpression* low) :
          operations::BinOp<numerics::NumericExpression, BoolOp>(up, low) {
      }

      Equal::~Equal() {
      }

      std::string Equal::toString() const {
        return left->toString() + " == " + right->toString();
      }
      bool Equal::isConstant() const {
        return false;
      }
    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */
