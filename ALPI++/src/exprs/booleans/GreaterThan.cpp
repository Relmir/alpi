/*
 * GreaterThan.cpp
 *
 *  Created on: Jan 29, 2014
 *      Author: Emmanuel BIGEON
 */

#include "GreaterThan.h"

namespace alpi {
  namespace exprs {
    namespace booleans {

      GreaterThan::GreaterThan(numerics::NumericExpression* up,
          numerics::NumericExpression* low) :
          operations::BinOp<numerics::NumericExpression, BoolOp>(up, low) {
      }

      GreaterThan::~GreaterThan() {
      }

      std::string GreaterThan::toString() const {
        return left->toString() + " > " + right->toString();
      }
      bool GreaterThan::isConstant() const {
        return false;
      }
    } /* namespace booleans */
  } /* namespace exprs */
} /* namespace alpi */
