/*
 * Qualifier.cpp
 *
 *  Created on: Jan 12, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Qualifier.h"
#include <iostream>
#include <sstream>

namespace alpi {
  namespace exprs {

    using namespace std;
    using namespace alpi::exprs::numerics;


    Qualifier::Qualifier(std::list<NumericExpression*> args) :
        arguments(args) {
    }

    Qualifier::~Qualifier() {
    }

    std::list<NumericExpression*> Qualifier::getExpressions() {
      return arguments;
    }
    std::string Qualifier::toString() const {
      if (arguments.empty()) {
        return "()";
      }
      ostringstream oss;
      oss << "(" << arguments.front()->toString();
      list<NumericExpression*> el = arguments;
      list<NumericExpression*>::iterator it = el.begin();
      ++it;
      for (; it != el.end(); ++it) {
        oss << ", " << (*it)->toString();
      }
      oss << ")";
      return oss.str();
    }
    bool Qualifier::isIndependant() const {
      list<NumericExpression*> el = arguments;
      for (list<NumericExpression*>::iterator it = el.begin();
          it != el.end(); ++it) {
        if (!(*it)->isIndependant()) {
          return false;
        }
      }
      return true;
    }
  }
}
