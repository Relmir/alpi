/*
 * Qualifier.h
 *
 *  Created on: Jan 12, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef QUALIFIER_H_
#define QUALIFIER_H_

#include <list>
#include "../exprs/numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    /** \brief A list of argument qualifying an expression.
     */
    class Qualifier {
      public:
        /** \brief Create the list
         *
         * \param args std::list<ExpressionNode*> the arguments
         *
         */
        Qualifier(std::list<numerics::NumericExpression*> args);
        /** \brief Get the arguments
         *
         * \return std::list<ExpressionNode*> the arguments
         *
         */
        std::list<numerics::NumericExpression*> getExpressions();
        /** \brief Get a string representation of this qualifying
         *      element.
         *
         * \return std::string the string representation
         *
         */
        std::string toString() const;
        /** \brief Test independance of this element to
         *      DecisionVariable.
         *
         * \return bool if there are no DecisionVariable in arguments
         *
         */
        bool isIndependant() const;
        virtual ~Qualifier();
      private:
        std::list<numerics::NumericExpression*> arguments; //!< The arguments
    };

  }
} /* namespace alpiparsertools */

#endif /* QUALIFIER_H_ */
