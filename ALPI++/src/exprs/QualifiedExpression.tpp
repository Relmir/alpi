namespace alpi {
  namespace exprs {

    template<class T>
    QualifiedExpression<T>::QualifiedExpression(T* n,
        Qualifier* q) :
        qualified(n), qualifier(q) {
    }

    template<class T>
    QualifiedExpression<T>::~QualifiedExpression() {
    }

    template<class T>
    T* QualifiedExpression<T>::getQualified() {
      return qualified;
    }
    template<class T>
    Qualifier* QualifiedExpression<T>::getQualifier() {
      return qualifier;
    }
    template<class T>
    std::string QualifiedExpression<T>::toString() const {
      return qualified->toString() + qualifier->toString();
    }
    template<class T>
    bool QualifiedExpression<T>::isConstant() const {
      return false;
    }
    template<class T>
    bool QualifiedExpression<T>::isIndependant() const {
      return qualified->isIndependant() && qualifier->isIndependant();
    }
    template<class T>
    bool QualifiedExpression<T>::isInteger() const {
      return qualified->isInteger();
    }
    template<class T>
    bool QualifiedExpression<T>::isVector() const {
      // XXX what if we have an expression of dimension 2 qualified only by one element ?
      return false;
    }
  }
}
