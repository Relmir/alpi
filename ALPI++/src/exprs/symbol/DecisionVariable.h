/*
 * DecisionVariable.h
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef DECISIONVARIABLE_H_
#define DECISIONVARIABLE_H_

#include "Variable.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace symbol {
      /** \brief The representation of a variable of the problem
       */
      class DecisionVariable: public exprs::symbol::Variable<numerics::NumericExpression> {
          exprs::numerics::NumericExpression *lower, *upper, *precision;
        public:
          /** \brief Create a decision variable
           *
           * \param name the name of the variable
           */
          DecisionVariable(std::string name);
          /** \brief Get the lower bound of this variable
           *
           * \return numerics::NumericExpression* the lower bound
           */
          exprs::numerics::NumericExpression *getLowerBound();
          /** \brief Get the upper bound of this variable
           *
           * \return numerics::NumericExpression* the upper bound
           */
          exprs::numerics::NumericExpression *getUpperBound();
          /** \brief Get the precision
           *
           * \return exprs::numerics::NumericExpression* the precision
           *
           */
          exprs::numerics::NumericExpression *getPrecision();
          /** \brief Set the lower bound
           *
           * \param node the value to set
           */
          void setLowerBound(exprs::numerics::NumericExpression *node);
          /** \brief Set the upper bound
           *
           * \param node the value to set
           */
          void setUpperBound(exprs::numerics::NumericExpression *node);
          /** \brief Set the precision
           *
           * \param node the value to set
           */
          void setPrecision(exprs::numerics::NumericExpression *node);
          // Inherited
          std::string toString() const;
          bool isConstant() const;
          bool isIndependant() const;
          ~DecisionVariable();
      };
    }
  } /* namespace alpiparsertools */
}
#endif /* DECISIONVARIABLE_H_ */
