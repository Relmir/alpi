/*
 * Symbol.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Symbol.h"

namespace alpi {
  namespace exprs {
    namespace symbol {
      Symbol::Symbol(std::string name) :
          name(name) {
      }
      Symbol::~Symbol() {
      }
      std::string Symbol::getName() const {
        return name;
      }
      std::string Symbol::toString() const {
        return name;
      }
//      bool operator==(Symbol const &lhs, Symbol const &rhs) {
//        return &lhs == &rhs;
//      }
    }
  }
}
