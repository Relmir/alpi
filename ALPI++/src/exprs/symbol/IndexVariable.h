/*
 * IndexVariable.h
 *
 *  Created on: Jan 15, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef INDEXVARIABLE_H_
#define INDEXVARIABLE_H_

#include <string>
#include "Variable.h"
#include "../numerics/NumericExpression.h"


namespace alpi {
  namespace exprs {
    namespace indexing {
      class Index;
    }
    namespace symbol {
      /** \brief A variable that is to go through an index
       */
      class IndexVariable: public Variable<numerics::NumericExpression> {
      public:
        /** \brief Create an index variable
         *
         * \param name the name of the variable
         */
        IndexVariable (std::string name);
        /** \brief Set the index this variable is associated to
         *
         * \param ind the index to associate with
         */
        void setOverIndex (alpi::exprs::indexing::Index *ind);
        /** \brief Get the index associated with this variable
         *
         * \return alpi::exprs::indexing::Index*
         */
        alpi::exprs::indexing::Index *getOverIndex();
        // Inherited
        std::string toString() const;
        bool isConstant() const;
        bool isIndependant() const;
        int priority() const {
          return 0;
        }
        ~IndexVariable();
      private:
        alpi::exprs::indexing::Index *overIndex;
      };
    }
  }

} /* namespace alpiparsertools */

#endif /* INDEXVARIABLE_H_ */
