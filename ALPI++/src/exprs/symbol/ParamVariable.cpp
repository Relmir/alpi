/*
 * ParamVariable.cpp
 *
 *  Created on: Jan 15, 2014
 *      Author: Emmanuel BIGEON
 */

#include "ParamVariable.h"
#include "../numerics/RelativeConstant.h"
#include "../indexing/Index.h"

namespace alpi {
  namespace exprs {
    namespace symbol {

      ParamVariable::ParamVariable(std::string name) :
          Variable<numerics::NumericExpression>(name), def(NULL) {
      }

      ParamVariable::~ParamVariable() {
      }

      void ParamVariable::setExpression(numerics::NumericExpression* node) {
        def = node;
      }
      numerics::NumericExpression* ParamVariable::getExpression() {
        return def;
      }
      std::string ParamVariable::toString() const {
        return getName();
      }
      bool ParamVariable::isConstant() const {
        return def->isConstant();
      }
      bool ParamVariable::isIndependant() const {
        return def->isIndependant();
      }
    }
  }
}
