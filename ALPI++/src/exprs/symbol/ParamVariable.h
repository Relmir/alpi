/*
 * ParamVariable.h
 *
 *  Created on: Jan 15, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef PARAMVARIABLE_H_
#define PARAMVARIABLE_H_

#include "Variable.h"
#include "../indexing/IndexingMultiRelation.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace symbol {
      /** \brief A variable representing an auxiliary function or a
       *      constant
       */
      class ParamVariable: public Variable<numerics::NumericExpression> {
      public:
        /** \brief Create a parameter variable
         *
         * \param name the name of the variable
         *
         */
        ParamVariable (std::string name);
        /** \brief Set the definition of the parameter
         *
         * \param node the definition to set
         */
        void setExpression (numerics::NumericExpression *node);
        /** \brief Get the definition of this parameter
         *
         * \return NumericExpression* the definiton
         *
         */
        alpi::exprs::numerics::NumericExpression *getExpression();
        // Inherited
        std::string toString() const;
        bool isConstant() const;
        bool isIndependant() const;
        int priority() const {
          return 0;
        }
        ~ParamVariable();
      private:
        alpi::exprs::numerics::NumericExpression *def;
      };
    }
  }
} /* namespace alpiparsertools */

#endif /* PARAMVARIABLE_H_ */
