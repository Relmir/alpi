/*
 * Variable.h
 *
 *  Created on: Jan 10, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef VARIABLE_H_
#define VARIABLE_H_

#include <string>
#include "../indexing/IndexedElement.h"
#include "Symbol.h"

namespace alpi {
  namespace exprs {
    namespace symbol {
      /** \brief A Variable is a named expression or unknown
       */
      template<class Rep>
      class Variable: public Rep,
          public Symbol,
          public indexing::IndexedElement {
        public:
          /** \brief Create a variable.
           *
           * \param name the variable name
           */
          Variable(std::string name) :
              Symbol(name), integer(false) {
          }
          /** \brief Set the integer status
           *
           * \param integer if this variable represents an integer
           */
          virtual void setInteger(bool integer) {
            this->integer = integer;
          }
          // Inherited
          virtual bool isInteger() const {
            return integer;
          }
          virtual bool isVector() const {
            return dimension() != 0;
          }
          int priority() const {
            return -1;
          }
          virtual std::string toString() const = 0;
          ~Variable() {
          }
        private:
          bool integer;
      };
    }
  }
} /* namespace alpiparsertools */

#endif /* VARIABLE_H_ */
