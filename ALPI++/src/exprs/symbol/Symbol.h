/*
 * Symbol.h
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef SYMBOL_H_
#define SYMBOL_H_
#include <string>
#include "../ExpressionNode.h"
#include "../numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    namespace symbol {
      /**
       * \brief Symbols are named expressions
       */
      class Symbol: virtual public ExpressionNode {
        public:
          /**
           * \brief constructs a symbol
           * \param name string the name of the symbol
           */
          Symbol(std::string name);
          /** \brief Get the symbol name
           * \return string The name of the symbol
           */
          std::string getName() const;
          // Inherited
          virtual std::string toString() const;
          virtual ~Symbol();
        private:
          std::string name;
      };

      /** \brief Test equality between symbols
       *
       * It is actually testing reference equality.
       * \param lhs Symbol const& left-hand side symbol
       * \param rhs Symbol const& right-hand side symbol
       * \return bool if they are equal
       *
       */
//      bool operator==(Symbol<T1> const &lhs, Symbol<T2> const &rhs);
    }
  }
} /* namespace alpiparsertools */

#endif /* SYMBOL_H_ */
