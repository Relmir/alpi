/*
 * IndexVariable.cpp
 *
 *  Created on: Jan 15, 2014
 *      Author: Emmanuel BIGEON
 */

#include "IndexVariable.h"

#include "../indexing/Index.h"
#include "Variable.h"

namespace alpi {
  namespace exprs {
    namespace symbol {

      using namespace std;

      IndexVariable::IndexVariable(std::string name) :
          Variable(name), overIndex(NULL) {
        setInteger(true);
      }

      void IndexVariable::setOverIndex(indexing::Index *ind) {
        overIndex = ind;
      }

      indexing::Index* IndexVariable::getOverIndex() {
        return overIndex;
      }
      IndexVariable::~IndexVariable() {
      }

      std::string IndexVariable::toString() const {
        std::string indStr = "";
        if (overIndex == NULL) {
          indStr += " (UNDEFINED INDEX)";
        } else {
          indStr = " in " + overIndex->toString();
        }
        return getName() + indStr;
      }

      bool IndexVariable::isConstant() const {
        return false;
      }
      bool IndexVariable::isIndependant() const {
        return overIndex->isIndependant();
      }
    }
  }
}
