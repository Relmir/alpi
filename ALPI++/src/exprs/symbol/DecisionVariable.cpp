/*
 * DecisionVariable.cpp
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#include "DecisionVariable.h"

namespace alpi {
  namespace exprs {
    namespace symbol {
      using namespace std;

      DecisionVariable::DecisionVariable(string name) :
          Variable(name), lower(NULL), upper(NULL), precision(NULL) {
      }

      DecisionVariable::~DecisionVariable() {
      }

      numerics::NumericExpression* DecisionVariable::getLowerBound() {
        return lower;
      }
      numerics::NumericExpression* DecisionVariable::getUpperBound() {
        return upper;
      }
      numerics::NumericExpression* DecisionVariable::getPrecision() {
        return precision;
      }
      void DecisionVariable::setLowerBound(numerics::NumericExpression* node) {
        lower = node;
      }
      void DecisionVariable::setUpperBound(numerics::NumericExpression* node) {
        upper = node;
      }
      void DecisionVariable::setPrecision(numerics::NumericExpression* node) {
        precision = node;
      }
      std::string DecisionVariable::toString() const {
        return getName() + "(Decision Variable) in ["
            + lower->toString() + ", " + upper->toString() + "] +/-"
            + precision->toString();
      }
      bool DecisionVariable::isConstant() const {
        return false;
      }
      bool DecisionVariable::isIndependant() const {
        return false;
      }
    }
  }
}
