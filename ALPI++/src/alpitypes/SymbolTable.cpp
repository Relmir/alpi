/*
 * SymbolTable.cpp
 *
 *  Created on: Jan 10, 2014
 *      Author: Emmanuel BIGEON
 */

#include "SymbolTable.h"
#include <iostream>

namespace alpi {
  namespace parsertools {

    using namespace std;
    using namespace alpi::exprs;
    using namespace alpi::exprs::symbol;

    SymbolTable::SymbolTable(SymbolTable* parent) :
        parent(parent) {
    }

    list<Symbol*> SymbolTable::getSymbols() {
      return syms;
    }
    int SymbolTable::addSymbol(Symbol* symbol) {
      bool cont = false;
      for (list<Symbol*>::iterator it = syms.begin();
          it != syms.end(); ++it) {
        cont = ((*it)->getName() == symbol->getName());
      }
      if (cont) {
        return 1;
      }
      syms.push_back(symbol);
      if (parent != NULL) {
        if (parent->searchSymbol(symbol->getName()) != NULL) {
          return 2;
        }
      }
      return 0;
    }

    SymbolTable::~SymbolTable() {
    }

    SymbolTable* SymbolTable::getParent() {
      return parent;
    }

    Symbol* SymbolTable::searchSymbol(std::string name) {
      for (list<Symbol*>::iterator it = syms.begin();
          it != syms.end(); ++it) {
        if ((*it)->getName() == name) {
          return *it;
        }
      }
      if (parent != NULL) {
        return parent->searchSymbol(name);
      }
      return NULL;
    }

    std::string SymbolTable::toString() {
      std::string str = "";
      if (parent != NULL) {
        str = parent->toString();
      }
      std::string tStr = "";
      list<Symbol*>::iterator it = syms.begin();
      while (it != syms.end()) {
        Symbol* sym = *it;
        tStr += sym->toString() + "\n";
        it++;
      }
      return str + tStr;
    }
  }
}
