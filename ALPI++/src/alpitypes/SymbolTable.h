/*
 * SymbolTable.h
 *
 *  Created on: Jan 10, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef SYMBOLTABLE_H_
#define SYMBOLTABLE_H_
#include "../exprs/symbol/Symbol.h"
#include <list>

namespace alpi {
  namespace parsertools {
    /** \brief A symbol table is a hierarchic storage container for
     * symbols
     */
    class SymbolTable {
      public:
        /** \brief Create a symbol table that is under the given one.
         *
         * A null pointer given as the parent will create a top level
         * symbol table
         *
         * \param parent pointer to the parent table that may contain
         *      symbols that can be accessed by this one.
         * \return SymbolTable the newly created symbol table
         */
        SymbolTable(SymbolTable *parent);
        virtual ~SymbolTable();
        /** \brief get the table for the outer context
         *
         * \return SymbolTable* the parent table
         */
        SymbolTable *getParent();
        /** \brief tries to add a symbol in this table
         *
         * There are several possible outcomes to this function.
         *
         * -If the result is 0, the symbol was correctly added.
         *
         * -If it was 1, the symbol name was already present in the table
         * and the symbol was not added.
         *
         * -If it is 2, the symbol name was present in an outer context,
         * the symbol was added to this symbol table and will be
         * shadowing the previously defined symbol in this context
         *
         * \param symbol the symbol to add in this table
         * \return int 0 if the symbol was added without error or
         *       warning, an error flag otherwise
         */
        int addSymbol(alpi::exprs::symbol::Symbol*symbol);
        /** \brief Searches a symbol.
         *
         * This function searches the symbol in this table. If it is
         * found, a pointer to it is then returned, else the search is
         * performed in the parent table (if it exists).
         * \param name the name of the symbol
         * \param
         * \return Symbol* a pointer to the found symbol, null if none
         *       were found
         */
        exprs::symbol::Symbol *searchSymbol(std::string name);
        /** \brief get the symbols that are in this table
         *
         * \return std::list<exprs::symbol::Symbol*> the symbols
         *       registered in this specific context
         */
        std::list<exprs::symbol::Symbol *> getSymbols();
        /** \brief a string representation of this table.
         *
         * \return std::string a string representation of this table.
         */
        std::string toString();
      private:
        /** \brief The parent table.
         */
        SymbolTable *parent;
        /** \brief The symbols of this specific context.
         */
        std::list<exprs::symbol::Symbol*> syms;
    };
  }
} /* namespace alpiparsertools */

#endif /* SYMBOLTABLE_H_ */
