/*
 * forgen.h
 *
 *  Created on: Dec 9, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef FORGEN_H_
#define FORGEN_H_

#include <iostream>
#include <string>
#include <list>

/*
 * This file declare the minimum functions to generate Fortran files.
 */

namespace forgen {

// Module writing
  /** \brief Write a fortran module header in the stream
   *
   * \param file the stream to write in
   * \param moduleName the name of the module
   * \param moduleUsed the used modules
   * \param nbModules the number of used modules
   * \return void
   */
  void writeModuleHeader(std::ostream &file, std::string moduleName,
      std::string *moduleUsed, int nbModules);
  /** \brief Write a fortran constant instruction in the stream
   *
   * \param file the stream to write in
   * \param type the type of the constant
   * \param name the name of the constant
   * \param value the value of the constant
   * \return void
   */
  void writeModuleConstant(std::ostream &file, std::string type,
      std::string name, std::string value);
  /** \brief Write a fortran interface instruction to a stream
   *
   * \param file the stream
   * \param interfaceName the interface name
   * \param interfacedNames the interfaced operations names
   * \param nbInterfaced the number of operations
   * \return void
   *
   */
  void writeModuleInterface(std::ostream &file,
      std::string interfaceName, std::string *interfacedNames,
      int nbInterfaced);
  /** \brief Write the fortran delimiter for content of the file
   *
   * \param file the stream
   * \return void
   */
  void writeModuleStartingDefinitions(std::ostream &file);
  /** \brief Write a fortran comment to a stream
   *
   * \param file the stream
   * \param comment the comment
   * \return void
   */
  void writeModuleComment(std::ostream &file, std::string comment);
  /** \brief Write a fortran function header to a stream
   *
   * \param mof the stream
   * \param name the function name
   * \param params the function parameters
   * \param nbParams the number of parameters
   * \param resultName the anme of the result, empty if it is the
   *      function name
   * \param types the types of the function and of the parameters
   * \param intents the intents of the parameters
   * \return void
   */
  void writeModuleFunctionHeader(std::ostream &mof, std::string name,
      std::string *params, int nbParams, std::string resultName,
      std::string *types, std::string *intents, bool recursive = false);
  /** \brief Write a function instruction to a stream
   *
   * \param mod the stream
   * \param inst the instruction
   * \param blk the indentation (default to 0)
   * \return void
   */
  void writeModuleFunctionInstruction(std::ostream &mod,
      std::string inst, int blk = 0);
  /** \brief Write a serie of function instruction to a stream
   *
   * \param mod the stream
   * \param inst the instructions
   * \param blk the indentation (default to 0)
   * \return void
   */
  void writeModuleFunctionInstructions(std::ostream& mod,
      std::list<std::string> inst, int blk = 0);
  /** \brief Write a fortran function footer to a stream
   *
   * \param mod the stream
   * \param name the function name
   * \return void
   */
  void writeModuleFunctionEnd(std::ostream &mod, std::string name);
  /** \brief Write a fortran subroutine header to a stream
   *
   * \param mof the stream
   * \param name the subroutine name
   * \param params the parameters names
   * \param nbParams the number of parameters
   * \param types the parameter types
   * \param intents the parameter intents
   * \return void
   */
  void writeModuleSubroutineHeader(std::ostream &mof,
      std::string name, std::string *params, int nbParams,
      std::string *types, std::string *intents, bool recursive = false);
  /** \brief Write a fortran subroutine footer to a stream
   *
   * \param mod the stream
   * \param name the subroutine name
   * \return void
   */
  void writeModuleSubroutineEnd(std::ostream &mod, std::string name);
  /** \brief Write a fortran module footer to a stream
   *
   * \param mod_out the stream
   * \param name the module name
   * \return void
   */
  void writeModuleEnd(std::ostream &mod_out, std::string name);

// Program
  /** \brief Write a fortran program header to a stream
   *
   * \param file the stream
   * \param name the program name
   * \param moduleUsed std::string* the modules used
   * \param nbModules the number of used modules
   * \return void
   */
  void writeProgramHeader(std::ostream &file, std::string name,
      std::string *moduleUsed, int nbModules);
  /** \brief Write a fortran program instruction to a stream
   *
   * \param file the stream
   * \param inst the instruction
   * \param blk the indentation (default to 0)
   * \return void
   */
  void writeProgramInstruction(std::ostream &file, std::string inst,
      int blk = 0);
  void writeProgramInstructions(std::ostream& file,
      std::list<std::string> inst, int blk = 0);
  /** \brief Write a fortran program footer in a stream
   *
   * \param mod_out the stream
   * \param name std::string the name of the program
   * \return void
   */
  void writeProgramEnd(std::ostream &mod_out, std::string name);
}
#endif /* FORGEN_H_ */
