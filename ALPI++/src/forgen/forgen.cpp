/*
 * forgen.c
 *
 *  Created on: Dec 9, 2013
 *      Author: Emmanuel BIGEON
 */

#include "forgen.h"
#include <sstream>
#include <string>

namespace forgen {
  using namespace std;
  /**
   * \brief Write a fortran instruction line to file
   * \param file the stream to write into
   * \param
   *
   * <p>
   * Write a line of instruction to a stream. This might be splitted
   * around 80 characters to avoid compilation problems due to a too
   * long line in fortran.
   *
   */
  void writeLineToFile(ostream& file, string inst, string pre) {
    if (inst.length() + pre.length() < 80) {
      file << pre << inst << endl;
    } else {
      int ind = pre.length();
      file << pre;
      string item;
      stringstream ss(inst);
      while (getline(ss, item, ' ')) {
        ind += item.length() + 1;
        if (ind > 80) {
          file << "& " << endl;
          ind = 6 + pre.length() + item.length();
          file << pre << "    & " << item;
        } else {
          file << item << " ";
        }
      }
      file << endl;
    }
  }
// Modules
  /** \brief Write the header of a module
   *
   * \param file ostream& the stream to write into
   * \param moduleName string the name of the module
   * \param moduleUsed string* the modules used by the one being written
   * \param nbModules int the number of modules used
   *
   * This method should be called first when writing a module
   */
  void writeModuleHeader(ostream& file, string moduleName,
      string *moduleUsed, int nbModules) {
    int i;
    file << "module " << moduleName << endl;
    for (i = 0; i < nbModules; ++i) {
      file << "  use " << moduleUsed[i] << endl;
    }
    file << "  implicit none" << endl << endl;
  }
  /**
   * \brief writes a constant into a fortran module
   * \param file ostream&, the stream to write in
   * \param type string, the constant type name
   * \param value string, the string representation of the value
   */
  void writeModuleConstant(ostream& file, string type, string name,
      string value) {
    writeLineToFile(file,
        type + ", parameter:: " + name + " = " + value, "    ");
  }
  /**
   * \brief Writes interfaces (grouping of functions under a common
   *      name)
   * \param file ostream&, the stream to write in
   * \param interfaceName string, the name of the interface
   * \param interfacedNames string, the names of the grouped operations
   * \param nbInterfaced int, the number of interfaced operations
   */
  void writeModuleInterface(ostream& file, string interfaceName,
      string *interfacedNames, int nbInterfaced) {
    if (nbInterfaced) {
      int i;
      file << "    interface " << interfaceName << endl;
      i = 21 + interfacedNames[0].length();
      file << "    module procedure " << interfacedNames[0];
      for (int j = 1; j < nbInterfaced; ++j) {
        if (i + interfacedNames[j].length() > 78) {
          i = 8 + interfacedNames[j].length();
          file << ", &" << endl << "      & " << interfacedNames[j];
        } else {
          i += 2 + interfacedNames[j].length();
          file << ", " << interfacedNames[j];
        }
      }
      file << endl << "    end interface" << endl;
    }
  }
  /**
   * \brief Writes the delimiter between declaration and
   *      implementations
   * \param file ostream&, the stream to write in
   */
  void writeModuleStartingDefinitions(ostream& file) {
    file << "  contains" << endl;
  }
  /**
   * \brief Writes a commented line
   * \param file ostream&, the file to write to
   * \param comment string, the comment to write
   */
  void writeModuleComment(ostream& file, string comment) {
    file << "  !" << comment << endl;
  }
  /**
   * \brief Writes the header of a function implementation
   * \param mod ostream&, the stream to write to
   * \param name string, the name of the function
   * \param params string*, the names of the parameters
   * \param nbParams int, the number of parameters
   * \param resultName string, the name of the result
   * \param types string* the type names of the parameters (preceeded
   *      by the type of the function)
   * \param intents string*, the intent qualifier of the parameters
   */
  void writeModuleFunctionHeader(ostream& mod, string name,
      string *params, int nbParams, string resultName, string *types,
      string *intents, bool recursive) {
    int i;
    string blanks;
    blanks = "            " + string(name.length(), ' ');

    // Head
    mod << "    ";
    if (recursive) {
      mod << "recursive ";
    }
    mod << "function " << name << "(";
    if (nbParams) {
      mod << params[0];
      for (i = 1; i < nbParams; ++i) {
        mod << ", &" << endl << blanks << "& " << params[i];
      }
    }
    mod << ")";
    if (!resultName.empty()) {
      mod << " &" << endl << blanks << "& result (" << resultName
          << ")";
    }
    mod << endl;

    // Types
    if (resultName.empty()) {
      mod << "      " << types[0] << ":: " << name << endl;
    } else {
      mod << "      " << types[0] << ":: " << resultName << endl;
    }
    for (i = 0; i < nbParams; ++i) {
      mod << "      " << types[i + 1] << ", intent(" << intents[i]
          << "):: " << params[i] << endl;
    }
  }
  /**
   * \brief Writes an instruction in a function
   * \param mod ostream&, the file to write to
   * \param inst string, the instruction line to write
   * \param blk int, the indentation inside the function
   */
  void writeModuleFunctionInstruction(ostream& mod, string inst,
      int blk) {
    writeLineToFile(mod, inst, string(blk + 8, ' '));
  }
  void writeModuleFunctionInstructions(ostream& mod,
      list<string> inst, int blk) {
    for (list<string>::iterator it = inst.begin(); it != inst.end();
        it++) {
      writeLineToFile(mod, *it, string(blk + 8, ' '));
    }
  }
  /**
   * \brief Writes the end of a function
   * \param mod ostream&, the stream to write to
   * \param name string, the name of the function being ended
   */
  void writeModuleFunctionEnd(ostream& mod, string name) {
    mod << "    end function " << name << endl;
  }
  /**
   * \brief Write the header of a subroutine
   * \param mod ostream&, the stream to write to
   * \param name string, the name of the subroutine
   * \param params string*, the names of the parameters
   * \param nbParams int, the number of parameters
   * \param types string*, the type names of the parameters
   * \param intents string*, the intents of the parameters
   */
  void writeModuleSubroutineHeader(ostream& mod, string name,
      string *params, int nbParams, string *types, string *intents,
      bool recursive) {
    int i;
    string blanks;
    blanks = "            " + string(name.length(), ' ');
// Head
    mod << "    ";
    if (recursive) {
      mod << "recursive ";
    }
    mod << "subroutine " << name << "(";
    if (nbParams) {
      mod << params[0];
      for (i = 1; i < nbParams; ++i) {
        mod << ", &" << endl << blanks << "& " << params[i];
      }
    }
    mod << ")" << endl;

    // Types
    for (i = 0; i < nbParams; ++i) {
      mod << "      " << types[i] << ", intent(" << intents[i]
          << "):: " << params[i] << endl;
    }
  }
  /**
   * \brief writes the end of a subroutine
   * \param mod ostream&, the stream to write to
   * \param name string, the name of the subroutine
   */
  void writeModuleSubroutineEnd(ostream& mod, string name) {
    mod << "    end subroutine " << name << endl;
  }
  /**
   * \brief Writes the end of a module
   * \param mod ostream&, the stream to write to
   * \param name string, the name of the module
   */
  void writeModuleEnd(ostream& mod_out, string name) {
    mod_out << "end module " << name << endl;
  }
// Program
  /**
   * \brief Writes the header of a program
   * \param mod ostream&, the stream to write to
   * \param name string, the name of the program
   * \param mods string*, the modules used
   * \param nbMods int, the number of module used
   */
  void writeProgramHeader(ostream& mod, string name, string *mods,
      int nbMods) {
    int i;
    mod << "program " << name << endl;
    for (i = 0; i < nbMods; ++i) {
      mod << "  use " << mods[i] << endl;
    }
    mod << "  implicit none" << endl << endl;
  }
  /**
   * \brief Writes an instruction in a program
   * \param file ostream&, the stream to write to
   * \param inst string, the instruction
   * \param blk int, the indentation of the instruction
   */
  void writeProgramInstruction(ostream& file, string inst, int blk) {
    writeLineToFile(file, inst, string(2 + blk, ' '));
  }
  void writeProgramInstructions(ostream& file, list<string> inst,
      int blk) {
    for (list<string>::iterator it = inst.begin(); it != inst.end();
        ++it) {
      writeLineToFile(file, *it, string(2 + blk, ' '));
    }
  }
  /**
   * \brief Writes the end of a program
   * \param mod_out ostream&, the stream to write to
   * \param name string, the name of the program
   */
  void writeProgramEnd(ostream& mod_out, string name) {
    mod_out << "end program " << name << endl;
  }
}
