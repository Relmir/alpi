/*
 * Function.cpp
 *
 *  Created on: Jan 16, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Function.h"
#include "../ftran/CodeLines.h"
#include "../ftran/ExpressionToFortran.h"

namespace alpi {
  namespace problem {
    using namespace std;
    using namespace alpi::exprs::symbol;

    Function::Function(ParamVariable* param) :
        p(param) {
    }
    Function::~Function() {
    }
    std::string Function::getName() {
      return p->getName();
    }
    bool Function::isVector() {
      return p->isVector();
    }
    ParamVariable* Function::getParamVariable() {
      return p;
    }
  }
} /* namespace alpiparsertools */
