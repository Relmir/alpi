/*
 * Constant.cpp
 *
 *  Created on: Jan 16, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Constant.h"

namespace alpi {
  namespace problem {
    using namespace alpi::exprs::symbol;

    Constant::Constant(ParamVariable* param) :
        p(param) {
    }
    Constant::~Constant() {
    }
    ParamVariable* Constant::getParam() {
      return p;
    }
  }  // namespace problem
} /* namespace alpiparsertools */
