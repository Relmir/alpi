/*
 * Constant.h
 *
 *  Created on: Jan 16, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef CONSTANT_H_
#define CONSTANT_H_

#include <string>
#include "../exprs/symbol/Symbol.h"
#include "../exprs/symbol/ParamVariable.h"

namespace alpi {
  namespace problem {
    /** \brief A numerical constant
     */
    class Constant {
    public:
      /** \brief Create a constant
       *
       * \param v the parameter that holds the constant
       */
      Constant (exprs::symbol::ParamVariable *v);
      /** \brief Get the parameter defining constant
       *
       * \return exprs::symbol::ParamVariable*
       *
       */
      exprs::symbol::ParamVariable *getParam();
      virtual ~Constant();
    private:
      exprs::symbol::ParamVariable *p;
    };
  }
} /* namespace alpiparsertools */

#endif /* CONSTANT_H_ */
