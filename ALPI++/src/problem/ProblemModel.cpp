/*
 * ProblemModel.cpp
 *
 *  Created on: Jan 16, 2014
 *      Author: Emmanuel BIGEON
 */

#include "ProblemModel.h"

namespace alpi {
  namespace problem {
    using namespace exprs;
    using namespace symbol;
    ProblemModel::ProblemModel(std::string name) :
        objective(NULL), name(name), precisionF(NULL) {
      // Setting problem defaults
      strOpts["knownMajor"] = "1.D300";
      strOpts["logFile"] = "DEF_" + name + ".res";
      intOpts["timeLimit"] = 1000;
      intOpts["memLimit"] = 2000000;
      intOpts["logFreq"] = 10000;
      boolOpts["taylor"] = false;
      boolOpts["constraintsPropagation"] = true;
      boolOpts["CPLEX"] = true;
      boolOpts["timeInSeconds"] = true;
      boolOpts["keepAll"] = false;
      boolOpts["normalStoppingCriterion"] = true;
      boolOpts["terminalDisplay"] = true;
      boolOpts["notCertifiedLP"] = false;
      boolOpts["exactConstraint"] = true;
      boolOpts["affineArithmeticForConstraints"] = false;
    }

    std::string ProblemModel::getName() {
      return name;
    }

    std::string ProblemModel::getReformulation() {
      return reformulation;
    }

    bool ProblemModel::useReformulation() {
      return !reformulation.empty();
    }
    void ProblemModel::setReformulation(std::string value) {
      reformulation = value;
    }

    std::list<Constant*> ProblemModel::getConstants() {
      return constants;
    }
    void ProblemModel::addConstant(Constant* c) {
      constants.push_back(c);
    }

    std::list<Function*> ProblemModel::getConstantFunctions() {
      return constantFunctions;
    }

    void ProblemModel::addConstantFunction(Function* c) {
      constantFunctions.push_back(c);
    }

    std::list<Function*> ProblemModel::getFunctions() {
      return functions;
    }

    void ProblemModel::addAuxiliaryFunction(Function* c) {
      functions.push_back(c);
    }

    std::list<Constraint*> ProblemModel::getConstraints() {
      return constraints;
    }

    void ProblemModel::addConstraint(Constraint* c) {
      constraints.push_back(c);
    }

    std::list<DecisionVariable*> ProblemModel::getVariables() {
      return vl;
    }

    void ProblemModel::addDecisionVariable(DecisionVariable* v) {
      vl.push_back(v);
    }

    Function* ProblemModel::getObjective() {
      return objective;
    }
    void ProblemModel::setObjective(Function* f) {
      objective = f;
    }
    bool ProblemModel::getOption(std::string name) {
      return boolOpts[name];
    }
    void ProblemModel::setOption(std::string name, bool val) {
      boolOpts[name] = val;
    }

    int ProblemModel::getIntOption(std::string name) {
      return intOpts[name];
    }

    void ProblemModel::setOption(std::string name, int value) {
      intOpts[name] = value;
    }
    std::string ProblemModel::getStringOption(std::string name) {
      return strOpts[name];
    }
    void ProblemModel::setOption(std::string name,
        std::string value) {
      strOpts[name] = value;
    }

    void ProblemModel::setObjectivePrecision(ExpressionNode* real) {
      precisionF = real;
    }
    ExpressionNode* ProblemModel::getObjectivePrecision() {
      return precisionF;
    }
    ProblemModel::~ProblemModel() {
    }

  }
}
