/*
 * Constraint.h
 *
 *  Created on: Jan 17, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef CONSTRAINT_H_
#define CONSTRAINT_H_

#include "Function.h"
#include "../exprs/numerics/NumericExpression.h"

namespace alpi {
  namespace problem {
    /** \brief A constraint relation
     */
    class Constraint {
    public:
      /** \brief Create an equality constraint
       *
       * \param fun the function that is constrained
       * \param expr the value it is constrained to
       */
      Constraint (Function *fun, exprs::numerics::NumericExpression *expr);
      /** \brief Create an inequality constraint
       *
       * \param fun the constrained function
       * \param low if this is defining a lower bound or an upper bound
       * \param expr the value the function is constrained to
       */
      Constraint (Function *fun, bool low,
                  exprs::numerics::NumericExpression *expr);
      /** \brief Create a double inequality constraint
       *
       * \param fun the constrained function
       * \param low the lower binding value
       * \param up the upper binding value
       */
      Constraint (Function *fun, exprs::numerics::NumericExpression *low,
                  exprs::numerics::NumericExpression *up);
      /** \brief Test if this is an equality constraint
       *
       * \return bool if this defined an equality constraint
       *
       */
      bool isEqualityConstraint() const;
      /** \brief Test if this defines an upper bound
       *
       * \return bool defines an upper bound
       *
       */
      bool defineUpperBound() const;
      /** \brief Test if this constraint defines a lower bound
       *
       * \return bool if this constraint defines a lower bound
       *
       */
      bool defineLowerBound() const;
      /** \brief Get the constrained function
       *
       * \return Function* the function
       *
       */
      Function *function();
      /** \brief Get the expression of the lower binding value
       *
       * \return exprs::numerics::NumericExpression* the lower binding expression
       *
       */
      exprs::numerics::NumericExpression *lowerBound();
      /** \brief Get the expression of the upper binding value
       *
       * \return exprs::numerics::NumericExpression* the upper binding expression
       *
       */
      exprs::numerics::NumericExpression *upperBound();
      /** \brief Get the expression of the equality constraint
       *
       * \return exprs::numerics::NumericExpression* the equality binding expression
       *
       */
      exprs::numerics::NumericExpression *equalityConstraint();
      /** \brief Get the precision value
       *
       * \return exprs::numerics::NumericExpression* the precision value
       *
       */
      exprs::numerics::NumericExpression *getPrecision();
      void setPrecision (exprs::numerics::NumericExpression *r);
      /** \brief Get a string representation
       *
       * \return std::string the representation
       *
       */
      std::string toString() const;
      virtual ~Constraint();
    private:
      bool equal;
      bool low, up;
      exprs::numerics::NumericExpression *prec;
      Function *f;
      exprs::numerics::NumericExpression *first, *second;
    };
  }
} /* namespace alpiparsertools */

#endif /* CONSTRAINT_H_ */
