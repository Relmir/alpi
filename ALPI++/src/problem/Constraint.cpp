/*
 * Constraint.cpp
 *
 *  Created on: Jan 17, 2014
 *      Author: Emmanuel BIGEON
 */

#include "Constraint.h"
#include <iostream>
#include <sstream>

namespace alpi {
  namespace problem {
    using namespace std;
    using namespace alpi::exprs;

    Constraint::Constraint(Function* fun, numerics::NumericExpression *expr) :
        equal(true), low(false), up(false), prec(NULL), f(fun),
            first(expr), second(NULL) {

    }
    Constraint::Constraint(Function* fun, bool low,
        numerics::NumericExpression *expr) :
        equal(false), low(low), up(!low), prec(NULL), f(fun),
            first(expr), second(NULL) {

    }
    Constraint::Constraint(Function* fun, numerics::NumericExpression *low,
        numerics::NumericExpression*up) :
        equal(false), low(true), up(true), prec(NULL), f(fun),
            first(low), second(up) {

    }

    Constraint::~Constraint() {
    }

    bool Constraint::isEqualityConstraint() const {
      return equal;
    }
    bool Constraint::defineUpperBound() const {
      return up;
    }
    bool Constraint::defineLowerBound() const {
      return low;
    }
    Function* Constraint::function() {
      return f;
    }
    numerics::NumericExpression* Constraint::lowerBound() {
      if (equal) {
        return NULL;
      }
      if (low) {
        return first;
      }
      return NULL;
    }
    numerics::NumericExpression* Constraint::upperBound() {
      if (equal) {
        return NULL;
      }
      if (low) {
        return second;
      }
      return first;
    }
    numerics::NumericExpression* Constraint::equalityConstraint() {
      if (equal) {
        return first;
      }
      return NULL;
    }

    numerics::NumericExpression* Constraint::getPrecision() {
      return prec;
    }

    void Constraint::setPrecision(numerics::NumericExpression* r) {
      prec = r;
    }

    string Constraint::toString() const {
      ostringstream oss;
      if (equal) {
        oss << "EqualityConstraint ";
      } else {
        oss << "InequalityConstraint ";
      }
      if (low) {
        oss << "LowerBinding ";
      }
      if (up) {
        oss << "UpperBinding ";
      }
      if (first != NULL) {
        oss << "First: " << first->toString() << " ";
      }
      if (second != NULL) {
        oss << "Second: " << second->toString() << " ";
      }
      if (prec != NULL) {
        oss << "Precision: " << prec->toString() << " ";
      }
      if (f != NULL) {
        oss << "Function: " << f->getName();
      }
      return oss.str();
    }
  }
} /* namespace alpiparsertools */
