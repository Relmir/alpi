/*
 * Function.h
 *
 *  Created on: Jan 16, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <list>
#include "../exprs/symbol/ParamVariable.h"

namespace alpi {
  namespace problem {
    /** \brief An auxiliary function or a constant function
     */
    class Function {
    public:
      /** \brief Create a function
       *
       * \param v the parameter carying the definition of this function
       */
      Function (exprs::symbol::ParamVariable *v);
      /** \brief Get the name of the function
       *
       * \return std::string the name of the function
       *
       */
      std::string getName();
      /** \brief Get the defining parameter variable
       *
       * \return exprs::symbol::ParamVariable* the defining variable
       *
       */
      exprs::symbol::ParamVariable *getParamVariable();
      /** \brief Test if the result of this function is a vector
       *
       * \return bool vector status of the function
       *
       */
      bool isVector();
      virtual ~Function();
    private:
      exprs::symbol::ParamVariable *p;
    };
  }
} /* namespace alpiparsertools */

#endif /* FUNCTION_H_ */
