/*
 * ProblemModel.h
 *
 *  Created on: Jan 16, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef PROBLEMMODEL_H_
#define PROBLEMMODEL_H_

#include <list>
#include <map>
#include <string>

#include "Constant.h"
#include "Constraint.h"
#include "../exprs/symbol/DecisionVariable.h"
#include "Function.h"

namespace alpi {
  namespace problem {
    /** \brief A problem model in IBBA
     */
    class ProblemModel {
    public:
      /** \brief Create a model
       *
       * \param name the name of the model
       */
      ProblemModel (std::string name);
      /** \brief Get the problem name
       *
       * \return std::string the name
       *
       */
      std::string getName();
      /** \brief Get the reformulation
       *
       * \return std::string the reformulation (or empty string)
       *
       */
      std::string getReformulation();
      /** \brief Test if there is a reformulation set
       *
       * \return bool if this proble uses a reformulation
       *
       */
      bool useReformulation();
      /** \brief Set the reformulation
       *
       * \param reformulation the reformulation to set
       */
      void setReformulation (std::string reformulation);
      /** \brief Get the constants
       *
       * \return std::list<Constant*> The problem's constants
       *
       */
      std::list<Constant *> getConstants();
      /** \brief Add a constant to the problem
       *
       * \param c the constant
       */
      void addConstant (Constant *c);
      /** \brief Get the constant functions
       *
       * \return std::list<Function*> The problem's constant functions
       *
       */
      std::list<Function *> getConstantFunctions();
      /** \brief Add a constant function to the problem
       *
       * \param f the function
       */
      void addConstantFunction (Function *f);
      /** \brief Get auxiliary functions
       *
       * \return std::list<Function*> The problem's auxiliary functions
       *
       */
      std::list<Function *> getFunctions();
      /** \brief Add an auxiliary function
       *
       * \param f the function
       */
      void addAuxiliaryFunction (Function *f);
      /** \brief Get all the constraints
       *
       * \return std::list<Constraint*> the problem's constraints
       *
       */
      std::list<Constraint *> getConstraints();
      /** \brief Add a constraint
       *
       * \param c the constraint to add
       */
      void addConstraint (Constraint *c);
      /** \brief Get the problem's decision variables
       *
       * \return std::list<DecisionVariable*> the decision variables
       *
       */
      std::list<alpi::exprs::symbol::DecisionVariable *> getVariables();
      /** \brief Add a decision variable to the problem
       *
       * \param  the DecisionVariable to add
       */
      void addDecisionVariable (alpi::exprs::symbol::DecisionVariable *v);
      /** \brief Get the objective function
       *
       * \return Function* the objective
       *
       */
      Function *getObjective();
      /** \brief Set the objective function
       *
       * \param f the function to set
       */
      void setObjective (Function *f);
      /** \brief Get the option value
       *
       * \param optionName the name of the option
       * \return bool the option value
       *
       */
      bool getOption (std::string optionName);
      /** \brief Set the option value
       *
       * \param name the name of the option
       * \param value the value to set
       */
      void setOption (std::string name, bool value);
      /** \brief Get the option value
       *
       * \param optionName the name of the option
       * \return int the option value
       *
       */
      int getIntOption (std::string optionName);
      /** \brief Set the option value
       *
       * \param name the name of the option
       * \param value the value to set
       */
      void setOption (std::string name, int value);
      /** \brief Get the option value
       *
       * \param optionName the name of the option
       * \return std::string the option value
       *
       */
      std::string getStringOption (std::string optionName);
      /** \brief Set the option value
       *
       * \param name the name of the option
       * \param value the value to set
       */
      void setOption (std::string name, std::string value);
      /** \brief Set the precision on F
       *
       * \param real the precision on the objective function
       */
      void setObjectivePrecision (exprs::ExpressionNode *real);
      /** \brief Get the precision on the objective function
       *
       * \return exprs::ExpressionNode* the precision on F
       *
       */
      exprs::ExpressionNode *getObjectivePrecision();
      virtual ~ProblemModel();
    private:
      std::list<alpi::exprs::symbol::DecisionVariable *> vl;
      std::list<Constant *> constants;
      std::list<Function *> constantFunctions;
      std::list<Function *> functions;
      std::list<Constraint *> constraints;
      //        std::list<ParamVariable*> pvl;
      Function *objective;
      std::string name;
      std::string reformulation;
      std::map<std::string, bool> boolOpts;
      std::map<std::string, int> intOpts;
      std::map<std::string, std::string> strOpts;
      exprs::ExpressionNode *precisionF;
    };
  }
} /* namespace alpiparsertools */

#endif /* PROBLEMMODEL_H_ */
