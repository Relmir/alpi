/*
 * extractors.cpp
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#include "extractors.h"

#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>

#include "../exprs/symbol/DecisionVariable.h"
#include "../exprs/symbol/ParamVariable.h"

namespace alpi {

  namespace extract {

    using namespace std;
    using namespace alpi::exprs;
    using namespace alpi::exprs::symbol;

    string getVariableList(ExpressionNode* node) {
      ostringstream oss;
      oss << "(";
      set<DecisionVariable*> dvl = getDecisionVariablesIn(node);
      if (!dvl.empty()) {
        set<DecisionVariable*>::iterator it = dvl.begin();
        oss << "USER_" << (*it)->getName();
        ++it;
        for (; it != dvl.end(); ++it) {
          oss << ", USER_" << (*it)->getName();
        }
      }
      oss << ")";
      return oss.str();
    }
    bool isRecursiveDefinition(alpi::exprs::symbol::ParamVariable* p) {
      set<ExpressionNode*> s;
      s.insert(p);
      return findSymbols(s, p->getExpression());
    }

  }  // namespace extract

}  // namespace alpi

