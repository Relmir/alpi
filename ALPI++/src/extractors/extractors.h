/*
 * extractors.h
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef EXTRACTORS_H_
#define EXTRACTORS_H_

#include <set>
#include <string>

//#include "../exprs/ExpressionNode.h"
#include "../exprs/symbol/DecisionVariable.h"

namespace alpi {
  namespace exprs {
    namespace symbol {
      class ParamVariable;
    } /* namespace symbol */
  } /* namespace exprs */
} /* namespace alpi */

namespace alpi {
  namespace extract {
    /** \brief Get decision variables as a string list.
     *
     * \param node alpi::exprs::ExpressionNode* the node to get decision
     *      variables from
     * \return std::string the string list (comma separated, beginning
     *      with an open parenthesis and ending with a close one)
     *
     */
    std::string getVariableList(alpi::exprs::ExpressionNode *node);
    /** \brief Get the set of decision variables
     *
     * \param node alpi::exprs::ExpressionNode* the node
     * \return std::set<alpi::exprs::symbol::DecisionVariable*> the
     *      decision variables present in the tree
     */
    std::set<alpi::exprs::symbol::DecisionVariable*>
    getDecisionVariablesIn(alpi::exprs::ExpressionNode *node,
        std::set<alpi::exprs::ExpressionNode*> parsed = std::set<
            alpi::exprs::ExpressionNode*>());

    bool isRecursiveDefinition(alpi::exprs::symbol::ParamVariable* p);
    bool findSymbols(std::set<alpi::exprs::ExpressionNode*> syms,
        alpi::exprs::ExpressionNode *node,
        std::set<alpi::exprs::ExpressionNode*> parsed = std::set<
        alpi::exprs::ExpressionNode*>());
  }  // namespace extract
}  // namespace alpi

#endif /* EXTRACTORS_H_ */
