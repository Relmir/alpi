/*
 * dec_var_extractor.cpp
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#include <iostream>
#include <list>
#include <set>
#include <string>

#include "../exprs/symbol/DecisionVariable.h"
#include "../exprs/indexing/IndexingMultiRelation.h"
#include "../exprs/Expressions.h"
#include "extractors.h"

namespace alpi {
  namespace extract {
    using namespace std;
    using namespace alpi::exprs::symbol;
    using namespace alpi::exprs::operations;
    using namespace alpi::exprs::indexing;
    using namespace alpi::exprs::numerics;
    using namespace alpi::exprs;

    set<DecisionVariable*> getDecisionVariablesIn(ParamVariable* node,
        set<ExpressionNode*> parsed) {
      if (parsed.find(node) != parsed.end()) {
        return set<DecisionVariable*>();
      }
      set<ExpressionNode*> p(parsed);
      p.insert(node);
      return getDecisionVariablesIn(node->getExpression(), p);
    }
    template<class P, class R>
    set<DecisionVariable*> getDecisionVariablesIn(BinOp<P, R>* node,
        set<ExpressionNode*> parsed) {
      if (parsed.find(node) != parsed.end()) {
        return set<DecisionVariable*>();
      }
      set<DecisionVariable*> l;
      set<DecisionVariable*> tmp = getDecisionVariablesIn(
          node->getLeft(), parsed);
      l.insert(tmp.begin(), tmp.end());
      tmp = getDecisionVariablesIn(node->getRight(), parsed);
      l.insert(tmp.begin(), tmp.end());
      return l;
    }
    template<class P>
    set<DecisionVariable*> getDecisionVariablesIn(BigOp<P>* node,
        set<ExpressionNode*> parsed) {
      if (parsed.find(node) != parsed.end()) {
        return set<DecisionVariable*>();
      }
      return getDecisionVariablesIn(node->getExpression(), parsed);
    }
    template<class P, class R>
    set<DecisionVariable*> getDecisionVariablesIn(UnOp<P, R>* node,
        set<ExpressionNode*> parsed) {
      if (parsed.find(node) != parsed.end()) {
        return set<DecisionVariable*>();
      }
      return getDecisionVariablesIn(node->getArgument(), parsed);
    }
    set<DecisionVariable*> getDecisionVariablesIn(
        AssignmentMap<numerics::NumericExpression>* node,
        set<ExpressionNode*> parsed) {
      if (parsed.find(node) != parsed.end()) {
        return set<DecisionVariable*>();
      }
      set<DecisionVariable*> l = getDecisionVariablesIn(
          node->getDefault(), parsed);
      list<IndexingMultiRelation*> inds =
          node->getIndexingRelations();
      for (list<IndexingMultiRelation*>::iterator it = inds.begin();
          it != inds.end(); ++it) {
        set<DecisionVariable*> r = getDecisionVariablesIn(
            node->getAssignment(*it), parsed);
        l.insert(r.begin(), r.end());
      }
      return l;
    }
    template<class P>
    set<DecisionVariable*> getDecisionVariablesIn(
        QualifiedExpression<P>* node, set<ExpressionNode*> parsed) {
      if (parsed.find(node) != parsed.end()) {
        return set<DecisionVariable*>();
      }
      set<DecisionVariable*> l;
      set<DecisionVariable*> tmp = getDecisionVariablesIn(
          node->getQualified(), parsed);
      l.insert(tmp.begin(), tmp.end());
      list<NumericExpression*> le = node->getQualifier()
          ->getExpressions();
      for (list<NumericExpression*>::iterator ite = le.begin();
          ite != le.end(); ++ite) {
        tmp = getDecisionVariablesIn(*ite, parsed);
        l.insert(tmp.begin(), tmp.end());
      }
      return l;
    }
    set<DecisionVariable*> getDecisionVariablesIn(
        ExpressionNode* node, set<ExpressionNode*> parsed) {
      if (parsed.find(node) != parsed.end()) {
        return set<DecisionVariable*>();
      }
      // Dispatch method
      Variable<NumericExpression>* v = dynamic_cast<Variable<
          NumericExpression>*>(node);
      if (v != NULL) {
        // Specific variables
        DecisionVariable* dv = dynamic_cast<DecisionVariable*>(node);
        if (dv != NULL) {
          set<DecisionVariable*> l;
          l.insert(dv);
          return l;
        }
        ParamVariable* pv = dynamic_cast<ParamVariable*>(node);
        if (pv != NULL) {
          return getDecisionVariablesIn(pv, parsed);
        }

        // Others aren't concerned
        return set<DecisionVariable*>();
      }
      BinOp<booleans::BoolOp, booleans::BoolOp>* binop =
          dynamic_cast<BinOp<booleans::BoolOp, booleans::BoolOp>*>(node);
      if (binop != NULL) {
        // Specific binops...

        // Default binops
        return getDecisionVariablesIn(binop, parsed);
      }
      BinOp<numerics::NumericExpression, booleans::BoolOp>* binopnb =
          dynamic_cast<BinOp<numerics::NumericExpression,
              booleans::BoolOp>*>(node);
      if (binopnb != NULL) {
        // Specific binops...

        // Default binops
        return getDecisionVariablesIn(binopnb, parsed);
      }
      BinOp<numerics::NumericExpression, numerics::NumericExpression>* binopnn =
          dynamic_cast<BinOp<numerics::NumericExpression,
              numerics::NumericExpression>*>(node);
      if (binopnn != NULL) {
        // Specific binops...

        // Default binops
        return getDecisionVariablesIn(binopnn, parsed);
      }
      {
        UnOp<numerics::NumericExpression, numerics::NumericExpression>* unop =
            dynamic_cast<UnOp<numerics::NumericExpression,
                numerics::NumericExpression>*>(node);
        if (unop != NULL) {
          // Specific unops

          // Default unops
          return getDecisionVariablesIn(unop, parsed);
        }
      }
      {
        UnOp<booleans::BoolOp, booleans::BoolOp>* unop =
            dynamic_cast<UnOp<booleans::BoolOp, booleans::BoolOp>*>(node);
        if (unop != NULL) {
          // Specific unops

          // Default unops
          return getDecisionVariablesIn(unop, parsed);
        }
      }
      // BigOp
      {
        BigOp<numerics::NumericExpression>* bigop =
            dynamic_cast<BigOp<numerics::NumericExpression>*>(node);
        if (bigop != NULL) {
          // Specific bigops

          // Default bigops
          return getDecisionVariablesIn(bigop, parsed);
        }
      }
      QualifiedExpression<numerics::NumericExpression>* qe =
          dynamic_cast<QualifiedExpression<numerics::NumericExpression>*>(node);
      if (qe != NULL) {
        return getDecisionVariablesIn(qe, parsed);
      }
      RealConstant* rc = dynamic_cast<RealConstant*>(node);
      if (rc != NULL) {
        return set<DecisionVariable*>();
      }
      RelativeConstant* ic = dynamic_cast<RelativeConstant*>(node);
      if (ic != NULL) {
        return set<DecisionVariable*>();
      }

      AssignmentMap<numerics::NumericExpression>* am =
          dynamic_cast<AssignmentMap<numerics::NumericExpression>*>(node);
      if (am != NULL) {
        return getDecisionVariablesIn(am, parsed);
      }

      cerr << "Unknonw extractor for decision variables in \""
          << node->toString() << "\"" << endl;
      return set<DecisionVariable*>();
    }

    bool findSymbols(set<ExpressionNode*> syms, Qualifier* node,
        set<ExpressionNode*> parsed) {
      list<NumericExpression*> l = node->getExpressions();
      for (list<NumericExpression*>::iterator it = l.begin();
          it != l.end(); ++it) {
        if (findSymbols(syms, *it, parsed)) {
          return true;
        }
      }
      return false;
    }
    bool findSymbols(set<ExpressionNode*> syms,
        IndexingMultiRelation* node, set<ExpressionNode*> parsed) {
      vector<Index*> indexes = node->getIndexingSets();
      for (unsigned int var = 0; var < indexes.size(); ++var) {
        if (findSymbols(syms, indexes[var]->getStart(), parsed)
            || findSymbols(syms, indexes[var]->getStep(), parsed)
            || findSymbols(syms, indexes[var]->getStop(), parsed)) {
          return true;
        }
      }
      return findSymbols(syms, node->getPredicate(), parsed);
    }
    bool findSymbols(set<ExpressionNode*> syms,
        AssignmentMap<numerics::NumericExpression>* node,
        set<ExpressionNode*> parsed) {
      bool present = findSymbols(syms, node->getDefault(), parsed);
      if (present) return true;
      list<IndexingMultiRelation*> inds =
          node->getIndexingRelations();
      for (list<IndexingMultiRelation*>::iterator it = inds.begin();
          it != inds.end(); ++it) {
        present = findSymbols(syms, *it, parsed)
            || findSymbols(syms, node->getAssignment(*it), parsed);
        if (present) return true;
      }
      return false;

    }
    bool findSymbols(set<ExpressionNode*> syms, ExpressionNode *node,
        set<ExpressionNode*> parsed) {
      if (node == NULL) return false;
      if (syms.find(node) != syms.end()) {
        return true;
      }
      if (parsed.find(node) != parsed.end()) {
        return false;
      }

      Variable<NumericExpression>* v = dynamic_cast<Variable<
          NumericExpression>*>(node);
      if (v != NULL) {
        // Specific variables
        DecisionVariable* dv = dynamic_cast<DecisionVariable*>(node);
        if (dv != NULL) {
          return false;
        }
        ParamVariable* pv = dynamic_cast<ParamVariable*>(node);
        if (pv != NULL) {
          set<ExpressionNode*> p(parsed);
          p.insert(pv);
          return findSymbols(syms, pv->getExpression(), p);
        }

        // Others aren't concerned
        return false;
      }
      BinOp<booleans::BoolOp, booleans::BoolOp>* binop =
          dynamic_cast<BinOp<booleans::BoolOp, booleans::BoolOp>*>(node);
      if (binop != NULL) {
        // Specific binops...

        // Default binops
        return findSymbols(syms, binop->getLeft(), parsed)
            || findSymbols(syms, binop->getRight(), parsed);
      }
      BinOp<numerics::NumericExpression, booleans::BoolOp>* binopnb =
          dynamic_cast<BinOp<numerics::NumericExpression,
              booleans::BoolOp>*>(node);
      if (binopnb != NULL) {
        // Specific binops...

        // Default binops
        return findSymbols(syms, binopnb->getLeft(), parsed)
            || findSymbols(syms, binopnb->getRight(), parsed);
      }
      BinOp<numerics::NumericExpression, numerics::NumericExpression>* binopnn =
          dynamic_cast<BinOp<numerics::NumericExpression,
              numerics::NumericExpression>*>(node);
      if (binopnn != NULL) {
        // Specific binops...

        // Default binops
        return findSymbols(syms, binopnn->getLeft(), parsed)
            || findSymbols(syms, binopnn->getRight(), parsed);
      }
      {
        UnOp<numerics::NumericExpression, numerics::NumericExpression>* unop =
            dynamic_cast<UnOp<numerics::NumericExpression,
                numerics::NumericExpression>*>(node);
        if (unop != NULL) {
          // Specific unops

          // Default unops
          return findSymbols(syms, unop->getArgument(), parsed);
        }
      }
      {
        UnOp<booleans::BoolOp, booleans::BoolOp>* unop =
            dynamic_cast<UnOp<booleans::BoolOp, booleans::BoolOp>*>(node);
        if (unop != NULL) {
          // Specific unops

          // Default unops
          return findSymbols(syms, unop->getArgument(), parsed);
        }
      }
      // BigOp
      {
        BigOp<numerics::NumericExpression>* bigop =
            dynamic_cast<BigOp<numerics::NumericExpression>*>(node);
        if (bigop != NULL) {
          // Specific bigops

          // Default bigops
          return findSymbols(syms, bigop->getExpression(), parsed)
              || findSymbols(syms, bigop->getIndexing(), parsed);
        }
      }
      QualifiedExpression<numerics::NumericExpression>* qe =
          dynamic_cast<QualifiedExpression<numerics::NumericExpression>*>(node);
      if (qe != NULL) {
        return findSymbols(syms, qe->getQualified(), parsed)
            || findSymbols(syms, qe->getQualifier(), parsed);
      }
      RealConstant* rc = dynamic_cast<RealConstant*>(node);
      if (rc != NULL) {
        return false;
      }
      RelativeConstant* ic = dynamic_cast<RelativeConstant*>(node);
      if (ic != NULL) {
        return false;
      }

      AssignmentMap<numerics::NumericExpression>* am =
          dynamic_cast<AssignmentMap<numerics::NumericExpression>*>(node);
      if (am != NULL) {
        return findSymbols(syms, am, parsed);
      }

      cerr << "Unknonw extractor for testing presence of symbol in \""
          << node->toString() << "\"" << endl;
      return true;
    }
  }  // namespace extract
}  // namespace alpi
