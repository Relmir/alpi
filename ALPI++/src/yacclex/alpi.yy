%skeleton "lalr1.cc" /* -*- C++ -*- */
%glr-parser
%require "3.0.2"
%defines
%define parser_class_name {alpiparser}
%define api.token.constructor
%define api.value.type variant
%define parse.assert

%code requires
{
#include <string>
#include <list>
#include "../exprs/indexing/Index.h"
#include "../exprs/Expressions.h"
#include "../exprs/indexing/IndexingMultiRelation.h"
namespace alpiparsertools {
  class driver;
}
}
%param { alpiparsertools::driver& driver }
%locations
%initial-action
{
 @$.begin.filename = @$.end.filename = &driver.file;
};
%define parse.trace
%define parse.error verbose
%code
{
#include "../driver/driver.hh"
yy::alpiparser::symbol_type yylex(alpiparsertools::driver& driver);
}
%define api.token.prefix {TOK_}
%token  <std::string> NATURAL   "entier"
%token  <std::string> NUMBER    "réel"
%token  BOOLEAN   "booléen"
%token
  END  0    "end of file"
  OP_BRACK  "["
  CL_BRACK  "]"
  OP_PAREN  "("
  CL_PAREN  ")"
  OP_BRACE  "{"
  CL_BRACE  "}"
  PLUS      "+"
  MINUS     "-"
  POWER     "^"
  AND       "&&"
  OR        "||"
  TIMES     "*"
  DIVIDE    "/"
  SUM       "sum"
  PROD      "prod"
  EQ        "=="
  LEQ       "<="
  GEQ       ">="
  LT        "<"
  GT        ">"
  PREC      "+/-"
  AFFECT    "="
  COLON     ":"
  MINIMIZE  "minimize"
  SET       "set"
  VAR       "var"
  PARAM     "param"
  SUBJECT_TO  "subject to"
  OPTION    "option"
  IN        "in"
  BY        "by"
  SPAN      ".."
  COMMA     ","
  SEMICOLON ";"
  INTEGER   "integer"
;
%token  <std::string> FUN       "fonction du langage (cos, sqrt, sin...)"
%token  <std::string> IDENT     "identifier"
%token  <std::string> FILENAME  "nom de fichier"

%type <alpi::exprs::numerics::NumericExpression*> Relative
%type <alpi::exprs::numerics::RelativeConstant*> CstRelative

%type <alpi::exprs::numerics::RealConstant*> CstReal
%type <alpi::exprs::numerics::NumericExpression*> Real

%type <int> Test

%type <alpi::exprs::indexing::Index*> IndexDef
%type <alpi::exprs::numerics::NumericExpression*> ByStep
%type <alpi::exprs::indexing::Index*> Index
%type <alpi::exprs::indexing::IndexingMultiRelation*> IndexingOpt
%type <alpi::exprs::indexing::IndexingMultiRelation*> IndexingList
%type <alpi::exprs::symbol::IndexVariable*> Indexing
%type <alpi::exprs::indexing::Index*> InIndexOpt
%type <alpi::exprs::booleans::BoolOp*> IndexingReducer
%type <alpi::exprs::booleans::BoolOp*> BExpression

%type <std::string> Ident
%type <alpi::exprs::numerics::NumericExpression*> Expression
%type <std::list<alpi::exprs::numerics::NumericExpression*>> ListExpression
%type <alpi::exprs::Qualifier*> IdentQualif

%type <alpi::exprs::numerics::NumericExpression*> EndConstraint

%type <std::string> Anything

%type <alpi::exprs::indexing::IndexingMultiRelation*> IdentDefQualif
%type <alpi::exprs::indexing::IndexingMultiRelation*> IdentDefQualifParams
%type <alpi::exprs::indexing::IndexingMultiRelation*> IdentDefQualifParam


%left   PLUS      MINUS
%left   NEG       TIMES     DIVIDE
%right  POWER

%left   OR
%left   AND

%% 
%start ProblemDesc;

//===================
// upper rules
ProblemDesc:
          Instructions END
        ;

Instructions:
          %empty
        | Instructions Instruction SEMICOLON
        ;

Instruction:
          SetDef
        | VariableDef
        | DefParam
        | DefCtr
        | ObjDef
        | Option
        | SimpleAffectation
        ;

// End upper rules
//===================
// lower rules
Ident:
          IDENT
          { $$ = driver.identFromIdent($1); }
        ;

CstRelative:
          MINUS NATURAL
          { $$ = new alpiparsertools::RelativeConstant("-"+$2); }
        | NATURAL
          { $$ = new alpiparsertools::RelativeConstant($1); }
        ;

Relative:
          CstRelative
          { $$ = driver.numerics()->relativeFromCst($1); }
        | Ident
          { $$ = driver.numerics()->relativeFromVariable($1); }
        | MINUS Ident
          { $$ = driver.numerics()->relativeFromOpposeVariable($2); }
        ;

CstReal:
          NUMBER
          { $$ = new alpiparsertools::RealConstant($1); }
        | MINUS NUMBER
          { $$ = new alpiparsertools::RealConstant("-"+$2); }
        ;

Real:
          CstReal
          { $$ = driver.numerics()->realFromCst($1); }
        | Ident
          { $$ = driver.numerics()->realFromVariable($1); }
        | MINUS Ident
          { $$ = driver.numerics()->realFromOpposeVariable($2); }
        ;

Test:
          LEQ
          { $$ = -1; }
        | EQ
          { $$ = 0; }
        | GEQ
          { $$ = 1; }
        | LT
          { $$ = -2; }
        | GT
          { $$ = 2; }
        ;

//================
// Indexes        
IndexDef:
          Expression SPAN Expression ByStep
          { $$ = driver.indexing()->indexDef($1, $3, $4); }
        ;
        
ByStep:
          BY Expression
          { std::swap($$, $2); }
        | %empty
          { $$ = driver.indexing()->indexDefByStepEmpty(); }
        ;

Index:
          Ident
          { $$ = driver.getIndex($1); }
        | IndexDef
          { $$ = driver.indexing()->indexFromDef($1); }
        ;

IndexingOpt:
          %empty
          { $$ = driver.indexing()->indexingOptEmpty(); }
        | OP_BRACE IndexingList IndexingReducer CL_BRACE
          { $$ = driver.indexing()->indexingOptFromSetExpr($2, $3); }
        ;

IndexingList:
          Indexing
          { $$ = driver.indexing()->indexingListFromOne($1); }
        | IndexingList COMMA Indexing
          { $$ = driver.indexing()->indexingListFromNext($1, $3); }
        ;
        
Indexing:
          Ident InIndexOpt
          { $$ = driver.indexing()->indexingFromIdentOptInIndex($1, $2); }
        | IndexDef
          { $$ = driver.indexing()->indexingFromDef($1); }
        ;

InIndexOpt:
          IN Index
          { $$ = driver.indexing()->inIndexFromIndex($2); }
        | %empty
          { $$ = driver.indexing()->inIndexFromEmpty(); }
        ;
        
IndexingReducer:
          COLON BExpression
          { $$ = driver.indexing()->reducerFromTest($2); }
        | %empty
          { $$ = driver.indexing()->reducerFromEmpty(); }
        ;
// Indexes
//================
// Sets
SetDef:
          SET Ident
          { driver.setDefAfterIdent($2); }
          IndexingOpt
          { driver.setDefAfterIndexing($2, $4); }
          SetDefAttrList
        ;

SetDefAttrList:
          %empty
        | SetDefAttrList SetDefAttr
        ;
        
SetDefAttr: 
          AFFECT IndexDef
          { driver.setDefAttrAffect($2); }
        ;
// Sets
//================
// Variables
VariableDef:
          VAR Ident
          { driver.variableCreationBeforeIndexing($2); }
          IndexingOpt
          { driver.variableCreation($2, $4); }
          VarAttrOpt
          { driver.variableCreationEnd(); }
        ;

VarAttrOpt:
          %empty
        | VarAttrs
        ;
        
VarAttrs:
          VarAttr
        | VarAttrs COMMA VarAttr
        ;

VarAttr:
          LEQ Expression
          { driver.variableUpperBound($2); }
        | GEQ Expression
          { driver.variableLowerBound($2); }
        | PREC Expression
          { driver.variablePrecision($2); }
        | INTEGER
          { driver.variableSetInteger(); }
        | AFFECT Expression
          { /* Ignored for now */ }
        ;
// Variables
//================
// Expressions
Expression:
          FUN OP_PAREN ListExpression CL_PAREN
          { $$ = driver.expressions()->expressionFromFunctionCall($1, $3); }
        | Ident
          IdentQualif
          { $$ = driver.expressions()->expressionFromIdentifier($1, $2); }
        | CstRelative
          { $$ = driver.expressions()->expressionFromConstant($1); }
        | CstReal
          { $$ = driver.expressions()->expressionFromConstant($1); }
        | Expression PLUS Expression
          { $$ = driver.expressions()->expressionFromAddition($1, $3); }
        | Expression MINUS Expression
          { $$ = driver.expressions()->expressionFromDifference($1, $3); }
        | Expression TIMES Expression
          { $$ = driver.expressions()->expressionFromMultiplication($1, $3); }
        | Expression DIVIDE Expression
          { $$ = driver.expressions()->expressionFromDivision($1, $3); }
        | Expression POWER Expression
          { $$ = driver.expressions()->expressionFromPower($1, $3); }
        | MINUS Expression %prec NEG
          { $$ = driver.expressions()->expressionFromOppose($2); }
        | OP_PAREN Expression CL_PAREN
          { std::swap($$, $2); }
        | SUM OP_BRACE
          { driver.expressions()->expressionFromSumBeforeIndexing(); }
          IndexingList IndexingReducer
          CL_BRACE OP_PAREN Expression CL_PAREN
          { $$ = driver.expressions()->expressionFromSum($4, $5, $8); }
        | PROD OP_BRACE
          { driver.expressions()->expressionFromProdBeforeIndexing(); }
          IndexingList IndexingReducer
          CL_BRACE OP_PAREN Expression CL_PAREN
          { $$ = driver.expressions()->expressionFromProd($4, $5, $8); }
        ;

IdentQualif:
          %empty
          { $$ = driver.expressions()->identQualifFromEmpty(); }
        | OP_BRACK ListExpression CL_BRACK
          { $$ = driver.expressions()->identQualifFromArgs($2); }
        ;

ListExpression:
          Expression
          { $$ = driver.expressions()->listExpressionFromUnit($1); }
        | ListExpression COMMA Expression
          { $$ = driver.expressions()->listExpressionFromList($1, $3); }
        ;
        
BExpression:
          BExpression AND BExpression
          { $$ = driver.expressions()->booleanAndExpression($1, $3); }
        | BExpression OR BExpression
          { $$ = driver.expressions()->booleanOrExpression($1, $3); }
        | Expression Test Expression
          { $$ = driver.expressions()->booleanTestExpression($1, $2, $3); }
        ;
// Expressions
//================
// Constraints
DefCtr:
          SUBJECT_TO Ident
          { driver.constraints()->definitionBeforeIndex($2); }
          IndexingOpt
          { driver.constraints()->definitionAfterIndex($2, $4); }
          COLON Expression Test Expression EndConstraint DefCtrAttrs
          { driver.constraints()->definitionEnd($2, $4, $7, $8, $9, $10); }
        ;

EndConstraint:
          %empty
          { $$ = driver.constraints()->endConstraintFromEmpty(); }
        | LEQ Expression
          { $$ = driver.constraints()->endConstraintFromTest($2); }
        ;

DefCtrAttrs:
          %empty
        | DefCtrAttrs COMMA DefCtrAttr
        ;

DefCtrAttr:
          PREC Expression
          { driver.constraints()->attributePrecision($2); }
        ;
// Constraints
//================
// Objective
ObjDef:
          MINIMIZE Ident
          { driver.objective()->definition($2); }
          ObjDefAttrsOpt
          { driver.objective()->definitionEnd(); }          
        ;
        
ObjDefAttrsOpt:
          %empty
        | ObjDefAttrs
        ;

ObjDefAttrs:
          ObjDefAttr
        | ObjDefAttrs COMMA ObjDefAttr
        ;

ObjDefAttr:
          COLON Expression
          { driver.objective()->affectExpression($2); }
        | PREC Real
          { driver.objective()->setPrecision($2); }
        ;
// Objective
//================
// Options
Option:
          OPTION Anything AFFECT Anything
          { driver.options()->setOption($2, $4); }
        ;

Anything:
          CstReal
          { $$ = driver.options()->anythingFromConstant($1); }
        | CstRelative
          { $$ = driver.options()->anythingFromConstant($1); }
        | Ident
          { $$ = driver.options()->anythingFromIdent($1); }
        | FILENAME
          { $$ = driver.options()->anythingFromFileName($1); }
        | SUM
          { $$ = driver.options()->anythingFromSum(); }
        | PROD
          { $$ = driver.options()->anythingFromProd(); }
        | PARAM
          { $$ = driver.options()->anythingFromParam(); }
        | SET
          { $$ = driver.options()->anythingFromSet(); }
        | MINIMIZE
          { $$ = driver.options()->anythingFromMinimize(); }
        ;
// Options
//================
// Params
DefParam:
          PARAM Ident 
          { driver.defParamBeforeIndexing($2); }
          IndexingOpt
          { driver.defParamAfterIndexing($2, $4); }
          ParamAttrOpt
          { driver.defParamEnd(); }
        ;
        
ParamAttrOpt:
          %empty
        | ParamAttrs
        ;

ParamAttrs:
          ParamAttr
        | ParamAttrs COMMA ParamAttr
        ;

ParamAttr:
          AFFECT Expression
          { driver.defParamExpression($2); }
        | INTEGER
          { driver.defParamInteger(); }
        ;

SimpleAffectation:
          Ident
          { driver.params()->enterSimpleAffectation($1); }
          IdentDefQualif AFFECT Expression
          { driver.params()->simpleAffectation($1, $3, $5); }
        ;
        
IdentDefQualif:
          %empty
          { $$ = driver.params()->identDefQualifFromEmpty(); }
        | OP_PAREN IdentDefQualifParams CL_PAREN
          { $$ = driver.params()->identDefQualifFromList($2); }
        ;

IdentDefQualifParams:
          IdentDefQualifParam
          { $$ = driver.params()->identDefQualifParamsFromUnit($1); }
        | IdentDefQualifParams COMMA IdentDefQualifParam
          { $$ = driver.params()->identDefQualifParamsFromNext($1, $3); }
        ;

IdentDefQualifParam:
          Expression
          { $$ = driver.params()->identDefQualifParamFromRelative($1); }
        | OP_BRACE Indexing IndexingReducer CL_BRACE
          { $$ = driver.params()->identDefQualifParamFromIndexing($2, $3); }
        ;
// Params
//================
%%

void
yy::alpiparser::error (const location_type& loc,
    const std::string& m) {
  driver.error(loc, m);
}
