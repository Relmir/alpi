/*
 * expression_node_printer.h
 *
 *  Created on: Jan 27, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef EXPRESSION_NODE_PRINTER_H_
#define EXPRESSION_NODE_PRINTER_H_

#include <ostream>
#include <string>

namespace alpi {
  namespace exprs {
    class ExpressionNode;
    namespace operations {
      template<class P, class R>
      class BinOp;
      template<class P, class R>
      class UnOp;
    }
    namespace numerics {
      class NumericExpression;
    }
    namespace booleans {
      class BoolOp;
    }
  }
  namespace print {

    /** \brief Print an expression to a stream
     *
     * \param node the node to print
     * \param stream the stream
     * \return void
     *
     */
    void print(alpi::exprs::ExpressionNode* node,
        std::ostream& stream);
    /** \brief Print an expression to a stream
     *
     * \param node the node to print
     * \param stream the stream
     * \return void
     *
     */
    template<class Param, class Result>
    void print(alpi::exprs::operations::BinOp<Param, Result>* node,
        std::ostream& stream, std::string lhs, std::string rhs);
    /** \brief Print an expression to a stream
     *
     * \param node the node to print
     * \param stream the stream
     * \return void
     *
     */
    template<class P, class R>
    void print(alpi::exprs::operations::UnOp<P, R>* node,
        std::ostream& stream, std::string inNode);

    bool needParenthesis(alpi::exprs::numerics::NumericExpression* in,
        alpi::exprs::numerics::NumericExpression* out, int position);

    bool needParenthesis(alpi::exprs::numerics::NumericExpression* in,
        alpi::exprs::booleans::BoolOp* out, int position);
    bool needParenthesis(alpi::exprs::booleans::BoolOp* in,
        alpi::exprs::booleans::BoolOp* out, int position);
    std::string symbol(alpi::exprs::ExpressionNode* node);
  }  // namespace print

}  // namespace alpi
#include "expression_node_printer.tpp"

#endif /* EXPRESSION_NODE_PRINTER_H_ */
