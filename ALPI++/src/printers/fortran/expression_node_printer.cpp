/*
 * expression_node_printer.cpp
 *
 *  Created on: Jan 27, 2014
 *      Author: Emmanuel BIGEON
 */

#include "expression_node_printer.h"

#include <iostream>
#include <map>
#include <string>

#include "../../exprs/booleans/BooleanAnd.h"
#include "../../exprs/booleans/BooleanOr.h"
#include "../../exprs/booleans/Equal.h"
#include "../../exprs/booleans/GreaterEqual.h"
#include "../../exprs/booleans/GreaterThan.h"
#include "../../exprs/booleans/LowerEqual.h"
#include "../../exprs/booleans/LowerThan.h"
#include "../../exprs/numerics/RealConstant.h"
#include "../../exprs/numerics/RelativeConstant.h"
#include "../../exprs/operations/Addition.h"
#include "../../exprs/operations/Difference.h"
#include "../../exprs/operations/Division.h"
#include "../../exprs/operations/Multiplication.h"
#include "../../exprs/operations/Oppose.h"
#include "../../exprs/operations/Power.h"
#include "../../exprs/symbol/Symbol.h"

namespace alpi {

  namespace print {
    namespace fortran {

      using namespace std;
      using namespace alpi::exprs::booleans;
      using namespace alpi::exprs::operations;
      using namespace alpi::exprs;
      using namespace alpi::exprs::symbol;

      std::string symbol(ExpressionNode* node) {
        Addition* add = dynamic_cast<Addition*>(node);
        if (add != NULL) {
          return "+";
        }
        Difference* diff = dynamic_cast<Difference*>(node);
        Oppose* opp = dynamic_cast<Oppose*>(node);
        if (diff != NULL || opp != NULL) {
          return "-";
        }
        Division* div = dynamic_cast<Division*>(node);
        if (div != NULL) {
          return "/";
        }
        Power* pow = dynamic_cast<Power*>(node);
        if (pow != NULL) {
          return "**";
        }
        Multiplication* multi = dynamic_cast<Multiplication*>(node);
        if (multi != NULL) {
          return "*";
        }
        BooleanAnd* band = dynamic_cast<BooleanAnd*>(node);
        if (band != NULL) {
          return ".AND.";
        }
        BooleanOr* bor = dynamic_cast<BooleanOr*>(node);
        if (bor != NULL) {
          return ".OR.";
        }
        LowerThan* lt = dynamic_cast<LowerThan*>(node);
        if (lt != NULL) {
          return "<";
        }
        LowerEqual* le = dynamic_cast<LowerEqual*>(node);
        if (le != NULL) {
          return "<=";
        }
        Equal* eq = dynamic_cast<Equal*>(node);
        if (eq != NULL) {
          return "==";
        }
        GreaterEqual* ge = dynamic_cast<GreaterEqual*>(node);
        if (ge != NULL) {
          return ">=";
        }
        GreaterThan* gt = dynamic_cast<GreaterThan*>(node);
        if (gt != NULL) {
          return ">";
        }

        cerr << "UNDEFINED SYMBOL FOR BIN OP " << node->toString()
            << endl;
        return ".UNDEFINED.";
      }

      void print(ExpressionNode* node, ostream& stream,
          map<Symbol*, Symbol*> mapping) {
        // BinOps
        BinOp<BoolOp, BoolOp>* binop = dynamic_cast<BinOp<BoolOp,
            BoolOp>*>(node);
        if (binop != NULL) {
          print(binop, stream, mapping);
          return;
        }
        BinOp<NumericExpression, BoolOp>* binopn = dynamic_cast<BinOp<
            NumericExpression, BoolOp>*>(node);
        if (binopn != NULL) {
          print(binopn, stream, mapping);
          return;
        }

        RealConstant* rec = dynamic_cast<RealConstant*>(node);
        if (rec != NULL) {
          stream << rec->toString();
          return;
        }
        RelativeConstant* rlc = dynamic_cast<RelativeConstant*>(node);
        if (rlc != NULL) {
          stream << rlc->toString();
          return;
        }

        Symbol* sym = dynamic_cast<Symbol*>(node);
        if (sym != NULL) {
          if (mapping.find(sym) != mapping.end()) {
            stream << "(" << mapping[sym]->getName() << ")";
          } else {
            stream << "(" << sym->getName() << ")";
          }
          return;
        }

        cerr << "fortran expression printer: Unknown translation for "
            << node->toString() << endl;
        stream << "(" << node->toString() << ")";
      }

      bool needParenthesis(numerics::NumericExpression* in,
          numerics::NumericExpression* out, int position) {
        Addition* add = dynamic_cast<Addition*>(out);
        if (add != NULL) {
          Oppose* op = dynamic_cast<Oppose*>(in);
          if (op != NULL) {
            return true;
          }
          return false;
        }
        Difference* diff = dynamic_cast<Difference*>(out);
        if (diff != NULL) {
          Oppose* op = dynamic_cast<Oppose*>(in);
          if (op != NULL) {
            return true;
          }
          if (position == 1) {
            Addition* addIn = dynamic_cast<Addition*>(in);
            if (addIn != NULL) {
              return true;
            }
          }
          return false;
        }
        Multiplication* mult = dynamic_cast<Multiplication*>(out);
        Division* div = dynamic_cast<Division*>(out);
        if (div != NULL || mult != NULL) {
          Oppose* op = dynamic_cast<Oppose*>(in);
          Addition* addIn = dynamic_cast<Addition*>(in);
          Difference* diffIn = dynamic_cast<Difference*>(in);
          if (diffIn != NULL || addIn != NULL || op != NULL) {
            return true;
          }
          return false;
        }
        Power* pow = dynamic_cast<Power*>(out);
        if (pow != NULL) {
          Oppose* op = dynamic_cast<Oppose*>(in);
          Addition* addIn = dynamic_cast<Addition*>(in);
          Difference* diffIn = dynamic_cast<Difference*>(in);
          Multiplication* mult = dynamic_cast<Multiplication*>(in);
          Division* div = dynamic_cast<Division*>(in);
          if (div != NULL || mult != NULL || diffIn != NULL
              || addIn != NULL || op != NULL) {
            return true;
          }
          return false;
        }

        Oppose* opp = dynamic_cast<Oppose*>(out);
        if (opp != NULL) {
          Oppose* op = dynamic_cast<Oppose*>(in);
          Addition* addIn = dynamic_cast<Addition*>(in);
          Difference* diffIn = dynamic_cast<Difference*>(in);
          if (diffIn != NULL || addIn != NULL || op != NULL) {
            return true;
          }
          return false;
        }
        return false;
      }

      bool needParenthesis(numerics::NumericExpression* in,
          booleans::BoolOp* out, int position) {

        return false;
      }

      bool needParenthesis(booleans::BoolOp* in,
          booleans::BoolOp* out, int position) {
        return true;
      }
    }
  }  // namespace print

}  // namespace alpi

