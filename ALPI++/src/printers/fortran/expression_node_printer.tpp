namespace alpi {
  namespace print {
    namespace fortran {

      template<class P, class R>
      void print(alpi::exprs::operations::UnOp<P, R>* node,
          std::ostream& stream, std::string in) {
        if (needParenthesis(node->getArgument(), node, 0)) {
          stream << symbol(node) << "(" << in << ")";
        } else {
          stream << symbol(node) << in;
        }
      }

      template<class Param, class Result>
      void print(alpi::exprs::operations::BinOp<Param, Result>* node,
          std::ostream& stream, std::string lhs, std::string rhs) {
        if (needParenthesis(node->getLeft(), node, 0)) {
          stream << "(" << lhs << ")";
        } else {
          stream << lhs;
        }
        stream << " " << symbol(node) << " ";
        if (needParenthesis(node->getRight(), node, 1)) {
          stream << "(" << rhs << ")";
        } else {
          stream << rhs;
        }
        return;
      }
      template<class Param, class Result>
      void print(alpi::exprs::operations::BinOp<Param, Result>* node,
          std::ostream& stream,
          std::map<alpi::exprs::symbol::Symbol*,
              alpi::exprs::symbol::Symbol*> mapping) {
        if (needParenthesis(node->getLeft(), node, 0)) {
          stream << "(";
          print(node->getLeft(), stream, mapping);
          stream << ")";
        } else {
          print(node->getLeft(), stream, mapping);
        }
        stream << " " << symbol(node) << " ";
        if (needParenthesis(node->getRight(), node, 1)) {
          stream << "(";
          print(node->getRight(), stream, mapping);
          stream << ")";
        } else {
          print(node->getRight(), stream, mapping);
        }
        return;
      }
    }
  }
}