namespace alpi {
  namespace print {

    template<class P, class R>
    void print(alpi::exprs::operations::UnOp<P, R>* node,
        std::ostream& stream, std::string in) {
      if (needParenthesis(node->getArgument(), node, 0)) {
        stream << symbol(node) << "(" << in << ")";
      } else {
        stream << symbol(node) << in;
      }
    }

    template<class Param, class Result>
    void print(alpi::exprs::operations::BinOp<Param, Result>* node,
        std::ostream& stream, std::string lhs, std::string rhs) {
      if (needParenthesis(node->getLeft(), node, 0)) {
        stream << "(" << lhs << ")";
      } else {
        stream << lhs;
      }
      stream << " " << symbol(node) << " ";
      if (needParenthesis(node->getRight(), node, 1)) {
        stream << "(" << rhs << ")";
      } else {
        stream << rhs;
      }
      return;
    }

  }
}