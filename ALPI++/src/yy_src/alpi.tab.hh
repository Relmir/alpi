// A Bison parser, made by GNU Bison 3.0.2.

// Skeleton interface for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2013 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

/**
 ** \file alpi.tab.hh
 ** Define the yy::parser class.
 */

// C++ LALR(1) parser skeleton written by Akim Demaille.

#ifndef YY_YY_ALPI_TAB_HH_INCLUDED
# define YY_YY_ALPI_TAB_HH_INCLUDED
// //                    "%code requires" blocks.
#line 11 "../src/yacclex/alpi.yy" // lalr1.cc:372

#include <string>
#include <list>
#include "../exprs/indexing/Index.h"
#include "../exprs/Expressions.h"
#include "../exprs/indexing/IndexingMultiRelation.h"
namespace alpiparsertools {
  class driver;
}

#line 55 "alpi.tab.hh" // lalr1.cc:372

# include <cassert>
# include <vector>
# include <iostream>
# include <stdexcept>
# include <string>
# include "stack.hh"
# include "location.hh"
#include <typeinfo>
#ifndef YYASSERT
# include <cassert>
# define YYASSERT assert
#endif


#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif


namespace yy {
#line 131 "alpi.tab.hh" // lalr1.cc:372



  /// A char[S] buffer to store and retrieve objects.
  ///
  /// Sort of a variant, but does not keep track of the nature
  /// of the stored data, since that knowledge is available
  /// via the current state.
  template <size_t S>
  struct variant
  {
    /// Type of *this.
    typedef variant<S> self_type;

    /// Empty construction.
    variant ()
      : yytname_ (YY_NULLPTR)
    {}

    /// Construct and fill.
    template <typename T>
    variant (const T& t)
      : yytname_ (typeid (T).name ())
    {
      YYASSERT (sizeof (T) <= S);
      new (yyas_<T> ()) T (t);
    }

    /// Destruction, allowed only if empty.
    ~variant ()
    {
      YYASSERT (!yytname_);
    }

    /// Instantiate an empty \a T in here.
    template <typename T>
    T&
    build ()
    {
      YYASSERT (!yytname_);
      YYASSERT (sizeof (T) <= S);
      yytname_ = typeid (T).name ();
      return *new (yyas_<T> ()) T;
    }

    /// Instantiate a \a T in here from \a t.
    template <typename T>
    T&
    build (const T& t)
    {
      YYASSERT (!yytname_);
      YYASSERT (sizeof (T) <= S);
      yytname_ = typeid (T).name ();
      return *new (yyas_<T> ()) T (t);
    }

    /// Accessor to a built \a T.
    template <typename T>
    T&
    as ()
    {
      YYASSERT (yytname_ == typeid (T).name ());
      YYASSERT (sizeof (T) <= S);
      return *yyas_<T> ();
    }

    /// Const accessor to a built \a T (for %printer).
    template <typename T>
    const T&
    as () const
    {
      YYASSERT (yytname_ == typeid (T).name ());
      YYASSERT (sizeof (T) <= S);
      return *yyas_<T> ();
    }

    /// Swap the content with \a other, of same type.
    ///
    /// Both variants must be built beforehand, because swapping the actual
    /// data requires reading it (with as()), and this is not possible on
    /// unconstructed variants: it would require some dynamic testing, which
    /// should not be the variant's responsability.
    /// Swapping between built and (possibly) non-built is done with
    /// variant::move ().
    template <typename T>
    void
    swap (self_type& other)
    {
      YYASSERT (yytname_);
      YYASSERT (yytname_ == other.yytname_);
      std::swap (as<T> (), other.as<T> ());
    }

    /// Move the content of \a other to this.
    ///
    /// Destroys \a other.
    template <typename T>
    void
    move (self_type& other)
    {
      build<T> ();
      swap<T> (other);
      other.destroy<T> ();
    }

    /// Copy the content of \a other to this.
    template <typename T>
    void
    copy (const self_type& other)
    {
      build<T> (other.as<T> ());
    }

    /// Destroy the stored \a T.
    template <typename T>
    void
    destroy ()
    {
      as<T> ().~T ();
      yytname_ = YY_NULLPTR;
    }

  private:
    /// Prohibit blind copies.
    self_type& operator=(const self_type&);
    variant (const self_type&);

    /// Accessor to raw memory as \a T.
    template <typename T>
    T*
    yyas_ ()
    {
      void *yyp = yybuffer_.yyraw;
      return static_cast<T*> (yyp);
     }

    /// Const accessor to raw memory as \a T.
    template <typename T>
    const T*
    yyas_ () const
    {
      const void *yyp = yybuffer_.yyraw;
      return static_cast<const T*> (yyp);
     }

    union
    {
      /// Strongest alignment constraints.
      long double yyalign_me;
      /// A buffer large enough to store any of the semantic values.
      char yyraw[S];
    } yybuffer_;

    /// Whether the content is built: if defined, the name of the stored type.
    const char *yytname_;
  };


  /// A Bison parser.
  class alpiparser
  {
  public:
#ifndef YYSTYPE
    /// An auxiliary type to compute the largest semantic type.
    union union_type
    {
      // IdentQualif
      char dummy1[sizeof(alpi::exprs::Qualifier*)];

      // IndexingReducer
      // BExpression
      char dummy2[sizeof(alpi::exprs::booleans::BoolOp*)];

      // IndexDef
      // Index
      // InIndexOpt
      char dummy3[sizeof(alpi::exprs::indexing::Index*)];

      // IndexingOpt
      // IndexingList
      // IdentDefQualif
      // IdentDefQualifParams
      // IdentDefQualifParam
      char dummy4[sizeof(alpi::exprs::indexing::IndexingMultiRelation*)];

      // Real
      // ByStep
      // Expression
      // EndConstraint
      char dummy5[sizeof(alpi::exprs::numerics::NumericExpression*)];

      // CstReal
      char dummy6[sizeof(alpi::exprs::numerics::RealConstant*)];

      // CstRelative
      char dummy7[sizeof(alpi::exprs::numerics::RelativeConstant*)];

      // Indexing
      char dummy8[sizeof(alpi::exprs::symbol::IndexVariable*)];

      // Test
      char dummy9[sizeof(int)];

      // ListExpression
      char dummy10[sizeof(std::list<alpi::exprs::numerics::NumericExpression*>)];

      // "entier"
      // "réel"
      // "fonction du langage (cos, sqrt, sin...)"
      // "identifier"
      // "nom de fichier"
      // Ident
      // Anything
      char dummy11[sizeof(std::string)];
};

    /// Symbol semantic values.
    typedef variant<sizeof(union_type)> semantic_type;
#else
    typedef YYSTYPE semantic_type;
#endif
    /// Symbol locations.
    typedef location location_type;

    /// Syntax errors thrown from user actions.
    struct syntax_error : std::runtime_error
    {
      syntax_error (const location_type& l, const std::string& m);
      location_type location;
    };

    /// Tokens.
    struct token
    {
      enum yytokentype
      {
        TOK_END = 0,
        TOK_NATURAL = 258,
        TOK_NUMBER = 259,
        TOK_BOOLEAN = 260,
        TOK_OP_BRACK = 261,
        TOK_CL_BRACK = 262,
        TOK_OP_PAREN = 263,
        TOK_CL_PAREN = 264,
        TOK_OP_BRACE = 265,
        TOK_CL_BRACE = 266,
        TOK_PLUS = 267,
        TOK_MINUS = 268,
        TOK_POWER = 269,
        TOK_AND = 270,
        TOK_OR = 271,
        TOK_TIMES = 272,
        TOK_DIVIDE = 273,
        TOK_SUM = 274,
        TOK_PROD = 275,
        TOK_EQ = 276,
        TOK_LEQ = 277,
        TOK_GEQ = 278,
        TOK_LT = 279,
        TOK_GT = 280,
        TOK_PREC = 281,
        TOK_AFFECT = 282,
        TOK_COLON = 283,
        TOK_MINIMIZE = 284,
        TOK_SET = 285,
        TOK_VAR = 286,
        TOK_PARAM = 287,
        TOK_SUBJECT_TO = 288,
        TOK_OPTION = 289,
        TOK_IN = 290,
        TOK_BY = 291,
        TOK_SPAN = 292,
        TOK_COMMA = 293,
        TOK_SEMICOLON = 294,
        TOK_INTEGER = 295,
        TOK_FUN = 296,
        TOK_IDENT = 297,
        TOK_FILENAME = 298,
        TOK_NEG = 299
      };
    };

    /// (External) token type, as returned by yylex.
    typedef token::yytokentype token_type;

    /// Internal symbol number.
    typedef int symbol_number_type;

    /// Internal symbol number for tokens (subsumed by symbol_number_type).
    typedef unsigned char token_number_type;

    /// A complete symbol.
    ///
    /// Expects its Base type to provide access to the symbol type
    /// via type_get().
    ///
    /// Provide access to semantic value and location.
    template <typename Base>
    struct basic_symbol : Base
    {
      /// Alias to Base.
      typedef Base super_type;

      /// Default constructor.
      basic_symbol ();

      /// Copy constructor.
      basic_symbol (const basic_symbol& other);

      /// Constructor for valueless symbols, and symbols from each type.

  basic_symbol (typename Base::kind_type t, const location_type& l);

  basic_symbol (typename Base::kind_type t, const alpi::exprs::Qualifier* v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const alpi::exprs::booleans::BoolOp* v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const alpi::exprs::indexing::Index* v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const alpi::exprs::indexing::IndexingMultiRelation* v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const alpi::exprs::numerics::NumericExpression* v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const alpi::exprs::numerics::RealConstant* v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const alpi::exprs::numerics::RelativeConstant* v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const alpi::exprs::symbol::IndexVariable* v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const int v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const std::list<alpi::exprs::numerics::NumericExpression*> v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const std::string v, const location_type& l);


      /// Constructor for symbols with semantic value.
      basic_symbol (typename Base::kind_type t,
                    const semantic_type& v,
                    const location_type& l);

      ~basic_symbol ();

      /// Destructive move, \a s is emptied into this.
      void move (basic_symbol& s);

      /// The semantic value.
      semantic_type value;

      /// The location.
      location_type location;

    private:
      /// Assignment operator.
      basic_symbol& operator= (const basic_symbol& other);
    };

    /// Type access provider for token (enum) based symbols.
    struct by_type
    {
      /// Default constructor.
      by_type ();

      /// Copy constructor.
      by_type (const by_type& other);

      /// The symbol type as needed by the constructor.
      typedef token_type kind_type;

      /// Constructor from (external) token numbers.
      by_type (kind_type t);

      /// Steal the symbol type from \a that.
      void move (by_type& that);

      /// The (internal) type number (corresponding to \a type).
      /// -1 when this symbol is empty.
      symbol_number_type type_get () const;

      /// The token.
      token_type token () const;

      enum { empty = 0 };

      /// The symbol type.
      /// -1 when this symbol is empty.
      token_number_type type;
    };

    /// "External" symbols: returned by the scanner.
    typedef basic_symbol<by_type> symbol_type;

    // Symbol constructors declarations.
    static inline
    symbol_type
    make_END (const location_type& l);

    static inline
    symbol_type
    make_NATURAL (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_NUMBER (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_BOOLEAN (const location_type& l);

    static inline
    symbol_type
    make_OP_BRACK (const location_type& l);

    static inline
    symbol_type
    make_CL_BRACK (const location_type& l);

    static inline
    symbol_type
    make_OP_PAREN (const location_type& l);

    static inline
    symbol_type
    make_CL_PAREN (const location_type& l);

    static inline
    symbol_type
    make_OP_BRACE (const location_type& l);

    static inline
    symbol_type
    make_CL_BRACE (const location_type& l);

    static inline
    symbol_type
    make_PLUS (const location_type& l);

    static inline
    symbol_type
    make_MINUS (const location_type& l);

    static inline
    symbol_type
    make_POWER (const location_type& l);

    static inline
    symbol_type
    make_AND (const location_type& l);

    static inline
    symbol_type
    make_OR (const location_type& l);

    static inline
    symbol_type
    make_TIMES (const location_type& l);

    static inline
    symbol_type
    make_DIVIDE (const location_type& l);

    static inline
    symbol_type
    make_SUM (const location_type& l);

    static inline
    symbol_type
    make_PROD (const location_type& l);

    static inline
    symbol_type
    make_EQ (const location_type& l);

    static inline
    symbol_type
    make_LEQ (const location_type& l);

    static inline
    symbol_type
    make_GEQ (const location_type& l);

    static inline
    symbol_type
    make_LT (const location_type& l);

    static inline
    symbol_type
    make_GT (const location_type& l);

    static inline
    symbol_type
    make_PREC (const location_type& l);

    static inline
    symbol_type
    make_AFFECT (const location_type& l);

    static inline
    symbol_type
    make_COLON (const location_type& l);

    static inline
    symbol_type
    make_MINIMIZE (const location_type& l);

    static inline
    symbol_type
    make_SET (const location_type& l);

    static inline
    symbol_type
    make_VAR (const location_type& l);

    static inline
    symbol_type
    make_PARAM (const location_type& l);

    static inline
    symbol_type
    make_SUBJECT_TO (const location_type& l);

    static inline
    symbol_type
    make_OPTION (const location_type& l);

    static inline
    symbol_type
    make_IN (const location_type& l);

    static inline
    symbol_type
    make_BY (const location_type& l);

    static inline
    symbol_type
    make_SPAN (const location_type& l);

    static inline
    symbol_type
    make_COMMA (const location_type& l);

    static inline
    symbol_type
    make_SEMICOLON (const location_type& l);

    static inline
    symbol_type
    make_INTEGER (const location_type& l);

    static inline
    symbol_type
    make_FUN (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_IDENT (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_FILENAME (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_NEG (const location_type& l);


    /// Build a parser object.
    alpiparser (alpiparsertools::driver& driver_yyarg);
    virtual ~alpiparser ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

#if YYDEBUG
    /// The current debugging stream.
    std::ostream& debug_stream () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);
#endif

    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);

    /// Report a syntax error.
    void error (const syntax_error& err);

  private:
    /// This class is not copyable.
    alpiparser (const alpiparser&);
    alpiparser& operator= (const alpiparser&);

    /// State numbers.
    typedef int state_type;

    /// Generate an error message.
    /// \param yystate   the state where the error occurred.
    /// \param yytoken   the lookahead token type, or yyempty_.
    virtual std::string yysyntax_error_ (state_type yystate,
                                         symbol_number_type yytoken) const;

    /// Compute post-reduction state.
    /// \param yystate   the current state
    /// \param yysym     the nonterminal to push on the stack
    state_type yy_lr_goto_state_ (state_type yystate, int yysym);

    /// Whether the given \c yypact_ value indicates a defaulted state.
    /// \param yyvalue   the value to check
    static bool yy_pact_value_is_default_ (int yyvalue);

    /// Whether the given \c yytable_ value indicates a syntax error.
    /// \param yyvalue   the value to check
    static bool yy_table_value_is_error_ (int yyvalue);

    static const signed char yypact_ninf_;
    static const signed char yytable_ninf_;

    /// Convert a scanner token number \a t to a symbol number.
    static token_number_type yytranslate_ (token_type t);

    // Tables.
  // YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
  // STATE-NUM.
  static const short int yypact_[];

  // YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
  // Performed when YYTABLE does not specify something else to do.  Zero
  // means the default is an error.
  static const unsigned char yydefact_[];

  // YYPGOTO[NTERM-NUM].
  static const short int yypgoto_[];

  // YYDEFGOTO[NTERM-NUM].
  static const short int yydefgoto_[];

  // YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
  // positive, shift that token.  If negative, reduce the rule whose
  // number is the opposite.  If YYTABLE_NINF, syntax error.
  static const short int yytable_[];

  static const short int yycheck_[];

  // YYSTOS[STATE-NUM] -- The (internal number of the) accessing
  // symbol of state STATE-NUM.
  static const unsigned char yystos_[];

  // YYR1[YYN] -- Symbol number of symbol that rule YYN derives.
  static const unsigned char yyr1_[];

  // YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.
  static const unsigned char yyr2_[];


    /// Convert the symbol name \a n to a form suitable for a diagnostic.
    static std::string yytnamerr_ (const char *n);


    /// For a symbol, its name in clear.
    static const char* const yytname_[];
#if YYDEBUG
  // YYRLINE[YYN] -- Source line where rule number YYN was defined.
  static const unsigned short int yyrline_[];
    /// Report on the debug stream that the rule \a r is going to be reduced.
    virtual void yy_reduce_print_ (int r);
    /// Print the state stack on the debug stream.
    virtual void yystack_print_ ();

    // Debugging.
    int yydebug_;
    std::ostream* yycdebug_;

    /// \brief Display a symbol type, value and location.
    /// \param yyo    The output stream.
    /// \param yysym  The symbol.
    template <typename Base>
    void yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const;
#endif

    /// \brief Reclaim the memory associated to a symbol.
    /// \param yymsg     Why this token is reclaimed.
    ///                  If null, print nothing.
    /// \param yysym     The symbol.
    template <typename Base>
    void yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const;

  private:
    /// Type access provider for state based symbols.
    struct by_state
    {
      /// Default constructor.
      by_state ();

      /// The symbol type as needed by the constructor.
      typedef state_type kind_type;

      /// Constructor.
      by_state (kind_type s);

      /// Copy constructor.
      by_state (const by_state& other);

      /// Steal the symbol type from \a that.
      void move (by_state& that);

      /// The (internal) type number (corresponding to \a state).
      /// "empty" when empty.
      symbol_number_type type_get () const;

      enum { empty = 0 };

      /// The state.
      state_type state;
    };

    /// "Internal" symbol: element of the stack.
    struct stack_symbol_type : basic_symbol<by_state>
    {
      /// Superclass.
      typedef basic_symbol<by_state> super_type;
      /// Construct an empty symbol.
      stack_symbol_type ();
      /// Steal the contents from \a sym to build this.
      stack_symbol_type (state_type s, symbol_type& sym);
      /// Assignment, needed by push_back.
      stack_symbol_type& operator= (const stack_symbol_type& that);
    };

    /// Stack type.
    typedef stack<stack_symbol_type> stack_type;

    /// The stack.
    stack_type yystack_;

    /// Push a new state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the symbol
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, stack_symbol_type& s);

    /// Push a new look ahead token on the state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the state
    /// \param sym  the symbol (for its value and location).
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, state_type s, symbol_type& sym);

    /// Pop \a n symbols the three stacks.
    void yypop_ (unsigned int n = 1);

    // Constants.
    enum
    {
      yyeof_ = 0,
      yylast_ = 292,     ///< Last index in yytable_.
      yynnts_ = 58,  ///< Number of nonterminal symbols.
      yyempty_ = -2,
      yyfinal_ = 3, ///< Termination state number.
      yyterror_ = 1,
      yyerrcode_ = 256,
      yyntokens_ = 45  ///< Number of tokens.
    };


    // User arguments.
    alpiparsertools::driver& driver;
  };

  // Symbol number corresponding to token number t.
  inline
  alpiparser::token_number_type
  alpiparser::yytranslate_ (token_type t)
  {
    static
    const token_number_type
    translate_table[] =
    {
     0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44
    };
    const unsigned int user_token_number_max_ = 299;
    const token_number_type undef_token_ = 2;

    if (static_cast<int>(t) <= yyeof_)
      return yyeof_;
    else if (static_cast<unsigned int> (t) <= user_token_number_max_)
      return translate_table[t];
    else
      return undef_token_;
  }

  inline
  alpiparser::syntax_error::syntax_error (const location_type& l, const std::string& m)
    : std::runtime_error (m)
    , location (l)
  {}

  // basic_symbol.
  template <typename Base>
  inline
  alpiparser::basic_symbol<Base>::basic_symbol ()
    : value ()
  {}

  template <typename Base>
  inline
  alpiparser::basic_symbol<Base>::basic_symbol (const basic_symbol& other)
    : Base (other)
    , value ()
    , location (other.location)
  {
      switch (other.type_get ())
    {
      case 76: // IdentQualif
        value.copy< alpi::exprs::Qualifier* > (other.value);
        break;

      case 61: // IndexingReducer
      case 78: // BExpression
        value.copy< alpi::exprs::booleans::BoolOp* > (other.value);
        break;

      case 54: // IndexDef
      case 56: // Index
      case 60: // InIndexOpt
        value.copy< alpi::exprs::indexing::Index* > (other.value);
        break;

      case 57: // IndexingOpt
      case 58: // IndexingList
      case 100: // IdentDefQualif
      case 101: // IdentDefQualifParams
      case 102: // IdentDefQualifParam
        value.copy< alpi::exprs::indexing::IndexingMultiRelation* > (other.value);
        break;

      case 52: // Real
      case 55: // ByStep
      case 73: // Expression
      case 82: // EndConstraint
        value.copy< alpi::exprs::numerics::NumericExpression* > (other.value);
        break;

      case 51: // CstReal
        value.copy< alpi::exprs::numerics::RealConstant* > (other.value);
        break;

      case 50: // CstRelative
        value.copy< alpi::exprs::numerics::RelativeConstant* > (other.value);
        break;

      case 59: // Indexing
        value.copy< alpi::exprs::symbol::IndexVariable* > (other.value);
        break;

      case 53: // Test
        value.copy< int > (other.value);
        break;

      case 77: // ListExpression
        value.copy< std::list<alpi::exprs::numerics::NumericExpression*> > (other.value);
        break;

      case 3: // "entier"
      case 4: // "réel"
      case 41: // "fonction du langage (cos, sqrt, sin...)"
      case 42: // "identifier"
      case 43: // "nom de fichier"
      case 49: // Ident
      case 91: // Anything
        value.copy< std::string > (other.value);
        break;

      default:
        break;
    }

  }


  template <typename Base>
  inline
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const semantic_type& v, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {
    (void) v;
      switch (this->type_get ())
    {
      case 76: // IdentQualif
        value.copy< alpi::exprs::Qualifier* > (v);
        break;

      case 61: // IndexingReducer
      case 78: // BExpression
        value.copy< alpi::exprs::booleans::BoolOp* > (v);
        break;

      case 54: // IndexDef
      case 56: // Index
      case 60: // InIndexOpt
        value.copy< alpi::exprs::indexing::Index* > (v);
        break;

      case 57: // IndexingOpt
      case 58: // IndexingList
      case 100: // IdentDefQualif
      case 101: // IdentDefQualifParams
      case 102: // IdentDefQualifParam
        value.copy< alpi::exprs::indexing::IndexingMultiRelation* > (v);
        break;

      case 52: // Real
      case 55: // ByStep
      case 73: // Expression
      case 82: // EndConstraint
        value.copy< alpi::exprs::numerics::NumericExpression* > (v);
        break;

      case 51: // CstReal
        value.copy< alpi::exprs::numerics::RealConstant* > (v);
        break;

      case 50: // CstRelative
        value.copy< alpi::exprs::numerics::RelativeConstant* > (v);
        break;

      case 59: // Indexing
        value.copy< alpi::exprs::symbol::IndexVariable* > (v);
        break;

      case 53: // Test
        value.copy< int > (v);
        break;

      case 77: // ListExpression
        value.copy< std::list<alpi::exprs::numerics::NumericExpression*> > (v);
        break;

      case 3: // "entier"
      case 4: // "réel"
      case 41: // "fonction du langage (cos, sqrt, sin...)"
      case 42: // "identifier"
      case 43: // "nom de fichier"
      case 49: // Ident
      case 91: // Anything
        value.copy< std::string > (v);
        break;

      default:
        break;
    }
}


  // Implementation of basic_symbol constructor for each type.

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const alpi::exprs::Qualifier* v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const alpi::exprs::booleans::BoolOp* v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const alpi::exprs::indexing::Index* v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const alpi::exprs::indexing::IndexingMultiRelation* v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const alpi::exprs::numerics::NumericExpression* v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const alpi::exprs::numerics::RealConstant* v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const alpi::exprs::numerics::RelativeConstant* v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const alpi::exprs::symbol::IndexVariable* v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const int v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const std::list<alpi::exprs::numerics::NumericExpression*> v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  alpiparser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const std::string v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}


  template <typename Base>
  inline
  alpiparser::basic_symbol<Base>::~basic_symbol ()
  {
    // User destructor.
    symbol_number_type yytype = this->type_get ();
    switch (yytype)
    {
   default:
      break;
    }

    // Type destructor.
    switch (yytype)
    {
      case 76: // IdentQualif
        value.template destroy< alpi::exprs::Qualifier* > ();
        break;

      case 61: // IndexingReducer
      case 78: // BExpression
        value.template destroy< alpi::exprs::booleans::BoolOp* > ();
        break;

      case 54: // IndexDef
      case 56: // Index
      case 60: // InIndexOpt
        value.template destroy< alpi::exprs::indexing::Index* > ();
        break;

      case 57: // IndexingOpt
      case 58: // IndexingList
      case 100: // IdentDefQualif
      case 101: // IdentDefQualifParams
      case 102: // IdentDefQualifParam
        value.template destroy< alpi::exprs::indexing::IndexingMultiRelation* > ();
        break;

      case 52: // Real
      case 55: // ByStep
      case 73: // Expression
      case 82: // EndConstraint
        value.template destroy< alpi::exprs::numerics::NumericExpression* > ();
        break;

      case 51: // CstReal
        value.template destroy< alpi::exprs::numerics::RealConstant* > ();
        break;

      case 50: // CstRelative
        value.template destroy< alpi::exprs::numerics::RelativeConstant* > ();
        break;

      case 59: // Indexing
        value.template destroy< alpi::exprs::symbol::IndexVariable* > ();
        break;

      case 53: // Test
        value.template destroy< int > ();
        break;

      case 77: // ListExpression
        value.template destroy< std::list<alpi::exprs::numerics::NumericExpression*> > ();
        break;

      case 3: // "entier"
      case 4: // "réel"
      case 41: // "fonction du langage (cos, sqrt, sin...)"
      case 42: // "identifier"
      case 43: // "nom de fichier"
      case 49: // Ident
      case 91: // Anything
        value.template destroy< std::string > ();
        break;

      default:
        break;
    }

  }

  template <typename Base>
  inline
  void
  alpiparser::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move(s);
      switch (this->type_get ())
    {
      case 76: // IdentQualif
        value.move< alpi::exprs::Qualifier* > (s.value);
        break;

      case 61: // IndexingReducer
      case 78: // BExpression
        value.move< alpi::exprs::booleans::BoolOp* > (s.value);
        break;

      case 54: // IndexDef
      case 56: // Index
      case 60: // InIndexOpt
        value.move< alpi::exprs::indexing::Index* > (s.value);
        break;

      case 57: // IndexingOpt
      case 58: // IndexingList
      case 100: // IdentDefQualif
      case 101: // IdentDefQualifParams
      case 102: // IdentDefQualifParam
        value.move< alpi::exprs::indexing::IndexingMultiRelation* > (s.value);
        break;

      case 52: // Real
      case 55: // ByStep
      case 73: // Expression
      case 82: // EndConstraint
        value.move< alpi::exprs::numerics::NumericExpression* > (s.value);
        break;

      case 51: // CstReal
        value.move< alpi::exprs::numerics::RealConstant* > (s.value);
        break;

      case 50: // CstRelative
        value.move< alpi::exprs::numerics::RelativeConstant* > (s.value);
        break;

      case 59: // Indexing
        value.move< alpi::exprs::symbol::IndexVariable* > (s.value);
        break;

      case 53: // Test
        value.move< int > (s.value);
        break;

      case 77: // ListExpression
        value.move< std::list<alpi::exprs::numerics::NumericExpression*> > (s.value);
        break;

      case 3: // "entier"
      case 4: // "réel"
      case 41: // "fonction du langage (cos, sqrt, sin...)"
      case 42: // "identifier"
      case 43: // "nom de fichier"
      case 49: // Ident
      case 91: // Anything
        value.move< std::string > (s.value);
        break;

      default:
        break;
    }

    location = s.location;
  }

  // by_type.
  inline
  alpiparser::by_type::by_type ()
     : type (empty)
  {}

  inline
  alpiparser::by_type::by_type (const by_type& other)
    : type (other.type)
  {}

  inline
  alpiparser::by_type::by_type (token_type t)
    : type (yytranslate_ (t))
  {}

  inline
  void
  alpiparser::by_type::move (by_type& that)
  {
    type = that.type;
    that.type = empty;
  }

  inline
  int
  alpiparser::by_type::type_get () const
  {
    return type;
  }

  inline
  alpiparser::token_type
  alpiparser::by_type::token () const
  {
    // YYTOKNUM[NUM] -- (External) token number corresponding to the
    // (internal) symbol number NUM (which must be that of a token).  */
    static
    const unsigned short int
    yytoken_number_[] =
    {
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299
    };
    return static_cast<token_type> (yytoken_number_[type]);
  }
  // Implementation of make_symbol for each symbol type.
  alpiparser::symbol_type
  alpiparser::make_END (const location_type& l)
  {
    return symbol_type (token::TOK_END, l);
  }

  alpiparser::symbol_type
  alpiparser::make_NATURAL (const std::string& v, const location_type& l)
  {
    return symbol_type (token::TOK_NATURAL, v, l);
  }

  alpiparser::symbol_type
  alpiparser::make_NUMBER (const std::string& v, const location_type& l)
  {
    return symbol_type (token::TOK_NUMBER, v, l);
  }

  alpiparser::symbol_type
  alpiparser::make_BOOLEAN (const location_type& l)
  {
    return symbol_type (token::TOK_BOOLEAN, l);
  }

  alpiparser::symbol_type
  alpiparser::make_OP_BRACK (const location_type& l)
  {
    return symbol_type (token::TOK_OP_BRACK, l);
  }

  alpiparser::symbol_type
  alpiparser::make_CL_BRACK (const location_type& l)
  {
    return symbol_type (token::TOK_CL_BRACK, l);
  }

  alpiparser::symbol_type
  alpiparser::make_OP_PAREN (const location_type& l)
  {
    return symbol_type (token::TOK_OP_PAREN, l);
  }

  alpiparser::symbol_type
  alpiparser::make_CL_PAREN (const location_type& l)
  {
    return symbol_type (token::TOK_CL_PAREN, l);
  }

  alpiparser::symbol_type
  alpiparser::make_OP_BRACE (const location_type& l)
  {
    return symbol_type (token::TOK_OP_BRACE, l);
  }

  alpiparser::symbol_type
  alpiparser::make_CL_BRACE (const location_type& l)
  {
    return symbol_type (token::TOK_CL_BRACE, l);
  }

  alpiparser::symbol_type
  alpiparser::make_PLUS (const location_type& l)
  {
    return symbol_type (token::TOK_PLUS, l);
  }

  alpiparser::symbol_type
  alpiparser::make_MINUS (const location_type& l)
  {
    return symbol_type (token::TOK_MINUS, l);
  }

  alpiparser::symbol_type
  alpiparser::make_POWER (const location_type& l)
  {
    return symbol_type (token::TOK_POWER, l);
  }

  alpiparser::symbol_type
  alpiparser::make_AND (const location_type& l)
  {
    return symbol_type (token::TOK_AND, l);
  }

  alpiparser::symbol_type
  alpiparser::make_OR (const location_type& l)
  {
    return symbol_type (token::TOK_OR, l);
  }

  alpiparser::symbol_type
  alpiparser::make_TIMES (const location_type& l)
  {
    return symbol_type (token::TOK_TIMES, l);
  }

  alpiparser::symbol_type
  alpiparser::make_DIVIDE (const location_type& l)
  {
    return symbol_type (token::TOK_DIVIDE, l);
  }

  alpiparser::symbol_type
  alpiparser::make_SUM (const location_type& l)
  {
    return symbol_type (token::TOK_SUM, l);
  }

  alpiparser::symbol_type
  alpiparser::make_PROD (const location_type& l)
  {
    return symbol_type (token::TOK_PROD, l);
  }

  alpiparser::symbol_type
  alpiparser::make_EQ (const location_type& l)
  {
    return symbol_type (token::TOK_EQ, l);
  }

  alpiparser::symbol_type
  alpiparser::make_LEQ (const location_type& l)
  {
    return symbol_type (token::TOK_LEQ, l);
  }

  alpiparser::symbol_type
  alpiparser::make_GEQ (const location_type& l)
  {
    return symbol_type (token::TOK_GEQ, l);
  }

  alpiparser::symbol_type
  alpiparser::make_LT (const location_type& l)
  {
    return symbol_type (token::TOK_LT, l);
  }

  alpiparser::symbol_type
  alpiparser::make_GT (const location_type& l)
  {
    return symbol_type (token::TOK_GT, l);
  }

  alpiparser::symbol_type
  alpiparser::make_PREC (const location_type& l)
  {
    return symbol_type (token::TOK_PREC, l);
  }

  alpiparser::symbol_type
  alpiparser::make_AFFECT (const location_type& l)
  {
    return symbol_type (token::TOK_AFFECT, l);
  }

  alpiparser::symbol_type
  alpiparser::make_COLON (const location_type& l)
  {
    return symbol_type (token::TOK_COLON, l);
  }

  alpiparser::symbol_type
  alpiparser::make_MINIMIZE (const location_type& l)
  {
    return symbol_type (token::TOK_MINIMIZE, l);
  }

  alpiparser::symbol_type
  alpiparser::make_SET (const location_type& l)
  {
    return symbol_type (token::TOK_SET, l);
  }

  alpiparser::symbol_type
  alpiparser::make_VAR (const location_type& l)
  {
    return symbol_type (token::TOK_VAR, l);
  }

  alpiparser::symbol_type
  alpiparser::make_PARAM (const location_type& l)
  {
    return symbol_type (token::TOK_PARAM, l);
  }

  alpiparser::symbol_type
  alpiparser::make_SUBJECT_TO (const location_type& l)
  {
    return symbol_type (token::TOK_SUBJECT_TO, l);
  }

  alpiparser::symbol_type
  alpiparser::make_OPTION (const location_type& l)
  {
    return symbol_type (token::TOK_OPTION, l);
  }

  alpiparser::symbol_type
  alpiparser::make_IN (const location_type& l)
  {
    return symbol_type (token::TOK_IN, l);
  }

  alpiparser::symbol_type
  alpiparser::make_BY (const location_type& l)
  {
    return symbol_type (token::TOK_BY, l);
  }

  alpiparser::symbol_type
  alpiparser::make_SPAN (const location_type& l)
  {
    return symbol_type (token::TOK_SPAN, l);
  }

  alpiparser::symbol_type
  alpiparser::make_COMMA (const location_type& l)
  {
    return symbol_type (token::TOK_COMMA, l);
  }

  alpiparser::symbol_type
  alpiparser::make_SEMICOLON (const location_type& l)
  {
    return symbol_type (token::TOK_SEMICOLON, l);
  }

  alpiparser::symbol_type
  alpiparser::make_INTEGER (const location_type& l)
  {
    return symbol_type (token::TOK_INTEGER, l);
  }

  alpiparser::symbol_type
  alpiparser::make_FUN (const std::string& v, const location_type& l)
  {
    return symbol_type (token::TOK_FUN, v, l);
  }

  alpiparser::symbol_type
  alpiparser::make_IDENT (const std::string& v, const location_type& l)
  {
    return symbol_type (token::TOK_IDENT, v, l);
  }

  alpiparser::symbol_type
  alpiparser::make_FILENAME (const std::string& v, const location_type& l)
  {
    return symbol_type (token::TOK_FILENAME, v, l);
  }

  alpiparser::symbol_type
  alpiparser::make_NEG (const location_type& l)
  {
    return symbol_type (token::TOK_NEG, l);
  }



} // yy
#line 1681 "alpi.tab.hh" // lalr1.cc:372




#endif // !YY_YY_ALPI_TAB_HH_INCLUDED
