// A Bison parser, made by GNU Bison 3.0.2.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2013 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.

#line 37 "alpi.tab.cc" // lalr1.cc:399

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "alpi.tab.hh"

// User implementation prologue.

#line 51 "alpi.tab.cc" // lalr1.cc:407
// Unqualified %code blocks.
#line 30 "../src/yacclex/alpi.yy" // lalr1.cc:408

#include "../driver/driver.hh"
yy::alpiparser::symbol_type yylex(alpiparsertools::driver& driver);

#line 58 "alpi.tab.cc" // lalr1.cc:408


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyempty = true)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)


namespace yy {
#line 144 "alpi.tab.cc" // lalr1.cc:474

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  alpiparser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              // Fall through.
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }


  /// Build a parser object.
  alpiparser::alpiparser (alpiparsertools::driver& driver_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      driver (driver_yyarg)
  {}

  alpiparser::~alpiparser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/



  // by_state.
  inline
  alpiparser::by_state::by_state ()
    : state (empty)
  {}

  inline
  alpiparser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
  alpiparser::by_state::move (by_state& that)
  {
    state = that.state;
    that.state = empty;
  }

  inline
  alpiparser::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
  alpiparser::symbol_number_type
  alpiparser::by_state::type_get () const
  {
    return state == empty ? 0 : yystos_[state];
  }

  inline
  alpiparser::stack_symbol_type::stack_symbol_type ()
  {}


  inline
  alpiparser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.location)
  {
      switch (that.type_get ())
    {
      case 76: // IdentQualif
        value.move< alpi::exprs::Qualifier* > (that.value);
        break;

      case 61: // IndexingReducer
      case 78: // BExpression
        value.move< alpi::exprs::booleans::BoolOp* > (that.value);
        break;

      case 54: // IndexDef
      case 56: // Index
      case 60: // InIndexOpt
        value.move< alpi::exprs::indexing::Index* > (that.value);
        break;

      case 57: // IndexingOpt
      case 58: // IndexingList
      case 100: // IdentDefQualif
      case 101: // IdentDefQualifParams
      case 102: // IdentDefQualifParam
        value.move< alpi::exprs::indexing::IndexingMultiRelation* > (that.value);
        break;

      case 52: // Real
      case 55: // ByStep
      case 73: // Expression
      case 82: // EndConstraint
        value.move< alpi::exprs::numerics::NumericExpression* > (that.value);
        break;

      case 51: // CstReal
        value.move< alpi::exprs::numerics::RealConstant* > (that.value);
        break;

      case 50: // CstRelative
        value.move< alpi::exprs::numerics::RelativeConstant* > (that.value);
        break;

      case 59: // Indexing
        value.move< alpi::exprs::symbol::IndexVariable* > (that.value);
        break;

      case 53: // Test
        value.move< int > (that.value);
        break;

      case 77: // ListExpression
        value.move< std::list<alpi::exprs::numerics::NumericExpression*> > (that.value);
        break;

      case 3: // "entier"
      case 4: // "réel"
      case 41: // "fonction du langage (cos, sqrt, sin...)"
      case 42: // "identifier"
      case 43: // "nom de fichier"
      case 49: // Ident
      case 91: // Anything
        value.move< std::string > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty;
  }

  inline
  alpiparser::stack_symbol_type&
  alpiparser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 76: // IdentQualif
        value.copy< alpi::exprs::Qualifier* > (that.value);
        break;

      case 61: // IndexingReducer
      case 78: // BExpression
        value.copy< alpi::exprs::booleans::BoolOp* > (that.value);
        break;

      case 54: // IndexDef
      case 56: // Index
      case 60: // InIndexOpt
        value.copy< alpi::exprs::indexing::Index* > (that.value);
        break;

      case 57: // IndexingOpt
      case 58: // IndexingList
      case 100: // IdentDefQualif
      case 101: // IdentDefQualifParams
      case 102: // IdentDefQualifParam
        value.copy< alpi::exprs::indexing::IndexingMultiRelation* > (that.value);
        break;

      case 52: // Real
      case 55: // ByStep
      case 73: // Expression
      case 82: // EndConstraint
        value.copy< alpi::exprs::numerics::NumericExpression* > (that.value);
        break;

      case 51: // CstReal
        value.copy< alpi::exprs::numerics::RealConstant* > (that.value);
        break;

      case 50: // CstRelative
        value.copy< alpi::exprs::numerics::RelativeConstant* > (that.value);
        break;

      case 59: // Indexing
        value.copy< alpi::exprs::symbol::IndexVariable* > (that.value);
        break;

      case 53: // Test
        value.copy< int > (that.value);
        break;

      case 77: // ListExpression
        value.copy< std::list<alpi::exprs::numerics::NumericExpression*> > (that.value);
        break;

      case 3: // "entier"
      case 4: // "réel"
      case 41: // "fonction du langage (cos, sqrt, sin...)"
      case 42: // "identifier"
      case 43: // "nom de fichier"
      case 49: // Ident
      case 91: // Anything
        value.copy< std::string > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }


  template <typename Base>
  inline
  void
  alpiparser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
  alpiparser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  inline
  void
  alpiparser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
  alpiparser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
  alpiparser::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  alpiparser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  alpiparser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  alpiparser::debug_level_type
  alpiparser::debug_level () const
  {
    return yydebug_;
  }

  void
  alpiparser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  inline alpiparser::state_type
  alpiparser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
  alpiparser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
  alpiparser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  alpiparser::parse ()
  {
    /// Whether yyla contains a lookahead.
    bool yyempty = true;

    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    // User initialization code.
    #line 24 "../src/yacclex/alpi.yy" // lalr1.cc:725
{
 yyla.location.begin.filename = yyla.location.end.filename = &driver.file;
}

#line 526 "alpi.tab.cc" // lalr1.cc:725

    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyempty)
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            symbol_type yylookahead (yylex (driver));
            yyla.move (yylookahead);
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
        yyempty = false;
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Discard the token being shifted.
    yyempty = true;

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 76: // IdentQualif
        yylhs.value.build< alpi::exprs::Qualifier* > ();
        break;

      case 61: // IndexingReducer
      case 78: // BExpression
        yylhs.value.build< alpi::exprs::booleans::BoolOp* > ();
        break;

      case 54: // IndexDef
      case 56: // Index
      case 60: // InIndexOpt
        yylhs.value.build< alpi::exprs::indexing::Index* > ();
        break;

      case 57: // IndexingOpt
      case 58: // IndexingList
      case 100: // IdentDefQualif
      case 101: // IdentDefQualifParams
      case 102: // IdentDefQualifParam
        yylhs.value.build< alpi::exprs::indexing::IndexingMultiRelation* > ();
        break;

      case 52: // Real
      case 55: // ByStep
      case 73: // Expression
      case 82: // EndConstraint
        yylhs.value.build< alpi::exprs::numerics::NumericExpression* > ();
        break;

      case 51: // CstReal
        yylhs.value.build< alpi::exprs::numerics::RealConstant* > ();
        break;

      case 50: // CstRelative
        yylhs.value.build< alpi::exprs::numerics::RelativeConstant* > ();
        break;

      case 59: // Indexing
        yylhs.value.build< alpi::exprs::symbol::IndexVariable* > ();
        break;

      case 53: // Test
        yylhs.value.build< int > ();
        break;

      case 77: // ListExpression
        yylhs.value.build< std::list<alpi::exprs::numerics::NumericExpression*> > ();
        break;

      case 3: // "entier"
      case 4: // "réel"
      case 41: // "fonction du langage (cos, sqrt, sin...)"
      case 42: // "identifier"
      case 43: // "nom de fichier"
      case 49: // Ident
      case 91: // Anything
        yylhs.value.build< std::string > ();
        break;

      default:
        break;
    }


      // Compute the default @$.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 12:
#line 148 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.identFromIdent(yystack_[0].value.as< std::string > ()); }
#line 700 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 13:
#line 153 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::RelativeConstant* > () = new alpiparsertools::RelativeConstant("-"+yystack_[0].value.as< std::string > ()); }
#line 706 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 14:
#line 155 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::RelativeConstant* > () = new alpiparsertools::RelativeConstant(yystack_[0].value.as< std::string > ()); }
#line 712 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 15:
#line 169 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::RealConstant* > () = new alpiparsertools::RealConstant(yystack_[0].value.as< std::string > ()); }
#line 718 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 16:
#line 171 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::RealConstant* > () = new alpiparsertools::RealConstant("-"+yystack_[0].value.as< std::string > ()); }
#line 724 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 17:
#line 176 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.numerics()->realFromCst(yystack_[0].value.as< alpi::exprs::numerics::RealConstant* > ()); }
#line 730 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 18:
#line 178 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.numerics()->realFromVariable(yystack_[0].value.as< std::string > ()); }
#line 736 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 19:
#line 180 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.numerics()->realFromOpposeVariable(yystack_[0].value.as< std::string > ()); }
#line 742 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 20:
#line 185 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< int > () = -1; }
#line 748 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 21:
#line 187 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< int > () = 0; }
#line 754 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 22:
#line 189 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< int > () = 1; }
#line 760 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 23:
#line 191 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< int > () = -2; }
#line 766 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 24:
#line 193 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< int > () = 2; }
#line 772 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 25:
#line 200 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::Index* > () = driver.indexing()->indexDef(yystack_[3].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[1].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 778 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 26:
#line 205 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { std::swap(yylhs.value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 784 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 27:
#line 207 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.indexing()->indexDefByStepEmpty(); }
#line 790 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 28:
#line 212 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::Index* > () = driver.getIndex(yystack_[0].value.as< std::string > ()); }
#line 796 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 29:
#line 214 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::Index* > () = driver.indexing()->indexFromDef(yystack_[0].value.as< alpi::exprs::indexing::Index* > ()); }
#line 802 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 30:
#line 219 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.indexing()->indexingOptEmpty(); }
#line 808 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 31:
#line 221 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.indexing()->indexingOptFromSetExpr(yystack_[2].value.as< alpi::exprs::indexing::IndexingMultiRelation* > (), yystack_[1].value.as< alpi::exprs::booleans::BoolOp* > ()); }
#line 814 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 32:
#line 226 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.indexing()->indexingListFromOne(yystack_[0].value.as< alpi::exprs::symbol::IndexVariable* > ()); }
#line 820 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 33:
#line 228 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.indexing()->indexingListFromNext(yystack_[2].value.as< alpi::exprs::indexing::IndexingMultiRelation* > (), yystack_[0].value.as< alpi::exprs::symbol::IndexVariable* > ()); }
#line 826 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 34:
#line 233 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::symbol::IndexVariable* > () = driver.indexing()->indexingFromIdentOptInIndex(yystack_[1].value.as< std::string > (), yystack_[0].value.as< alpi::exprs::indexing::Index* > ()); }
#line 832 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 35:
#line 235 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::symbol::IndexVariable* > () = driver.indexing()->indexingFromDef(yystack_[0].value.as< alpi::exprs::indexing::Index* > ()); }
#line 838 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 36:
#line 240 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::Index* > () = driver.indexing()->inIndexFromIndex(yystack_[0].value.as< alpi::exprs::indexing::Index* > ()); }
#line 844 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 37:
#line 242 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::Index* > () = driver.indexing()->inIndexFromEmpty(); }
#line 850 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 38:
#line 247 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::booleans::BoolOp* > () = driver.indexing()->reducerFromTest(yystack_[0].value.as< alpi::exprs::booleans::BoolOp* > ()); }
#line 856 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 39:
#line 249 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::booleans::BoolOp* > () = driver.indexing()->reducerFromEmpty(); }
#line 862 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 40:
#line 256 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.setDefAfterIdent(yystack_[0].value.as< std::string > ()); }
#line 868 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 41:
#line 258 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.setDefAfterIndexing(yystack_[2].value.as< std::string > (), yystack_[0].value.as< alpi::exprs::indexing::IndexingMultiRelation* > ()); }
#line 874 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 45:
#line 269 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.setDefAttrAffect(yystack_[0].value.as< alpi::exprs::indexing::Index* > ()); }
#line 880 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 46:
#line 276 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.variableCreationBeforeIndexing(yystack_[0].value.as< std::string > ()); }
#line 886 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 47:
#line 278 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.variableCreation(yystack_[2].value.as< std::string > (), yystack_[0].value.as< alpi::exprs::indexing::IndexingMultiRelation* > ()); }
#line 892 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 48:
#line 280 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.variableCreationEnd(); }
#line 898 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 53:
#line 295 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.variableUpperBound(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 904 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 54:
#line 297 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.variableLowerBound(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 910 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 55:
#line 299 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.variablePrecision(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 916 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 56:
#line 301 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.variableSetInteger(); }
#line 922 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 57:
#line 303 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { /* Ignored for now */ }
#line 928 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 58:
#line 310 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromFunctionCall(yystack_[3].value.as< std::string > (), yystack_[1].value.as< std::list<alpi::exprs::numerics::NumericExpression*> > ()); }
#line 934 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 59:
#line 313 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromIdentifier(yystack_[1].value.as< std::string > (), yystack_[0].value.as< alpi::exprs::Qualifier* > ()); }
#line 940 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 60:
#line 315 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromConstant(yystack_[0].value.as< alpi::exprs::numerics::RelativeConstant* > ()); }
#line 946 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 61:
#line 317 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromConstant(yystack_[0].value.as< alpi::exprs::numerics::RealConstant* > ()); }
#line 952 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 62:
#line 319 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromAddition(yystack_[2].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 958 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 63:
#line 321 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromDifference(yystack_[2].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 964 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 64:
#line 323 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromMultiplication(yystack_[2].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 970 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 65:
#line 325 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromDivision(yystack_[2].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 976 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 66:
#line 327 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromPower(yystack_[2].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 982 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 67:
#line 329 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromOppose(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 988 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 68:
#line 331 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { std::swap(yylhs.value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[1].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 994 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 69:
#line 333 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.expressions()->expressionFromSumBeforeIndexing(); }
#line 1000 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 70:
#line 336 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromSum(yystack_[5].value.as< alpi::exprs::indexing::IndexingMultiRelation* > (), yystack_[4].value.as< alpi::exprs::booleans::BoolOp* > (), yystack_[1].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1006 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 71:
#line 338 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.expressions()->expressionFromProdBeforeIndexing(); }
#line 1012 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 72:
#line 341 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.expressions()->expressionFromProd(yystack_[5].value.as< alpi::exprs::indexing::IndexingMultiRelation* > (), yystack_[4].value.as< alpi::exprs::booleans::BoolOp* > (), yystack_[1].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1018 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 73:
#line 346 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::Qualifier* > () = driver.expressions()->identQualifFromEmpty(); }
#line 1024 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 74:
#line 348 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::Qualifier* > () = driver.expressions()->identQualifFromArgs(yystack_[1].value.as< std::list<alpi::exprs::numerics::NumericExpression*> > ()); }
#line 1030 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 75:
#line 353 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::list<alpi::exprs::numerics::NumericExpression*> > () = driver.expressions()->listExpressionFromUnit(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1036 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 76:
#line 355 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::list<alpi::exprs::numerics::NumericExpression*> > () = driver.expressions()->listExpressionFromList(yystack_[2].value.as< std::list<alpi::exprs::numerics::NumericExpression*> > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1042 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 77:
#line 360 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::booleans::BoolOp* > () = driver.expressions()->booleanAndExpression(yystack_[2].value.as< alpi::exprs::booleans::BoolOp* > (), yystack_[0].value.as< alpi::exprs::booleans::BoolOp* > ()); }
#line 1048 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 78:
#line 362 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::booleans::BoolOp* > () = driver.expressions()->booleanOrExpression(yystack_[2].value.as< alpi::exprs::booleans::BoolOp* > (), yystack_[0].value.as< alpi::exprs::booleans::BoolOp* > ()); }
#line 1054 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 79:
#line 364 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::booleans::BoolOp* > () = driver.expressions()->booleanTestExpression(yystack_[2].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[1].value.as< int > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1060 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 80:
#line 371 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.constraints()->definitionBeforeIndex(yystack_[0].value.as< std::string > ()); }
#line 1066 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 81:
#line 373 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.constraints()->definitionAfterIndex(yystack_[2].value.as< std::string > (), yystack_[0].value.as< alpi::exprs::indexing::IndexingMultiRelation* > ()); }
#line 1072 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 82:
#line 375 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.constraints()->definitionEnd(yystack_[9].value.as< std::string > (), yystack_[7].value.as< alpi::exprs::indexing::IndexingMultiRelation* > (), yystack_[4].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[3].value.as< int > (), yystack_[2].value.as< alpi::exprs::numerics::NumericExpression* > (), yystack_[1].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1078 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 83:
#line 380 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.constraints()->endConstraintFromEmpty(); }
#line 1084 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 84:
#line 382 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::numerics::NumericExpression* > () = driver.constraints()->endConstraintFromTest(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1090 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 87:
#line 392 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.constraints()->attributePrecision(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1096 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 88:
#line 399 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.objective()->definition(yystack_[0].value.as< std::string > ()); }
#line 1102 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 89:
#line 401 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.objective()->definitionEnd(); }
#line 1108 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 94:
#line 416 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.objective()->affectExpression(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1114 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 95:
#line 418 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.objective()->setPrecision(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1120 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 96:
#line 425 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.options()->setOption(yystack_[2].value.as< std::string > (), yystack_[0].value.as< std::string > ()); }
#line 1126 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 97:
#line 430 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromConstant(yystack_[0].value.as< alpi::exprs::numerics::RealConstant* > ()); }
#line 1132 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 98:
#line 432 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromConstant(yystack_[0].value.as< alpi::exprs::numerics::RelativeConstant* > ()); }
#line 1138 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 99:
#line 434 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromIdent(yystack_[0].value.as< std::string > ()); }
#line 1144 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 100:
#line 436 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromFileName(yystack_[0].value.as< std::string > ()); }
#line 1150 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 101:
#line 438 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromSum(); }
#line 1156 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 102:
#line 440 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromProd(); }
#line 1162 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 103:
#line 442 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromParam(); }
#line 1168 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 104:
#line 444 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromSet(); }
#line 1174 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 105:
#line 446 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< std::string > () = driver.options()->anythingFromMinimize(); }
#line 1180 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 106:
#line 453 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.defParamBeforeIndexing(yystack_[0].value.as< std::string > ()); }
#line 1186 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 107:
#line 455 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.defParamAfterIndexing(yystack_[2].value.as< std::string > (), yystack_[0].value.as< alpi::exprs::indexing::IndexingMultiRelation* > ()); }
#line 1192 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 108:
#line 457 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.defParamEnd(); }
#line 1198 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 113:
#line 472 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.defParamExpression(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1204 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 114:
#line 474 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.defParamInteger(); }
#line 1210 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 115:
#line 479 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.params()->enterSimpleAffectation(yystack_[0].value.as< std::string > ()); }
#line 1216 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 116:
#line 481 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { driver.params()->simpleAffectation(yystack_[4].value.as< std::string > (), yystack_[2].value.as< alpi::exprs::indexing::IndexingMultiRelation* > (), yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1222 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 117:
#line 486 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.params()->identDefQualifFromEmpty(); }
#line 1228 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 118:
#line 488 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.params()->identDefQualifFromList(yystack_[1].value.as< alpi::exprs::indexing::IndexingMultiRelation* > ()); }
#line 1234 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 119:
#line 493 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.params()->identDefQualifParamsFromUnit(yystack_[0].value.as< alpi::exprs::indexing::IndexingMultiRelation* > ()); }
#line 1240 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 120:
#line 495 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.params()->identDefQualifParamsFromNext(yystack_[2].value.as< alpi::exprs::indexing::IndexingMultiRelation* > (), yystack_[0].value.as< alpi::exprs::indexing::IndexingMultiRelation* > ()); }
#line 1246 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 121:
#line 500 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.params()->identDefQualifParamFromRelative(yystack_[0].value.as< alpi::exprs::numerics::NumericExpression* > ()); }
#line 1252 "alpi.tab.cc" // lalr1.cc:847
    break;

  case 122:
#line 502 "../src/yacclex/alpi.yy" // lalr1.cc:847
    { yylhs.value.as< alpi::exprs::indexing::IndexingMultiRelation* > () = driver.params()->identDefQualifParamFromIndexing(yystack_[2].value.as< alpi::exprs::symbol::IndexVariable* > (), yystack_[1].value.as< alpi::exprs::booleans::BoolOp* > ()); }
#line 1258 "alpi.tab.cc" // lalr1.cc:847
    break;


#line 1262 "alpi.tab.cc" // lalr1.cc:847
            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state,
                                           yyempty ? yyempty_ : yyla.type_get ()));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyempty)
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyempty = true;
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    yyerror_range[1].location = yystack_[yylen - 1].location;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyempty)
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyempty)
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
  alpiparser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what());
  }

  // Generate an error message.
  std::string
  alpiparser::yysyntax_error_ (state_type yystate, symbol_number_type yytoken) const
  {
    std::string yyres;
    // Number of reported tokens (one for the "unexpected", one per
    // "expected").
    size_t yycount = 0;
    // Its maximum.
    enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
    // Arguments of yyformat.
    char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];

    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yytoken) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
       - Of course, the expected token list depends on states to have
         correct lookahead information, and it depends on the parser not
         to perform extra reductions after fetching a lookahead from the
         scanner and before detecting a syntax error.  Thus, state
         merging (from LALR or IELR) and default reductions corrupt the
         expected token list.  However, the list is correct for
         canonical LR with one exception: it will still contain any
         token that will not be accepted due to an error action in a
         later state.
    */
    if (yytoken != yyempty_)
      {
        yyarg[yycount++] = yytname_[yytoken];
        int yyn = yypact_[yystate];
        if (!yy_pact_value_is_default_ (yyn))
          {
            /* Start YYX at -YYN if negative to avoid negative indexes in
               YYCHECK.  In other words, skip the first -YYN actions for
               this state because they are default actions.  */
            int yyxbegin = yyn < 0 ? -yyn : 0;
            // Stay within bounds of both yycheck and yytname.
            int yychecklim = yylast_ - yyn + 1;
            int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
            for (int yyx = yyxbegin; yyx < yyxend; ++yyx)
              if (yycheck_[yyx + yyn] == yyx && yyx != yyterror_
                  && !yy_table_value_is_error_ (yytable_[yyx + yyn]))
                {
                  if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                    {
                      yycount = 1;
                      break;
                    }
                  else
                    yyarg[yycount++] = yytname_[yyx];
                }
          }
      }

    char const* yyformat = YY_NULLPTR;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
        YYCASE_(0, YY_("syntax error"));
        YYCASE_(1, YY_("syntax error, unexpected %s"));
        YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    // Argument number.
    size_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += yytnamerr_ (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const signed char alpiparser::yypact_ninf_ = -109;

  const signed char alpiparser::yytable_ninf_ = -38;

  const short int
  alpiparser::yypact_[] =
  {
    -109,    21,     8,  -109,  -109,   -32,   -32,   -32,   -32,   -32,
      99,  -109,    23,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,   105,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,     1,  -109,
      56,   -14,    57,    57,    57,    57,  -109,  -109,    99,    81,
      59,     3,   136,  -109,    45,  -109,   136,  -109,  -109,  -109,
    -109,  -109,   136,   136,   156,    85,    88,    96,   127,  -109,
    -109,   274,    50,  -109,   136,     5,  -109,  -109,  -109,   274,
     -14,   202,  -109,   -11,  -109,    93,  -109,    -4,   -16,   107,
     253,   108,   178,   211,   124,  -109,  -109,   136,   136,  -109,
     136,   136,   136,   136,   136,  -109,    81,   274,  -109,  -109,
     136,  -109,   136,   136,   137,   136,   120,   136,   136,   136,
     136,  -109,  -109,   112,  -109,   136,  -109,  -109,   113,  -109,
     136,  -109,   141,   136,   136,   274,    52,    -3,    79,    79,
     124,   124,   124,  -109,    54,  -109,  -109,   149,    98,  -109,
    -109,   239,   136,  -109,   274,   274,   274,   274,    -4,   274,
     -16,   149,  -109,   -11,   -11,  -109,   136,  -109,  -109,  -109,
    -109,  -109,  -109,   136,   136,   136,   136,  -109,  -109,  -109,
    -109,   136,   142,   154,   274,   274,  -109,   153,   274,   246,
     171,   172,   136,  -109,   136,   136,   274,   143,   260,   267,
     157,  -109,  -109,   136,  -109,   274
  };

  const unsigned char
  alpiparser::yydefact_[] =
  {
       3,     0,     0,     1,     2,     0,     0,     0,     0,     0,
       0,    12,     0,   115,     5,     6,     8,     9,    10,     7,
      11,    88,    40,    46,   106,    80,    14,    15,     0,   101,
     102,   105,   104,   103,   100,    99,    98,    97,     0,     4,
     117,    90,    30,    30,    30,    30,    13,    16,     0,     0,
       0,     0,     0,    89,    91,    92,     0,    41,    47,   107,
      81,    96,     0,     0,     0,     0,     0,     0,    73,    60,
      61,   121,     0,   119,     0,     0,    18,    17,    95,    94,
       0,    73,    35,    39,    32,     0,    43,    49,   109,     0,
       0,    39,    13,    15,    67,    69,    71,     0,     0,    59,
       0,     0,     0,     0,     0,   118,     0,   116,    19,    93,
       0,    34,     0,     0,     0,     0,    42,     0,     0,     0,
       0,    56,    48,    50,    51,     0,   114,   108,   110,   111,
       0,    68,     0,     0,     0,    75,     0,     0,    62,    63,
      66,    64,    65,   120,    73,    29,    36,     0,    38,    33,
      31,    27,     0,    44,    53,    54,    55,    57,     0,   113,
       0,     0,   122,    39,    39,    58,     0,    74,    21,    20,
      22,    23,    24,     0,     0,     0,     0,    25,    45,    52,
     112,     0,     0,     0,    76,    79,    77,    78,    26,    83,
       0,     0,     0,    85,     0,     0,    84,    82,     0,     0,
       0,    70,    72,     0,    86,    87
  };

  const short int
  alpiparser::yypgoto_[] =
  {
    -109,  -109,  -109,  -109,    24,    10,    -5,  -109,    25,  -108,
    -109,  -109,    34,   -18,   -57,  -109,   -90,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,    26,   -49,  -109,
    -109,  -109,    84,   -54,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,   129,  -109,   140,  -109,  -109,  -109,
    -109,  -109,    44,  -109,  -109,  -109,  -109,   101
  };

  const short int
  alpiparser::yydefgoto_[] =
  {
      -1,     1,     2,    12,    68,    69,    70,    78,   173,    82,
     177,   146,    57,    83,    84,   111,   114,    14,    42,    86,
     116,   153,    15,    43,    87,   122,   123,   124,    85,   133,
     134,    99,   136,   148,    16,    45,    89,   193,   197,   204,
      17,    41,    53,    54,    55,    18,    38,    19,    44,    88,
     127,   128,   129,    20,    40,    50,    72,    73
  };

  const short int
  alpiparser::yytable_[] =
  {
      71,   132,   145,    79,   167,    37,    91,    27,     4,    47,
      11,   125,    51,    90,    52,    94,    75,   112,   117,   118,
      36,     3,   119,   120,   126,   107,    13,   113,    48,    21,
      22,    23,    24,    25,    35,   166,   121,     5,     6,     7,
       8,     9,    10,    37,   178,    11,    77,    11,   135,   135,
      11,   138,   139,   140,   141,   142,   149,    71,    36,   105,
      98,   165,    39,   147,    49,   -28,   151,    56,   154,   155,
     156,   157,    35,   182,   183,    76,   159,    58,    59,    60,
      81,   161,   -28,    80,    26,    27,    74,    81,   106,    62,
     166,    63,   -28,   102,    64,    95,   103,   104,    96,   108,
      65,    66,    26,    27,    97,   100,   101,   102,    46,    47,
     103,   104,    28,   174,   175,   163,   164,   184,    29,    30,
     186,   187,    67,    11,   185,   147,   147,   188,    31,    32,
     115,    33,   189,    98,   144,   130,   112,    81,   102,    26,
      27,    11,    34,   196,    62,   198,   199,   152,   150,    64,
     158,   160,   162,   190,   205,    65,    66,    81,    81,    92,
      93,   100,   101,   102,    62,   191,   103,   104,   174,    64,
     168,   169,   170,   171,   172,    65,    66,    67,    11,   194,
     195,   200,   137,   203,   179,   -13,   181,   -13,    61,   -13,
     -13,   -13,   -13,   -13,   -13,   -13,   -13,    67,    11,   -13,
     -13,   -13,   -13,   -13,   180,   -13,   -13,   143,    98,   109,
       0,     0,     0,   -37,   -13,   -13,   -13,   -13,   -15,     0,
     -15,     0,   -15,   -15,   -15,   -15,   -15,   -15,   -15,   -15,
     -37,     0,   -15,   -15,   -15,   -15,   -15,   110,   -15,   -15,
     -37,     0,     0,     0,     0,     0,     0,   -15,   -15,   -15,
     -15,   100,   101,   102,     0,     0,   103,   104,   100,   101,
     102,     0,   131,   103,   104,   100,   101,   102,   192,   201,
     103,   104,   100,   101,   102,   176,   202,   103,   104,   100,
     101,   102,     0,     0,   103,   104,   100,   101,   102,     0,
       0,   103,   104
  };

  const short int
  alpiparser::yycheck_[] =
  {
      49,    91,   110,    52,     7,    10,    63,     4,     0,     4,
      42,    27,    26,    62,    28,    64,    13,    28,    22,    23,
      10,     0,    26,    27,    40,    74,     2,    38,    27,     5,
       6,     7,     8,     9,    10,    38,    40,    29,    30,    31,
      32,    33,    34,    48,   152,    42,    51,    42,    97,    98,
      42,   100,   101,   102,   103,   104,   113,   106,    48,     9,
       6,     9,    39,   112,     8,    11,   115,    10,   117,   118,
     119,   120,    48,   163,   164,    51,   125,    43,    44,    45,
      56,   130,    28,    38,     3,     4,    27,    63,    38,     8,
      38,    10,    38,    14,    13,    10,    17,    18,    10,    75,
      19,    20,     3,     4,     8,    12,    13,    14,     3,     4,
      17,    18,    13,    15,    16,   133,   134,   166,    19,    20,
     174,   175,    41,    42,   173,   174,   175,   176,    29,    30,
      37,    32,   181,     6,   110,    28,    28,   113,    14,     3,
       4,    42,    43,   192,     8,   194,   195,    27,    11,    13,
      38,    38,    11,    11,   203,    19,    20,   133,   134,     3,
       4,    12,    13,    14,     8,    11,    17,    18,    15,    13,
      21,    22,    23,    24,    25,    19,    20,    41,    42,     8,
       8,    38,    98,    26,   158,     7,   161,     9,    48,    11,
      12,    13,    14,    15,    16,    17,    18,    41,    42,    21,
      22,    23,    24,    25,   160,    27,    28,   106,     6,    80,
      -1,    -1,    -1,    11,    36,    37,    38,    39,     7,    -1,
       9,    -1,    11,    12,    13,    14,    15,    16,    17,    18,
      28,    -1,    21,    22,    23,    24,    25,    35,    27,    28,
      38,    -1,    -1,    -1,    -1,    -1,    -1,    36,    37,    38,
      39,    12,    13,    14,    -1,    -1,    17,    18,    12,    13,
      14,    -1,     9,    17,    18,    12,    13,    14,    22,     9,
      17,    18,    12,    13,    14,    36,     9,    17,    18,    12,
      13,    14,    -1,    -1,    17,    18,    12,    13,    14,    -1,
      -1,    17,    18
  };

  const unsigned char
  alpiparser::yystos_[] =
  {
       0,    46,    47,     0,     0,    29,    30,    31,    32,    33,
      34,    42,    48,    49,    62,    67,    79,    85,    90,    92,
      98,    49,    49,    49,    49,    49,     3,     4,    13,    19,
      20,    29,    30,    32,    43,    49,    50,    51,    91,    39,
      99,    86,    63,    68,    93,    80,     3,     4,    27,     8,
     100,    26,    28,    87,    88,    89,    10,    57,    57,    57,
      57,    91,     8,    10,    13,    19,    20,    41,    49,    50,
      51,    73,   101,   102,    27,    13,    49,    51,    52,    73,
      38,    49,    54,    58,    59,    73,    64,    69,    94,    81,
      73,    59,     3,     4,    73,    10,    10,     8,     6,    76,
      12,    13,    14,    17,    18,     9,    38,    73,    49,    89,
      35,    60,    28,    38,    61,    37,    65,    22,    23,    26,
      27,    40,    70,    71,    72,    27,    40,    95,    96,    97,
      28,     9,    61,    74,    75,    73,    77,    77,    73,    73,
      73,    73,    73,   102,    49,    54,    56,    73,    78,    59,
      11,    73,    27,    66,    73,    73,    73,    73,    38,    73,
      38,    73,    11,    58,    58,     9,    38,     7,    21,    22,
      23,    24,    25,    53,    15,    16,    36,    55,    54,    72,
      97,    53,    61,    61,    73,    73,    78,    78,    73,    73,
      11,    11,    22,    82,     8,     8,    73,    83,    73,    73,
      38,     9,     9,    26,    84,    73
  };

  const unsigned char
  alpiparser::yyr1_[] =
  {
       0,    45,    46,    47,    47,    48,    48,    48,    48,    48,
      48,    48,    49,    50,    50,    51,    51,    52,    52,    52,
      53,    53,    53,    53,    53,    54,    55,    55,    56,    56,
      57,    57,    58,    58,    59,    59,    60,    60,    61,    61,
      63,    64,    62,    65,    65,    66,    68,    69,    67,    70,
      70,    71,    71,    72,    72,    72,    72,    72,    73,    73,
      73,    73,    73,    73,    73,    73,    73,    73,    73,    74,
      73,    75,    73,    76,    76,    77,    77,    78,    78,    78,
      80,    81,    79,    82,    82,    83,    83,    84,    86,    85,
      87,    87,    88,    88,    89,    89,    90,    91,    91,    91,
      91,    91,    91,    91,    91,    91,    93,    94,    92,    95,
      95,    96,    96,    97,    97,    99,    98,   100,   100,   101,
     101,   102,   102
  };

  const unsigned char
  alpiparser::yyr2_[] =
  {
       0,     2,     2,     0,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     1,     1,     2,     1,     1,     2,
       1,     1,     1,     1,     1,     4,     2,     0,     1,     1,
       0,     4,     1,     3,     2,     1,     2,     0,     2,     0,
       0,     0,     6,     0,     2,     2,     0,     0,     6,     0,
       1,     1,     3,     2,     2,     2,     1,     2,     4,     2,
       1,     1,     3,     3,     3,     3,     3,     2,     3,     0,
       9,     0,     9,     0,     3,     1,     3,     3,     3,     3,
       0,     0,    11,     0,     2,     0,     3,     2,     0,     4,
       0,     1,     1,     3,     2,     2,     4,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     0,     0,     6,     0,
       1,     1,     3,     2,     1,     0,     5,     0,     3,     1,
       3,     1,     4
  };



  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const alpiparser::yytname_[] =
  {
  "\"end of file\"", "error", "$undefined", "\"entier\"", "\"réel\"",
  "\"booléen\"", "\"[\"", "\"]\"", "\"(\"", "\")\"", "\"{\"", "\"}\"",
  "\"+\"", "\"-\"", "\"^\"", "\"&&\"", "\"||\"", "\"*\"", "\"/\"",
  "\"sum\"", "\"prod\"", "\"==\"", "\"<=\"", "\">=\"", "\"<\"", "\">\"",
  "\"+/-\"", "\"=\"", "\":\"", "\"minimize\"", "\"set\"", "\"var\"",
  "\"param\"", "\"subject to\"", "\"option\"", "\"in\"", "\"by\"",
  "\"..\"", "\",\"", "\";\"", "\"integer\"",
  "\"fonction du langage (cos, sqrt, sin...)\"", "\"identifier\"",
  "\"nom de fichier\"", "NEG", "$accept", "ProblemDesc", "Instructions",
  "Instruction", "Ident", "CstRelative", "CstReal", "Real", "Test",
  "IndexDef", "ByStep", "Index", "IndexingOpt", "IndexingList", "Indexing",
  "InIndexOpt", "IndexingReducer", "SetDef", "$@1", "$@2",
  "SetDefAttrList", "SetDefAttr", "VariableDef", "$@3", "$@4",
  "VarAttrOpt", "VarAttrs", "VarAttr", "Expression", "$@5", "$@6",
  "IdentQualif", "ListExpression", "BExpression", "DefCtr", "$@7", "$@8",
  "EndConstraint", "DefCtrAttrs", "DefCtrAttr", "ObjDef", "$@9",
  "ObjDefAttrsOpt", "ObjDefAttrs", "ObjDefAttr", "Option", "Anything",
  "DefParam", "$@10", "$@11", "ParamAttrOpt", "ParamAttrs", "ParamAttr",
  "SimpleAffectation", "$@12", "IdentDefQualif", "IdentDefQualifParams",
  "IdentDefQualifParam", YY_NULLPTR
  };

#if YYDEBUG
  const unsigned short int
  alpiparser::yyrline_[] =
  {
       0,   125,   125,   129,   130,   134,   135,   136,   137,   138,
     139,   140,   147,   152,   154,   168,   170,   175,   177,   179,
     184,   186,   188,   190,   192,   199,   204,   206,   211,   213,
     218,   220,   225,   227,   232,   234,   239,   241,   246,   248,
     256,   258,   255,   263,   264,   268,   276,   278,   275,   284,
     285,   289,   290,   294,   296,   298,   300,   302,   309,   311,
     314,   316,   318,   320,   322,   324,   326,   328,   330,   333,
     332,   338,   337,   345,   347,   352,   354,   359,   361,   363,
     371,   373,   370,   379,   381,   386,   387,   391,   399,   398,
     405,   406,   410,   411,   415,   417,   424,   429,   431,   433,
     435,   437,   439,   441,   443,   445,   453,   455,   452,   461,
     462,   466,   467,   471,   473,   479,   478,   485,   487,   492,
     494,   499,   501
  };

  // Print the state stack on the debug stream.
  void
  alpiparser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  alpiparser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG



} // yy
#line 1807 "alpi.tab.cc" // lalr1.cc:1155
#line 506 "../src/yacclex/alpi.yy" // lalr1.cc:1156


void
yy::alpiparser::error (const location_type& loc,
    const std::string& m) {
  driver.error(loc, m);
}
