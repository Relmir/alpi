/*
 * constant_evaluator_relative.h
 *
 *  Created on: Jan 22, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef CONSTANT_EVALUATOR_RELATIVE_H_
#define CONSTANT_EVALUATOR_RELATIVE_H_

#include "../exprs/ExpressionNode.h"
#include "../exprs/numerics/RelativeConstant.h"

namespace alpi_eval {

  /** \brief Tries to evaluate an expression into a constant integer
   *
   * \param node alpi::exprs::ExpressionNode* the node to evaluate
   * \return alpi::exprs::RelativeConstant* the resulting constant (or
   *      null)
   *
   */
  alpi::exprs::numerics::RelativeConstant* constantEvaluateRelative(alpi::exprs::numerics::NumericExpression* node);

}  // namespace alpi_eval

#endif /* CONSTANT_EVALUATOR_RELATIVE_H_ */
