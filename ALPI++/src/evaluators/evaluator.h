/*
 * evaluator.h
 *
 *  Created on: Feb 5, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef EVALUATOR_H_
#define EVALUATOR_H_

#include <map>

#include "../exprs/ExpressionNode.h"

namespace alpi {
  namespace exprs {
    namespace symbol {
      class Symbol;
    }  // namespace symbol
    namespace booleans {
      class BooleanAnd;
      class BooleanOr;
      class BoolOp;
      class Equal;
      class GreaterEqual;
      class GreaterThan;
      class LowerEqual;
      class LowerThan;
    } /* namespace booleans */
    namespace numerics {
      class NumericExpression;
    }
  } /* namespace exprs */
} /* namespace alpi */

namespace alpi {
  namespace eval {

    using namespace alpi::exprs;
    using namespace alpi::exprs::symbol;
    using namespace alpi::exprs::booleans;

    bool evaluateBoolean(BoolOp* expression,
        std::map<Symbol*, ExpressionNode*> mapping);

    bool evaluateBoolean(BooleanAnd* expression,
        std::map<Symbol*, ExpressionNode*> mapping);
    bool evaluateBoolean(BooleanOr* expression,
        std::map<Symbol*, ExpressionNode*> mapping);
    bool evaluateBoolean(LowerThan* expression,
        std::map<Symbol*, ExpressionNode*> mapping);
    bool evaluateBoolean(LowerEqual* expression,
        std::map<Symbol*, ExpressionNode*> mapping);
    bool evaluateBoolean(Equal* expression,
        std::map<Symbol*, ExpressionNode*> mapping);
    bool evaluateBoolean(GreaterEqual* expression,
        std::map<Symbol*, ExpressionNode*> mapping);
    bool evaluateBoolean(GreaterThan* expression,
        std::map<Symbol*, ExpressionNode*> mapping);

    double evaluateDouble(numerics::NumericExpression* node,
        std::map<Symbol*, ExpressionNode*> mapping);

    int evaluateInteger(numerics::NumericExpression* node,
        std::map<Symbol*, ExpressionNode*> mapping);
  }  // namespace eval
}  // namespace alpi

#endif /* EVALUATOR_H_ */
