/*
 * constant_evaluator_relative.cpp
 *
 *  Created on: Jan 22, 2014
 *      Author: Emmanuel BIGEON
 */

#include "constant_evaluator_relative.h"

#include <iostream>
#include <string>

#include "../exprs/symbol/ParamVariable.h"
#include "../exprs/numerics/RelativeConstant.h"
#include "evaluator.h"

namespace alpi_eval {

  using namespace std;
  using namespace alpi::exprs;
  using namespace alpi::exprs::numerics;
  using namespace alpi::exprs::symbol;

  RelativeConstant* constantEvaluateRelative(
      NumericExpression* node) {
    RelativeConstant* cst = dynamic_cast<RelativeConstant*>(node);
    if (cst != NULL) {
      return cst;
    }
    ParamVariable* pv = dynamic_cast<ParamVariable*>(node);
    if (pv != NULL) {
      if (pv->isIndependant() && !pv->isVector()) {
        return constantEvaluateRelative(pv->getExpression());
      }
    }

    return new RelativeConstant(
        alpi::eval::evaluateInteger(node,
            map<Symbol*, ExpressionNode*>()));
  }

}  // namespace alpi_eval
