/*
 * evaluator.cpp
 *
 *  Created on: Feb 5, 2014
 *      Author: Emmanuel BIGEON
 */

#include "evaluator.h"

#include <cmath>
#include <iostream>
#include <map>
#include <string>

#include "../exprs/symbol/IndexVariable.h"
#include "../exprs/QualifiedExpression.h"
#include "../exprs/booleans/BooleanAnd.h"
#include "../exprs/booleans/BooleanOr.h"
#include "../exprs/booleans/Equal.h"
#include "../exprs/booleans/GreaterEqual.h"
#include "../exprs/booleans/GreaterThan.h"
#include "../exprs/booleans/LowerEqual.h"
#include "../exprs/booleans/LowerThan.h"
#include "../exprs/numerics/RealConstant.h"
#include "../exprs/numerics/RelativeConstant.h"
#include "../exprs/operations/Addition.h"
#include "../exprs/operations/CosineFunctionCall.h"
#include "../exprs/operations/Difference.h"
#include "../exprs/operations/Division.h"
#include "../exprs/operations/Multiplication.h"
#include "../exprs/operations/Oppose.h"
#include "../exprs/operations/Power.h"
#include "../exprs/operations/SineFunctionCall.h"
#include "../exprs/operations/SqrtFunctionCall.h"
#include "../exprs/symbol/Symbol.h"
#include "../exprs/symbol/ParamVariable.h"
#include <vector>
#include "../exprs/indexing/IndexingMultiRelation.h"

double cos(double __x);
double sin(double __x);
double sqrt(double __x);

namespace alpi {
  namespace eval {

    using namespace std;
    using namespace alpi::exprs::booleans;
    using namespace alpi::exprs::symbol;
    using namespace alpi::exprs::operations;
    using namespace alpi::exprs::builtin;
    using namespace alpi::exprs::numerics;
    using namespace alpi::exprs;

    bool evaluateBoolean(BoolOp* expression,
        map<Symbol*, ExpressionNode*> mapping) {
      BooleanAnd* band = dynamic_cast<BooleanAnd*>(expression);
      if (band != NULL) {
        return evaluateBoolean(band, mapping);
      }
      BooleanOr* bor = dynamic_cast<BooleanOr*>(expression);
      if (bor != NULL) {
        return evaluateBoolean(bor, mapping);
      }
      LowerThan* lt = dynamic_cast<LowerThan*>(expression);
      if (lt != NULL) {
        return evaluateBoolean(lt, mapping);
      }
      LowerEqual* le = dynamic_cast<LowerEqual*>(expression);
      if (le != NULL) {
        return evaluateBoolean(le, mapping);
      }
      Equal* eq = dynamic_cast<Equal*>(expression);
      if (eq != NULL) {
        return evaluateBoolean(eq, mapping);
      }
      GreaterEqual* ge = dynamic_cast<GreaterEqual*>(expression);
      if (ge != NULL) {
        return evaluateBoolean(ge, mapping);
      }
      GreaterThan* gt = dynamic_cast<GreaterThan*>(expression);
      if (gt != NULL) {
        return evaluateBoolean(gt, mapping);
      }
      cerr << "evaluator: Unknown evaluation for boolean "
          << expression->toString() << endl;
      return false;
    }

    bool evaluateBoolean(BooleanAnd* expression,
        std::map<Symbol*, ExpressionNode*> mapping) {
      BoolOp* lhs = dynamic_cast<BoolOp*>(expression->getLeft());
      if (lhs != NULL) {
        BoolOp* rhs = dynamic_cast<BoolOp*>(expression->getRight());
        if (rhs != NULL) {
          return evaluateBoolean(lhs, mapping)
              && evaluateBoolean(rhs, mapping);
        }
      }
      cerr
          << "evaluator: right or left hand-side of this and expression isn't a boolean."
          << endl;
      return false;
    }
    bool evaluateBoolean(BooleanOr* expression,
        std::map<Symbol*, ExpressionNode*> mapping) {
      BoolOp* lhs = dynamic_cast<BoolOp*>(expression->getLeft());
      if (lhs != NULL) {
        BoolOp* rhs = dynamic_cast<BoolOp*>(expression->getRight());
        if (rhs != NULL) {
          return evaluateBoolean(lhs, mapping)
              || evaluateBoolean(rhs, mapping);
        }
      }
      cerr
          << "evaluator: right or left hand-side of this or expression isn't a boolean."
          << endl;
      return false;
    }
    bool evaluateBoolean(LowerThan* expression,
        std::map<Symbol*, ExpressionNode*> mapping) {
      return evaluateDouble(expression->getLeft(), mapping)
          < evaluateDouble(expression->getRight(), mapping);
    }
    bool evaluateBoolean(LowerEqual* expression,
        std::map<Symbol*, ExpressionNode*> mapping) {
      return evaluateDouble(expression->getLeft(), mapping)
          <= evaluateDouble(expression->getRight(), mapping);
    }
    bool evaluateBoolean(Equal* expression,
        std::map<Symbol*, ExpressionNode*> mapping) {
      return evaluateDouble(expression->getLeft(), mapping)
          == evaluateDouble(expression->getRight(), mapping);
    }
    bool evaluateBoolean(GreaterEqual* expression,
        std::map<Symbol*, ExpressionNode*> mapping) {
      return evaluateDouble(expression->getLeft(), mapping)
          >= evaluateDouble(expression->getRight(), mapping);
    }
    bool evaluateBoolean(GreaterThan* expression,
        std::map<Symbol*, ExpressionNode*> mapping) {
      return evaluateDouble(expression->getLeft(), mapping)
          > evaluateDouble(expression->getRight(), mapping);
    }

    double evaluateDouble(numerics::NumericExpression* node,
        map<Symbol*, alpi::exprs::ExpressionNode*> mapping) {

      RealConstant* rc = dynamic_cast<RealConstant*>(node);
      if (rc != NULL) {
        return rc->toDouble();
      }

      numerics::RelativeConstant* rlc =
          dynamic_cast<numerics::RelativeConstant*>(node);
      if (rlc != NULL) {
        return rlc->toInt();
      }

      Symbol* sym = dynamic_cast<Symbol*>(node);
      if (sym != NULL) {
        if (mapping.find(sym) != mapping.end()) {
          numerics::NumericExpression* ne =
              dynamic_cast<numerics::NumericExpression*>(mapping[sym]);
          if (ne != NULL) {
            return evaluateDouble(ne, mapping);
          }
          cerr << "Symbol isn't a numeric: " << sym->getName()
              << endl;
          return 0;
        }
        cerr << "Symbol value is unknown: " << sym->getName() << endl;
        return 0;
      }

      Addition* add = dynamic_cast<Addition*>(node);
      if (add != NULL) {
        return evaluateDouble(add->getLeft(), mapping)
            + evaluateDouble(add->getRight(), mapping);
      }
      CosineFunctionCall* cosine =
          dynamic_cast<CosineFunctionCall*>(node);
      if (cosine != NULL) {
        return cos(evaluateDouble(cosine->getArgument(), mapping));
      }
      Difference* diff = dynamic_cast<Difference*>(node);
      if (diff != NULL) {
        return evaluateDouble(diff->getLeft(), mapping)
            - evaluateDouble(diff->getRight(), mapping);
      }
      Division* div = dynamic_cast<Division*>(node);
      if (div != NULL) {
        return evaluateDouble(div->getLeft(), mapping)
            / evaluateDouble(div->getRight(), mapping);
      }
      Multiplication* mul = dynamic_cast<Multiplication*>(node);
      if (mul != NULL) {
        return evaluateDouble(mul->getLeft(), mapping)
            * evaluateDouble(mul->getRight(), mapping);
      }
      Oppose* op = dynamic_cast<Oppose*>(node);
      if (op != NULL) {
        return -evaluateDouble(op->getArgument(), mapping);
      }
      SineFunctionCall* sine = dynamic_cast<SineFunctionCall*>(node);
      if (sine != NULL) {
        return sin(evaluateDouble(sine->getArgument(), mapping));
      }
      SqrtFunctionCall* sq = dynamic_cast<SqrtFunctionCall*>(node);
      if (sq != NULL) {
        return sqrt(evaluateDouble(sq->getArgument(), mapping));
      }

      QualifiedExpression<NumericExpression>* qe =
          dynamic_cast<QualifiedExpression<NumericExpression>*>(node);
      if (qe != NULL) {
        if (qe->getQualified()->isConstant() || qe->getQualifier() == NULL) {
          return evaluateDouble(qe->getQualified(), mapping);
        } else {
          indexing::IndexedElement* ie = dynamic_cast<indexing::IndexedElement*>(qe->getQualified());
          if (ie != NULL) {
            map<Symbol*, ExpressionNode*> newMapping(mapping);
            Qualifier* qu = qe->getQualifier();
            list<NumericExpression*> exp = qu->getExpressions();
            int i = 0;
            vector<IndexVariable*> inds = ie->getIndexing()->getIndexingVariables();
            for (list<NumericExpression*>::iterator it = exp.begin(); it != exp.end(); ++it) {
              newMapping[inds[i++]] = *it;
            }
            return evaluateDouble(qe->getQualified(), newMapping);
          } else {
            cerr << "evaluator: evaluate indexed value for non indexed expression "
                << qe->getQualified()->toString() << endl;
          }
        }
      }

      // TODO Power, Sum, Prod, Qualified expression

      cerr << "evaluator: implement numeric evaluation for "
          << node->toString() << endl;
      return 0;
    }

    int evaluateInteger(numerics::NumericExpression* node,
        std::map<Symbol*, ExpressionNode*> mapping) {
      numerics::RelativeConstant* rlc =
          dynamic_cast<numerics::RelativeConstant*>(node);
      if (rlc != NULL) {
        return rlc->toInt();
      }

      Symbol* sym = dynamic_cast<Symbol*>(node);
      if (sym != NULL) {
        if (mapping.find(sym) != mapping.end()) {
          numerics::NumericExpression* ne =
              dynamic_cast<numerics::NumericExpression*>(mapping[sym]);
          if (ne != NULL) {
            return evaluateInteger(ne, mapping);
          }
          cerr << "evaluator: Symbol isn't a numeric: " << sym->getName()
              << endl;
          return 0;
        }
        // Try to test if it is independant, then try to evaluate the
        // expression behind it
        if (sym->isIndependant()) {
          if (sym->isInteger()) {
            ParamVariable* pv =
                dynamic_cast<ParamVariable*>(sym);
            if (pv != NULL) {
              return evaluateInteger(pv->getExpression(), mapping);
            }
          }
        }

        cerr << "evaluator: Symbol value is unknown: " << sym->getName() << endl;
        return 0;
      }

      Addition* add = dynamic_cast<Addition*>(node);
      if (add != NULL) {
        return evaluateInteger(add->getLeft(), mapping)
            + evaluateInteger(add->getRight(), mapping);
      }
      CosineFunctionCall* cosine =
          dynamic_cast<CosineFunctionCall*>(node);
      if (cosine != NULL) {
        return cos(evaluateInteger(cosine->getArgument(), mapping));
      }
      Difference* diff = dynamic_cast<Difference*>(node);
      if (diff != NULL) {
        return evaluateInteger(diff->getLeft(), mapping)
            - evaluateInteger(diff->getRight(), mapping);
      }
      Division* div = dynamic_cast<Division*>(node);
      if (div != NULL) {
        return evaluateInteger(div->getLeft(), mapping)
            / evaluateInteger(div->getRight(), mapping);
      }
      Multiplication* mul = dynamic_cast<Multiplication*>(node);
      if (mul != NULL) {
        return evaluateInteger(mul->getLeft(), mapping)
            * evaluateInteger(mul->getRight(), mapping);
      }
      Oppose* op = dynamic_cast<Oppose*>(node);
      if (op != NULL) {
        return -evaluateInteger(op->getArgument(), mapping);
      }
      SineFunctionCall* sine = dynamic_cast<SineFunctionCall*>(node);
      if (sine != NULL) {
        return sin(evaluateInteger(sine->getArgument(), mapping));
      }
      SqrtFunctionCall* sq = dynamic_cast<SqrtFunctionCall*>(node);
      if (sq != NULL) {
        return sqrt(evaluateInteger(sq->getArgument(), mapping));
      }

      QualifiedExpression<NumericExpression>* qe =
          dynamic_cast<QualifiedExpression<NumericExpression>*>(node);
      if (qe != NULL) {
        if (qe->getQualified()->isConstant() || qe->getQualifier() == NULL) {
          return evaluateInteger(qe->getQualified(), mapping);
        } else {
          indexing::IndexedElement* ie = dynamic_cast<indexing::IndexedElement*>(qe->getQualified());
          if (ie != NULL) {
            map<Symbol*, ExpressionNode*> newMapping(mapping);
            Qualifier* qu = qe->getQualifier();
            list<NumericExpression*> exp = qu->getExpressions();
            int i = 0;
            vector<IndexVariable*> inds = ie->getIndexing()->getIndexingVariables();
            for (list<NumericExpression*>::iterator it = exp.begin(); it != exp.end(); ++it) {
              newMapping[inds[i++]] = *it;
            }
            return evaluateInteger(qe->getQualified(), newMapping);
          } else {
            cerr << "evaluator: evaluate indexed value for non indexed expression "
                << qe->getQualified()->toString() << endl;
          }
        }
      }

      // TODO Power, Sum, Prod, Qualified expression

      cerr << "evaluator: implement numeric (integer) evaluation for "
          << node->toString() << endl;
      return 0;
    }
  }  // namespace eval
}  // namespace alpi
