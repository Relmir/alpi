/*
 * ConstraintDriver.cpp
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#include "ConstraintDriver.h"

#include "../exprs/symbol/ParamVariable.h"
#include "../exprs/numerics/NumericExpression.h"
#include "../problem/Constraint.h"
#include "../problem/Function.h"
#include "../problem/ProblemModel.h"
#include "driver.hh"

namespace alpi {
  namespace parser {
    using namespace alpi::problem;
    using namespace alpiparsertools;


    ConstraintDriver::ConstraintDriver(alpiparsertools::driver* driver) :
        driv(driver), curr(NULL), prec(NULL) {

    }

    ConstraintDriver::~ConstraintDriver() {
    }

    void ConstraintDriver::definitionBeforeIndex(std::string name) {
      curr = new ParamVariable(name);
      if (driv->addSymbolToTable(curr) != 0) {
        driv->error(
            "Cannot create function " + name
                + " as it already exists");
      }
      driv->raiseContext();
    }
    void ConstraintDriver::definitionAfterIndex(std::string name,
        IndexingMultiRelation* index) {
      curr->setIndex(index);
    }
    void ConstraintDriver::definitionEnd(std::string name,
        IndexingMultiRelation* index, numerics::NumericExpression* lhs, int test,
        numerics::NumericExpression* mhs, numerics::NumericExpression* rhs) {
      Function* fun = new Function(curr);

      Constraint* c;
      if (test == 0) {
        if (rhs != NULL) {
          driv->error(
              "Cannot define equality constraint and inequality constraint in one instruction.");
        }
        if (mhs->isIndependant()) {
          if (lhs->isIndependant()) {
            driv->error(
                "A constraint must be constraining the decision variables...");
          }
//        driv->getModel()->addAuxiliaryFunction(fun);
          c = new Constraint(fun, mhs);
          curr->setExpression(lhs);
        } else if (lhs->isIndependant()) {
//        driv->getModel()->addAuxiliaryFunction(fun);
          c = new Constraint(fun, lhs);
          curr->setExpression(mhs);
        } else {
          c = new Constraint(NULL, NULL);
          driv->error(
              "Cannot create a constraint between two expression depending on the decision variables.");
        }
      } else if (test == -1) {
        if (rhs == NULL) {
          if (mhs->isIndependant()) {
            if (lhs->isIndependant()) {
              driv->error(
                  "A constraint must be constraining the decision variables...");
            }
//          driv->getModel()->addAuxiliaryFunction(fun);
            c = new Constraint(fun, false, mhs);
            curr->setExpression(lhs);
          } else if (lhs->isIndependant()) {
//          driv->getModel()->addAuxiliaryFunction(fun);
            c = new Constraint(fun, true, lhs);
            curr->setExpression(mhs);
          } else {
            c = new Constraint(NULL, NULL);
            driv->error(
                "Cannot create a constraint between two expression depending on the decision variables.");
          }
        } else {
          if (!(rhs->isIndependant() && lhs->isIndependant())) {
            driv->error(
                "The middle expression must be the only one depending on variables");
          }
          if (mhs->isIndependant()) {
            driv->error(
                "The middle expression must be depending on variables");
          }
//        driv->getModel()->addAuxiliaryFunction(fun);
          c = new Constraint(fun, lhs, rhs);
          curr->setExpression(mhs);
        }
      } else /*if (test == 1)*/ {
        if (rhs != NULL) {
          driv->error(
              "Cannot define opposed inequality constraints in one instruction.");
        }
        if (mhs->isIndependant()) {
          if (lhs->isIndependant()) {
            driv->error(
                "A constraint must be constraining the decision variables...");
          }
//        driv->getModel()->addAuxiliaryFunction(fun);
          c = new Constraint(fun, true, mhs);
          curr->setExpression(lhs);
        } else if (lhs->isIndependant()) {
//        driv->getModel()->addAuxiliaryFunction(fun);
          c = new Constraint(fun, false, lhs);
          curr->setExpression(mhs);
        } else {
          c = new Constraint(NULL, NULL);
          driv->error(
              "Cannot create a constraint between two expression depending on the decision variables.");
        }
      }
      // Choose constructor
      if (prec != NULL) {
        c->setPrecision(prec);
      }
      driv->getModel()->addConstraint(c);
      driv->lowerContext();
      curr = NULL;
      prec = NULL;
    }
    NumericExpression* ConstraintDriver::endConstraintFromEmpty() {
      return NULL;
    }
    NumericExpression* ConstraintDriver::endConstraintFromTest(
        NumericExpression* node) {
      return node;
    }
    void ConstraintDriver::attributePrecision(numerics::NumericExpression* real) {
      if (real->isVector()) {
        driv->error(
            "Precision must be a real, " + real->toString()
                + " is a vector...");
      }
      prec = real;
    }
  }
} /* namespace alpiparsertools */
