/*
 * NumericsDriver.h
 *
 *  Created on: Jan 15, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef NUMERICSDRIVER_H_
#define NUMERICSDRIVER_H_

#include "../exprs/numerics/RelativeConstant.h"
#include "../exprs/numerics/RealConstant.h"

namespace alpiparsertools {
  class driver;
}

namespace alpi {
  namespace parser {
    using namespace alpi::exprs;
    using namespace alpi::exprs::numerics;

    /** \brief The driver for rule relating to numerics
     */
    class NumericsDriver {
        alpiparsertools::driver* drive;
      public:
        /** \brief Create the driver
         */
        NumericsDriver(alpiparsertools::driver* driv);
        /** \brief Transparent operation
         *
         * \param cst RelativeConstant* the relative constraint
         * \return ExpressionNode* the node
         *
         */
        NumericExpression* relativeFromCst(RelativeConstant* cst);
        /** \brief Transparent operation
         *
         * \param cst RelativeConstant* the relative constraint
         * \return ExpressionNode* the node
         *
         */
        NumericExpression* relativeFromVariable(std::string name);
        /** \brief Transparent operation
         *
         * \param cst RelativeConstant* the relative constraint
         * \return ExpressionNode* the node
         *
         */
        NumericExpression* relativeFromOpposeVariable(std::string name);
        /** \brief Transform a relative into a real
         *
         * \param cst RelativeConstant* the relative
         * \return RealConstant* the real
         *
         */
        RealConstant* cstRealFromCstRelative(RelativeConstant* cst);
        /** \brief Transparent operation
         *
         * \param cst RealConstant* the real
         * \return ExpressionNode* the node
         *
         */
        NumericExpression* realFromCst(RealConstant* cst);
        /** \brief Transparent operation
         *
         * \param cst RelativeConstant* the relative constraint
         * \return ExpressionNode* the node
         *
         */
        NumericExpression* realFromVariable(std::string name);
        /** \brief Convert a variable to a numeric representation
         *
         * \param name the variable name
         * \return NumericExpression* the node
         *
         */
        NumericExpression* realFromOpposeVariable(std::string name);
        virtual ~NumericsDriver();
    };

  }
} /* namespace alpiparsertools */

#endif /* NUMERICSDRIVER_H_ */
