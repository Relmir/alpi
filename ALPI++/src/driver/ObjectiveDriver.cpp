/*
 * ObjectiveDriver.cpp
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#include "ObjectiveDriver.h"
#include "driver.hh"

namespace alpi {
  namespace parser {

    using namespace alpiparsertools;
    using namespace alpi::problem;

    ObjectiveDriver::ObjectiveDriver(driver* driver) :
        driv(driver), curr(NULL) {
    }

    ObjectiveDriver::~ObjectiveDriver() {
    }

    void ObjectiveDriver::definition(std::string name) {
      curr = new Function(new ParamVariable(name));
      driv->raiseContext();
    }
    void ObjectiveDriver::definitionEnd() {
      driv->getModel()->setObjective(curr);
      driv->lowerContext();
      curr = NULL;
    }
    void ObjectiveDriver::affectExpression(numerics::NumericExpression* node) {
      curr->getParamVariable()->setExpression(node);
    }
    void ObjectiveDriver::setPrecision(numerics::NumericExpression* real) {
      if (real->isVector()) {
        driv->error(
            "Precision must be a real, " + real->toString()
                + " is a vector...");
      }
      driv->getModel()->setObjectivePrecision(real);
    }
  } /* namespace alpiparsertools */
}
