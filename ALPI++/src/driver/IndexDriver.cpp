/*
 * IndexDriver.cpp
 *
 *  Created on: Jan 20, 2014
 *      Author: Emmanuel BIGEON
 */

#include "IndexDriver.h"

#include <iostream>
#include <sstream>

#include "../exprs/indexing/Index.h"
#include "../exprs/indexing/IndexingMultiRelation.h"
#include "../exprs/symbol/IndexVariable.h"
#include "driver.hh"

namespace alpi {
  namespace parser {

    using namespace alpiparsertools;

    int count_fresh = 0;
    std::string fresh() {
      std::ostringstream oss;
      oss << "_i" << (++count_fresh);
      return oss.str();
    }

    IndexDriver::IndexDriver(driver* owner) :
        drive(owner) {
    }

    IndexDriver::~IndexDriver() {
    }
    IndexingMultiRelation* IndexDriver::indexingOptEmpty() {
      return new IndexingMultiRelation();
    }

    IndexingMultiRelation* IndexDriver::indexingOptFromSetExpr(
        IndexingMultiRelation* rel, booleans::BoolOp* reducer) {
      rel->setPredicate(reducer);
      return rel;
    }

    Index* IndexDriver::indexFromDef(Index* i) {
      return i;
    }
    Index* IndexDriver::indexDef(numerics::NumericExpression* start,
        numerics::NumericExpression* stop, numerics::NumericExpression* step) {
      Index *i = new Index(start, stop, step);
      return i;
    }

    numerics::NumericExpression* IndexDriver::indexDefByStepEmpty() {
      return NULL;
    }

    IndexingMultiRelation* IndexDriver::indexingListFromOne(
        IndexVariable* rel) {
      IndexingMultiRelation* i = new IndexingMultiRelation();
      i->addRelation(rel);
      return i;
    }
    IndexingMultiRelation* IndexDriver::indexingListFromNext(
        IndexingMultiRelation* list, IndexVariable* next) {
      list->addRelation(next);
      return list;
    }
    IndexVariable* IndexDriver::indexingFromIdentOptInIndex(
        std::string name, Index* i) {
      Index *ind = i;
      std::string n = name;
      if (i == NULL) {
        ind = drive->getIndex(name);
        n = fresh();
      }
      // Add symbol to symbolTable
      if (ind != NULL) {
        IndexVariable* v = new IndexVariable(n);
        v->setOverIndex(ind);
        drive->addSymbolToTable(v);
        return v;
      } else {
        return NULL;
      }
    }
    IndexVariable* IndexDriver::indexingFromDef(Index* i) {
      IndexVariable* v = new IndexVariable(fresh());
      drive->addSymbolToTable(v);
      v->setOverIndex(i);
      return v;
    }
    Index* IndexDriver::inIndexFromIndex(Index* i) {
      return i;
    }
    Index* IndexDriver::inIndexFromEmpty() {
      return NULL;
    }

    booleans::BoolOp* IndexDriver::reducerFromTest(booleans::BoolOp* test) {
      return test;
    }
    booleans::BoolOp* IndexDriver::reducerFromEmpty() {
      return NULL;
    }
  } /* namespace alpiparsertools */
}
