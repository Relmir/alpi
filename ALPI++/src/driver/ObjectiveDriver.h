/*
 * ObjectiveDriver.h
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef OBJECTIVEDRIVER_H_
#define OBJECTIVEDRIVER_H_

#include <string>
#include "../exprs/numerics/NumericExpression.h"
#include "../problem/Function.h"

namespace alpiparsertools {
  class driver;
}  // namespace alpiparsertools

namespace alpi {
  namespace parser {
    using namespace alpi::exprs;

    /** \brief The driver for rule relating to the objective function
     */
    class ObjectiveDriver {
      public:
        /** \brief Create the driver
         *
         * \param driver alpiparsertools::driver* the global driver
         *
         */
        ObjectiveDriver(alpiparsertools::driver* driver);
        /** \brief Starting the definition, setting the name
         *
         * \param name std::string the name
         * \return void
         *
         */
        void definition(std::string name);
        /** \brief Cleaning definition
         *
         * \return void
         *
         */
        void definitionEnd();
        /** \brief Set the expression of the function
         *
         * \param node numerics::NumericExpression* the expression
         * \return void
         *
         */
        void affectExpression(numerics::NumericExpression* node);
        /** \brief Set the precision
         *
         * \param real numerics::NumericExpression* the precision
         * \return void
         *
         */
        void setPrecision(numerics::NumericExpression* real);
        virtual ~ObjectiveDriver();
      private:
        alpiparsertools::driver* driv;
        alpi::problem::Function* curr;
    };

  }
} /* namespace alpiparsertools */

#endif /* OBJECTIVEDRIVER_H_ */
