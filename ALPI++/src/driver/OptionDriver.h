/*
 * OptionDriver.h
 *
 *  Created on: Jan 22, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef OPTIONDRIVER_H_
#define OPTIONDRIVER_H_

#include "../problem/ProblemModel.h"
#include "../exprs/numerics/RealConstant.h"
#include "../exprs/numerics/RelativeConstant.h"
namespace alpiparsertools {
  class driver;
}  // namespace alpiparsertools

namespace alpi {
  namespace parser {
    /** \brief The driver for option related rules
     */
    class OptionDriver {
      public:
        /** \brief Create the driver
         *
         * \param model the model to put options in
         * \param driver the global driver
         */
        OptionDriver(alpi::problem::ProblemModel* model,
            alpiparsertools::driver* driver);
        /** \brief Transform a real in a string
         *
         * \param real the real
         * \return std::string the string
         */
        std::string anythingFromConstant(
            alpi::exprs::numerics::RealConstant* real);
        /** \brief Transforms a relative in a string
         *
         * \param real the relative
         * \return std::String the string
         */
        std::string anythingFromConstant(
            alpi::exprs::numerics::RelativeConstant* real);
        /** \brief Transparent operation
         *
         * \param name std::string the identifier
         * \return std::string the string
         */
        std::string anythingFromIdent(std::string name);
        /** \brief Tranparent operation
         *
         * \param name std::string the file name
         * \return std::string the string
         *
         */
        std::string anythingFromFileName(std::string name);
        /** \brief Transform sum token into string
         *
         * \return std::string "sum"
         *
         */
        std::string anythingFromSum();
        /** \brief Transform prod token into string
         *
         * \return std::string "prod"
         *
         */
        std::string anythingFromProd();
        /** \brief Trnasform param token into string
         *
         * \return std::string "param"
         *
         */
        std::string anythingFromParam();
        /** \brief Transform set token into string
         *
         * \return std::string "set"
         *
         */
        std::string anythingFromSet();
        /** \brief Transform minimize token into string
         *
         * \return std::string "minimize"
         *
         */
        std::string anythingFromMinimize();
        /** \brief Set the option
         *
         * \param name the option name
         * \param value the option value
         * \return void
         *
         */
        void setOption(std::string name, std::string value);
        ~OptionDriver();
      private:
        alpi::problem::ProblemModel *mod;
        alpiparsertools::driver* drive;
    };

  }
} /* namespace alpiparsertools */

#endif /* OPTIONDRIVER_H_ */
