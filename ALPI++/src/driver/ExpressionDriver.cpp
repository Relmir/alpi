/*
 * ExpressionDriver.cpp
 *
 *  Created on: Feb 4, 2014
 *      Author: Emmanuel BIGEON
 */

#include "ExpressionDriver.h"

//#include <stddef.h>
#include <list>
#include <string>

#include "../exprs/booleans/BooleanAnd.h"
#include "../exprs/booleans/BooleanOr.h"
#include "../exprs/booleans/Equal.h"
#include "../exprs/booleans/GreaterEqual.h"
#include "../exprs/booleans/GreaterThan.h"
#include "../exprs/booleans/LowerEqual.h"
#include "../exprs/booleans/LowerThan.h"
#include "../exprs/indexing/IndexingMultiRelation.h"
#include "../exprs/operations/Addition.h"
#include "../exprs/operations/Difference.h"
#include "../exprs/operations/Division.h"
#include "../exprs/operations/Multiplication.h"
#include "../exprs/operations/Oppose.h"
#include "../exprs/operations/Power.h"
#include "../exprs/operations/ProdOnIndex.h"
#include "../exprs/operations/SumOnIndex.h"
#include "../exprs/operations/CosineFunctionCall.h"
#include "../exprs/operations/SineFunctionCall.h"
#include "../exprs/operations/SqrtFunctionCall.h"
#include "../exprs/QualifiedExpression.h"
#include "../exprs/numerics/RealConstant.h"
#include "../exprs/numerics/RelativeConstant.h"
#include "../exprs/symbol/Symbol.h"
#include "driver.hh"

namespace alpi {
  namespace parser {

    using namespace alpi::exprs::booleans;
    using namespace alpi::exprs::builtin;
    using namespace alpi::exprs;
    using namespace alpi::exprs::operations;
    using namespace alpi::exprs::numerics;

    ExpressionDriver::ExpressionDriver(driver* driv) :
        drive(driv) {
    }

    ExpressionDriver::~ExpressionDriver() {
    }

    BoolOp* ExpressionDriver::booleanAndExpression(BoolOp* lhs,
        BoolOp* rhs) {
      return new BooleanAnd(lhs, rhs);
    }

    BoolOp* ExpressionDriver::booleanOrExpression(BoolOp* lhs,
        BoolOp* rhs) {
      return new alpi::exprs::booleans::BooleanOr(lhs, rhs);
    }

    BoolOp* ExpressionDriver::booleanTestExpression(
        numerics::NumericExpression* lhs, int test,
        numerics::NumericExpression* rhs) {
      switch (test) {
      case -2:
        return new LowerThan(lhs, rhs);
        break;
      case -1:
        return new LowerEqual(lhs, rhs);
        break;
      case 0:
        return new Equal(lhs, rhs);
        break;
      case 1:
        return new GreaterEqual(lhs, rhs);
        break;
      case 2:
        return new GreaterThan(lhs, rhs);
        break;
      default:
        drive->error("Unknown test");
        break;
      }
      return NULL;
    }
    void ExpressionDriver::expressionFromSumBeforeIndexing() {
      drive->raiseContext();
    }

    NumericExpression* ExpressionDriver::expressionFromSum(
        alpi::exprs::indexing::IndexingMultiRelation* rel,
        BoolOp *predicate, NumericExpression* expr) {
      rel->setPredicate(predicate);
      drive->lowerContext();
      return new alpi::exprs::operations::SumOnIndex(rel, expr);
    }

    void ExpressionDriver::expressionFromProdBeforeIndexing() {
      drive->raiseContext();
    }

    NumericExpression* ExpressionDriver::expressionFromProd(
        alpi::exprs::indexing::IndexingMultiRelation* rel,
        BoolOp *predicate, NumericExpression* expr) {
      rel->setPredicate(predicate);
      drive->lowerContext();
      return new alpi::exprs::operations::ProdOnIndex(rel, expr);
    }

    NumericExpression* ExpressionDriver::expressionFromAddition(
        NumericExpression* leftTerm, NumericExpression* rightTerm) {
      return new Addition(leftTerm, rightTerm);
    }

    NumericExpression* ExpressionDriver::expressionFromDifference(
        NumericExpression* left, NumericExpression* right) {
      return new Difference(left, right);
    }

    NumericExpression* ExpressionDriver::expressionFromMultiplication(
        NumericExpression* leftFactor,
        NumericExpression* rightFactor) {
      return new Multiplication(leftFactor, rightFactor);
    }

    NumericExpression* ExpressionDriver::expressionFromDivision(
        NumericExpression* numerator,
        NumericExpression* denominator) {
      return new Division(numerator, denominator);
    }

    NumericExpression* ExpressionDriver::expressionFromPower(
        NumericExpression* value, NumericExpression* exponent) {
      return new Power(value, exponent);
    }
    NumericExpression* ExpressionDriver::expressionFromIdentifier(
        std::string name, Qualifier* qualifier) {
      Symbol *s = drive->getSymbol(name);
      if (s != NULL) {
        NumericExpression* ne = dynamic_cast<NumericExpression*>(s);
        if (ne != NULL) {
          if (qualifier == NULL) {
            if (s->isVector()) {
              drive->error(
                  "Expression can only use reals, not vectors ("
                      + s->toString() + " is one)");
            }
            return ne;
          }
          return new QualifiedExpression<NumericExpression>(ne, qualifier);
        } else {
          drive->error("Not a numeric value: " + name);
          return NULL;
        }
      }
      drive->error("Unknown identifier " + name);
      return NULL;

    }

    NumericExpression* ExpressionDriver::expressionFromConstant(
        RealConstant *value) {
      return value;
    }

    NumericExpression* ExpressionDriver::expressionFromConstant(
        RelativeConstant*value) {
      return value;
    }

    NumericExpression* ExpressionDriver::expressionFromOppose(
        NumericExpression* inner) {
      return new Oppose(inner);
    }

    NumericExpression* ExpressionDriver::expressionFromFunctionCall(
        std::string name, std::list<NumericExpression*> args) {
      if (name == "cos") {
        if (args.size() != 1) {
          drive->error(
              "Cosine function needs and only accepts one argument !");
        }
        return new CosineFunctionCall(args.front());
      } else if (name == "sin") {
        if (args.size() != 1) {
          drive->error(
              "Sine function needs and only accepts one argument !");
        }
        return new SineFunctionCall(args.front());
      } else if (name == "sqrt") {
        if (args.size() != 1) {
          drive->error(
              "Square root function needs and only accepts one argument !");
        }
        return new SqrtFunctionCall(args.front());
      }
      drive->error("Unrecognized function " + name);
      return NULL;
    }
    Qualifier* ExpressionDriver::identQualifFromEmpty() {
      return NULL;
    }

    Qualifier* ExpressionDriver::identQualifFromArgs(
        std::list<NumericExpression*> args) {
      return new Qualifier(args);
    }

    std::list<NumericExpression*> ExpressionDriver::listExpressionFromUnit(
        NumericExpression* node) {
      std::list<NumericExpression*> l = std::list<NumericExpression*>();
      l.push_front(node);
      return l;
    }

    std::list<NumericExpression*> ExpressionDriver::listExpressionFromList(
        std::list<NumericExpression*> list, NumericExpression* node) {
      list.push_back(node);
      return list;
    }

  } /* namespace parser */
} /* namespace alpi */
