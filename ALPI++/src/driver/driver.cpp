/*
 * driver.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#include "driver.hh"
#include "../exprs/Expressions.h"
#include "../exprs/symbol/DecisionVariable.h"

namespace alpiparsertools {

  using namespace std;
  using namespace alpi::exprs::builtin;
  using namespace alpi::problem;
  using namespace alpi::parsertools;
  using namespace alpi::exprs::operations;

  Index defolt = Index();
  Symbol* inDefinition;

  driver::driver(ProblemModel* mod) :
      trace_scanning(false), trace_parsing(false), fatal(false),
          symbolTable(new SymbolTable(NULL)), num(this), ind(this),
          ctr(this), obj(this), opts(mod, this), par(this),
          expr(this), model(mod) {
  }

  driver::~driver() {
  }

  void driver::makeProblem() {
    std::list<Symbol*> syms = symbolTable->getSymbols();
    for (list<Symbol*>::iterator it = syms.begin(); it != syms.end();
        ++it) {
      DecisionVariable* dv = dynamic_cast<DecisionVariable*>(*it);
      if (dv != NULL) {
        model->addDecisionVariable(dv);
        continue;
      }
      ParamVariable* pv = dynamic_cast<ParamVariable*>(*it);
      if (pv != NULL) {
        if (pv->getExpression()->isConstant()) {
          model->addConstant(new Constant(pv));
        } else if (pv->getExpression()->isIndependant()) {
          model->addConstantFunction(new Function(pv));
        } else {
          model->addAuxiliaryFunction(new Function(pv));
        }

      }
    }
  }

  int driver::parse(const std::string& f) {
    file = f;
    scan_begin();
    yy::alpiparser parser(*this);
    parser.set_debug_level(trace_parsing);
    int res = parser.parse();
    if (symbolTable->getParent() != NULL) {
      error("Not at root context when finishing parsing !!!");
    }
    makeProblem();
    scan_end();
    return res;
  }

  void driver::error(const yy::location& loc, const std::string& m) {
    fatal = true;
    std::cerr << "[ ERROR ] " << "[" << loc << "]: " << m
        << std::endl;
  }

  void driver::error(const std::string& m) {
    fatal = true;
    std::cerr << "[ ERROR ] " << m << std::endl;
  }

  void driver::warning(const yy::location& loc,
      const std::string& m) {
    std::cerr << "[WARNING] " << "[" << loc << "]: " << m
        << std::endl;
  }

  void driver::warning(const std::string& m) {
    std::cerr << "[WARNING] " << m << std::endl;
  }

  // Registered elements
  ProblemModel* driver::getModel() {
    return model;
  }

  template<class T>
  Variable<T>* driver::getVariable(std::string name) {
    Symbol*s = symbolTable->searchSymbol(name);
    if (s != NULL) {
      Variable<T>* r = dynamic_cast<Variable<T>*>(s);
      if (r != NULL) {
        return r;
      } else {
        error(name + " isn't defined as a variable.");
        return NULL;
      }
    }
    error(name + " isn't defined.");
    return NULL;
  }

  Symbol* driver::getSymbol(std::string name) {
    return symbolTable->searchSymbol(name);
  }

  IndexVariable* driver::getIndexVariable(std::string name) {
    Symbol*s = symbolTable->searchSymbol(name);
    if (s != NULL) {
      IndexVariable* r = dynamic_cast<IndexVariable*>(s);
      if (r != NULL) {
        return r;
      } else {
        error(name + " isn't defined as a variable.");
        return NULL;
      }
    }
    error(name + " isn't defined.");
    return NULL;
  }

  ParamVariable* driver::getParamVariable(std::string name) {
    Symbol*s = symbolTable->searchSymbol(name);
    if (s != NULL) {
      ParamVariable* r = dynamic_cast<ParamVariable*>(s);
      if (r != NULL) {
        return r;
      } else {
        error(name + " isn't defined as a variable.");
        return NULL;
      }
    }
    error(name + " isn't defined.");
    return NULL;
  }
  alpi::exprs::indexing::Index* driver::getIndex(std::string name) {
    Symbol*s = symbolTable->searchSymbol(name);
    if (s != NULL) {
      Index* r = dynamic_cast<Index*>(s);
      if (r != NULL) {
        return r;
      } else {
        error(name + " isn't defined as a set.");
        return NULL;
      }
    }
    error(name + " isn't defined.");
    return NULL;
  }
  int driver::addSymbolToTable(Symbol* s) {
    return symbolTable->addSymbol(s);
  }

  void driver::lowerContext() {
    symbolTable = symbolTable->getParent();
    if (symbolTable == NULL) {
      cerr << "No Context !!!" << endl;
    }
  }

  void driver::raiseContext() {
    symbolTable = new SymbolTable(symbolTable);
  }

  std::string driver::identFromIdent(std::string name) {
    return name;
  }

  NumericsDriver* driver::numerics() {
    return &num;
  }

  ObjectiveDriver* driver::objective() {
    return &obj;
  }
  ConstraintDriver* driver::constraints() {
    return &ctr;
  }
  OptionDriver* driver::options() {
    return &opts;
  }
  ParamDriver* driver::params() {
    return &par;
  }
  // Indexes
  IndexDriver* driver::indexing() {
    return &ind;
  }

  ExpressionDriver* driver::expressions() {
    return &expr;
  }
  // Set
  void driver::setDefAfterIdent(std::string name) {
    Index* ind = new Index(name);
    int res = symbolTable->addSymbol(ind);
    inDefinition = ind;
    if (res == 1) {
      error(name + " is already defined.");
      return;
    }
    if (res == 2) {
      warning(name + " is already defined in an other context.");
    }
  }

  void driver::setDefAfterIndexing(std::string name,
      IndexingMultiRelation *index) {
    // TODO (emmanuel) set indexind to index....
  }

  void driver::setDefAttrAffect(Index *index) {
    ((Index*) inDefinition)->setDefinition(index);
    free(index);
    inDefinition = NULL;
  }

  void driver::variableCreationBeforeIndexing(std::string name) {
    DecisionVariable* v = new DecisionVariable(name);
    symbolTable->addSymbol(v);
    inDefinition = v;
    symbolTable = new SymbolTable(symbolTable);
  }

  void driver::variableCreation(std::string name,
      IndexingMultiRelation* index) {
    ((DecisionVariable*) symbolTable->getParent()->searchSymbol(name))
        ->setIndex(index);
  }

  void driver::variableCreationEnd() {
    symbolTable = symbolTable->getParent();
    inDefinition = NULL;
  }

  void driver::variableUpperBound(NumericExpression* node) {
    ((DecisionVariable*) inDefinition)->setUpperBound(node);
  }

  void driver::variableLowerBound(NumericExpression *node) {
    ((DecisionVariable*) inDefinition)->setLowerBound(node);
  }

  void driver::variablePrecision(NumericExpression*node) {
    ((DecisionVariable*) inDefinition)->setPrecision(node);
  }

  void driver::variableSetInteger() {
    ((DecisionVariable*) inDefinition)->setInteger(true);
  }

  // Params
  void driver::defParamBeforeIndexing(std::string name) {
    ParamVariable* param = new ParamVariable(name);
    int res = symbolTable->addSymbol(param);
    inDefinition = param;
    raiseContext();
    if (res == 1) {
      error(name + " is already defined.");
      return;
    }
    if (res == 2) {
      warning(name + " is already defined in an other context.");
    }
  }

  void driver::defParamAfterIndexing(std::string name,
      IndexingMultiRelation* ind) {
    ((ParamVariable*) inDefinition)->setIndex(ind);
  }

  void driver::defParamExpression(numerics::NumericExpression* expr) {
    // check if integer, and if it is, check the expression
    if (((ParamVariable*) inDefinition)->isInteger()) {
      if (expr->isInteger()) {
        ((ParamVariable*) inDefinition)->setExpression(expr);
      } else {
        error(
            "Trying to set a non integer expression to an integer param ("
                + inDefinition->getName() + ")");
      }
    } else {
      ((ParamVariable*) inDefinition)->setExpression(expr);
    }
  }
  void driver::defParamInteger() {
    ((ParamVariable*) inDefinition)->setInteger(true);
    if (((ParamVariable*) inDefinition)->getExpression() != NULL) {
      if (!((ParamVariable*) inDefinition)->getExpression()->isInteger()) {
        error(
            "Non integer expression defines an integer param ("
                + inDefinition->getName() + ")");
      }
    }
  }
  void driver::defParamEnd() {
    if (((ParamVariable*) inDefinition)->getExpression() == NULL) {
      ((ParamVariable*) inDefinition)->setExpression(
          new RelativeConstant(0));
    }
    lowerContext();
    inDefinition = NULL;
  }
} /* namespace alpiparsertools */
