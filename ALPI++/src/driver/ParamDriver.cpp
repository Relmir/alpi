/*
 * ParamDriver.cpp
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

#include "ParamDriver.h"

//#include <iostream>
#include <iterator>
//#include <list>
#include <vector>

#include "../exprs/AssignmentMap.h"
#include "../exprs/booleans/BooleanAnd.h"
#include "../exprs/indexing/Index.h"
#include "../exprs/indexing/IndexingMultiRelation.h"
#include "../exprs/symbol/IndexVariable.h"
#include "../exprs/symbol/ParamVariable.h"
#include "driver.hh"

namespace alpi {
  namespace parser {

    using namespace alpiparsertools;
    ParamDriver::ParamDriver(driver* driv) :
        drive(driv) {
    }

    void ParamDriver::enterSimpleAffectation(std::string name) {
      drive->raiseContext();
    }

    void ParamDriver::simpleAffectation(std::string name,
        IndexingMultiRelation* mask,
        numerics::NumericExpression* value) {
      // take the symbol affected by name
      ParamVariable* p = drive->getParamVariable(name);
      if (p->isInteger() && !(value->isInteger())) {
        drive->error(
            "Trying to set a non integer value to an integer (" + name
                + ")");
        return;
      }
      if (p->getIndexing() == NULL && p->getExpression() != NULL) {
        drive->error(
            "Trying to set a value for an already set param (" + name
                + ")");
        return;
      }
      if (mask == NULL) {
        p->setExpression(value);
        return;
      }
      ExpressionNode* def = p->getExpression();
      AssignmentMap<numerics::NumericExpression>* am =
          dynamic_cast<AssignmentMap<numerics::NumericExpression>*>(def);
      if (am == NULL) {
        am = new AssignmentMap<numerics::NumericExpression>(p->getExpression(), p->getIndexing());
        p->setExpression(am);
      }
      am->addAssignement(mask, value);
      drive->lowerContext();
    }
    IndexingMultiRelation* ParamDriver::identDefQualifFromEmpty() {
      return NULL;
    }
    IndexingMultiRelation* ParamDriver::identDefQualifFromList(
        IndexingMultiRelation* list) {
      return list;
    }
    IndexingMultiRelation* ParamDriver::identDefQualifParamsFromUnit(
        IndexingMultiRelation* unit) {
      return unit;
    }
    IndexingMultiRelation* ParamDriver::identDefQualifParamsFromNext(
        IndexingMultiRelation* curr, IndexingMultiRelation* next) {
      std::vector<IndexVariable*> vl = next->getIndexingVariables();
      for (std::vector<IndexVariable*>::iterator it = vl.begin();
          it != vl.end(); ++it) {
        curr->addRelation(*it);
      }
      if (curr->getPredicate() == NULL) {
        curr->setPredicate(next->getPredicate());
      } else if (next->getPredicate() != NULL) {
        curr->setPredicate(
            new alpi::exprs::booleans::BooleanAnd(
                curr->getPredicate(), next->getPredicate()));
      }
      return curr;
    }
    IndexingMultiRelation* ParamDriver::identDefQualifParamFromRelative(
        numerics::NumericExpression* rel) {
      IndexingMultiRelation* relation = new IndexingMultiRelation();
      IndexVariable* v = new IndexVariable("");
      if (!rel->isInteger() || rel->isVector()) {
        drive->error(rel->toString() + " isn't defining an integer!");
      }
      v->setOverIndex(new Index(rel, rel));
      relation->addRelation(v);
      return relation;
    }
    IndexingMultiRelation* ParamDriver::identDefQualifParamFromIndexing(
        IndexVariable* index, BoolOp* predicate) {
      IndexingMultiRelation* relation = new IndexingMultiRelation();
      relation->addRelation(index);
      relation->setPredicate(predicate);
      return relation;
    }
    ParamDriver::~ParamDriver() {
    }

  } /* namespace alpiparsertools */
}
