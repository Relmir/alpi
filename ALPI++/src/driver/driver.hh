/*
 * driver.hh
 *
 *  Created on: Jan 9, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef DRIVER_HH_
#define DRIVER_HH_

#include <string>
#include <map>
#include <list>
#include <iostream>
#include "../yy_src/alpi.tab.hh"
#include "../alpitypes/SymbolTable.h"
#include "../exprs/symbol/Variable.h"
#include "../exprs/numerics/NumericExpression.h"
#include "../exprs/Qualifier.h"
#include "NumericsDriver.h"
#include "IndexDriver.h"
#include "ConstraintDriver.h"
#include "ObjectiveDriver.h"
#include "OptionDriver.h"
#include "ParamDriver.h"
#include "ExpressionDriver.h"
#include "../problem/ProblemModel.h"

#define YY_DECL \
    yy::alpiparser::symbol_type yylex(alpiparsertools::driver& driver)
YY_DECL;

namespace yy {
  class alpiparser;
}

namespace alpiparsertools {

  using namespace alpi::parser;

  /** \brief The driver of the parsing
   */
  class driver {
    /** \brief Finalize the construction of the problem
     *
     * \return void
     *
     */
    void makeProblem();
  public:
    /** \brief Create a driver
     *
     * \param mod the problem model to write to
     *
     */
    driver(alpi::problem::ProblemModel *mod);
    virtual ~driver();

    /** \brief Is the scanning trace kept (for debug of the input file)
     */
    bool trace_scanning;
    /** \brief Is the parsing trace kept (for debug of the input file)
     */
    bool trace_parsing;
    /**
     * \brief has the parsing been done smoothly ?
     */
    bool fatal;

    /** \brief Initialization of the parsing environment
     *
     * \return void
     *
     */
    void scan_begin();
    /** \brief Finalizing the parsing environment before destruction
     *
     * \return void
     *
     */
    void scan_end();

    /** \brief Parse a file
     *
     * \param f const std::string& path to the file
     * \return int if the parsing went right
     *
     */
    int parse(const std::string &f);
    /** \brief The file path name
     */
    std::string file;

    /** \brief Prints an error message (and flag it)
     *
     * \param loc the location to attach to the message
     * \param m the message
     * \return void
     *
     */
    void error(const yy::location &loc, const std::string &m);
    /** \brief Prints an error message (and flag it)
     *
     * \param m the message
     * \return void
     *
     */
    void error(const std::string &m);
    /** \brief Prints a warning message
     *
     * \param loc the loation to attach to the message
     * \param m the message
     * \return void
     *
     */
    void warning(const yy::location &loc, const std::string &m);
    /** \brief Prints a warning message
     *
     * \param m the message
     * \return void
     *
     */
    void warning(const std::string &m);

    // Registered elements
    /** \brief Get the problem model
     *
     * \return alpi::problem::ProblemModel* the problem model
     *
     */
    alpi::problem::ProblemModel *getModel();
    /** \brief Searches for a given variable in the symbol table
     *
     * \param name the name of the variable
     * \return alpi::exprs::symbol::Variable* a pointer to the variable
     *
     */
    template<class T>
    alpi::exprs::symbol::Variable<T> *getVariable(std::string name);
    /** \brief Searches for a given symbol in the symbol table
     *
     * \param name the name of the symbol
     * \return alpi::exprs::symbol::Symbol* a pointer to the symbol
     *
     */
    alpi::exprs::symbol::Symbol*getSymbol(std::string name);
    /** \brief Search for a given index variable in the symbol table
     *
     * \param name the name of the variable
     * \return alpi::exprs::symbol::IndexVariable* a pointer to the
     *      IndexVariable concerned
     */
    alpi::exprs::symbol::IndexVariable *getIndexVariable(
      std::string name);
    /** \brief Get the parameter variable from the symbol table
     *
     * \param name the name of the parameter
     * \return alpi::exprs::symbol::ParamVariable* the parameter
     *      variable
     */
    alpi::exprs::symbol::ParamVariable *getParamVariable(
      std::string name);
    /** \brief Get the index registered in the symbol table.
     *
     * \param name std::string the registration name
     * \return alpi::exprs::indexing::Index* the index
     *
     */
    alpi::exprs::indexing::Index *getIndex(std::string name);

    /** \brief Add a symbol to the symbol table
     *
     * \param s alpi::exprs::symbol::Symbol* the symbol to add
     * \return int the status of the addition
     *
     */
    int addSymbolToTable(alpi::exprs::symbol::Symbol *s);

    /** \brief get beck to outer context
     */
    void lowerContext();
    /** \brief create a new inner context and enter it
     */
    void raiseContext();
    // Numerics
    /** \brief Get the numerics related driver
     *
     * \return NumericsDriver* the numeric driver
     *
     */
    NumericsDriver *numerics();

    // Tools
    /** \brief Handle the rule "%Ident = %IDENT"
     *
     * \param name std::string the text of %IDENT
     * \return std::string the input string
     *
     */
    std::string identFromIdent(std::string name);

    // Indexes
    /** \brief Get the Index related element Driver
     *
     * \return IndexDriver* the index driver
     *
     */
    IndexDriver *indexing();

    // Set
    // TODO (emmanuel) Move to IndexDriver
    void setDefAfterIdent(std::string name);
    void setDefAfterIndexing(std::string name,
                             alpi::exprs::indexing::IndexingMultiRelation *index);
    void setDefAttrAffect(alpi::exprs::indexing::Index *index);

    // Variables
    // TODO (emmanuel) Move to VariableDriver
    void variableCreationBeforeIndexing(std::string name);
    void variableCreation(std::string name,
                          alpi::exprs::indexing::IndexingMultiRelation *index);
    void variableCreationEnd();

    void variableUpperBound(alpi::exprs::numerics::NumericExpression *node);
    void variableLowerBound(alpi::exprs::numerics::NumericExpression *node);
    void variablePrecision(alpi::exprs::numerics::NumericExpression *node);
    void variableSetInteger();

    // Constraints
    /** \brief Get the constraint driver
     *
     * \return ConstraintDriver* the constraint driver
     *
     */
    ConstraintDriver *constraints();

    // Objective
    /** \brief Get the objective driver
     *
     * \return ObjectiveDriver* the objective driver
     *
     */
    ObjectiveDriver *objective();

    // Params
    /** \brief Get the param driver
     *
     * \return ParamDriver* the param driver
     *
     */
    ParamDriver *params();

    /**
     * \brief Get the driver for expressions
     *
     * \return ExpressionDriver* the expression driver
     */
    ExpressionDriver* expressions();

    // TODO (emmanuel) Move to param driver
    void defParamBeforeIndexing(std::string name);
    void defParamAfterIndexing(std::string name,
                               alpi::exprs::indexing::IndexingMultiRelation *ind);
    void defParamExpression(alpi::exprs::numerics::NumericExpression *expr);
    void defParamInteger();
    void defParamEnd();
    // Options
    /** \brief Get the option driver
     *
     * \return OptionDriver* the option driver
     *
     */
    OptionDriver *options();
  private:
    alpi::parsertools::SymbolTable *symbolTable;
    NumericsDriver num;
    IndexDriver ind;
    ConstraintDriver ctr;
    ObjectiveDriver obj;
    OptionDriver opts;
    ParamDriver par;
    ExpressionDriver expr;
    alpi::problem::ProblemModel *model;
  };

} /* namespace alpiparsertools */

#endif /* DRIVER_HH_ */
