/*
 * NumericsDriver.cpp
 *
 *  Created on: Jan 15, 2014
 *      Author: Emmanuel BIGEON
 */

#include "NumericsDriver.h"
#include "driver.hh"
#include "../exprs/operations/Oppose.h"

namespace alpi {
  namespace parser {

    using namespace alpi::exprs::operations;

    NumericsDriver::NumericsDriver(driver* driv) :drive(driv) {
    }

    NumericsDriver::~NumericsDriver() {
    }

    NumericExpression* NumericsDriver::relativeFromCst(
        RelativeConstant* cst) {
      return cst;
    }
    RealConstant* NumericsDriver::cstRealFromCstRelative(
        RelativeConstant* cst) {
      return new RealConstant(cst->toInt());
    }
    NumericExpression* NumericsDriver::realFromCst(
        RealConstant* cst) {
      return cst;
    }

    NumericExpression* NumericsDriver::relativeFromVariable(std::string name) {
      Symbol *s = drive->getSymbol(name);
      if (s!=NULL) {
        IndexVariable* iv = dynamic_cast<IndexVariable*>(s);
        if (iv != NULL) {
          return iv;
        }
        ParamVariable* pv = dynamic_cast<ParamVariable*>(s);
        if (pv != NULL && !pv->isVector() && pv->isInteger()) {
          return pv;
        }
        DecisionVariable* dv = dynamic_cast<DecisionVariable*>(s);
        if (dv != NULL && !dv->isVector() && dv->isInteger()) {
          return dv;
        }
        drive->error("Symbol is not a numeric integer variable: "+name);
        return NULL;
      }
      drive->error("Undefined symbol: "+name);
      return NULL;
    }
    NumericExpression* NumericsDriver::relativeFromOpposeVariable(std::string name) {
        Symbol *s = drive->getSymbol(name);
        if (s!=NULL) {
          IndexVariable* iv = dynamic_cast<IndexVariable*>(s);
          if (iv != NULL) {
            return new Oppose(iv);
          }
          ParamVariable* pv = dynamic_cast<ParamVariable*>(s);
          if (pv != NULL && !pv->isVector() && pv->isInteger()) {
            return new Oppose(pv);
          }
          DecisionVariable* dv = dynamic_cast<DecisionVariable*>(s);
          if (dv != NULL && !dv->isVector() && dv->isInteger()) {
            return new Oppose(dv);
          }
          drive->error("Symbol is not a numeric integer variable: "+name);
          return NULL;
        }
        drive->error("Undefined symbol: "+name);
        return NULL;
    }
    NumericExpression* NumericsDriver::realFromVariable(std::string name) {
      Symbol *s = drive->getSymbol(name);
      if (s!=NULL) {
        IndexVariable* iv = dynamic_cast<IndexVariable*>(s);
        if (iv != NULL) {
          return iv;
        }
        ParamVariable* pv = dynamic_cast<ParamVariable*>(s);
        if (pv != NULL && !pv->isVector()) {
          return pv;
        }
        DecisionVariable* dv = dynamic_cast<DecisionVariable*>(s);
        if (dv != NULL && !dv->isVector()) {
          return dv;
        }
        drive->error("Symbol is not a numeric variable: "+name);
        return NULL;
      }
      drive->error("Undefined symbol: "+name);
      return NULL;
    }
    NumericExpression* NumericsDriver::realFromOpposeVariable(
        std::string name) {
      Symbol *s = drive->getSymbol(name);
      if (s!=NULL) {
        IndexVariable* iv = dynamic_cast<IndexVariable*>(s);
        if (iv != NULL) {
          return new Oppose(iv);
        }
        ParamVariable* pv = dynamic_cast<ParamVariable*>(s);
        if (pv != NULL && !pv->isVector()) {
          return new Oppose(pv);
        }
        DecisionVariable* dv = dynamic_cast<DecisionVariable*>(s);
        if (dv != NULL && !dv->isVector()) {
          return new Oppose(dv);
        }
        drive->error("Symbol is not a numeric variable: "+name);
        return NULL;
      }
      drive->error("Undefined symbol: "+name);
      return NULL;
    }

  }
} /* namespace alpiparsertools */
