/*
 * IndexDriver.h
 *
 *  Created on: Jan 20, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef INDEXDRIVER_H_
#define INDEXDRIVER_H_

#include <string>

//#include "../exprs/booleans/BoolOp.h"
//#include "../exprs/indexing/Index.h"
//#include "../exprs/indexing/IndexingMultiRelation.h"

namespace alpi {
  namespace exprs {
    namespace booleans {
      class BoolOp;
    } /* namespace booleans */
    namespace indexing {
      class Index;
      class IndexingMultiRelation;
    } /* namespace indexing */
    namespace symbol {
      class IndexVariable;
    } /* namespace symbol */
    namespace numerics {
      class NumericExpression;
    }
  } /* namespace exprs */
} /* namespace alpi */

namespace alpiparsertools {
  class driver;
}  // namespace alpiparsertools

namespace alpi {
  namespace parser {
    using namespace alpi::exprs::indexing;
    using namespace alpi::exprs;
    using namespace alpi::exprs::symbol;

    /** \brief The driver for index related rules
     */
    class IndexDriver {
      public:
        /** \brief Create the driver
         *
         * \param owner alpiparsertools::driver* the global driver
         *
         */
        IndexDriver(alpiparsertools::driver* owner);
        /** \brief The rule "%IndexingOpt : %empty"
         *
         * \return IndexingMultiRelation* an empty relation, or null
         *
         */
        IndexingMultiRelation* indexingOptEmpty();
        /** \brief The rule "%IndexingOpt : %IndexingList %Reducer"
         *
         * \param rel the resulting indexing from the indexing list
         * \param reducer the predicate that will reduce the indexing
         *      set
         * \return IndexingMultiRelation* the set of relations
         *      associated with the predicate
         *
         */
        IndexingMultiRelation* indexingOptFromSetExpr(
            IndexingMultiRelation* rel, booleans::BoolOp* reducer);
        /** \brief The rule "%IndexingList : %Indexing"
         *
         * \param rel the indexing variable
         * \return IndexingMultiRelation* a relation made of one
         *      variable
         *
         */
        IndexingMultiRelation* indexingListFromOne(
            IndexVariable* rel);
        /** \brief The rule "%IndexingList : %IndexingList %COMMA
         *      %Indexing"
         *
         * \param list the result of the previous indexings
         * \param next the next relation to add
         * \return IndexingMultiRelation* the concatenated list of
         *      relations
         */
        IndexingMultiRelation* indexingListFromNext(
            IndexingMultiRelation* list, IndexVariable* next);

        /** \brief The rule "%Indexing : %Ident %inIndexOpt"
         *
         * \param name the name of the variable or of the index
         * \param i an index for the variable or null
         * \return IndexVariable* the resulting index variable
         *
         */
        IndexVariable* indexingFromIdentOptInIndex(std::string name,
            Index* i);
        /** \brief Create an indexing variable over an anonymous index
         *
         * \param i alpi::exprs::indexing::Index* the index
         * \return IndexVariable* the variable
         *
         */
        IndexVariable* indexingFromDef(
            alpi::exprs::indexing::Index* i);
        /** \brief The rule "%InIndexOpt : %Index"
         *
         * \param i alpi::exprs::indexing::Index* the index
         * \return alpi::exprs::indexing::Index* the index
         *
         */
        alpi::exprs::indexing::Index* inIndexFromIndex(
            alpi::exprs::indexing::Index* i);
        /** \brief The rule "%InIndexOpt : %empty"
         *
         * \return alpi::exprs::indexing::Index* null
         *
         */
        alpi::exprs::indexing::Index* inIndexFromEmpty();

        /** \brief Create the reducer expression
         *
         * \param test the test expression
         * \return numerics::NumericExpression* the reducer
         *
         */
        booleans::BoolOp* reducerFromTest(booleans::BoolOp* test);
        /** \brief Create the reducer expression
         *
         * \return numerics::NumericExpression* null
         *
         */
        booleans::BoolOp* reducerFromEmpty();

        /** \brief Create an index from an anonymous definition
         *
         * \param i Index* the anonymous index
         * \return Index* the index
         *
         */
        Index* indexFromDef(Index* i);
        /** \brief Define an anonymous index
         *
         * \param start the starting point
         * \param stop the stopping point
         * \param step the step value
         * \return Index* the index
         *
         */
        Index* indexDef(numerics::NumericExpression* start, numerics::NumericExpression* stop,
            numerics::NumericExpression* step);
        /** \brief Create an expression out of nothing
         *
         * \return numerics::NumericExpression* null
         *
         */
        numerics::NumericExpression* indexDefByStepEmpty();
        virtual ~IndexDriver();
      private:
        alpiparsertools::driver* drive;
    };

  }
} /* namespace alpiparsertools */

#endif /* INDEXDRIVER_H_ */
