/*
 * ExpressionDriver.h
 *
 *  Created on: Feb 4, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef EXPRESSIONDRIVER_H_
#define EXPRESSIONDRIVER_H_

//#include <list>

#include <string>

#include "../exprs/numerics/NumericExpression.h"

namespace alpi {
  namespace exprs {
    class Qualifier;
    namespace numerics {
      class RealConstant;
      class RelativeConstant;
      class NumericExpression;
    } /* namespace numerics */
    namespace booleans {
      class BoolOp;
    } /* namespace booleans */
    namespace indexing {
      class IndexingMultiRelation;
    } /* namespace indexing */
  } /* namespace exprs */
} /* namespace alpi */

namespace alpiparsertools {
  class driver;
}  // namespace alpiparsertools

namespace alpi {
  namespace parser {

    using namespace alpi::exprs::booleans;
    using namespace alpi::exprs;
    using namespace alpiparsertools;

    class ExpressionDriver {
        driver* drive;
      public:
        /**
         * \brief Create a driver for expression related rules.
         */
        ExpressionDriver(driver* driv);
        /**
         * \brief Create the and boolean operation.
         *
         * \param lhs left hand side of the and
         * \param rhs right hand side of the and
         * \return the and operation representation
         */
        BoolOp* booleanAndExpression(BoolOp* lhs, BoolOp* rhs);
        /**
         * \brief Create the or boolean operation.
         *
         * \param lhs left hand side of the or
         * \param rhs right hand side of the or
         * \return the or operation representation
         */
        BoolOp* booleanOrExpression(BoolOp* lhs, BoolOp* rhs);
        /**
         * \brief Create a comparison operation.
         *
         * \param lhs left hand side of the test
         * \param test the comparator
         * \param rhs right hand side of the test
         * \return the and operation representation
         */
        BoolOp* booleanTestExpression(
            numerics::NumericExpression* lhs, int test,
            numerics::NumericExpression* rhs);
        void expressionFromSumBeforeIndexing();
        alpi::exprs::numerics::NumericExpression *expressionFromSum(
            alpi::exprs::indexing::IndexingMultiRelation* rel,
            BoolOp *predicate,
            alpi::exprs::numerics::NumericExpression *expr);
        void expressionFromProdBeforeIndexing();
        alpi::exprs::numerics::NumericExpression *expressionFromProd(
            alpi::exprs::indexing::IndexingMultiRelation* rel,
            BoolOp *predicate,
            alpi::exprs::numerics::NumericExpression *expr);
        alpi::exprs::numerics::NumericExpression *expressionFromAddition(
            alpi::exprs::numerics::NumericExpression *leftTerm,
            alpi::exprs::numerics::NumericExpression *rightTerm);
        alpi::exprs::numerics::NumericExpression *expressionFromDifference(
            alpi::exprs::numerics::NumericExpression *left,
            alpi::exprs::numerics::NumericExpression *right);
        alpi::exprs::numerics::NumericExpression *expressionFromMultiplication(
            alpi::exprs::numerics::NumericExpression *leftFactor,
            alpi::exprs::numerics::NumericExpression *rightFactor);
        alpi::exprs::numerics::NumericExpression *expressionFromDivision(
            alpi::exprs::numerics::NumericExpression *numerator,
            alpi::exprs::numerics::NumericExpression *denominator);
        alpi::exprs::numerics::NumericExpression *expressionFromPower(
            alpi::exprs::numerics::NumericExpression *value,
            alpi::exprs::numerics::NumericExpression *exponent);
        alpi::exprs::numerics::NumericExpression *expressionFromIdentifier(
            std::string name, alpi::exprs::Qualifier *qualifier);
        alpi::exprs::numerics::NumericExpression *expressionFromConstant(
            alpi::exprs::numerics::RealConstant *value);
        alpi::exprs::numerics::NumericExpression *expressionFromConstant(
            alpi::exprs::numerics::RelativeConstant *value);
        alpi::exprs::numerics::NumericExpression *expressionFromOppose(
            alpi::exprs::numerics::NumericExpression *inner);
        alpi::exprs::numerics::NumericExpression *expressionFromFunctionCall(
            std::string name,
            std::list<alpi::exprs::numerics::NumericExpression*> args);
        alpi::exprs::Qualifier *identQualifFromEmpty();
        alpi::exprs::Qualifier *identQualifFromArgs(
            std::list<alpi::exprs::numerics::NumericExpression *> args);

        std::list<alpi::exprs::numerics::NumericExpression *> listExpressionFromUnit(
            alpi::exprs::numerics::NumericExpression *node);
        std::list<alpi::exprs::numerics::NumericExpression *> listExpressionFromList(
            std::list<alpi::exprs::numerics::NumericExpression *> list,
            alpi::exprs::numerics::NumericExpression *node);
        virtual ~ExpressionDriver();
    };

  } /* namespace parser */
} /* namespace alpi */

#endif /* EXPRESSIONDRIVER_H_ */
