/*
 * OptionDriver.cpp
 *
 *  Created on: Jan 22, 2014
 *      Author: Emmanuel BIGEON
 */

#include "OptionDriver.h"
#include <cstdlib>
#include <iostream>
#include "driver.hh"

namespace alpi {
  namespace parser {

    using namespace alpiparsertools;
    using namespace alpi::problem;

    OptionDriver::OptionDriver(ProblemModel* model, driver* driver) :
        mod(model), drive(driver) {
    }

    OptionDriver::~OptionDriver() {
    }

    std::string OptionDriver::anythingFromConstant(
        RealConstant* real) {
      return real->toString();
    }
    std::string OptionDriver::anythingFromConstant(
        RelativeConstant* real) {
      return real->toString();
    }
    std::string OptionDriver::anythingFromIdent(std::string name) {
      return name;
    }
    std::string OptionDriver::anythingFromFileName(std::string name) {
      return name;
    }
    std::string OptionDriver::anythingFromSum() {
      return "sum";
    }
    std::string OptionDriver::anythingFromProd() {
      return "prod";
    }
    std::string OptionDriver::anythingFromParam() {
      return "param";
    }
    std::string OptionDriver::anythingFromSet() {
      return "set";
    }
    std::string OptionDriver::anythingFromMinimize() {
      return "minimize";
    }

    bool checkReformulationName(std::string reform) {
      return reform == "AFFINE_F" || reform == "AFFINE_RB"
          || reform == "AFFINE2" || reform == "AFFINE3_RB"
          || reform == "rAF2_IA" || reform == "rAF3_IA"
          || reform == "AFFINE3" || reform == "AF3_IA"
          || reform == "AFFINE_S" || reform == "AF2_IA"
          || reform == "AFFINE_Rev";
    }

    void OptionDriver::setOption(std::string name,
        std::string value) {
      // Options keys are:
      if (name == "reformulation") {
        if (value == "RIEN") mod->setReformulation("");
        else if (checkReformulationName(value)) mod->setReformulation(
            value);
        else drive->error("Unknown reformulation " + name);
      } else if (name == "logFile") mod->setOption(name, value);
      else if (name == "logFreq") mod->setOption(name,
          atoi(value.c_str()));
      else if (name == "knownMajor") mod->setOption(name, value);
      else if (name == "timeLimit") mod->setOption(name,
          atoi(value.c_str()));
      else if (name == "memLimit") mod->setOption(name,
          atoi(value.c_str()));
      else if (name == "taylor") mod->setOption(name,
          value == "true");
      else if (name == "constraintsPropagation") mod->setOption(name,
          value == "true");
      else if (name == "CPLEX") mod->setOption(name, value == "true");
      else if (name == "timeInSeconds") mod->setOption(name,
          value == "true");
      else if (name == "keepAll") mod->setOption(name,
          value == "true");
      else if (name == "normalStoppingCriterion") mod->setOption(name,
          value == "true");
      else if (name == "terminalDisplay") mod->setOption(name,
          value == "true");
      else if (name == "notCertifiedLP") mod->setOption(name,
          value == "true");
      else if (name == "exactConstraint") mod->setOption(name,
          value == "true");
      else if (name == "affineArithmeticForConstraints") mod
          ->setOption(name, value == "true");
      else std::cerr << "Unknown option key " << name << std::endl;
    }
  } /* namespace alpiparsertools */
}
