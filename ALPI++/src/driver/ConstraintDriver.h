/*
 * ConstraintDriver.h
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef CONSTRAINTDRIVER_H_
#define CONSTRAINTDRIVER_H_

#include <string>

namespace alpi {
  namespace exprs {
    namespace indexing {
      class IndexingMultiRelation;
    } /* namespace indexing */
    namespace numerics {
      class NumericExpression;
    } /* namespace numerics */
    namespace symbol {
      class ParamVariable;
    } /* namespace symbol */
    namespace numerics {
      class NumericExpression;
    }
  } /* namespace exprs */
} /* namespace alpi */

namespace alpiparsertools {
  class driver;
}  // namespace alpiparsertools

namespace alpi {
  namespace parser {
    using namespace alpi::exprs::indexing;
    using namespace alpi::exprs::symbol;
    using namespace alpi::exprs;
    using namespace alpi::exprs::numerics;

    /** \brief The driver for constraint related elements
     */
    class ConstraintDriver {
    public:
      /** \brief Create a constraint driver
       *
       * \param driver alpiparsertools::driver* the global driver
       *
       */
      ConstraintDriver(alpiparsertools::driver *driver);
      /** \brief Starting the definition (only the name is known)
       *
       * \param name the name of the constraint being defined
       * \return void
       */
      void definitionBeforeIndex(std::string name);
      /** \brief Set the indexing part of this definition
       *
       * \param name the name of the constraint being defined
       * \param index the indexing
       * \return void
       *
       */
      void definitionAfterIndex(std::string name,
                                IndexingMultiRelation *index);
      /** \brief Wraps up things in this definition
       *
       * \param name the name of the constraint being defined
       * \param index the indexing on this definition
       * \param lhs the left-hand side of the constraint
       * \param test the testing operation
       * \param mhs the middle or right-hand side of the constraint
       * \param rhs the right-hand side of the constraint
       * \return void
       */
      void definitionEnd(std::string name, IndexingMultiRelation *index,
                         numerics::NumericExpression *lhs, int test,
                         numerics::NumericExpression *mhs, numerics::NumericExpression *rhs);
      /** \brief Handles the "%EndConstraint: %empty" rule
       *
       * \return NumericExpression* null
       *
       */
      NumericExpression*endConstraintFromEmpty();
      /** \brief Handle the "%EndConstraint: <= %Expression" rule
       *
       * \param node NumericExpression* the expression that defines an
       *      upper bound
       * \return NumericExpression* the upper bound expression
       *
       */
      NumericExpression *endConstraintFromTest(NumericExpression *node);
      /** \brief Set the precision on the constraint being defined
       *
       * \param real NumericExpression* the precision
       * \return void
       *
       */
      void attributePrecision(numerics::NumericExpression *real);
      virtual ~ConstraintDriver();
    private:
      alpiparsertools::driver *driv;
      ParamVariable *curr;
      numerics::NumericExpression *prec;
    };

  }
} /* namespace alpiparsertools */

#endif /* CONSTRAINTDRIVER_H_ */
