/*
 * ParamDriver.h
 *
 *  Created on: Jan 24, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef PARAMDRIVER_H_
#define PARAMDRIVER_H_

#include <string>

namespace alpi {
  namespace exprs {
    namespace indexing {
      class IndexingMultiRelation;
    } /* namespace indexing */
    namespace symbol {
      class IndexVariable;
    } /* namespace symbol */
    namespace booleans {
      class BoolOp;
    } /* namespace booleans */
    namespace numerics {
      class NumericExpression;
    }
  } /* namespace exprs */
} /* namespace alpi */

namespace alpiparsertools {
  class driver;
}  // namespace alpiparsertools
namespace alpi {
  namespace parser {
    using namespace alpi::exprs;
    using namespace alpi::exprs::indexing;
    using namespace alpi::exprs::numerics;

    /** \brief The driver for parameter related rules
     */
    class ParamDriver {
      public:
        /** \brief Create the driver
         *
         * \param driv alpiparsertools::driver* the global driver
         *
         */
        ParamDriver(alpiparsertools::driver* driv);
        void enterSimpleAffectation(std::string name);
        /** \brief Add an assignment for a variable and a mask
         *
         * \param name the name of the variable to change
         * \param mask the mask of the change
         * \param value the value of the definition
         * \return void
         *
         */
        void simpleAffectation(std::string name,
            IndexingMultiRelation* mask, numerics::NumericExpression* value);
        /** \brief The empty qualification
         *
         * \return IndexingMultiRelation* empty mask
         *
         */
        IndexingMultiRelation* identDefQualifFromEmpty();
        /** \brief Transparent operation
         *
         * \param list the mask
         * \return IndexingMultiRelation* the mask
         */
        IndexingMultiRelation* identDefQualifFromList(
            IndexingMultiRelation* list);
        /** \brief Transparent operation
         *
         * \param unit the element
         * \return IndexingMultiRelation* the list so far
         */
        IndexingMultiRelation* identDefQualifParamsFromUnit(
            IndexingMultiRelation* unit);
        /** \brief Assemble indexing relations (order-wise)
         *
         * \param curr the current list
         * \param next the element to append
         * \return IndexingMultiRelation* the resulting list
         *
         */
        IndexingMultiRelation* identDefQualifParamsFromNext(
            IndexingMultiRelation* curr, IndexingMultiRelation* next);
        /** \brief Get indexing relation from constant
         *
         * \param rel the constant
         * \return an indexing reduced to a single element
         *
         */
        IndexingMultiRelation* identDefQualifParamFromRelative(
            numerics::NumericExpression* rel);
        /** \brief Get indexing relation from index and predicate
         *
         * \param index the index for this dimension
         * \param predicate the predicate to reduce index
         * \return IndexingMultiRelation* the resulting relation
         *
         */
        IndexingMultiRelation* identDefQualifParamFromIndexing(
            symbol::IndexVariable* index, booleans::BoolOp* predicate);
        virtual ~ParamDriver();
      private:
        alpiparsertools::driver* drive;
    };

  }
} /* namespace alpiparsertools */

#endif /* PARAMDRIVER_H_ */
