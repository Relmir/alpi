/*
 * alpiwritertools.h
 *
 *  Created on: Jan 17, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIWRITERTOOLS_H_
#define ALPIWRITERTOOLS_H_

#include <set>

#include "../exprs/symbol/DecisionVariable.h"
#include "../exprs/symbol/ParamVariable.h"
#include "../problem/Constraint.h"

namespace alpiwriter {

  using namespace alpi::exprs::symbol;
  using namespace alpi::exprs::numerics;
  using namespace alpi::problem;
  using namespace std;

  /** \brief Conversion of list of DecisionVariable to one of Variable
   * \param vars the DecisionVariables
   * \return the converted list
   */
  list<Variable<NumericExpression>*> decisionVariablesToVariables(
      set<DecisionVariable*> vars);
  /** \brief Conversion of list of DecisionVariable to one of Variable
   * \param vars the DecisionVariables
   * \return the converted list
   */
  list<Variable<NumericExpression>*> decisionVariablesToVariables(
      list<DecisionVariable*> vars);
  /** \brief Get the number of variables as individuals
   * \param list std::list<Variable*> the variables
   * \return int the number of variables
   */
  int getNumberOfVariables(
      std::list<Variable<NumericExpression>*> list);
  /** \brief Get the number of variables as individuals
   * \param list std::list<DecisionVariable*> the variables
   * \return int the number of variables
   */
  int getNumberOfVariables(std::list<DecisionVariable*> list);
  /** \brief Get the number of constraints as individuals
   * \param list std::list<Constraint*> the constraints
   * \return int the number of constraints
   */
  int getNumberOfConstraints(std::list<Constraint*> list);
  /** \brief test if the given mask is accepted by the indexing set
   * \param mask the table of indicies
   * \param the indexing
   * \return if the mask is accepted
   */
  bool acceptedMask(int mask[],
      alpi::exprs::indexing::IndexingMultiRelation* rel);
  /** \brief Construct the mask from the set of indices
   * \param mask the mask
   * \param length the length of the mask
   * \return the string representing the qualification for that mask
   */
  string callMaskFromIndices(int mask[], int length);
  bool increaseMask(int ind[], alpi::exprs::indexing::IndexedElement* elem);
  int* createMask(alpi::exprs::indexing::IndexedElement* pv);
  int countAcceptableMasks(alpi::exprs::indexing::IndexedElement* elem);
  string typeOfVariable(Variable<NumericExpression>* v, string baseType);
}

#endif /* ALPIWRITERTOOLS_H_ */
