/*
 * alpiwritertools.cpp
 *
 *  Created on: Jan 17, 2014
 *      Author: Emmanuel BIGEON
 */

#include "alpiwritertools.h"

#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "../evaluators/evaluator.h"
#include "../exprs/indexing/IndexingMultiRelation.h"
#include "../exprs/numerics/RelativeConstant.h"
#include "../exprs/symbol/DecisionVariable.h"
#include "../exprs/symbol/IndexVariable.h"
#include "../exprs/symbol/Variable.h"
#include "../problem/Constraint.h"

namespace alpiwriter {
  int getNumberOfConstraints(std::list<Constraint*> list) {
    std::list<Constraint*>::iterator it;
    int nbCstrs = 0;
    for (it = list.begin(); it != list.end(); ++it) {
      if ((*it)->isEqualityConstraint()) {
        if ((*it)->function()->isVector()) {
          nbCstrs += countAcceptableMasks(
              (*it)->function()->getParamVariable());
        } else {
          nbCstrs++;
        }
      }
      if ((*it)->defineLowerBound()) {
        if ((*it)->function()->isVector()) {
          nbCstrs += countAcceptableMasks(
              (*it)->function()->getParamVariable());
        } else {
          nbCstrs++;
        }
      }
      if ((*it)->defineUpperBound()) {
        if ((*it)->function()->isVector()) {
          nbCstrs += countAcceptableMasks(
              (*it)->function()->getParamVariable());
        } else {
          nbCstrs++;
        }
      }
    }
    return nbCstrs;
  }

  int getNumberOfVariables(
      std::list<Variable<NumericExpression>*> list) {
    std::list<Variable<NumericExpression>*>::iterator it;
    int nb = 0;
    for (it = list.begin(); it != list.end(); ++it) {
      if ((*it)->isVector()) {
        nb += countAcceptableMasks(*it);
      } else {
        nb++;
      }
    }
    return nb;
  }

  int getNumberOfVariables(std::list<DecisionVariable*> list) {
    std::list<DecisionVariable*>::iterator it;
    int nb = 0;
    for (it = list.begin(); it != list.end(); ++it) {
      if ((*it)->isVector()) {
        nb += countAcceptableMasks(*it);
      } else {
        nb++;
      }
    }
    return nb;
  }

  list<Variable<NumericExpression>*> decisionVariablesToVariables(
      set<DecisionVariable*> vars) {
    list<Variable<NumericExpression>*> vl;
    for (set<DecisionVariable*>::iterator it = vars.begin();
        it != vars.end(); ++it) {
      vl.push_back(*it);
    }
    return vl;
  }
  list<Variable<NumericExpression>*> decisionVariablesToVariables(
      list<DecisionVariable*> vars) {
    list<Variable<NumericExpression>*> vl;
    for (list<DecisionVariable*>::iterator it = vars.begin();
        it != vars.end(); ++it) {
      vl.push_back(*it);
    }
    return vl;
  }

  using namespace alpi::exprs::indexing;
  using namespace alpi::exprs::numerics;
  using namespace alpi::exprs;

  bool acceptedMask(int mask[], IndexingMultiRelation* rel) {
    if (rel->getPredicate() == NULL) return true;
    vector<IndexVariable*> l = rel->getIndexingVariables();
    map<Symbol*, ExpressionNode*> mapping;
    for (unsigned int var = 0; var < l.size(); var++) {
      mapping[l.at(var)] = new RelativeConstant(mask[var]);
    }
    return alpi::eval::evaluateBoolean(rel->getPredicate(), mapping);
  }
  int countAcceptableMasks(IndexedElement* elem) {
    int* inds = createMask(elem);
    int i = 0;
    while (increaseMask(inds, elem)) {
      if (acceptedMask(inds, elem->getIndexing())) {
        i++;
      }
    }
    return i;
  }
  string callMaskFromIndices(int mask[], int length) {
    ostringstream oss;
    oss << "(" << mask[0];
    for (int var = 1; var < length; ++var) {
      oss << ", " << mask[var];
    }
    oss << ")";
    return oss.str();
  }

  /** \brief increase the mask
   *
   * This method tries to get the next mask. If this is the last one,
   * it returns false, and reset the mask to the first one
   * \param ind the mask
   * \param elem the element we are putting the mask on
   * \return if the mask could be changed for the next one
   */
  bool increaseMask(int ind[], IndexedElement* elem) {
    int d = elem->dimension();
    int t = d;
    for (t = d - 1; t >= 0; --t) {
      if (ind[t] == elem->length(t)) {
        ind[t] = 1;
      } else {
        ind[t] = ind[t] + 1;
        break;
      }
    }
    return t != -1;
  }

  int* createMask(IndexedElement* pv) {
    int d = pv->dimension();
    int* indices = new int[d];
    for (int var = 0; var < d; ++var) {
      indices[var] = 1;
    }
    indices[d - 1] = 0;
    return indices;
  }

  string typeOfVariable(Variable<NumericExpression>* v,
      string baseType) {
    if (v->isVector()) {
      std::ostringstream oss;
      oss << baseType << ", dimension(";
      if (v->dimension() == 1) oss << v->length(0);
      else oss << ":";
//      oss << v->length(0);
//      int d = v->dimension();
//      for (int j = 1; j < d; ++j) {
//        oss << ", " << v->length(j);
//      }
      oss << ")";
      return oss.str();
    } else {
      return baseType;
    }
  }
}

