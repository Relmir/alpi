/*
 * alpimakewriter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIMAKEWRITER_H_
#define ALPIMAKEWRITER_H_

#include <iostream>
#include <fstream>
#include "../problem/ProblemModel.h"

namespace alpiwriter {
  /** \brief Create the makefile for the specified problem
   *
   * \param model the problem to solve
   * \param mod_out the stream to write into
   * \return void
   *
   */
  void compileProblemModelToMakeFile(
      alpi::problem::ProblemModel& model, std::ostream& mod_out);
}

#endif /* ALPIMAKEWRITER_H_ */
