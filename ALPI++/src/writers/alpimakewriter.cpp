/*
 * alpimakewriter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpimakewriter.hpp"

namespace alpiwriter {
  void addObjectsForReformulation(std::ostream& file,
      std::string reformulation);
  void addReformulationTarget(std::ostream& file,
      std::string reformulation);
  void addReformulationDependencies(std::ostream& file,
      std::string reformulation);
  void compileProblemModelToMakeFile(
      alpi::problem::ProblemModel& model, std::ostream& mod_out) {
    mod_out << "PROG      = " + model.getName() << std::endl;
    mod_out << std::endl;

    mod_out << "OPTSF90   = -O3 -xia -m64 -r8const" << std::endl;
    mod_out << "OPTS      = " << std::endl;
    mod_out << "L1        = $(CPLEXHOME)/lib/x86-64_sles10_4.1/";
    mod_out << "static_pic/libcplex.a  $(LIB_GCC)/libgcc.a"
        << std::endl;
    mod_out << "I1        = $(CPLEXHOME)/include/ilcplex"
        << std::endl;
    mod_out << "LIBS      = $(L1) -lc -lm -lpthread -lnsl"
        << std::endl;
    mod_out << "INCLUDE   = -I$(I1) -I." << std::endl;
    mod_out << "COMPILE   = $(COMPILATEUR) $(OPTIONS_COMPILATEUR) "
        "$(INCLUDE) -c" << std::endl;
    mod_out << std::endl;
    mod_out << std::endl;

    mod_out << "OBJETS    = cplexcf.o Mod_Interval.o ";
    if (model.useReformulation()) {
      addObjectsForReformulation(mod_out, model.getReformulation());
    }
    mod_out << "Mod_Gest_Minimier.o Mod_Cplex.o "
        << "Mod_Propagation.o Mod_Opt_Glob.o ";
    mod_out << "Mod_" << model.getName() << ".o " << model.getName()
        << ".o";

    mod_out << std::endl;
    mod_out << "COMP.CC   = c++" << std::endl;
    mod_out << "COMP.cc   = cc" << std::endl;
    mod_out << "COMP.F77  = f77" << std::endl;
    mod_out << "COMP.F90  = f90" << std::endl;
    mod_out << std::endl;
    mod_out << ".SUFFIXES: .o .C .c .f90" << std::endl;
    mod_out << std::endl;
    mod_out << "all: $(PROG)" << std::endl;
    mod_out << std::endl;
    mod_out << "$(PROG): $(OBJETS)" << std::endl;
    mod_out << "\t$(COMP.F90) -o $(PROG) $(OBJETS) $(LIBS) $(OPTSF90)"
        << std::endl;
    mod_out << std::endl;
    mod_out << "cplexcf.o: cplexcf.c" << std::endl;
    mod_out << "\t$(COMP.cc) $(OPTS) $(INCLUDE) -c $<" << std::endl;
    mod_out << std::endl;

    if (model.useReformulation()) {
      addReformulationTarget(mod_out, model.getReformulation());
      mod_out << std::endl;
    }

    mod_out << "Mod_" << model.getName() << ".o: Mod_"
        << model.getName() << ".f90 ";
    if (model.useReformulation()) {
      addReformulationDependencies(mod_out, model.getReformulation());
    }
    mod_out << "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90"
        << std::endl;
    mod_out << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
        << std::endl;
    mod_out << std::endl;
    mod_out << "Mod_Cplex.o: Mod_Cplex.f90 Mod_Interval.o"
        << std::endl;
    mod_out << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
        << std::endl;
    mod_out << std::endl;
    mod_out << "Mod_Opt_Glob.o: Mod_Opt_Glob.f90 "
        "Mod_Gest_Minimier.o Mod_Interval.o" << std::endl;
    mod_out << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
        << std::endl;
    mod_out << std::endl;
    mod_out << model.getName() << ".o: " << model.getName()
        << ".f90 Mod_" << model.getName() << ".f90 Mod_"
        << model.getName() << ".o" << std::endl;
    mod_out << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
        << std::endl;
    mod_out << std::endl;
    mod_out << "%.o: %.f90" << std::endl;
    mod_out << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
        << std::endl;
    mod_out << std::endl;
    mod_out << "clean:" << std::endl;
    mod_out << "\trm *.mod *.o" << std::endl;
  }

  void addObjectsForReformulation(std::ostream& mod_out,
      std::string reformulation) {
    if (reformulation == "AFFINE_RB") {
      mod_out << "Mod_Parametres.o Mod_Affine_RB.o ";
    } else if (reformulation == "AFFINE2") {
      mod_out << "Mod_Parametres.o Mod_Affine2.o ";
    } else if (reformulation == "AFFINE3_RB") {
      mod_out << "Mod_Parametres.o Mod_Affine3_RB.o ";
    } else if (reformulation == "rAF2_IA") {
      mod_out << "Mod_Parametres.o Mod_Affine2_RB.o Mod_rAF2_IA.o ";
    } else if (reformulation == "rAF3_IA") {
      mod_out << "Mod_Parametres.o Mod_Affine3_RB.o Mod_rAF3_IA.o ";
    } else if (reformulation == "AFFINE3") {
      mod_out << "Mod_Parametres.o Mod_Affine3.o ";
    } else if (reformulation == "AF3_IA") {
      mod_out << "Mod_Parametres.o Mod_Affine3.o Mod_AF3_IA.o ";
    } else if (reformulation == "AFFINE_S") {
      mod_out << "Mod_Parametres.o Mod_Affine_S.o ";
    } else if (reformulation == "AFFINE_F") {
      mod_out << "Mod_Parametres.o Mod_Affine_F.o ";
    } else if (reformulation == "AF2_IA") {
      mod_out << "Mod_Parametres.o Mod_Affine2.o Mod_AF2_IA.o ";
    } else if (reformulation == "AFFINE_Rev") {
      mod_out << "Mod_Parametres.o Mod_Affine_Rev.o ";
    } else {
      std::cerr << "Unknown reformulation " << reformulation
          << std::endl;
    }
  }

  void addReformulationTarget(std::ostream& file,
      std::string reformulation) {
    int exist = 0;
    if (reformulation == "AFFINE_RB") {
      file << "Mod_Affine_RB.o: Mod_Affine_RB.f90 Mod_Interval.o"
          << " Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      exist++;
    } else if (reformulation == "AFFINE2") {
      file << "Mod_Affine2.o: Mod_Affine2.f90 Mod_Interval.o "
          << "Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      exist++;
    } else if (reformulation == "AFFINE3_RB") {
      file << "Mod_Affine3_RB.o: Mod_Affine3_RB.f90 "
          << "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90"
          << std::endl;
      exist++;
    } else if (reformulation == "rAF2_IA") {
      // Non-common
      file << "Mod_Affine2_RB.o: Mod_Affine2_RB.f90 "
          "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90"
          << std::endl;
      file << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
          << std::endl;
      file << std::endl;
      file << "Mod_rAF2_IA.o: Mod_rAF2_IA.f90 Mod_Interval.o "
          << "Mod_Affine2_RB.o Mod_Parametres.o Mod_Parametres.f90"
          << std::endl;
      exist++;
    } else if (reformulation == "rAF3_IA") {
      // Non-common
      file << "Mod_Affine3_RB.o: Mod_Affine3_RB.f90 Mod_Interval.o "
          "Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      file << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
          << std::endl;
      file << std::endl;
      file << "Mod_rAF3_IA.o: Mod_rAF3_IA.f90 Mod_Affine3_RB.o "
          "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90"
          << std::endl;
      exist++;
    } else if (reformulation == "AFFINE3") {
      file << "Mod_Affine3.o: Mod_Affine3.f90 Mod_Interval.o "
          "Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      exist++;
    } else if (reformulation == "AF3_IA") {
      file << "Mod_Affine3.o: Mod_Affine3.f90 Mod_Interval.o "
          "Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      file << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
          << std::endl;
      file << std::endl;
      file << "Mod_AF3_IA.o: Mod_AF3_IA.f90  Mod_Affine3.o "
          "Mod_Interval.o Mod_Parametres.o Mod_Parametres.f90"
          << std::endl;
      exist++;
    } else if (reformulation == "AFFINE_S") {
      file << "Mod_Affine_S.o: Mod_Affine_S.f90 Mod_Interval.o "
          "Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      exist++;
    } else if (reformulation == "AFFINE_F") {
      file << "Mod_Affine_F.o: Mod_Affine_F.f90 Mod_Interval.o "
          "Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      exist++;
    } else if (reformulation == "AF2_IA") {
      file << "Mod_Affine2.o: Mod_Affine2.f90 Mod_Interval.o "
          "Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      file << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
          << std::endl;
      file << std::endl;
      file << "Mod_AF2_IA.o: Mod_AF2_IA.f90 Mod_Interval.o "
          "Mod_Affine2.o Mod_Parametres.o Mod_Parametres.f90"
          << std::endl;
      exist++;
    } else if (reformulation == "AFFINE_Rev") {
      file << "Mod_Affine_Rev.o: Mod_Affine_Rev.f90 Mod_Interval.o "
          "Mod_Parametres.o Mod_Parametres.f90" << std::endl;
      exist++;
    } else {
      std::cerr << "Unknown reformulation " << reformulation
          << std::endl;
    }

    if (exist) {
      file << "\t$(COMP.F90) $(OPTSF90) $(INCLUDE) -c $<"
          << std::endl;
    }

  }
  void addReformulationDependencies(std::ostream& file,
      std::string reformulation) {
    if (reformulation == "AFFINE_RB") {
      file << "Mod_Affine_RB.o Mod_Affine_RB.f90 ";
    } else if (reformulation == "AFFINE2") {
      file << "Mod Affine2.o Mod_Affine2.f90";
    } else if (reformulation == "AFFINE3_RB") {
      file << "Mod_Affine3_RB.o Mod_Affine3_RB.f90 ";
    } else if (reformulation == "rAF2_IA") {
      file << "Mod_rAF2_IA.o Mod_Affine2_RB.o ";
    } else if (reformulation == "rAF3_IA") {
      file << "Mod_Affine3_RB.o Mod_rAF3_IA.o";
    } else if (reformulation == "AFFINE3") {
      file << "Mod_Affine3.o Mod_Affine3.f90 ";
    } else if (reformulation == "AF3_IA") {
      file << "Mod_AF3_IA.o Mod_Affine3.o ";
    } else if (reformulation == "AFFINE_S") {
      file << "Mod_Affine_S.o Mod_Affine_S.f90 ";
    } else if (reformulation == "AFFINE_F") {
      file << "Mod_Affine_F.o Mod_Affine_F.f90 ";
    } else if (reformulation == "AF2_IA") {
      file << "Mod_AF2_IA.o Mod_Affine2.o ";
    } else if (reformulation == "AFFINE_Rev") {
      file << "Mod_Affine_Rev.o Mod_Affine_Rev.f90 ";
    } else {
      std::cerr << "Unknown reformulation " << reformulation
          << std::endl;
    }
  }
}

