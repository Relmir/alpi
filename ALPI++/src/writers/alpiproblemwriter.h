/*
 * alpiproblemwriter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIPROBLEMWRITER_H_
#define ALPIPROBLEMWRITER_H_

#include <iostream>
#include <fstream>
#include "../problem/ProblemModel.h"

namespace alpiwriter {

  /** \brief Create the main program to solve this problem
   *
   * \param model the problem
   * \param mod_out the stream
   * \return void
   *
   */
  void compileProblemModelToProblem(
      alpi::problem::ProblemModel& model, std::ostream& mod_out);
}

#endif /* ALPIPROBLEMWRITER_H_ */
