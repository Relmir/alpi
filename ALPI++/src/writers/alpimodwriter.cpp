/*
 * alpimodwriter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include "alpimodwriter.h"

#include <cstdlib>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>

#include "../problem/Constant.h"
#include "../problem/Constraint.h"
#include "../exprs/symbol/DecisionVariable.h"
#include "../problem/Function.h"
#include "../problem/ProblemModel.h"
#include "../exprs/AssignmentMap.h"
#include "../exprs/symbol/ParamVariable.h"
#include "../extractors/extractors.h"
#include "../forgen/forgen.h"
#include "../ftran/CodeLines.h"
#include "../ftran/ExpressionToFortran.h"
#include "../ftran/TempVariable.h"
#include "alpiwritertools.h"
#include "../printers/fortran/expression_node_printer.h"

namespace alpiwriter {

  using namespace forgen;
  using namespace std;
  using namespace alpi::print;
  using namespace alpi::exprs::symbol;
  using namespace alpi::exprs::numerics;
  using namespace alpi::problem;

  void writeProblemModuleHeader(std::ostream& mod,
      ProblemModel& model);

  void writeProblemSimpleConstantsToMod(ProblemModel& model,
      std::ostream& mod_out);
  void writeProblemInterfacesToMod(ProblemModel& model,
      std::ostream& mod_out);
  void writeProblemSingleInterfaceToMod(ProblemModel& model,
      std::ostream& mod_out, Function& f);
  void writeProblemConstantFunctions(ProblemModel& model,
      std::ostream& mod_out);
  void writeProblemAuxiliaryFunctions(ProblemModel& model,
      std::ostream& mod_out);
  void writeProblemFunctionToMod(std::ostream& mod_out,
      ProblemModel& nodel, Function& f, std::string name,
      std::string resultName, std::string baseType);
  void writeProblemObjectiveFunction(ProblemModel& model,
      std::ostream& mod_out);
  void writeProblemConstraintsToMod(ProblemModel& model,
      std::ostream& mod_out);
  void writeBoundComputationToMod(ProblemModel& model,
      std::ostream& mod_out);
  void writeConstraintPropagationToMod(ProblemModel& model,
      std::ostream& mod_out);
  map<DecisionVariable*, string> mapVariablesToParameterExtraction(
      list<DecisionVariable*> vl);
  string writeCallToVariablesInParam(set<DecisionVariable*> vars,
      map<DecisionVariable*, string> mapping);
  void writeProblemSparseElementAccesses(ProblemModel& model,
      std::ostream& mod_out);

  void compileProblemModelToMod(ProblemModel& model,
      ostream& mod_out) {
    // Modules
    writeProblemModuleHeader(mod_out, model);
    // Fortran Constants
    writeProblemSimpleConstantsToMod(model, mod_out);
    mod_out << std::endl;
    // Fortran Interfaces
    writeProblemInterfacesToMod(model, mod_out);

    writeModuleStartingDefinitions(mod_out);

    // Constant functions
    writeProblemConstantFunctions(model, mod_out);
    // Accessors to sparse elements
    writeProblemSparseElementAccesses(model, mod_out);
    // Auxiliary Functions
    writeProblemAuxiliaryFunctions(model, mod_out);
    // Objective Function
    writeProblemObjectiveFunction(model, mod_out);

    mod_out << std::endl;
    writeProblemConstraintsToMod(model, mod_out);
    mod_out << std::endl;
    writeBoundComputationToMod(model, mod_out);
    mod_out << std::endl;
    writeConstraintPropagationToMod(model, mod_out);

    writeModuleEnd(mod_out, "Mod_" + model.getName());
  }

  void writeProblemModuleHeader(std::ostream& mod,
      ProblemModel& model) {
    int nbMods = 2;
    if (model.useReformulation()) {
      // Uses reformulation
      nbMods++;
    }
    std::string used[3];
    used[0] = "MOD_INTERVAL";
    if (model.useReformulation()) {
      used[1] = "MOD_" + model.getReformulation();
    }
    used[nbMods - 1] = "MOD_PROPAGATION";
    writeModuleHeader(mod, "Mod_" + model.getName(), used, nbMods);
  }

  void writeProblemSimpleConstantsToMod(ProblemModel& model,
      std::ostream& mod_out) {
    std::list<Constant*> csts = model.getConstants();
    if (!(csts.empty())) {
      mod_out << "      !==============================" << std::endl;
      mod_out << "      ! Constants" << std::endl;
      mod_out << "      !--------------------" << std::endl;
      std::list<Constant*>::iterator it;
      for (it = csts.begin(); it != csts.end(); it++) {
        Constant* c = *it;
        if (c->getParam()->isInteger()) writeModuleConstant(mod_out,
            "integer", "USER_" + c->getParam()->getName(),
            c->getParam()->getExpression()->toString());
        else writeModuleConstant(mod_out, "INTERVAL(8)",
            "USER_" + c->getParam()->getName(),
            c->getParam()->getExpression()->toString());
      }
    }

    if (model.getObjective()->getParamVariable()->isConstant()) {
      if (model.getObjective()->getParamVariable()->isInteger()) writeModuleConstant(
          mod_out, "integer",
          "USER_" + model.getObjective()->getName(),
          model.getObjective()->getParamVariable()->getExpression()
              ->toString());
      else writeModuleConstant(mod_out, "INTERVAL(8)",
          "USER_" + model.getObjective()->getName(),
          model.getObjective()->getParamVariable()->getExpression()
              ->toString());
    }
  }

  void writeProblemInterfacesToMod(ProblemModel& model,
      std::ostream& mod_out) {
    mod_out << "      !==============================" << std::endl;
    mod_out << "      ! Interfaces" << std::endl;
    mod_out << "      !--------------------" << std::endl;
    // Decision variable reshaped
    list<DecisionVariable*> vars = model.getVariables();
    for (list<DecisionVariable*>::iterator itv = vars.begin();
        itv != vars.end(); ++itv) {
      if ((*itv)->dimension() > 1) {
        std::string interfaced[3];
        int nbInterfaced = 2;
        interfaced[0] = "ACCESS_" + (*itv)->getName();
        interfaced[1] = "ACCESS_" + (*itv)->getName() + "_P";
        if (model.useReformulation()) {
          nbInterfaced++;
          interfaced[2] = "ACCESS_" + (*itv)->getName() + "_"
              + model.getReformulation();
        }
        writeModuleInterface(mod_out, interfaced[0], interfaced,
            nbInterfaced);
        mod_out << endl;
      }
    }

    // Auxiliary Functions
    std::list<Function*>::iterator it;
    list<Function*> funs = model.getFunctions();
    for (it = funs.begin(); it != funs.end(); ++it) {
      Function *f = *it;
      writeProblemSingleInterfaceToMod(model, mod_out, *f);
      mod_out << std::endl;
    }

    // Objective
    if (model.getObjective() == NULL) {
      std::cerr << "Objective function not set !" << std::endl
          << "The file cannot be generated correctly !" << std::endl;
    } else if (!model.getObjective()->getParamVariable()->isConstant()) {
      writeProblemSingleInterfaceToMod(model, mod_out,
          *(model.getObjective()));
    }
  }

  void writeProblemSingleInterfaceToMod(ProblemModel& model,
      std::ostream& mod_out, Function& f) {
    std::string interfaced[3];
    int nbInterfaced = 2;
    interfaced[0] = "USER_" + f.getName();
    interfaced[1] = "USER_" + f.getName() + "_P";
    if (model.useReformulation()) {
      nbInterfaced++;
      interfaced[2] = "USER_" + f.getName() + "_"
          + model.getReformulation();
    }
    writeModuleInterface(mod_out, interfaced[0], interfaced,
        nbInterfaced);
  }

  void writeProblemConstantFunctions(ProblemModel& model,
      std::ostream& mod_out) {
    if (!model.getConstantFunctions().empty()) {
      mod_out << "      !==============================" << std::endl;
      mod_out << "      ! Constant Functions" << std::endl;
      mod_out << "      !--------------------" << std::endl;
      std::list<Function*>::iterator it;
      list<Function*> cFuns = model.getConstantFunctions();
      for (it = cFuns.begin(); it != cFuns.end(); it++) {
        Function * f = *it;
        std::string name = f->getName();
        name = "USER_" + name;
        writeProblemFunctionToMod(mod_out, model, *f, name, "",
            "INTERVAL(8)");
        mod_out << std::endl;
      }
    }
  }

  void writeAccessorContent(ostream& mod_out, string name,
      string resultName, DecisionVariable*f, string baseType);

  void writeProblemSparseElementAccesses(ProblemModel& model,
      std::ostream& mod_out) {
    list<DecisionVariable*> all = model.getVariables();
    list<DecisionVariable*> filtered;
    for (list<DecisionVariable*>::iterator it = all.begin();
        it != all.end(); ++it) {
      if ((*it)->dimension() > 1) {
        filtered.push_back(*it);
      }
    }
    if (!filtered.empty()) {
      mod_out << "      !==============================" << std::endl;
      mod_out << "      ! Sparse Accessors" << std::endl;
      mod_out << "      !--------------------" << std::endl;
      for (list<DecisionVariable*>::iterator it = filtered.begin();
          it != filtered.end(); it++) {
        DecisionVariable* f = *it;
        std::string name = f->getName();
        name = "ACCESS_" + name;
        writeAccessorContent(mod_out, name, "", f, "INTERVAL(8)");
        mod_out << std::endl;
        writeAccessorContent(mod_out, name + "_P", name, f,
            "type(PNODE)");
        if (model.useReformulation()) {
          mod_out << std::endl;
          writeAccessorContent(mod_out,
              name + "_" + model.getReformulation(), name, f,
              "type(T" + model.getReformulation() + ")");
        }
        mod_out << std::endl;
      }
    }
  }

  void writeAccessorContent(ostream& mod_out, string name,
      string resultName, DecisionVariable*f, string baseType) {
    vector<IndexVariable*> inds = f->getIndexing()
        ->getIndexingVariables();
    string in = "in";
    string params[255];
    string intents[255];
    string types[255];
    string test = "(";
    params[0] = "USER_" + f->getName();
    intents[0] = in;
    types[0] = baseType;
    types[1] = baseType + ", dimension(:)";
    int i = 1;
    for (unsigned int var = 0; var < inds.size(); ++var) {
      params[i] = "USER_" + inds[var]->getName();
      intents[i] = in;
      i++;
      types[i] = "integer";
      test = test + inds[var]->getName() + " == USER_"
          + inds[var]->getName() + ") .AND. (";
    }

    writeModuleFunctionHeader(mod_out, name, params, i, resultName,
        types, intents);

    writeModuleFunctionInstruction(mod_out, "integer:: _ind");
    for (unsigned int var = 0; var < inds.size(); ++var) {
      writeModuleFunctionInstruction(mod_out,
          "integer:: " + inds[var]->getName());
    }
    writeModuleFunctionInstruction(mod_out, "_ind = 0");

    for (unsigned int var = 0; var < inds.size(); ++var) {
      ostringstream oss;
      CodeLines beg = convert(inds[var]->getOverIndex()->getStart());
      CodeLines end = convert(inds[var]->getOverIndex()->getStop());
      CodeLines by;
      bool step = inds[var]->getOverIndex()->getStep() != NULL;
      if (step) {
        by = convert(inds[var]->getOverIndex()->getStep());
      }

      oss << "do " << inds[var]->getName() << " = " << beg.getLast()
          << ", " << end.getLast();
      if (step) {
        oss << ", " << by.getLast();
      }
      writeModuleFunctionInstruction(mod_out, oss.str(), var * 2);
    }

    if (f->getIndexing()->getPredicate() != NULL) {
      ostringstream oss;
      oss << "if (";
      alpi::print::fortran::print(f->getIndexing()->getPredicate(),
          oss, map<Symbol*, Symbol*>());
      oss << ") then";
      writeModuleFunctionInstruction(mod_out, oss.str(),
          inds.size() * 2);
    }
    writeModuleFunctionInstruction(mod_out, "_ind = _ind + 1",
        inds.size() * 2 + 2);
    writeModuleFunctionInstruction(mod_out,
        "if (" + test.substr(0, test.length() - 8) + ") then",
        inds.size() * 2 + 2);
    writeModuleFunctionInstruction(mod_out,
        "ACCESS_" + f->getName() + " = USER_" + f->getName()
            + "(_ind)", inds.size() * 2 + 4);
    writeModuleFunctionInstruction(mod_out, "RETURN",
        inds.size() * 2 + 4);
    writeModuleFunctionInstruction(mod_out, "end if",
        inds.size() * 2 + 2);
    if (f->getIndexing()->getPredicate() != NULL) {
      writeModuleFunctionInstruction(mod_out, "end if",
          inds.size() * 2);
    }

    for (int var = inds.size() - 1; var >= 0; --var) {
      writeModuleFunctionInstruction(mod_out, "end do", var * 2);
    }

    writeModuleFunctionEnd(mod_out, name);
  }

  void writeProblemAuxiliaryFunctions(ProblemModel& model,
      std::ostream& mod_out) {
    if (!model.getFunctions().empty()) {
      mod_out << "      !==============================" << std::endl;
      mod_out << "      ! Auxiliary Functions" << std::endl;
      mod_out << "      !--------------------" << std::endl;

      list<Function*> l = model.getFunctions();
      list<Function*>::iterator it;
      for (it = l.begin(); it != l.end(); it++) {
        writeProblemFunctionToMod(mod_out, model, **it,
            "USER_" + (*it)->getName(), "RES_" + (*it)->getName(),
            "INTERVAL(8)");
        mod_out << std::endl;
        writeProblemFunctionToMod(mod_out, model, **it,
            "USER_" + (*it)->getName() + "_P",
            "RES_" + (*it)->getName(), "type(PNODE)");
        mod_out << std::endl;
        if (model.useReformulation()) {
          writeProblemFunctionToMod(mod_out, model, **it,
              "USER_" + (*it)->getName() + "_"
                  + model.getReformulation(),
              "RES_" + (*it)->getName(),
              "type(T" + model.getReformulation() + ")");
          mod_out << std::endl;
        }
      }
    }
  }

  void writeInSymbolsToFunction(ostream& mod_out, list<Symbol*> lSym,
      string baseType, string indeType);
  void writeInSymbolsLinksToFunction(ostream& mod_out,
      map<Symbol*, Symbol*> decls);

  template<class T>
  std::string declareVariable(Variable<T>* v, std::string basetype,
      string indeBaseType, string val = "");

  std::string declareVariable(Symbol* v, std::string basetype,
      string indeBaseType, string val = "") {
    Variable<NumericExpression>* vn = dynamic_cast<Variable<
        NumericExpression>*>(v);
    if (vn != NULL) {
      return declareVariable(vn, basetype, indeBaseType, val);
    }
    Variable<BoolOp>* vb = dynamic_cast<Variable<BoolOp>*>(v);
    if (vb != NULL) {
      return declareVariable(vb, basetype, indeBaseType, val);
    }
    cerr << "mod writer: the symbol isn't a variable "
        << v->toString() << endl;
    return "";
  }

  void writeProblemFunctionToMod(std::ostream& mod_out,
      ProblemModel& pb, Function& f, std::string name,
      std::string resultName, std::string baseType) {
    int i = 0;
    std::string t;
    list<Variable<NumericExpression>*> vl =
        decisionVariablesToVariables(
            alpi::extract::getDecisionVariablesIn(
                f.getParamVariable()));

    int vNumber = vl.size();
    std::list<Variable<NumericExpression>*>::iterator it;
    std::string params[255];
    std::string intents[255];
    std::string pTypes[255];
    bool vect = f.isVector();
    pTypes[0] = baseType;
    for (it = vl.begin(); it != vl.end(); it++) {
      Variable<NumericExpression>* v = *it;
      intents[i] = "in";
      params[i] = "USER_" + v->getName();
      pTypes[i + 1] = typeOfVariable(v, baseType);
      i++;
    }

    int d = 0;
    vector<Symbol*> indicesSyms;
    if (vect) {
      d = f.getParamVariable()->dimension();
      for (int var = 0; var < d; ++var) {
        params[i + var] = "USER_"
            + f.getParamVariable()->getIndexing()
                ->getIndexingVariables().at(var)->getName();
        pTypes[i + var + 1] = "integer";
        intents[i + var] = "in";
        // TODO make the correct variable
        indicesSyms.push_back(
            new alpi::parser::tools::TempVariable(params[i + var],
            NULL));
      }
    }

    writeModuleFunctionHeader(mod_out, name, params, vNumber + d,
        resultName, pTypes, intents,
        alpi::extract::isRecursiveDefinition(f.getParamVariable()));
//    writeModuleFunctionHeader(mod_out, name, params, vNumber,
//        resultName, pTypes, intents);

    list<string>::iterator it2;
    string n = resultName;
    if (resultName.empty()) {
      n = name;
    }
    list<Symbol*> lSym;

    ExpressionNode* def = f.getParamVariable()->getExpression();
    if (def == NULL) {
      cerr << "Undefined parameter " << f.getName() << endl;
      exit(1);
    }
    AssignmentMap<NumericExpression>* am = dynamic_cast<AssignmentMap<
        NumericExpression>*>(def);
    CodeLines amcl;
    if (am != NULL) {
      amcl = convert(n, am, indicesSyms);
      writeInSymbolsToFunction(mod_out, amcl.getSyms(), baseType,
          "INTERVAL(8)");
      list<string> lInst;
      writeInSymbolsLinksToFunction(mod_out, amcl.getMapping());
      writeModuleFunctionInstructions(mod_out, amcl.getPrev());
      writeModuleFunctionInstruction(mod_out, amcl.getLast());
    } else {
      // Classic
      CodeLines cl = convert(def);
      writeInSymbolsToFunction(mod_out, cl.getSyms(), baseType,
          "INTERVAL(8)");
      writeInSymbolsLinksToFunction(mod_out, cl.getMapping());
      writeModuleFunctionInstructions(mod_out, cl.getPrev());

      writeModuleFunctionInstruction(mod_out,
          n + " = " + cl.getLast());
    }

    writeModuleFunctionEnd(mod_out, name);
  }

  template<class T>
  std::string declareVariable(Variable<T>* v, std::string basetype,
      string indeBaseType, string val) {
    std::ostringstream oss;
    if (v->isInteger()) {
      oss << "integer";
    } else if (v->isIndependant()) {
      oss << indeBaseType;
    } else {
      oss << basetype;
    }
    if (v->isVector()) {
      int d = v->dimension();
      oss << ", dimension(" << v->length(0);
      for (int i = 1; i < d; ++i) {
        oss << ", " << v->length(i);
      }
      oss << ")";
    }
    oss << ":: " << v->getName();
    if (!val.empty()) {
      oss << " = " << val;
    }
    return oss.str();
  }

  void writeProblemObjectiveFunction(ProblemModel& model,
      ostream& mod_out) {
    string bufferPointer;
    string vars[4];
    string intents[4];
    string types[4];

    mod_out << "      !==============================" << endl;
    mod_out << "      ! Objective Function" << endl;
    mod_out << "      !--------------------" << endl;

    list<Variable<NumericExpression>*> args =
        decisionVariablesToVariables(model.getVariables());
    Function* f = model.getObjective();
    if (!f->getParamVariable()->isConstant()) {
      writeProblemFunctionToMod(mod_out, model, *f,
          "USER_" + f->getName(), "RES_" + f->getName(),
          "INTERVAL(8)");
      mod_out << std::endl;
      writeProblemFunctionToMod(mod_out, model, *f,
          "USER_" + f->getName() + "_P", "RES_" + f->getName(),
          "type(PNODE)");
      mod_out << std::endl;
      if (model.useReformulation()) {
        writeProblemFunctionToMod(mod_out, model, *f,
            "USER_" + f->getName() + "_" + model.getReformulation(),
            "RES_" + f->getName(),
            "type(T" + model.getReformulation() + ")");
        mod_out << std::endl;
      }
    }

    // Objective function Dispatch
    types[0] = "INTERVAL(8)";
    types[1] = "INTERVAL(8), dimension(:)";

    std::string var = "X";
    std::string in = "in";
    // For obscure reasons (in my point of view), &n != &name

    writeModuleFunctionHeader(mod_out, f->getName(), &(var), 1, "",
        types, &(in));
    map<DecisionVariable*, string> varSpans =
        mapVariablesToParameterExtraction(model.getVariables());
    string objCallParam = writeCallToVariablesInParam(
        alpi::extract::getDecisionVariablesIn(f->getParamVariable()),
        varSpans);
    if (f->getParamVariable()->isConstant()) {
      ostringstream oss;
      ExpressionNode* no = f->getParamVariable()->getExpression();
      alpi::print::fortran::print(no, oss, map<Symbol*, Symbol*>());
      writeModuleFunctionInstruction(mod_out,
          f->getName() + " = " + oss.str());
    } else writeModuleFunctionInstruction(mod_out,
        f->getName() + " = USER_" + f->getName() + objCallParam);
    writeModuleFunctionEnd(mod_out, f->getName());
    mod_out << std::endl;

    // Linearization
    vars[0] = "XI";
    vars[1] = "VAL";
    vars[2] = "REF";
    vars[3] = "ERR";
    types[0] = "INTERVAL(8), dimension(:)";
    types[1] = "logical";
    types[2] = "double precision, dimension(SIZE(XI))";
    types[3] = "INTERVAL(8)";
    intents[0] = "in";
    intents[1] = "out";
    intents[2] = "out";
    intents[3] = "out";

    std::ostringstream name;
    if (model.useReformulation()) {
      name << f->getName() << "_" << model.getReformulation();
    } else {
      name << f->getName() << "_RIEN";
    }
    writeModuleSubroutineHeader(mod_out, name.str(), vars, 4, types,
        intents);
    if (!model.useReformulation()) {
      writeModuleFunctionInstruction(mod_out, "REF = 0");
      writeModuleFunctionInstruction(mod_out, "VAL = .false.");
      writeModuleFunctionInstruction(mod_out, "ERR = [0]");
    } else {
      writeModuleFunctionInstruction(mod_out,
          "type(T" + model.getReformulation()
              + "), dimension(size(XI)):: X");
      writeModuleFunctionInstruction(mod_out,
          "type(T" + model.getReformulation() + "):: "
              + f->getName());
      writeModuleFunctionInstruction(mod_out,
          "call INIT_VARIA(X, XI)");
      if (f->getParamVariable()->isConstant()) {
        ostringstream oss;
        fortran::print(f->getParamVariable()->getExpression(), oss, map<Symbol*, Symbol*>());
        writeModuleFunctionInstruction(mod_out,
            f->getName() + " = " + oss.str());
      } else writeModuleFunctionInstruction(mod_out,
          f->getName() + " = USER_" + f->getName() + objCallParam);
      writeModuleFunctionInstruction(mod_out,
          "call LINEARIZATION(" + f->getName() + ", REF, VAL, ERR)");
    }

    writeModuleSubroutineEnd(mod_out, name.str());
  }

  void writeConstraints(ostream& mod_out, ProblemModel& model);

  void writeProblemConstraintsToMod(ProblemModel& model,
      std::ostream& mod_out) {
    std::list<Constraint*> list = model.getConstraints();
    std::list<Constraint*>::iterator c;

    mod_out << "      !==============================" << std::endl;
    mod_out << "      ! Constraints" << std::endl;
    mod_out << "      !--------------------" << std::endl;
    std::string params[2];
    params[0] = "i";
    params[1] = "X";
    std::string types[3];
    types[0] = "INTERVAL(8)";
    types[1] = "integer";
    types[2] = "INTERVAL(8), dimension(:)";
    std::string intents[2];
    intents[0] = "in";
    intents[1] = "in";

    std::list<DecisionVariable*> vars = model.getVariables();
    std::list<DecisionVariable*>::iterator itv = vars.begin();

    writeModuleFunctionHeader(mod_out, "CONTRAINTE_I", params, 2,
        "CONTRAINTE", types, intents);

    writeModuleFunctionInstruction(mod_out, "select case(i)");

    writeConstraints(mod_out, model);

    writeModuleFunctionInstruction(mod_out, "case default");
    writeModuleFunctionInstruction(mod_out,
        "  CONTRAINTE = INTERVAL(moinsinfini, plusinfini)");
    writeModuleFunctionInstruction(mod_out, "end select");

    writeModuleFunctionEnd(mod_out, "CONTRAINTE_I");

    if (model.useReformulation()) {
      mod_out << std::endl;

      params[1] = "XI";
      types[0] = "type(T" + model.getReformulation() + ")";

      writeModuleFunctionHeader(mod_out,
          "CONTRAINTE_" + model.getReformulation(), params, 2,
          "CONTRAINTE", types, intents);

      writeModuleFunctionInstruction(mod_out,
          "type(T" + model.getReformulation()
              + "), dimension(size(XI)):: X");
      writeModuleFunctionInstruction(mod_out,
          "call INIT_VARIA(X, XI)");

      writeModuleFunctionInstruction(mod_out, "select case(i)");

      writeConstraints(mod_out, model);

      writeModuleFunctionInstruction(mod_out, "case default");
      writeModuleFunctionInstruction(mod_out,
          "  call A_INFINI(CONTRAINTE)");
      writeModuleFunctionInstruction(mod_out, "end select");

      writeModuleFunctionEnd(mod_out,
          "CONTRAINTE_" + model.getReformulation());
    }
    mod_out << std::endl;

    params[1] = "X";
    types[0] = "INTERVAL(8)";

    writeModuleFunctionHeader(mod_out, "CONTRAINTE", params, 2, "",
        types, intents);

    if (model.useReformulation()
        && model.getOption("affineArithmeticForConstraints")) {
      writeModuleFunctionInstruction(mod_out, "INTERVAL(8):: TMP");
      writeModuleFunctionInstruction(mod_out,
          "TMP = CONTRAINTE_" + model.getReformulation() + "(i, X)");
      writeModuleFunctionInstruction(mod_out,
          "CONTRAINTE = (TMP .IX.(CONTRAINTE_I(i, X)))");
    } else {
      writeModuleFunctionInstruction(mod_out,
          "CONTRAINTE = (CONTRAINTE_I(i, X))");
    }

    writeModuleFunctionEnd(mod_out, "CONTRAINTE");

    mod_out << std::endl;
    std::string p[5];
    p[0] = "i";
    p[1] = "X";
    p[2] = "VAL";
    p[3] = "REF";
    p[4] = "ERR";
    std::string tps[5];
    tps[0] = "integer";
    tps[1] = "INTERVAL(8), dimension(:)";
    tps[2] = "logical";
    tps[3] = "double precision, dimension(size(X))";
    tps[4] = "INTERVAL(8)";
    std::string ins[5];
    ins[0] = ins[1] = "in";
    ins[2] = ins[3] = ins[4] = "out";

    writeModuleSubroutineHeader(mod_out, "CONTRAINTE_RELAX", p, 5,
        tps, ins);
    if (!model.useReformulation()) {
      writeModuleFunctionInstruction(mod_out, "REF = 0");
      writeModuleFunctionInstruction(mod_out, "VAL = .false.");
      writeModuleFunctionInstruction(mod_out, "ERR = [0]");
    } else {
      writeModuleFunctionInstruction(mod_out,
          "type(T" + model.getReformulation() + "):: CONTRAINTE");
      writeModuleFunctionInstruction(mod_out,
          "CONTRAINTE = CONTRAINTE_" + model.getReformulation()
              + "(i, X)");
      writeModuleFunctionInstruction(mod_out,
          "call LINEARIZATION(CONTRAINTE, REF, VAL, ERR)");
    }
    writeModuleSubroutineEnd(mod_out, "CONTRAINTE_RELAX");
  }

  string constraintFunctionCall(Constraint* c, ProblemModel& model,
      int* mask, int size);
  void writeConstraints(ostream& mod_out, ProblemModel& model) {
    std::list<Constraint*> list = model.getConstraints();
    std::list<Constraint*>::iterator c;
    std::list<Variable<NumericExpression>*> args =
        decisionVariablesToVariables(model.getVariables());
    int l = 1;
    for (c = list.begin(); c != list.end(); c++) {
      if ((*c)->isEqualityConstraint()) {

        ParamVariable* pv = (*c)->function()->getParamVariable();
        string paramSplitted = writeCallToVariablesInParam(
            alpi::extract::getDecisionVariablesIn(
                (*c)->function()->getParamVariable()),
            mapVariablesToParameterExtraction(model.getVariables()));

        if (pv->isVector()) {
          int* indices = createMask(pv);

          CodeLines cl = convert((*c)->equalityConstraint());
          if (!cl.getSyms().empty()) {
            // TODO Add fresh symbols
            cerr << "Cannot use symbols here" << endl;
          }
          std::list<std::string> prevs = cl.getPrev();
          std::string eqCode = cl.getLast();
          while (increaseMask(indices, pv)) {
            if (acceptedMask(indices, pv->getIndexing())) {
              std::ostringstream buffer2;
              buffer2 << "case(" << l++ << ")";

              writeModuleFunctionInstruction(mod_out, buffer2.str());

              for (std::list<std::string>::iterator its =
                  prevs.begin(); its != prevs.end(); ++its) {
                writeModuleFunctionInstruction(mod_out, *its);
              }

              writeModuleFunctionInstruction(mod_out,
                  "CONTRAINTE = ("
                      + constraintFunctionCall(*c, model, indices,
                          pv->dimension()) + " - (" + eqCode + "))", 2);
            }
          }
        } else {
          std::ostringstream buffer2;
          buffer2 << "case(" << l++ << ")";
          writeModuleFunctionInstruction(mod_out, buffer2.str());
          CodeLines cl = convert((*c)->equalityConstraint());
          if (!cl.getSyms().empty()) {
            cerr << "Cannot use symbols here" << endl;
          }
          std::list<std::string> prevs = cl.getPrev();
          for (std::list<std::string>::iterator its = prevs.begin();
              its != prevs.end(); ++its) {
            writeModuleFunctionInstruction(mod_out, *its);
          }
          std::string uBoundCode = cl.getLast();
          writeModuleFunctionInstruction(mod_out,
              "  CONTRAINTE = (USER_" + (*c)->function()->getName()
                  + paramSplitted + "-(" + uBoundCode + "))");
        }

      }
    }

    mod_out << std::endl;

    for (c = list.begin(); c != list.end(); ++c) {
      if (!((*c)->isEqualityConstraint())) {
        string paramSplitted = writeCallToVariablesInParam(
            alpi::extract::getDecisionVariablesIn(
                (*c)->function()->getParamVariable()),
            mapVariablesToParameterExtraction(model.getVariables()));
        if ((*c)->defineLowerBound()) {

          ParamVariable* pv = (*c)->function()->getParamVariable();

          if (pv->isVector()) {
            int d = pv->dimension();
            int indices[d];
            for (int var = 0; var < d; ++var) {
              indices[var] = 1;
            }
            indices[d - 1] = 0;

            CodeLines cl = convert((*c)->lowerBound());
            if (!cl.getSyms().empty()) {
              // TODO Add fresh symbols
              cerr << "Cannot use symbols here" << endl;
            }
            std::list<std::string> prevs = cl.getPrev();
            std::string eqCode = cl.getLast();
            while (increaseMask(indices, pv)) {
              if (acceptedMask(indices, pv->getIndexing())) {
                std::ostringstream buffer2;
                buffer2 << "case(" << l++ << ")";

                writeModuleFunctionInstruction(mod_out,
                    buffer2.str());
                for (std::list<std::string>::iterator its = prevs
                    .begin(); its != prevs.end(); ++its) {
                  writeModuleFunctionInstruction(mod_out, *its);
                }
                writeModuleFunctionInstruction(mod_out,
                    "CONTRAINTE = (" + eqCode + " - "
                        + constraintFunctionCall(*c, model, indices,
                            pv->dimension()) + ")", 2);
              }
            }
          } else {
            std::ostringstream buffer2;
            buffer2 << "case(" << l++ << ")";
            writeModuleFunctionInstruction(mod_out, buffer2.str());
            CodeLines cl = convert((*c)->lowerBound());
            if (!cl.getSyms().empty()) {
              cerr << "Cannot use symbols here" << endl;
            }
            std::list<std::string> prevs = cl.getPrev();
            for (std::list<std::string>::iterator its = prevs.begin();
                its != prevs.end(); ++its) {
              writeModuleFunctionInstruction(mod_out, *its);
            }
            std::string lBoundCode = cl.getLast();
            writeModuleFunctionInstruction(mod_out,
                "  CONTRAINTE = (" + lBoundCode + "- USER_"
                    + (*c)->function()->getName() + paramSplitted
                    + ")");
          }
        }
        if ((*c)->defineUpperBound()) {

          ParamVariable* pv = (*c)->function()->getParamVariable();

          if (pv->isVector()) {
            int d = pv->dimension();
            int indices[d];
            for (int var = 0; var < d; ++var) {
              indices[var] = 1;
            }
            indices[d - 1] = 0;

            CodeLines cl = convert((*c)->upperBound());
            if (!cl.getSyms().empty()) {
              // TODO Add fresh symbols
              cerr << "Cannot use symbols here" << endl;
            }
            std::list<std::string> prevs = cl.getPrev();
            std::string eqCode = cl.getLast();
            while (increaseMask(indices, pv)) {
              if (acceptedMask(indices, pv->getIndexing())) {
                std::ostringstream buffer2;
                buffer2 << "case(" << l++ << ")";
                writeModuleFunctionInstruction(mod_out,
                    buffer2.str());

                for (std::list<std::string>::iterator its = prevs
                    .begin(); its != prevs.end(); ++its) {
                  writeModuleFunctionInstruction(mod_out, *its);
                }
                writeModuleFunctionInstruction(mod_out,
                    "CONTRAINTE = ("
                        + constraintFunctionCall(*c, model, indices,
                            pv->dimension()) + " - (" + eqCode + "))",
                    2);
              }
            }
          } else {
            std::ostringstream buffer2;
            buffer2 << "case(" << l++ << ")";
            writeModuleFunctionInstruction(mod_out, buffer2.str());
            CodeLines cl = convert((*c)->upperBound());
            if (!cl.getSyms().empty()) {
              cerr << "Cannot use symbols here" << endl;
            }
            std::list<std::string> prevs = cl.getPrev();
            for (std::list<std::string>::iterator its = prevs.begin();
                its != prevs.end(); ++its) {
              writeModuleFunctionInstruction(mod_out, *its);
            }
            std::string uBoundCode = cl.getLast();
            writeModuleFunctionInstruction(mod_out,
                "  CONTRAINTE = (USER_" + (*c)->function()->getName()
                    + paramSplitted + "-(" + uBoundCode + "))");
          }
        }
      }
    }
  }

  void writeInConstraintReduction(ostream& mod, ProblemModel& model,
      ParamVariable* pv, int*l, NumericExpression* prec, int* blk);

  void writeBoundComputationToMod(ProblemModel& model,
      std::ostream& mod) {
    std::string params[1];
    std::string types[2];
    std::string intents[1];
    std::string bufferP;

    mod << "      !==============================" << std::endl;
    mod << "      ! Bound computations" << std::endl;
    mod << "      !--------------------" << std::endl;
    params[0] = "YV";
    types[0] = "INTERVAL(8)";
    types[1] = "INTERVAL(8), dimension(:)";
    intents[0] = "in";
    writeModuleFunctionHeader(mod, "T_RIEN", params, 1, "", types,
        intents);
    writeModuleFunctionInstruction(mod,
        "T_RIEN = INTERVAL(moinsinfini, plusinfini)");
    writeModuleFunctionEnd(mod, "T_RIEN");

    if (model.useReformulation()) {
      types[0] = "INTERVAL(8)";
      types[1] = "interval, dimension(:)";

      params[0] = "XI";
      writeModuleFunctionHeader(mod, "T_" + model.getReformulation(),
          params, 1, model.getObjective()->getName(), types, intents);
      writeModuleFunctionInstruction(mod, "integer:: i");
      writeModuleFunctionInstruction(mod,
          "type(T" + model.getReformulation()
              + "), dimension(size(XI)):: X");
      writeModuleFunctionInstruction(mod, "call INIT_VARIA(X, XI)");
      writeModuleFunctionInstruction(mod,
          model.getObjective()->getName() + " = USER_"
              + model.getObjective()->getName()
              + writeCallToVariablesInParam(
                  alpi::extract::getDecisionVariablesIn(
                      model.getObjective()->getParamVariable()),
                  mapVariablesToParameterExtraction(
                      model.getVariables())));
      writeModuleFunctionEnd(mod, "T_" + model.getReformulation());
    }
  }

  void writeConstraintPropagationToMod(ProblemModel& model,
      std::ostream& mod) {
    string params[3];
    string types[3];
    string intents[3];
    int i;
    int nbVar = getNumberOfVariables(model.getVariables());
    int nbCstr = getNumberOfConstraints(model.getConstraints());
    int l;
    list<Variable<NumericExpression>*> args =
        decisionVariablesToVariables(model.getVariables());
    map<DecisionVariable*, string> mapping =
        mapVariablesToParameterExtraction(model.getVariables());

    mod << "      !==============================" << std::endl;
    mod << "      ! Constraint Propagation" << std::endl;
    mod << "      !--------------------" << std::endl;

    params[0] = "XV";
    types[0] = "INTERVAL(8), dimension(:)";
    intents[0] = "inout";
    params[1] = "BOOL";
    types[1] = "logical";
    intents[1] = "inout";
    params[2] = "minmaxF";
    types[2] = "INTERVAL(8)";
    intents[2] = "in";
    writeModuleSubroutineHeader(mod, "REDUIRE_RIEN", params, 3, types,
        intents);
    writeModuleFunctionInstruction(mod, "BOOL = .true.");
    writeModuleSubroutineEnd(mod, "REDUIRE_RIEN");
    mod << endl;
    writeModuleSubroutineHeader(mod, "REDUIRE_ARBRE", params, 3,
        types, intents);

    {
      ostringstream buffer2;
      buffer2 << "type(PNODE), dimension(" << nbCstr + 1
          << "), save:: CONTRAINTE";
      writeModuleFunctionInstruction(mod, buffer2.str());
    }
    {
      ostringstream buffer2;
      buffer2 << "type(PNODE), dimension(" << nbVar << "), save:: X";
      writeModuleFunctionInstruction(mod, buffer2.str());
    }
    {
      ostringstream buffer2;
      buffer2 << "INTERVAL(8), dimension(" << nbVar << "), save:: XI";
      writeModuleFunctionInstruction(mod, buffer2.str());
    }
    writeModuleFunctionInstruction(mod, "if (BOOL) then");
    writeModuleFunctionInstruction(mod,
        "  if (.not. associated(X(1)%P)) then");
    writeModuleFunctionInstruction(mod, "    call INIT_VARIA(X, XI)");

    i = 0;
    list<Variable<NumericExpression>*> vl = args;
    for (std::list<Variable<NumericExpression>*>::iterator itv = vl
        .begin(); itv != vl.end(); ++itv) {
      int lim = countAcceptableMasks(*itv);
      for (int k = 0; k < lim; ++k) {
        i++;
        if ((*itv)->isInteger()) {
          ostringstream buffer2;
          buffer2 << "    call ENTIER(X(" << i << "))";
          writeModuleFunctionInstruction(mod, buffer2.str());
        }
      }
    }

    l = 0;
    list<Constraint*> cl = model.getConstraints();

    for (list<Constraint*>::iterator it = cl.begin(); it != cl.end();
        ++it) {
      if ((*it)->isEqualityConstraint()) {
        l++;
        CodeLines cl = convert((*it)->equalityConstraint());
        if (!cl.getSyms().empty()) {
          cerr << "Cannot use symbols here" << endl;
        }
        std::list<std::string> prevs = cl.getPrev();
        for (std::list<std::string>::iterator its = prevs.begin();
            its != prevs.end(); ++its) {
          writeModuleFunctionInstruction(mod, *its);
        }
        std::string eqCode = cl.getLast();
        if ((*it)->function()->isVector()) {
          ParamVariable* pv = (*it)->function()->getParamVariable();
          int* ind = createMask(pv);
          while (increaseMask(ind, pv)) {
            if (acceptedMask(ind, pv->getIndexing())) {
              ostringstream buffer2;
              buffer2 << "    CONTRAINTE(" << l << ") = ("
                  << constraintFunctionCall(*it, model, ind,
                      pv->dimension()) << " - (" << eqCode << "))";
              writeModuleFunctionInstruction(mod, buffer2.str());
              l++;
            }
          }
          l--;
        } else {
          ostringstream buffer2;
          buffer2 << "    CONTRAINTE(" << l << ") = (USER_"
              << (*it)->function()->getName()
              << writeCallToVariablesInParam(
                  alpi::extract::getDecisionVariablesIn(
                      (*it)->function()->getParamVariable()), mapping)
              << " - (" << eqCode << "))";
          writeModuleFunctionInstruction(mod, buffer2.str());
        }
      }
    }

    for (std::list<Constraint*>::iterator it = cl.begin();
        it != cl.end(); ++it) {
      if ((*it)->defineLowerBound()) {
        l++;
        CodeLines cl = convert((*it)->lowerBound());
        if (!cl.getSyms().empty()) {
          cerr << "Cannot use symbols here" << endl;
        }
        std::list<std::string> prevs = cl.getPrev();
        for (std::list<std::string>::iterator its = prevs.begin();
            its != prevs.end(); ++its) {
          writeModuleFunctionInstruction(mod, *its);
        }
        std::string lBoundCode = cl.getLast();
        if ((*it)->function()->isVector()) {
          ParamVariable* pv = (*it)->function()->getParamVariable();
          int* ind = createMask(pv);
          while (increaseMask(ind, pv)) {
            if (acceptedMask(ind, pv->getIndexing())) {
              ostringstream buffer2;
              buffer2 << "    CONTRAINTE(" << l << ") = ("
                  << lBoundCode << " - "
                  << constraintFunctionCall(*it, model, ind,
                      pv->dimension()) << ")";
              writeModuleFunctionInstruction(mod, buffer2.str());
              l++;
            }
          }
          l--;
        } else {
          ostringstream buffer2;
          buffer2 << "    CONTRAINTE(" << l << ") = (" << lBoundCode
              << " - USER_" << (*it)->function()->getName()
              << writeCallToVariablesInParam(
                  alpi::extract::getDecisionVariablesIn(
                      (*it)->function()->getParamVariable()), mapping)
              << ")";
          writeModuleFunctionInstruction(mod, buffer2.str());
        }
      }
      if ((*it)->defineUpperBound()) {
        l++;
        CodeLines cl = convert((*it)->upperBound());
        if (!cl.getSyms().empty()) {
          cerr << "Cannot use symbols here" << endl;
        }
        std::list<std::string> prevs = cl.getPrev();
        for (std::list<std::string>::iterator its = prevs.begin();
            its != prevs.end(); ++its) {
          writeModuleFunctionInstruction(mod, *its);
        }
        std::string uBoundCode = cl.getLast();
        if ((*it)->function()->isVector()) {
          ParamVariable* pv = (*it)->function()->getParamVariable();
          int* ind = createMask(pv);
          while (increaseMask(ind, pv)) {
            if (acceptedMask(ind, pv->getIndexing())) {
              ostringstream buffer2;
              buffer2 << "    CONTRAINTE(" << l << ") = ("
                  << constraintFunctionCall(*it, model, ind,
                      pv->dimension()) << " - (" << uBoundCode << "))";
              writeModuleFunctionInstruction(mod, buffer2.str());
              l++;
            }
          }
          l--;
        } else {
          ostringstream buffer2;
          buffer2 << "    CONTRAINTE(" << l << ") = (USER_"
              << (*it)->function()->getName()
              << writeCallToVariablesInParam(
                  alpi::extract::getDecisionVariablesIn(
                      (*it)->function()->getParamVariable()), mapping)
              << " - (" << uBoundCode << "))";
          writeModuleFunctionInstruction(mod, buffer2.str());
        }
      }
    }

    l++;
    {
      ostringstream buffer2;
      buffer2 << "    CONTRAINTE(" << l << ") = (USER_"
          << model.getObjective()->getName()
          << writeCallToVariablesInParam(
              alpi::extract::getDecisionVariablesIn(
                  model.getObjective()->getParamVariable()), mapping)
          << ")";
      writeModuleFunctionInstruction(mod, buffer2.str());
    }

    writeModuleFunctionInstruction(mod, "  else");
    writeModuleFunctionInstruction(mod,
        "    call DETRUIRE(CONTRAINTE)");
    writeModuleFunctionInstruction(mod, "    call DETRUIRE_VARIA(X)");
    writeModuleFunctionInstruction(mod, "  end if");
    writeModuleFunctionInstruction(mod, "else");
    writeModuleFunctionInstruction(mod, "  XI = XV");
    writeModuleFunctionInstruction(mod, "  if (.false.) then");
    writeModuleFunctionInstruction(mod, "    ! Dead code");
    l = 0;

    int blk = 2;

    for (std::list<Constraint*>::iterator it = cl.begin();
        it != cl.end(); ++it) {
      if ((*it)->isEqualityConstraint()) {
        if ((*it)->function()->isVector()) {
          ParamVariable* pv = (*it)->function()->getParamVariable();
          int* m = createMask(pv);
          while (increaseMask(m, pv)) {
            if (acceptedMask(m, pv->getIndexing())) {
              l++;
              ostringstream buffer2;
              if (model.getOption("exactConstraint")) {
                buffer2 << "elseif (.not.CONTRAINTE(" << l
                    << ") == [0]) then";
              } else {
                CodeLines p = convert((*it)->getPrecision());
                if (!p.getPrev().empty()) {
                  writeModuleFunctionInstruction(mod, "else", blk);
                  blk += 2;
                  list<string> prevs = p.getPrev();
                  list<string>::iterator it;
                  for (it = prevs.begin(); it != prevs.end(); ++it) {
                    writeModuleFunctionInstruction(mod, (*it), blk);
                  }
                } else {
                  buffer2 << "else";
                }
                buffer2 << "if (.not. CONTRAINTE(" << l << ") == [-"
                    << p.getLast() << ", " << p.getLast()
                    << "]) then";
              }
              writeModuleFunctionInstruction(mod, buffer2.str(), blk);
              writeModuleFunctionInstruction(mod, "BOOL = .false.",
                  blk);
              writeModuleFunctionInstruction(mod, "return", blk);
            }
          }
        } else {
          l++;
          ostringstream buffer2;
          if (model.getOption("exactConstraint")) {
            buffer2 << "elseif (.not.CONTRAINTE(" << l
                << ") == [0]) then";
          } else {
            CodeLines p = convert((*it)->getPrecision());
            if (!p.getPrev().empty()) {
              writeModuleFunctionInstruction(mod, "else", blk);
              blk += 2;
              list<string> prevs = p.getPrev();
              list<string>::iterator it;
              for (it = prevs.begin(); it != prevs.end(); ++it) {
                writeModuleFunctionInstruction(mod, (*it), blk);
              }
            } else {
              buffer2 << "else";
            }
            buffer2 << "if (.not. CONTRAINTE(" << l << ") == [-"
                << p.getLast() << ", " << p.getLast() << "]) then";
          }
          writeModuleFunctionInstruction(mod, buffer2.str(), blk);
          writeModuleFunctionInstruction(mod, "BOOL = .false.", blk);
          writeModuleFunctionInstruction(mod, "return", blk);
        }
      }
    }

    for (std::list<Constraint*>::iterator it = cl.begin();
        it != cl.end(); ++it) {
      if ((*it)->defineLowerBound()) {
        ParamVariable* pv = (*it)->function()->getParamVariable();
        writeInConstraintReduction(mod, model, pv, &l,
            (*it)->getPrecision(), &blk);
      }
      if ((*it)->defineUpperBound()) {
        ParamVariable* pv = (*it)->function()->getParamVariable();
        writeInConstraintReduction(mod, model, pv, &l,
            (*it)->getPrecision(), &blk);
      }
    }
    {
      ostringstream buffer2;
      buffer2 << "elseif (.not.CONTRAINTE(" << nbCstr + 1
          << ") == minmaxF) then";
      writeModuleFunctionInstruction(mod, buffer2.str(), blk);
    }
    writeModuleFunctionInstruction(mod, "BOOL = .false.", blk + 2);
    writeModuleFunctionInstruction(mod, "return", blk + 2);
    for (; blk > 0; blk -= 2) {
      writeModuleFunctionInstruction(mod, "end if", blk);
    }
    writeModuleFunctionInstruction(mod, "XV = XI", 2);
    writeModuleFunctionInstruction(mod, "end if");
    writeModuleFunctionInstruction(mod, "BOOL = .true.");
    writeModuleSubroutineEnd(mod, "REDUIRE_ARBRE");
  }

  map<DecisionVariable*, string> mapVariablesToParameterExtraction(
      list<DecisionVariable*> vl) {
    if (vl.empty()) {
      return map<DecisionVariable*, string>();
    }
    int l = 0;
    map<DecisionVariable*, string> mapping;
    for (list<DecisionVariable*>::iterator it = vl.begin();
        it != vl.end(); it++) {
      ostringstream buff;
      if ((*it)->isVector()) {
        int j = 1, d = (*it)->dimension();
        for (int i = 0; i < d; ++i) {
          j *= (*it)->length(i);
        }
        if (d == 1) {
          buff << "X(" << l + 1 << ":" << l + j << ")";
          l += j;
        } else {
          j = countAcceptableMasks(*it);
          buff << "X(" << l + 1 << ":" << l + j << ")";
          l += j;
        }
      } else {
        buff << "X(" << l + 1 << ")";
        l++;
      }
      mapping[*it] = buff.str();
    }
    return mapping;
  }
  string writeCallToVariablesInParam(set<DecisionVariable*> vars,
      map<DecisionVariable*, string> mapping) {
    if (vars.empty()) {
      return "";
    }
    ostringstream oss;
    oss << "(";
    set<DecisionVariable*>::iterator it = vars.begin();
    oss << mapping[*it];
    ++it;
    for (; it != vars.end(); ++it) {
      oss << ", " << mapping[*it];
    }
    oss << ")";
    return oss.str();
  }

  string constraintFunctionCall(Constraint* c, ProblemModel& model,
      int* mask, int size) {
    string paramSplitted = writeCallToVariablesInParam(
        alpi::extract::getDecisionVariablesIn(
            c->function()->getParamVariable()),
        mapVariablesToParameterExtraction(model.getVariables()));
    ostringstream oss;
    oss << "USER_" << c->function()->getName()
        << paramSplitted.substr(0, paramSplitted.length() - 1);
    for (int var = 0; var < size; ++var) {
      oss << ", " << mask[var];
    }
    oss << ")";
    return oss.str();
  }

  void writeInSymbolsToFunction(ostream& mod_out, list<Symbol*> lSym,
      string baseType, string indeType) {
    for (list<Symbol*>::iterator itv = lSym.begin();
        itv != lSym.end(); itv++) {
      writeModuleFunctionInstruction(mod_out,
          declareVariable(*itv, baseType, indeType));
    }
  }

  void writeInSymbolsLinksToFunction(ostream& mod_out,
      map<Symbol*, Symbol*> decls) {
    for (map<Symbol*, Symbol*>::iterator itm = decls.begin();
        itm != decls.end(); itm++) {
      CodeLines dcl = convert(itm->first);
      list<string> lInst = dcl.getPrev();
      for (list<string>::iterator its = lInst.begin();
          its != lInst.end(); its++) {
        writeModuleFunctionInstruction(mod_out, (*its));
      }
      writeModuleFunctionInstruction(mod_out,
          itm->second->getName() + " = " + dcl.getLast());
    }
  }

  void writeInConstraintReduction(ostream& mod, ProblemModel& model,
      ParamVariable* pv, int*l, NumericExpression* prec, int* blk) {
    if (pv->isVector()) {
      int* m = createMask(pv);
      while (increaseMask(m, pv)) {
        if (acceptedMask(m, pv->getIndexing())) {
          (*l)++;
          ostringstream buffer2;
          if (model.getOption("exactConstraint")) {
            buffer2 << "elseif (.not.CONTRAINTE(" << *l
                << ") == INTERVAL(moinsinfini, 0)) then";
          } else {
            CodeLines p = convert(prec);
            if (!p.getPrev().empty()) {
              writeModuleFunctionInstruction(mod, "else", *blk);
              *blk += 2;
              list<string> prevs = p.getPrev();
              list<string>::iterator it;
              for (it = prevs.begin(); it != prevs.end(); ++it) {
                writeModuleFunctionInstruction(mod, (*it), *blk);
              }
            } else {
              buffer2 << "else";
            }
            buffer2 << "if (.not. CONTRAINTE(" << *l
                << ") == INTERVAL(moinsinfini, " << p.getLast()
                << ")) then";
          }
          writeModuleFunctionInstruction(mod, buffer2.str(), *blk);
          writeModuleFunctionInstruction(mod, "BOOL = .false.", *blk);
          writeModuleFunctionInstruction(mod, "return", *blk);
        }
      }
    } else {
      (*l)++;
      ostringstream buffer2;
      if (model.getOption("exactConstraint")) {
        buffer2 << "elseif (.not.CONTRAINTE(" << *l
            << ") == INTERVAL(moinsinfini, 0)) then";
      } else {
        CodeLines p = convert(prec);
        if (!p.getPrev().empty()) {
          writeModuleFunctionInstruction(mod, "else", *blk);
          *blk += 2;
          list<string> prevs = p.getPrev();
          list<string>::iterator it;
          for (it = prevs.begin(); it != prevs.end(); ++it) {
            writeModuleFunctionInstruction(mod, (*it), *blk);
          }
        } else {
          buffer2 << "else";
        }
        buffer2 << "if (.not. CONTRAINTE(" << *l
            << ") == INTERVAL(moinsinfini, " << p.getLast()
            << ")) then";
      }
      writeModuleFunctionInstruction(mod, buffer2.str(), *blk);
      writeModuleFunctionInstruction(mod, "BOOL = .false.", *blk);
      writeModuleFunctionInstruction(mod, "return", *blk);
    }
  }
}
