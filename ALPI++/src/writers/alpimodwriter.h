/*
 * alpimodwriter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIMODWRITER_H_
#define ALPIMODWRITER_H_

#include <iostream>
#include <fstream>
#include "../problem/ProblemModel.h"
#include "../problem/Function.h"

namespace alpiwriter {

  using namespace alpi::problem;
  using namespace std;

  /** \brief Create the fortran module defining the elements of the
   *      problem
   *
   * \param model the problem
   * \param mod_out the stream
   * \return void
   *
   */
  void compileProblemModelToMod(ProblemModel& model, ostream& mod_out);
}
#endif /* ALPIMODWRITER_H_ */
