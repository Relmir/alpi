/*
 * alpiparamwriter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include <sstream>
#include "alpiparamwriter.h"
#include "alpiwritertools.h"

namespace alpiwriter {
  using namespace forgen;
  using namespace alpi::problem;

  void compileProblemModelToParameter(ProblemModel& model,
      std::ostream& mod_out) {
    writeModuleHeader(mod_out, "Mod_Parametres", NULL, 0);
    std::ostringstream buffer;
    int l = getNumberOfVariables(
        decisionVariablesToVariables(model.getVariables()));
    buffer << l;
    writeModuleConstant(mod_out, "integer", "NB_VARIA_CPLEX",
        buffer.str());
    writeModuleEnd(mod_out, "Mod_Parametres");
  }
}
