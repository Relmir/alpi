/*
 * alpiproblemwriter.c
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#include <sstream>
#include "alpiproblemwriter.h"
#include "../forgen/forgen.h"
#include "alpiwritertools.h"
#include "../ftran/CodeLines.h"
#include "../ftran/ExpressionToFortran.h"
#include "../evaluators/evaluator.h"

namespace alpiwriter {
  using namespace alpi::problem;
  using namespace std;
  using namespace forgen;

  void writeProblemProgramHeader(std::ostream& mod, ProblemModel& name);
  void writeOptionToProblem(ostream& mod_out, ProblemModel& model,
      int i, string name);
  void writeConstraintPrecision(ostream& mod_out, Constraint* ct,
      int i);
  bool isNonConstantOrParam(NumericExpression* num);
  void writeDecisionVariableAttributes(ostream& mod_out,
      DecisionVariable* v, int i);

  void compileProblemModelToProblem(ProblemModel& model,
      std::ostream& mod_out) {
    int i;
    int nbVars, nbCstrs, nbCeq;
//    std::ostringstream buffer;

    writeProblemProgramHeader(mod_out, model);

    nbVars = getNumberOfVariables(model.getVariables());

    nbCstrs = getNumberOfConstraints(model.getConstraints());

    {
      ostringstream buffer;
      buffer << "INTERVAL(8), dimension(" << nbVars << "):: BUTEES";
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out, "type(solution):: YI_SOL");
    writeProgramInstruction(mod_out, "INTERVAL(8):: AF_interval");
    {
      ostringstream buffer;
      buffer << "DOUBLE PRECISION, dimension(" << nbVars
          << "):: PRECISION_X";
      writeProgramInstruction(mod_out, buffer.str());
    }
    {
      ostringstream buffer;
      buffer << "DOUBLE PRECISION, dimension(" << nbCstrs
          << "):: PRECISION_C";
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out,
        "DOUBLE PRECISION:: PRECISION_F, Fmin_sol");
    writeProgramInstruction(mod_out,
        "logical, dimension(9):: OPTION");
    writeProgramInstruction(mod_out, "logical:: BOOL_MIP");
    {
      ostringstream buffer;
      buffer << "integer:: NB_ITS = "
          << model.getIntOption("logFreq");
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out,
        "integer:: nb_list_max, nombre_contrainte");
    writeProgramInstruction(mod_out,
        "integer:: nombre_in, TMAX, L_max_in");
    // XXX variable de categaorie
    int nbVarCategory = 0;
    {
      ostringstream buffer;
      buffer << "integer, dimension(" << nbVarCategory
          << "):: nb_var_cat, size_cat";
      writeProgramInstruction(mod_out, buffer.str());
    }
    {
      ostringstream buffer;
      buffer << "type(Pmatrix), dimension(" << nbVarCategory
          << "):: V_cat";
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out, "integer:: i, j, k");
    {
      ostringstream buffer;
      buffer << "interval, dimension(" << nbVars << "):: TMP_SOL";
//  buffer.clear(); buffer << "interval, dimension(%d):: TMP_SOL",
//  nbVars+nbVarCategoryTot);
      writeProgramInstruction(mod_out, buffer.str());
    }

    i = 1;
    list<DecisionVariable*> vl = model.getVariables();
    list<Variable<NumericExpression>*> args =
        decisionVariablesToVariables(vl);
    for (list<DecisionVariable*>::iterator it = vl.begin();
        it != vl.end(); ++it) {
      if ((*it)->isVector()) {
        int* mask = createMask((*it));
        while (increaseMask(mask, *it)) {
          if (acceptedMask(mask, (*it)->getIndexing())) {
            writeDecisionVariableAttributes(mod_out, *it, i);
            i++;
          }
        }
      } else {
        writeDecisionVariableAttributes(mod_out, *it, i);
        i++;
      }
    }

    i = 1;
    list<Constraint*> mcl = model.getConstraints();
    for (list<Constraint*>::iterator it = mcl.begin();
        it != mcl.end(); ++it) {
      if ((*it)->isEqualityConstraint()) {
        if ((*it)->function()->isVector()) {
          ParamVariable* pv = (*it)->function()->getParamVariable();
          int* m = createMask(pv);
          while (increaseMask(m, pv)) {
            if (acceptedMask(m, pv->getIndexing())) {
              writeConstraintPrecision(mod_out, *it, i);
              i++;
            }
          }
        } else {
          writeConstraintPrecision(mod_out, *it, i);
          i++;
        }
      }

    }
    nbCeq = i - 1;
    for (list<Constraint*>::iterator it = mcl.begin();
        it != mcl.end(); ++it) {
      if ((*it)->defineLowerBound()) {
        if ((*it)->function()->isVector()) {
          ParamVariable* pv = (*it)->function()->getParamVariable();
          int* m = createMask(pv);
          while (increaseMask(m, pv)) {
            if (acceptedMask(m, pv->getIndexing())) {
              writeConstraintPrecision(mod_out, *it, i);
              i++;
            }
          }
        } else {
          writeConstraintPrecision(mod_out, *it, i);
          i++;
        }
      }
      if ((*it)->defineUpperBound()) {
        if ((*it)->function()->isVector()) {
          ParamVariable* pv = (*it)->function()->getParamVariable();
          int* m = createMask(pv);
          while (increaseMask(m, pv)) {
            if (acceptedMask(m, pv->getIndexing())) {
              writeConstraintPrecision(mod_out, *it, i);
              i++;
            }
          }
        } else {
          writeConstraintPrecision(mod_out, *it, i);
          i++;
        }
      }
    }
    if (model.getObjectivePrecision()==NULL) {
      writeProgramInstruction(mod_out, "PRECISION_F = 1.D-5");
    }else {
    CodeLines cl = convert(model.getObjectivePrecision());
    list<string> prevs = cl.getPrev();
    for (list<string>::iterator its = prevs.begin();
        its != prevs.end(); ++its) {
      writeProgramInstruction(mod_out, *its);
    }
    writeProgramInstruction(mod_out, "PRECISION_F = " + cl.getLast());
    }
    {
      ostringstream buffer;
      buffer << "nombre_contrainte = " << nbCstrs;
      writeProgramInstruction(mod_out, buffer.str());
    }
    {
      ostringstream buffer;
      buffer << "nombre_in = " << nbCstrs - nbCeq;
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out, "nb_list_max = 2147483630");
    {
      ostringstream buffer;
      buffer << "Fmin_sol = " << model.getStringOption("knownMajor");
      writeProgramInstruction(mod_out, buffer.str());
    }
    {
      ostringstream buffer;
      buffer << "TMAX = " << model.getIntOption("timeLimit");
      writeProgramInstruction(mod_out, buffer.str());
    }
    {
      ostringstream buffer;
      buffer << "L_max_in = " << model.getIntOption("memLimit");
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeOptionToProblem(mod_out, model, 1, "taylor");
    writeOptionToProblem(mod_out, model, 2, "constraintsPropagation");
    writeOptionToProblem(mod_out, model, 3, "CPLEX");
    writeOptionToProblem(mod_out, model, 4, "timeInSeconds");
    writeOptionToProblem(mod_out, model, 5, "keepAll");
    writeOptionToProblem(mod_out, model, 6,
        "normalStoppingCriterion");
    writeOptionToProblem(mod_out, model, 7, "terminalDisplay");
    writeOptionToProblem(mod_out, model, 8, "notCertifiedLP");
    writeOptionToProblem(mod_out, model, 9, "exactConstraint");
    // AF_interval... depending on the reformulation
    if (!model.useReformulation()) {
      writeProgramInstruction(mod_out, "AF_interval = 0");
      writeProgramInstruction(mod_out, "OPTION(3) = .false.");
    } else if (model.getReformulation() == "AFFINE_RB") {
      writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
    } else if (model.getReformulation() == "AFFINE2") {
      writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
    } else if (model.getReformulation() == "AFFINE3_RB") {
      writeProgramInstruction(mod_out, "AF_interval = [0, 1]");
    } else if (model.getReformulation() == "rAF2_IA") {
      writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
    } else if (model.getReformulation() == "rAF3_IA") {
      writeProgramInstruction(mod_out, "AF_interval = [0, 1]");
    } else if (model.getReformulation() == "AFFINE3") {
      writeProgramInstruction(mod_out, "AF_interval = [0, 1]");
    } else if (model.getReformulation() == "AF3_IA") {
      writeProgramInstruction(mod_out, "AF_interval = [0, 1]");
    } else if (model.getReformulation() == "AFFINE_S") {
      writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
    } else if (model.getReformulation() == "AFFINE_F") {
      writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
    } else if (model.getReformulation() == "AF2_IA") {
      writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
    } else if (model.getReformulation() == "AFFINE_Rev") {
      writeProgramInstruction(mod_out, "AF_interval = [-1, 1]");
    } else {
      writeProgramInstruction(mod_out, "AF_interval = 0");
      writeProgramInstruction(mod_out, "OPTION(3) = .false.");
    }
    // XXX Category Variable
//  if (nbVarCategory) {
//
//  }
    {
      ostringstream buffer;
      buffer << "print *, \"" << model.getName() << " & " << nbVars
          << " & " << nbCstrs << "\"";
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out, "if (OPTION(7)) then");
    writeProgramInstruction(mod_out, "  print *, \" BUTEES : \"");
    {
      ostringstream buffer;
      buffer << "  do i = 1, " << nbVars;
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out,
        "    print *, \"   variable \", i, \" : \", BUTEES(i)");
    writeProgramInstruction(mod_out, "  end do");
    // XXX category variables
    writeProgramInstruction(mod_out, "end if");

    mod_out << std::endl;
    writeModuleComment(mod_out,
        "Argument de la fonction MIN_GLOB_CPLEX");
    writeModuleComment(mod_out, "");
    writeModuleComment(mod_out,
        "  FINT      Fonction a minimiser en arithmetique d'intervale");
    writeModuleComment(mod_out,
        "  FAFFINE_F Fonction objective en arithmetique affine");
    writeModuleComment(mod_out, "  AF_interval");
    writeModuleComment(mod_out,
        "            Variable des fonctions affines");
    writeModuleComment(mod_out,
        "  T1        Fonction de calcul des bornes par d'autres");
    writeModuleComment(mod_out, "            methodes");
    writeModuleComment(mod_out, "  REDUIRE_X Fonction de reduction");
    writeModuleComment(mod_out, "  CONTRAINTE");
    writeModuleComment(mod_out,
        "            Fonction des contraintes");
    writeModuleComment(mod_out, "  CONTRAINTE_RELAX");
    writeModuleComment(mod_out,
        "            Relaxation des contraintes");
    writeModuleComment(mod_out, "  BUTEES    Bornes des X(i)");
    writeModuleComment(mod_out,
        "  EPS       Precision des contraintes");
    writeModuleComment(mod_out,
        "  EPSV      Precision des variables");
    writeModuleComment(mod_out,
        "  EPSF      Precision de la fonction a minimiser");
    writeModuleComment(mod_out, "  NC        Nombre de contraintes");
    writeModuleComment(mod_out,
        "  NC_INEG   Nombre de contraintes d'inegalite");
    writeModuleComment(mod_out,
//      "  NNC_NOIR  Nombre de contraintes de type boite noire (non");
//  writeModuleComment(mod_out, "            pris en compte)");
//  writeModuleComment(mod_out,
        "  NB        Nombre d'iteration pour l'affichage");
    writeModuleComment(mod_out,
        "  nb_list   Taille maximum de la liste (methode heuristique)");
    writeModuleComment(mod_out,
        "  FFMIN     Borne de depart de la fonction a minimiser");
    writeModuleComment(mod_out, "  OPTION    options de IBBA");
    writeModuleComment(mod_out,
        "  NOMFICH   Nom du fichier pour stocker les resultats");
    writeModuleComment(mod_out, "  TMAX      Temps maximal");
    writeModuleComment(mod_out,
        "  L_max_in  Limite sur le nombre d'element pouvant etre");
    writeModuleComment(mod_out, "            stockes");
    writeModuleComment(mod_out, "  nb_var_cat");
    writeModuleComment(mod_out,
        "            Tableau contenant le nombre d'elements de");
    writeModuleComment(mod_out,
        "            variables associees a chaque categorie");
    writeModuleComment(mod_out,
        "  size_cat  Tableau contenant le nombre d'elements par");
    writeModuleComment(mod_out, "            categorie");
    writeModuleComment(mod_out,
        "  V_cat     Tableau contenant des matrices representant les");
    writeModuleComment(mod_out,
        "            valeurs de chaque element * chaque variable");
    mod_out << std::endl;
    {
      ostringstream buffer;
      buffer << "YI_SOL = IBBA(" << model.getObjective()->getName()
          << ", &";
      writeProgramInstruction(mod_out, buffer.str());
    }
    {
      ostringstream buffer;
      if (!model.useReformulation()) {
        buffer << "              " << model.getObjective()->getName()
            << "_RIEN, &";
      } else {
        buffer << "              " << model.getObjective()->getName()
            << "_" << model.getReformulation() << ", &";
      }
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out, "              AF_interval, &");
    if (!model.useReformulation()) {
      writeProgramInstruction(mod_out, "              T_RIEN, &");
    } else {
      ostringstream buffer;
      buffer << "              T_" << model.getReformulation()
          << ", &";
      writeProgramInstruction(mod_out, buffer.str());
    }
    if (nbCstrs) {
      writeProgramInstruction(mod_out,
          "              REDUIRE_ARBRE, &");
    } else {
      writeProgramInstruction(mod_out,
          "              REDUIRE_RIEN, &");
    }
    writeProgramInstruction(mod_out, "              CONTRAINTE, &");
    writeProgramInstruction(mod_out,
        "              CONTRAINTE_RELAX, &");
    writeProgramInstruction(mod_out, "              BUTEES, &");
    writeProgramInstruction(mod_out, "              PRECISION_C, &");
    writeProgramInstruction(mod_out, "              PRECISION_X, &");
    writeProgramInstruction(mod_out, "              PRECISION_F, &");
    writeProgramInstruction(mod_out,
        "              nombre_contrainte, &");
    writeProgramInstruction(mod_out, "              nombre_in, &");
    writeProgramInstruction(mod_out, "              NB_ITS, &");
    writeProgramInstruction(mod_out, "              nb_list_max, &");
    writeProgramInstruction(mod_out, "              Fmin_sol, &");
    writeProgramInstruction(mod_out, "              OPTION, &");
    {
      ostringstream buffer;
      buffer << "              '" << model.getStringOption("logFile")
          << "', &";
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out, "              TMAX, &");
    writeProgramInstruction(mod_out, "              L_max_in, &");
    writeProgramInstruction(mod_out, "              nb_var_cat, &");
    writeProgramInstruction(mod_out, "              size_cat, &");
    writeProgramInstruction(mod_out, "              V_cat)");
    mod_out << std::endl;
    writeProgramInstruction(mod_out, "if (OPTION(7)) then");
    writeProgramInstruction(mod_out, "  print *, \" SOLUTION : \"");
    {
      ostringstream buffer;
      buffer << "  do i = 1, " << nbVars;
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out,
        "    print *, \"   variable \", i, \" : \", YI_SOL%Y(i)");
    writeProgramInstruction(mod_out, "  end do");
// XXX variable de categorie
//  buffer.clear(); buffer << "  do i = 1, %d", nbVarCategory);
//  writeProgramInstruction(mod_out, buffer.str());
//  writeProgramInstruction(mod_out,
//      "    print *, \"   variable \", i, \" : \", YI_SOL%CAT(i)");
//  writeProgramInstruction(mod_out, "  end do");
    {
      ostringstream buffer;
      buffer << "  TMP_SOL(1:" << nbVars << ") = YI_SOL%Y";
      writeProgramInstruction(mod_out, buffer.str());
    }
    {
      ostringstream buffer;
      buffer << "  k=1+" << nbVars;
      writeProgramInstruction(mod_out, buffer.str());
    }
// XXX Category Variable
//  buffer.clear(); buffer << "  do i = 1, %d", nbVarCategory);
//  writeProgramInstruction(mod_out, buffer.str());
//  writeProgramInstruction(mod_out, "    do j = 1, nb_var_cat(i)");
//  writeProgramInstruction(mod_out,
//      "      TMP_SOL(k) = V_cat(i)%P(YI_SOL%CAT(i), j)");
//  writeProgramInstruction(mod_out, "      k = k+1");
//  writeProgramInstruction(mod_out, "    end do");
//  writeProgramInstruction(mod_out, "  end do");
    writeProgramInstruction(mod_out,
        "  print *, \" valeur minimale de la fonction : \", &");
    {
      ostringstream buffer;
      buffer << "           " << model.getObjective()->getName()
          << "(TMP_SOL)";
      writeProgramInstruction(mod_out, buffer.str());
    }
    writeProgramInstruction(mod_out, "  print *, \" \"");
    if (nbCstrs) {
      writeProgramInstruction(mod_out,
          "  print *, \" CONTRAINTES : \"");
      writeProgramInstruction(mod_out,
          "  do i = 1, nombre_contrainte");
      writeProgramInstruction(mod_out,
          "    print *, \" variable \", i, \" : \", &");
      writeProgramInstruction(mod_out,
          "             CONTRAINTE(i, TMP_SOL)");
      writeProgramInstruction(mod_out, "  end do");
    }
    writeProgramInstruction(mod_out, "end if");
    mod_out << std::endl;
    writeProgramInstruction(mod_out, "deallocate(YI_SOL%Y)");
    writeProgramInstruction(mod_out, "deallocate(YI_SOL%CAT)");

    writeProgramEnd(mod_out, "ibba_"+model.getName());
  }

  void writeProblemProgramHeader(ostream& mod, ProblemModel& model) {
    std::string mods[2];
    mods[0] = "MOD_OPT_GLOB_CPLEX";
    mods[1] = "Mod_" + model.getName();
    writeProgramHeader(mod, "ibba_"+model.getName(), mods, 2);
  }
  void writeOptionToProblem(ostream& mod_out, ProblemModel& model,
      int i, string name) {
    ostringstream buffer;
    if (!model.getOption(name)) {
      buffer << "OPTION(" << i << ") = .false.";
    } else {
      buffer << "OPTION(" << i << ") = .true.";
    }
    writeProgramInstruction(mod_out, buffer.str());
  }

  void writeConstraintPrecision(ostream& mod_out, Constraint* ct,
      int i) {
    if (ct->getPrecision() == NULL) {
      ostringstream buffer;
      buffer << "PRECISION_C(" << i << ") = 1.D-5";
      writeProgramInstruction(mod_out, buffer.str());
      return;
    }
    CodeLines cl = convert(ct->getPrecision());
    list<string> prevs = cl.getPrev();
    for (list<string>::iterator its = prevs.begin();
        its != prevs.end(); ++its) {
      writeProgramInstruction(mod_out, *its);
    }
    ostringstream buffer;
    buffer << "PRECISION_C(" << i << ") = ";
    writeProgramInstruction(mod_out, buffer.str() + cl.getLast());
  }

  bool isNonConstantOrParam(NumericExpression* num) {
    RealConstant* cst = dynamic_cast<RealConstant*>(num);
    if (cst != NULL) {
      return false;
    }
    RelativeConstant* rlc = dynamic_cast<RelativeConstant*>(num);
    if (rlc != NULL) {
      return false;
    }
    BinOp<NumericExpression, NumericExpression>* binop =
        dynamic_cast<BinOp<NumericExpression, NumericExpression>*>(num);
    if (binop != NULL) {
      return isNonConstantOrParam(binop->getLeft())
          || isNonConstantOrParam(binop->getRight());
    }
    UnOp<NumericExpression, NumericExpression>* unop =
        dynamic_cast<UnOp<NumericExpression, NumericExpression>*>(num);
    if (unop != NULL) {
      return isNonConstantOrParam(unop->getArgument());
    }
    return true;
  }

  void writeDecisionVariableAttributes(ostream& mod_out,
      DecisionVariable* v, int i) {
    if (v->isInteger()) {
      ostringstream buffer;
      buffer << "PRECISION_X(" << i << ") = 0";
      writeProgramInstruction(mod_out, buffer.str());
    } else if (v->getPrecision() != NULL) {
      ostringstream buffer;
      CodeLines p = convert(v->getPrecision());
      writeProgramInstructions(mod_out, p.getPrev());
      buffer << "PRECISION_X(" << i << ") = " << p.getLast();
      writeProgramInstruction(mod_out, buffer.str());
    } else {
      ostringstream buffer;
      buffer << "PRECISION_X(" << i << ") = 1.D-8";
      writeProgramInstruction(mod_out, buffer.str());
    }
    {
      ostringstream buffer;
      buffer << "BUTEES(" << i << ") = INTERVAL(";
      if (v->getLowerBound() == NULL) {
        buffer << "moinsinfini, ";
      } else {
        CodeLines lb = convert(v->getLowerBound());
        writeProgramInstructions(mod_out, lb.getPrev());
        if (isNonConstantOrParam(v->getLowerBound())) {
          buffer << "INF(" << lb.getLast() << "), ";
        } else {
          buffer << lb.getLast() << ", ";
        }
      }
      if (v->getUpperBound() == NULL) {
        buffer << "plusinfini";
      } else {
        CodeLines ub = convert(v->getUpperBound());
        writeProgramInstructions(mod_out, ub.getPrev());
        if (isNonConstantOrParam(v->getUpperBound())) {
          buffer << "SUP(" << ub.getLast() << ")";
        } else {
          buffer << ub.getLast();
        }
      }
      buffer << ")";
      writeProgramInstruction(mod_out, buffer.str());
    }
  }
}
