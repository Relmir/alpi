/*
 * alpiparamwriter.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Emmanuel BIGEON
 */

#ifndef ALPIPARAMWRITER_H_
#define ALPIPARAMWRITER_H_

#include <iostream>
#include <fstream>
#include "../problem/ProblemModel.h"
#include "../forgen/forgen.h"

namespace alpiwriter {

  /** \brief Create the parameter fortran module corresponding to this
   *      problem
   *
   * This part is doomed to disappear as soon as we will have
   * implemented the necessary mechaniism in the reformulations
   * \param model teh problem
   * \param mod_out the stream
   * \return void
   *
   */
  void compileProblemModelToParameter(alpi::problem::ProblemModel &model,
                                      std::ostream &mod_out);
}

#endif /* ALPIPARAMWRITER_H_ */
