/*
 * TempVariable.cpp
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#include "TempVariable.h"
#include <iostream>

namespace alpi {
  namespace parser {
    namespace tools {

      using namespace std;
      using namespace alpi::exprs;

      TempVariable::TempVariable(std::string name,
          ExpressionNode* node) :
          Variable<numerics::NumericExpression>(name), underground(node) {
      }
      bool TempVariable::isIndependant() const {
        if (underground == NULL) {
          return true;
        }
        return underground->isIndependant();
      }
      bool TempVariable::isConstant() const {
        return true;
      }
      std::string TempVariable::toString() const {
        return getName();
      }
    }
  }
} /* namespace alpiparsertools */
