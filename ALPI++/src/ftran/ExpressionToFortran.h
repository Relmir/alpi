/*
 * ExpressionToFortran.h
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef EXPRESSIONTOFORTRAN_H_
#define EXPRESSIONTOFORTRAN_H_

#include "../exprs/Expressions.h"
#include "CodeLines.h"
#include <map>
#include "../exprs/symbol/DecisionVariable.h"

namespace forgen {

  using namespace alpi::exprs::builtin;
  using namespace alpi::exprs::operations;
  using namespace alpi::exprs::booleans;
  using namespace alpi::exprs::symbol;
  using namespace alpi::exprs::indexing;
  using namespace alpi::exprs;

  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(Addition* node, std::map<Symbol*, Symbol*> remap =
      std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(BooleanAnd* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(BooleanOr* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(CosineFunctionCall* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(DecisionVariable* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>(), std::string suppl = "");
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(Difference* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(Division* node, std::map<Symbol*, Symbol*> remap =
      std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(Equal* node, std::map<Symbol*, Symbol*> remap =
      std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(ExpressionNode* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(GreaterEqual* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(GreaterThan* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(IndexVariable* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(LowerEqual* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(LowerThan* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(Multiplication* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(Oppose* node, std::map<Symbol*, Symbol*> remap =
      std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(ParamVariable* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>(), std::string suppl = "");
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(Power* node, std::map<Symbol*, Symbol*> remap =
      std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(ProdOnIndex* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  template<class T>
  CodeLines convert(QualifiedExpression<T>* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(Qualifier* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(RealConstant* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(RelativeConstant* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(SineFunctionCall* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(SqrtFunctionCall* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert an Expression into instructions.
   *
   * \param node the expression
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions to compute and the value as last
   *
   */
  CodeLines convert(SumOnIndex* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Create an affectation to name following the assignment map.
   *
   * \param name the name of the variable to affect
   * \param node the assignment map
   * \param indices the indices to use (ordered)
   * \param remap the mapping of expression variables to the program
   *      variables
   * \return CodeLines the instructions for this affectation
   *
   */
  CodeLines convert(std::string name, AssignmentMap<numerics::NumericExpression>* node,
      std::vector<Symbol*> indices, std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
  /** \brief Convert indexed affectation to name
   *
   * \param name the name of the variable affected
   * \param ind the index
   * \param node the expression we loop on
   * \param the mapping of variables from the expression to variables in
   *      the code
   * \return CodeLines the instructions to execute
   */
  CodeLines convertIndexed(std::string name,
      IndexingMultiRelation* ind, ExpressionNode* node,
      std::map<Symbol*, Symbol*> remap =
          std::map<Symbol*, Symbol*>());
} /* namespace alpiparsertools */

#endif /* EXPRESSIONTOFORTRAN_H_ */
