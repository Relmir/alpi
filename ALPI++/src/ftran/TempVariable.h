/*
 * TempVariable.h
 *
 *  Created on: Jan 23, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef TEMPVARIABLE_H_
#define TEMPVARIABLE_H_

#include "../exprs/symbol/Variable.h"

namespace alpi {
namespace parser {
namespace tools {
  /** \brief A temporary variable (accumulator, anonymous index...)
   */
  class TempVariable: public alpi::exprs::symbol::Variable<alpi::exprs::numerics::NumericExpression> {
    alpi::exprs::ExpressionNode *underground;
  public:
    /** \brief Create a temporary variable
     *
     * \param name the name of the variable
     * \param node the underlying expression to check independance,
     *      vector status, integer status...
     */
    TempVariable (std::string name, alpi::exprs::ExpressionNode *node);
    // Inherited
    bool isIndependant() const;
    bool isConstant() const;
    std::string toString() const;
  };
}}} /* namespace alpiparsertools */

#endif /* TEMPVARIABLE_H_ */
