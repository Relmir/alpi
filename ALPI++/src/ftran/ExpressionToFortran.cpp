/*
 * ExpressionToFortran.cpp
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#include "ExpressionToFortran.h"

#include <cstdio>
#include <iostream>
#include <iterator>
#include <list>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "../exprs/AssignmentMap.h"
#include "../exprs/booleans/BooleanAnd.h"
#include "../exprs/booleans/BooleanOr.h"
#include "../exprs/booleans/Equal.h"
#include "../exprs/booleans/GreaterEqual.h"
#include "../exprs/booleans/GreaterThan.h"
#include "../exprs/booleans/LowerEqual.h"
#include "../exprs/booleans/LowerThan.h"
#include "../exprs/indexing/Index.h"
#include "../exprs/indexing/IndexingMultiRelation.h"
#include "../exprs/numerics/RealConstant.h"
#include "../exprs/numerics/RelativeConstant.h"
#include "../exprs/operations/Addition.h"
#include "../exprs/operations/BigOp.h"
#include "../exprs/operations/BinOp.h"
#include "../exprs/operations/CosineFunctionCall.h"
#include "../exprs/operations/Difference.h"
#include "../exprs/operations/Division.h"
#include "../exprs/operations/Multiplication.h"
#include "../exprs/operations/Oppose.h"
#include "../exprs/operations/Power.h"
#include "../exprs/operations/ProdOnIndex.h"
#include "../exprs/operations/SineFunctionCall.h"
#include "../exprs/operations/SqrtFunctionCall.h"
#include "../exprs/operations/SumOnIndex.h"
#include "../exprs/operations/UnOp.h"
#include "../exprs/QualifiedExpression.h"
#include "../exprs/Qualifier.h"
#include "../exprs/symbol/IndexVariable.h"
#include "../exprs/symbol/ParamVariable.h"
#include "../extractors/extractors.h"
#include "../printers/fortran/expression_node_printer.h"
#include "../evaluators/evaluator.h"
#include "TempVariable.h"

namespace forgen {

  using namespace alpi::parser::tools;
  using namespace std;
  using namespace alpi::print;
  using namespace alpi::print::fortran;
  using namespace alpi::exprs::booleans;
  using namespace alpi::exprs::builtin;
  using namespace alpi::exprs::symbol;
  using namespace alpi::exprs::numerics;

  int fresh_count = 0;
  string fresh() {
    ostringstream oss;
    oss << "_fresh" << (++fresh_count);
    return oss.str();
  }

  string varListToString(list<Symbol*> vars) {
    if (vars.empty()) {
      return "()";
    }
    ostringstream oss;
    list<Symbol*>::iterator it = vars.begin();
    oss << "(USER_" << (*it)->getName();
    ++it;
    for (; it != vars.end(); ++it) {
      oss << ", USER_" << (*it)->getName();
    }
    oss << ")";
    return oss.str();
  }

  string makeTestForMask(IndexingMultiRelation* ind,
      vector<Symbol*> syms);

  template<class P, class R>
  CodeLines convertGenericBinOp(BinOp<P, R>* op,
      std::map<Symbol*, Symbol*> remap);

  CodeLines convert(ExpressionNode* node,
      map<Symbol*, Symbol*> remap) {
    BinOp<BoolOp, BoolOp>* binopbb =
        dynamic_cast<BinOp<BoolOp, BoolOp>*>(node);
    if (binopbb != NULL) {
      // Dispatch binops
      return convertGenericBinOp(binopbb, remap);
    }
    BinOp<numerics::NumericExpression, BoolOp>* binopnb =
        dynamic_cast<BinOp<numerics::NumericExpression, BoolOp>*>(node);
    if (binopnb != NULL) {
      // Dispatch binops
      return convertGenericBinOp(binopnb, remap);
    }
    BinOp<numerics::NumericExpression, numerics::NumericExpression>* binopnn =
        dynamic_cast<BinOp<numerics::NumericExpression,
            numerics::NumericExpression>*>(node);
    if (binopnn != NULL) {
      // Dispatch binops
      return convertGenericBinOp(binopnn, remap);
    }
    UnOp<NumericExpression, NumericExpression>* unop =
        dynamic_cast<UnOp<NumericExpression, NumericExpression>*>(node);
    if (unop != NULL) {
      // Dispatch unops
      CosineFunctionCall* cfc =
          dynamic_cast<CosineFunctionCall*>(node);
      if (cfc != NULL) {
        return convert(cfc, remap);
      }
      SineFunctionCall* sfc = dynamic_cast<SineFunctionCall*>(node);
      if (sfc != NULL) {
        return convert(sfc, remap);
      }
      Oppose* opp = dynamic_cast<Oppose*>(node);
      if (opp != NULL) {
        return convert(opp, remap);
      }
      SqrtFunctionCall* sq = dynamic_cast<SqrtFunctionCall*>(node);
      if (sq != NULL) {
        return convert(sq, remap);
      }
    }

    // BigOp (operation on index)
    BigOp<NumericExpression>* bigop = dynamic_cast<BigOp<
        NumericExpression>*>(node);
    if (bigop != NULL) {
      SumOnIndex* soi = dynamic_cast<SumOnIndex*>(node);
      if (soi != NULL) {
        return convert(soi, remap);
      }
      ProdOnIndex* poi = dynamic_cast<ProdOnIndex*>(node);
      if (poi != NULL) {
        return convert(poi, remap);
      }
    }

    // Others
    RelativeConstant* rc = dynamic_cast<RelativeConstant*>(node);
    if (rc != NULL) {
      return convert(rc, remap);
    }
    RealConstant* rec = dynamic_cast<RealConstant*>(node);
    if (rec != NULL) {
      return convert(rec, remap);
    }
    DecisionVariable* v = dynamic_cast<DecisionVariable*>(node);
    if (v != NULL) {
      return convert(v, remap);
    }
    ParamVariable* pv = dynamic_cast<ParamVariable*>(node);
    if (pv != NULL) {
      return convert(pv, remap, "");
    }
    {
      QualifiedExpression<NumericExpression>* qe =
          dynamic_cast<QualifiedExpression<NumericExpression>*>(node);
      if (qe != NULL) {
        return convert(qe, remap);
      }
    }
    {
      QualifiedExpression<BoolOp>* qe =
          dynamic_cast<QualifiedExpression<BoolOp>*>(node);
      if (qe != NULL) {
        return convert(qe, remap);
      }
    }
    IndexVariable* iv = dynamic_cast<IndexVariable*>(node);
    if (iv != NULL) {
      return convert(iv, remap);
    }
    if (node == NULL) {
      std::cerr << "undefined conversion for NULL expression"
          << std::endl;
      return CodeLines();
    }
    std::cerr << "undefined conversion for expression \""
        << node->toString() << "\"" << std::endl;
    return CodeLines();
  }

  CodeLines convert(string name,
      AssignmentMap<numerics::NumericExpression>* node,
      vector<Symbol*> indices, map<Symbol*, Symbol*> remap) {
    CodeLines cl;
    list<IndexingMultiRelation*> lAff = node->getIndexingRelations();
    std::string pre = "";

    map<Symbol*, Symbol*> myRemapExt(remap);

    for (list<IndexingMultiRelation*>::iterator it = lAff.begin();
        it != lAff.end(); ++it) {
      ostringstream buffer;
      buffer << pre << "if (" << makeTestForMask(*it, indices)
          << ") then";
      cl.addLine(buffer.str());

      map<Symbol*, Symbol*> myRemap(myRemapExt);
      vector<IndexVariable*> inds = (*it)->getIndexingVariables();
      for (unsigned int var = 0; var < inds.size(); ++var) {
        if (!(inds[var]->getName().empty())) {
          myRemap[inds[var]] = indices[var];
        }
      }

      CodeLines c = convert(node->getAssignment(*it), myRemap);
      cl.addSymbols(c.getSyms());
      cl.addLines(c.getPrev());
      cl.addMapping(c.getMapping());
      cl.addLine(string(2, ' ') + name + " = " + c.getLast());
      map<Symbol*, Symbol*> m = c.getMapping();
      for (map<Symbol*, Symbol*>::iterator itm = m.begin();
          itm != m.end(); ++itm) {
        myRemapExt[itm->first] = itm->second;
      }
      pre = "else ";
    }

    if (node->getDefault() != NULL) {
      // Default //
      cl.addLine("else");

      map<Symbol*, Symbol*> myRemap(myRemapExt);
      vector<IndexVariable*> inds = node->getDefaultIndexing()
          ->getIndexingVariables();
      for (unsigned int var = 0; var < inds.size(); ++var) {
        if (!(inds[var]->getName().empty())) {
          myRemap[inds[var]] = indices[var];
        }
      }
      CodeLines c = convert(node->getDefault(), myRemap);
      cl.addSymbols(c.getSyms());
      cl.addLines(c.getPrev());
      cl.addMapping(c.getMapping());
      cl.addLine(string(2, ' ') + name + " = " + c.getLast());
    }
    cl.addLine("end if");
    return cl;
  }

  CodeLines convert(Addition* node, map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(CosineFunctionCall* node,
      map<Symbol*, Symbol*> remap) {
    CodeLines in = convert(node->getArgument(), remap);
    CodeLines r;
    r.addLines(in.getPrev());
    r.addSymbols(in.getSyms());
    r.addMapping(in.getMapping());
    r.addLine("cos(" + in.getLast() + ")");
    return r;
  }
  CodeLines convert(SineFunctionCall* node,
      map<Symbol*, Symbol*> remap) {
    CodeLines in = convert(node->getArgument(), remap);
    CodeLines r;
    r.addLines(in.getPrev());
    r.addSymbols(in.getSyms());
    r.addMapping(in.getMapping());
    r.addLine("sin(" + in.getLast() + ")");
    return r;
  }
  CodeLines convert(Difference* node, map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(Division* node, map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }

  CodeLines convert(Multiplication* node,
      map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }

  CodeLines convert(Oppose* node, map<Symbol*, Symbol*> remap) {
    CodeLines in = convert(node->getArgument(), remap);
    CodeLines r;
    r.addLines(in.getPrev());
    r.addSymbols(in.getSyms());
    r.addMapping(in.getMapping());
    ostringstream oss;
    print(node, oss, in.getLast());
    r.addLine(oss.str());
    return r;
  }

  CodeLines convert(Power* node, map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }

  CodeLines convert(RealConstant* node, map<Symbol*, Symbol*> remap) {
    return node->toString();
  }
  CodeLines convert(RelativeConstant* node,
      map<Symbol*, Symbol*> remap) {
    return node->toString();
  }

  CodeLines makeSurround(IndexingMultiRelation* rel);
  CodeLines makeEnder(IndexingMultiRelation* rel);
  int getIndentationSize(IndexingMultiRelation* rel);
  template<class P>
  CodeLines convertGenericBigOp(BigOp<P>* op,
      std::map<Symbol*, Symbol*> remap, BinOp<P, P>* under_op,
      string acc_init_val) {
    CodeLines result;
    CodeLines surround(""), ender;
    int nb = 0;
    TempVariable* v = new TempVariable(fresh(), op->getExpression());
    result.addSymbol(v);
    result.addLine(v->getName() + " = " + acc_init_val);
    surround = makeSurround(op->getIndexing());
    ender = makeEnder(op->getIndexing());
    nb = getIndentationSize(op->getIndexing());
    if (op->getIndexing()->getPredicate() != NULL) {
      nb += 2;
    }
    std::map<Symbol*, Symbol*> m = surround.getMapping();
    for (map<Symbol*, Symbol*>::iterator it = m.begin();
        it != m.end(); ++it) {
      remap[it->first] = it->second;
    }
    CodeLines cl = convert(op->getExpression(), remap);
    result.addSymbols(surround.getSyms());
    result.addMapping(surround.getMapping());
    result.addSymbols(cl.getSyms());
    result.addMapping(cl.getMapping());

    result.addLines(surround.getPrev());

    list<string>::iterator it2;
    list<string> lInst = cl.getPrev();
    for (it2 = lInst.begin(); it2 != lInst.end(); it2++) {
      result.addLine(string(nb, ' ') + (*it2));
    }
    ostringstream oss;
    print(under_op, oss, v->getName(), cl.getLast());

    result.addLine(
        string(nb, ' ') + v->getName() + " = " + oss.str());
    result.addLines(ender.getPrev());
    result.addLine(ender.getLast());
    result.addLine(v->getName());
    return result;
  }
  CodeLines convert(SumOnIndex* node,
      std::map<Symbol*, Symbol*> remap) {
    return convertGenericBigOp(node, remap,
        new Addition(
            new TempVariable("absent", node->getExpression()),
            node->getExpression()), "0");
  }
  CodeLines convert(ProdOnIndex* node,
      std::map<Symbol*, Symbol*> remap) {
    return convertGenericBigOp(node, remap,
        new Multiplication(
            new TempVariable("absent", node->getExpression()),
            node->getExpression()), "1");
  }
  CodeLines convert(DecisionVariable* node,
      map<Symbol*, Symbol*> remap, string suppl) {
    if (remap.find(node) != remap.end()) {
      return CodeLines(remap[node]->getName());
    }

    if (node->dimension() > 1) {
      return CodeLines(
          "ACCESS_" + node->getName() + "(USER_" + node->getName()
              + ", " + suppl + ")");
    }

    if (!suppl.empty()) {
      return CodeLines("USER_" + node->getName() + "(" + suppl + ")");
    }

    return CodeLines("USER_" + node->getName());
  }
  CodeLines convert(ParamVariable* node, map<Symbol*, Symbol*> remap,
      string supplVar) {
    ostringstream oss;
    if (remap.find(node) != remap.end()) {
      return CodeLines(remap[node]->getName());
    }
    if (node->isConstant()) {
      return CodeLines("USER_" + node->getName());
    }
    oss << "USER_" << node->getName();
    bool beg = node->isIndependant();
    if (!beg) {
      string s = alpi::extract::getVariableList(
          node->getExpression());
      oss << s.substr(0, s.length() - 1);
    } else {
      oss << "(";
    }
    if (!supplVar.empty()) {
      if (!beg) {
        oss << ", ";
      }
      oss << supplVar;
    }
    return CodeLines(oss.str() + ")");
  }
  CodeLines convert(Qualifier* qualif, map<Symbol*, Symbol*> remap) {
    CodeLines r;
    list<NumericExpression*> el = qualif->getExpressions();
    if (el.empty()) {
      r.addLine("()");
      return r;
    }
    CodeLines e;
    ostringstream oss;
    list<NumericExpression*>::iterator it = el.begin();
    e = convert(*it, remap);
    r.addLines(e.getPrev());
    r.addSymbols(e.getSyms());
    oss << "(" << e.getLast();
    ++it;
    for (; it != el.end(); ++it) {
      e = convert(*it, remap);
      r.addLines(e.getPrev());
      r.addSymbols(e.getSyms());
      oss << ", " << e.getLast();
    }
    oss << ")";
    r.addLine(oss.str());
    return r;
  }
  template<class T>
  CodeLines convert(QualifiedExpression<T>* node,
      map<Symbol*, Symbol*> remap) {

    CodeLines pl = convert(node->getQualifier(), remap);
    CodeLines r;
    r.addLines(pl.getPrev());
    r.addSymbols(pl.getSyms());
    r.addMapping(pl.getMapping());
    bool b = node->getQualified()->isConstant();
    ParamVariable* pv = dynamic_cast<ParamVariable*>(node
        ->getQualified());
    if (pv != NULL && !b) {
      string s = pl.getLast();
      CodeLines pvl = convert(pv, remap, s.substr(1, s.length() - 2));
      r.addLine(pvl.getLast());
      return r;
    }
    DecisionVariable* dv = dynamic_cast<DecisionVariable*>(node
        ->getQualified());
    if (dv != NULL) {
      string s = pl.getLast();
      CodeLines dvl = convert(dv, remap, s.substr(1, s.length() - 2));
      r.addLine(dvl.getLast());
      return r;
    }
    map<Symbol*, Symbol*> m = pl.getMapping();
    for (map<Symbol*, Symbol*>::iterator it = m.begin();
        it != m.end(); ++it) {
      remap[it->first] = it->second;
    }
    CodeLines cl = convert(node->getQualified(), remap);
    r.addLines(cl.getPrev());
    r.addSymbols(cl.getSyms());
    r.addMapping(cl.getMapping());
    if (b) r.addLine(cl.getLast());
    else r.addLine(cl.getLast() + pl.getLast());
    return r;
  }

  CodeLines convert(IndexVariable* node,
      map<Symbol*, Symbol*> remap) {
    CodeLines r;
    if (remap.find(node) != remap.end()) r.addLine(
        remap[node]->getName());
    else r.addLine("USER_" + node->getName());
    return r;
  }
  CodeLines convertIndexed(string name, IndexingMultiRelation* ind,
      ExpressionNode* node, map<Symbol*, Symbol*> remap) {
    CodeLines result;
    CodeLines surround(""), ender;
    int nb = 0;
    surround = makeSurround(ind);
    ender = makeEnder(ind);
    nb = getIndentationSize(ind);
    if (ind->getPredicate() != NULL) {
      nb += 2;
    }

    CodeLines cl = convert(node, remap);
    result.addSymbols(surround.getSyms());
    result.addSymbols(cl.getSyms());
    result.addMapping(cl.getMapping());

    result.addLines(surround.getPrev());

    list<string>::iterator it2;
    list<string> lInst = cl.getPrev();
    for (it2 = lInst.begin(); it2 != lInst.end(); it2++) {
      result.addLine(string(nb, ' ') + (*it2));
    }
    result.addLine(
        string(nb, ' ') + name + surround.getLast() + " = "
            + cl.getLast());
    result.addLines(ender.getPrev());
    result.addLine(ender.getLast());
    return result;
  }

  CodeLines makeSurround(IndexingMultiRelation* rel) {
    ostringstream oss;
    CodeLines r;
    int blank = 0;
    vector<IndexVariable*> vl = rel->getIndexingVariables();
    list<string> prevs;
    list<Symbol*> syms;
    for (vector<IndexVariable*>::iterator it = vl.begin();
        it != vl.end(); ++it) {
      Index* index = (*it)->getOverIndex();
      CodeLines begin = convert(index->getStart());
      prevs = begin.getPrev();
      for (list<string>::iterator its = prevs.begin();
          its != prevs.end(); ++its) {
        r.addLine(string(blank, ' ') + (*its));
      }
      r.addSymbols(begin.getSyms());
      if ((*it)->getOverIndex()->length() == 1) {
        oss << ", " << begin.getLast();
      } else {
        IndexVariable* iv = new IndexVariable(
            "USER_" + (*it)->getName());
        iv->setOverIndex(index);
        r.addSymbol(iv);
        CodeLines stop = convert(iv->getOverIndex()->getStop());
        prevs = stop.getPrev();
        for (list<string>::iterator its = prevs.begin();
            its != prevs.end(); ++its) {
          r.addLine(string(blank, ' ') + (*its));
        }
        r.addSymbols(stop.getSyms());
        string step = "";
        if (iv->getOverIndex()->getStep() != NULL) {
          CodeLines clstep = convert(iv->getOverIndex()->getStep());
          prevs = clstep.getPrev();
          for (list<string>::iterator its = prevs.begin();
              its != prevs.end(); ++its) {
            r.addLine(string(blank, ' ') + (*its));
          }
          r.addSymbols(clstep.getSyms());
          step = ", " + clstep.getLast();
        }
        r.addLine(
            string(blank, ' ') + "do " + iv->getName() + " = "
                + begin.getLast() + ", " + stop.getLast() + step);
        blank += 2;
        oss << ", " << iv->getName();
      }
    }

    if (rel->getPredicate() != NULL) {
      CodeLines predicate = convert(rel->getPredicate());
      r.addSymbols(predicate.getSyms());

      prevs = predicate.getPrev();
      for (list<string>::iterator its = prevs.begin();
          its != prevs.end(); ++its) {
        r.addLine(string(blank, ' ') + (*its));
      }

      r.addLine(
          string(blank, ' ') + "if (" + predicate.getLast()
              + ") then");
    }

    r.addLine("(" + oss.str().substr(2) + ")");
    return r;

  }
  CodeLines makeEnder(IndexingMultiRelation* rel) {
    int blank = getIndentationSize(rel);
    CodeLines r;
    if (rel->getPredicate() != NULL) {
      r.addLine(string(blank, ' ') + "end if");
    }
    vector<IndexVariable*> vl = rel->getIndexingVariables();
    for (vector<IndexVariable*>::reverse_iterator it = vl.rbegin();
        it != vl.rend(); ++it) {
      if ((*it)->getOverIndex()->length() != 1) {
        blank -= 2;
        r.addLine(string(blank, ' ') + "end do");
      }
    }
    return r;
  }
  int getIndentationSize(IndexingMultiRelation* rel) {
    int i = 0;
    vector<IndexVariable*> vl = rel->getIndexingVariables();
    for (vector<IndexVariable*>::iterator it = vl.begin();
        it != vl.end(); ++it) {
      if ((*it)->getOverIndex()->length() != 1) {
        i += 2;
      }
    }
    return i;
  }
  CodeLines convert(BooleanAnd* node,
      std::map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(BooleanOr* node,
      std::map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(LowerThan* node,
      std::map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(LowerEqual* node,
      std::map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(Equal* node, std::map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(GreaterThan* node,
      std::map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(GreaterEqual* node,
      std::map<Symbol*, Symbol*> remap) {
    return convertGenericBinOp(node, remap);
  }
  CodeLines convert(SqrtFunctionCall* node,
      std::map<Symbol*, Symbol*> remap) {
    CodeLines arg = convert(node->getArgument(), remap);
    CodeLines r;
    r.addLines(arg.getPrev());
    r.addSymbols(arg.getSyms());
    r.addMapping(arg.getMapping());
    r.addLine("sqrt(" + arg.getLast() + ")");
    return r;
  }

  template<class P, class R>
  CodeLines convertGenericBinOp(BinOp<P, R>* op,
      std::map<Symbol*, Symbol*> remap) {
    CodeLines r;
    CodeLines a = convert(op->getLeft(), remap);
    std::map<Symbol*, Symbol*> m = a.getMapping();
    for (map<Symbol*, Symbol*>::iterator it = m.begin();
        it != m.end(); ++it) {
      remap[it->first] = it->second;
    }
    CodeLines b = convert(op->getRight(), remap);
    r.addMapping(a.getMapping());
    r.addMapping(b.getMapping());
    r.addSymbols(a.getSyms());
    r.addSymbols(b.getSyms());
    r.addLines(a.getPrev());
    r.addLines(b.getPrev());
    ostringstream oss;
    print(op, oss, a.getLast(), b.getLast());
    r.addLine(oss.str());
    return r;
  }

  string makeTestForMask(IndexVariable* ind, string symName) {
    ostringstream oss;
    int beg = alpi::eval::evaluateInteger(
        ind->getOverIndex()->getStart(),
        map<Symbol*, ExpressionNode*>());
    int end = alpi::eval::evaluateInteger(
        ind->getOverIndex()->getStop(),
        map<Symbol*, ExpressionNode*>());
    oss << symName << " <= " << end << " .AND. " << symName << " >= "
        << beg;
    if (ind->getOverIndex()->getStep() != NULL) {
      int by = alpi::eval::evaluateInteger(
          ind->getOverIndex()->getStep(),
          map<Symbol*, ExpressionNode*>());
      if (by != 1) {
        oss << " .AND. ((" << symName << " - " << beg << ")/" << by
            << ")*" << by << " == " << symName;
      }
    }
    return oss.str();
  }

  string makeTestForMask(IndexingMultiRelation* ind,
      vector<Symbol*> syms) {
    ostringstream oss;
    vector<IndexVariable*> indices = ind->getIndexingVariables();
    oss << "(" << makeTestForMask(indices[0], syms[0]->getName())
        << ")";
    map<Symbol*, Symbol*> remap;
    for (unsigned int var = 1; var < indices.size(); ++var) {
      oss << ".AND.("
          << makeTestForMask(indices[var], syms[var]->getName())
          << ")";
      remap[indices[var]] = syms[var];
    }
    if (ind->getPredicate() != NULL) {
      oss << ".AND.(";

      alpi::print::fortran::print(ind->getPredicate(), oss, remap);
      oss << ")";
    }
    return oss.str();
  }

} /* namespace alpiparsertools */
