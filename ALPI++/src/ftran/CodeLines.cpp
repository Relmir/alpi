/*
 * CodeLines.cpp
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#include <sstream>
#include "CodeLines.h"

namespace forgen {

  CodeLines::CodeLines() {
  }
  CodeLines::CodeLines(std::string line) :
      last(line) {
  }
  CodeLines::~CodeLines() {
  }

  std::string CodeLines::toString() {
    std::ostringstream oss;
    std::list<std::string>::iterator lit;
    for (lit = prev.begin(); lit != prev.end(); ++lit) {
      oss << (*lit) << std::endl;
    }
    oss << last << std::endl;
    return oss.str();
  }
  void CodeLines::addLine(std::string line) {
    if (!last.empty()) {
      std::string s = last;
      prev.push_back(s);
    }
    last = line;
  }
  void CodeLines::addLines(std::list<std::string> lines) {
    if (!lines.empty()) {
      if (!last.empty()) {
        std::string s = last;
        prev.push_back(s);
      }
      prev.insert(prev.end(), lines.begin(), lines.end());
      prev.pop_back();
      last = *(lines.rbegin());
    }
  }

  std::list<std::string> CodeLines::getPrev() {
    return prev;
  }
  void CodeLines::addSymbol(alpi::exprs::symbol::Symbol* line) {
    syms.push_back(line);
  }
  std::list<alpi::exprs::symbol::Symbol*> CodeLines::getSyms() {
    return syms;
  }
  std::string CodeLines::getLast() {
    return last;
  }
  void CodeLines::addSymbols(
      std::list<alpi::exprs::symbol::Symbol*> vs) {
    for (std::list<alpi::exprs::symbol::Symbol*>::iterator itv = vs
        .begin(); itv != vs.end(); itv++) {
      syms.push_back(*itv);
    }
  }
  /**
   * \brief Get the mappings defined in this code snippet
   * \return the mappings
   */
  std::map<alpi::exprs::symbol::Symbol*,
      alpi::exprs::symbol::Symbol*> CodeLines::getMapping() {
    return mapping;
  }
  /** \brief add maapings to thos in this code snippet
   */
  void CodeLines::addMapping(
      std::map<alpi::exprs::symbol::Symbol*,
          alpi::exprs::symbol::Symbol*> m) {
    std::map<alpi::exprs::symbol::Symbol*,
        alpi::exprs::symbol::Symbol*>::iterator it;
    for (it = m.begin(); it != m.end(); ++it) {
      mapping[it->first] = it->second;
    }
  }
  void CodeLines::addMapping(alpi::exprs::symbol::Symbol* key,
      alpi::exprs::symbol::Symbol* replacement) {
    mapping[key] = replacement;
  }
} /* namespace alpiparsertools */
