/*
 * CodeLines.h
 *
 *  Created on: Jan 21, 2014
 *      Author: Emmanuel BIGEON
 */

#ifndef CODELINES_H_
#define CODELINES_H_

#include <list>
#include <map>
#include <string>

#include "../exprs/symbol/Variable.h"

namespace forgen {

  /** \brief Lines of instruction to compute the last line value.
   */
  class CodeLines {
    public:
      /** \brief Create an empty set
       */
      CodeLines();
      /** \brief Create a set with already one line (current last)
       * \param line the line
       */
      CodeLines(std::string line);
      /** \brief Add several lines to a set
       * \param line the lines
       * \return void
       */
      void addLines(std::list<std::string> line);
      /** \brief Add a line to the set
       * \param line the line
       * \return void
       */
      void addLine(std::string line);
      /** \brief Add a symbol to the necessary symbols for these
       *      instructions
       * \param v the variable
       * \return void
       */
      void addSymbol(alpi::exprs::symbol::Symbol* v);
      /** \brief Add several necessary symbols for these instructions
       * \param vs the variables
       * \return void
       */
      void addSymbols(std::list<alpi::exprs::symbol::Symbol*> vs);
      /** \brief Get the necessary symbols
       * \return std::list<alpi::exprs::symbol::Variable*> the symbols
       */
      std::list<alpi::exprs::symbol::Symbol*> getSyms();
      /** \brief Get the instructions (but the last)
       * \return std::list<std::string> all but the last instructions
       */
      std::list<std::string> getPrev();
      /** \brief Get the last instruction (may be a value computation)
       * \return std::string the last line
       */
      std::string getLast();
      /** \brief A string representation of this element
       * \return std::string the representation
       */
      std::string toString();
      /**
       * \brief Get the mappings defined in this code snippet
       * \return the mappings
       */
      std::map<alpi::exprs::symbol::Symbol*,
          alpi::exprs::symbol::Symbol*> getMapping();
      /** \brief add mappings to those in this code snippet
       */
      void addMapping(
          std::map<alpi::exprs::symbol::Symbol*,
              alpi::exprs::symbol::Symbol*> m);
      /** \brief add a mapping to those in this code snippet
       */
      void addMapping(
          alpi::exprs::symbol::Symbol* key,
              alpi::exprs::symbol::Symbol* replacement);
      virtual ~CodeLines();
    private:
      std::string last;
      std::list<std::string> prev;
      std::map<alpi::exprs::symbol::Symbol*,
          alpi::exprs::symbol::Symbol*> mapping;
      std::list<alpi::exprs::symbol::Symbol*> syms;
  };

} /* namespace alpiparsertools */

#endif /* CODELINES_H_ */
