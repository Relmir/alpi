################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/driver/ConstraintDriver.cpp \
../src/driver/ExpressionDriver.cpp \
../src/driver/IndexDriver.cpp \
../src/driver/NumericsDriver.cpp \
../src/driver/ObjectiveDriver.cpp \
../src/driver/OptionDriver.cpp \
../src/driver/ParamDriver.cpp \
../src/driver/driver.cpp 

OBJS += \
./src/driver/ConstraintDriver.o \
./src/driver/ExpressionDriver.o \
./src/driver/IndexDriver.o \
./src/driver/NumericsDriver.o \
./src/driver/ObjectiveDriver.o \
./src/driver/OptionDriver.o \
./src/driver/ParamDriver.o \
./src/driver/driver.o 

CPP_DEPS += \
./src/driver/ConstraintDriver.d \
./src/driver/ExpressionDriver.d \
./src/driver/IndexDriver.d \
./src/driver/NumericsDriver.d \
./src/driver/ObjectiveDriver.d \
./src/driver/OptionDriver.d \
./src/driver/ParamDriver.d \
./src/driver/driver.d 


# Each subdirectory must supply rules for building sources it contributes
src/driver/%.o: ../src/driver/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


