################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/exprs/operations/Addition.cpp \
../src/exprs/operations/CosineFunctionCall.cpp \
../src/exprs/operations/Difference.cpp \
../src/exprs/operations/Division.cpp \
../src/exprs/operations/Mutliplication.cpp \
../src/exprs/operations/Oppose.cpp \
../src/exprs/operations/Power.cpp \
../src/exprs/operations/ProdOnIndex.cpp \
../src/exprs/operations/SineFunctionCall.cpp \
../src/exprs/operations/SqrtFunctionCall.cpp \
../src/exprs/operations/SumOnIndex.cpp 

OBJS += \
./src/exprs/operations/Addition.o \
./src/exprs/operations/CosineFunctionCall.o \
./src/exprs/operations/Difference.o \
./src/exprs/operations/Division.o \
./src/exprs/operations/Mutliplication.o \
./src/exprs/operations/Oppose.o \
./src/exprs/operations/Power.o \
./src/exprs/operations/ProdOnIndex.o \
./src/exprs/operations/SineFunctionCall.o \
./src/exprs/operations/SqrtFunctionCall.o \
./src/exprs/operations/SumOnIndex.o 

CPP_DEPS += \
./src/exprs/operations/Addition.d \
./src/exprs/operations/CosineFunctionCall.d \
./src/exprs/operations/Difference.d \
./src/exprs/operations/Division.d \
./src/exprs/operations/Mutliplication.d \
./src/exprs/operations/Oppose.d \
./src/exprs/operations/Power.d \
./src/exprs/operations/ProdOnIndex.d \
./src/exprs/operations/SineFunctionCall.d \
./src/exprs/operations/SqrtFunctionCall.d \
./src/exprs/operations/SumOnIndex.d 


# Each subdirectory must supply rules for building sources it contributes
src/exprs/operations/%.o: ../src/exprs/operations/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


