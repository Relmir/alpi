################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/exprs/indexing/Index.cpp \
../src/exprs/indexing/IndexedElement.cpp \
../src/exprs/indexing/IndexingMultiRelation.cpp 

OBJS += \
./src/exprs/indexing/Index.o \
./src/exprs/indexing/IndexedElement.o \
./src/exprs/indexing/IndexingMultiRelation.o 

CPP_DEPS += \
./src/exprs/indexing/Index.d \
./src/exprs/indexing/IndexedElement.d \
./src/exprs/indexing/IndexingMultiRelation.d 


# Each subdirectory must supply rules for building sources it contributes
src/exprs/indexing/%.o: ../src/exprs/indexing/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


