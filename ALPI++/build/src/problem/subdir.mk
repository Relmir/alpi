################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/problem/Constant.cpp \
../src/problem/Constraint.cpp \
../src/problem/Function.cpp \
../src/problem/ProblemModel.cpp 

OBJS += \
./src/problem/Constant.o \
./src/problem/Constraint.o \
./src/problem/Function.o \
./src/problem/ProblemModel.o 

CPP_DEPS += \
./src/problem/Constant.d \
./src/problem/Constraint.d \
./src/problem/Function.d \
./src/problem/ProblemModel.d 


# Each subdirectory must supply rules for building sources it contributes
src/problem/%.o: ../src/problem/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


