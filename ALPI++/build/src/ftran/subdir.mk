################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ftran/CodeLines.cpp \
../src/ftran/ExpressionToFortran.cpp \
../src/ftran/TempVariable.cpp 

OBJS += \
./src/ftran/CodeLines.o \
./src/ftran/ExpressionToFortran.o \
./src/ftran/TempVariable.o 

CPP_DEPS += \
./src/ftran/CodeLines.d \
./src/ftran/ExpressionToFortran.d \
./src/ftran/TempVariable.d 


# Each subdirectory must supply rules for building sources it contributes
src/ftran/%.o: ../src/ftran/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


