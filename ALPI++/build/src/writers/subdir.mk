################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/writers/alpimakewriter.cpp \
../src/writers/alpimodwriter.cpp \
../src/writers/alpiparamwriter.cpp \
../src/writers/alpiproblemwriter.cpp \
../src/writers/alpiwritertools.cpp 

OBJS += \
./src/writers/alpimakewriter.o \
./src/writers/alpimodwriter.o \
./src/writers/alpiparamwriter.o \
./src/writers/alpiproblemwriter.o \
./src/writers/alpiwritertools.o 

CPP_DEPS += \
./src/writers/alpimakewriter.d \
./src/writers/alpimodwriter.d \
./src/writers/alpiparamwriter.d \
./src/writers/alpiproblemwriter.d \
./src/writers/alpiwritertools.d 


# Each subdirectory must supply rules for building sources it contributes
src/writers/%.o: ../src/writers/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


